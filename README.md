# README #

## Overview ##
NetherealSharp is an Interactive Media Framework that incorporates a number of native libraries in a managed, high productivity .NET environment. Unlike other frameworks such as XNA/Monogame, Netherealsharp also provides a Game Engine project with Editor, giving users the flexibility of harnessing the power of a working game engine with editor, creating light weight applications using the framework api, or taking things further by creating a new custom engine on top of the existing framework. 

Netherealsharp like the Monogame project utilities a number of different libraries under the hood that are wrapped in an universal abstraction layer such that developers can write once and not have to worry about differing APIs and cross platform support, simply pass in the relevant implementation paramaters at device creation and everything else is handled in single uniform manner. Of course, by static casting more platform specific aspects of the API can be accessed down the line for optimization, and you can even compile for multiple implementations and even allow users pick these at runtime.

## Framework ##

Features:

* Accelerated Graphics API (DirectX11, DirectX12, OpenGL)
* Physics API (PhysX, Bullet, BEPU, Box2D)
* Audio API (XAudio2, DirectAudio, OpenAL)
* Network API (LidGren)
* Platform specific output targets supporting WinForms, WPF, GTK+


## Engine ##
Features:

* Content Pipeline utilising configurable serialization methods to balance between security and modifiability (Compression, XML, JSON, binary)

## NOTE: Frozen! ##
refer to https://bitbucket.org/Nallack/nethereal for native version