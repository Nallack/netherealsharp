﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;

using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using NetherealSharp.Core.Serialization;

namespace NetherealSharp.Demo
{
    public class FirstPersonGame : Game3D
    {

		public override void Initialize()
		{
			base.Initialize();
		}

		public override void LoadContent()
		{
			ShaderType basic = ShaderType.Vertex | ShaderType.Pixel;
			ShaderType geometry = basic | ShaderType.Geometry;
			//Load Shared Content
			ContentManager.Add<EffectPass>("Sprite", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/sprite", basic, VertexPNT.Layout));
			ContentManager.Add<EffectPass>("SpriteArray",EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/spritearray", basic, VertexPNT.Layout));
			ContentManager.Add<EffectPass>("Phong", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/phong", basic, VertexPNT.Layout));
			ContentManager.Add<EffectPass>("Shadow", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/shadow", basic, VertexPNT.Layout));
			ContentManager.Add<EffectPass>("ShadowArray", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/shadowarray", geometry, VertexPNT.Layout));
			ContentManager.Add<EffectPass>("FontScreen", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/font", basic, VertexPNT.Layout));

			ContentManager.Add<EffectPass>("CopyPass", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Postprocessing/Copy", basic, VertexPNT.Layout));

            ContentManager.Add<EffectPass>("PrimitiveOrtho", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/primitive", basic, VertexPC.Layout));
            ContentManager.Add<EffectPass>("PrimitiveScreen", EffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/screenprimitive", basic, VertexPC.Layout));

			ContentManager.Add<Texture2D>("TestTexture", Texture2D.LoadFromFile(GraphicsManager.GraphicsDevice, "Content/Textures/uvtemplate.bmp"));

			ContentManager.Add<Material>("TestMaterial", new BasicMaterial(GraphicsManager.GraphicsDevice, "TestMaterial")
			{
				/*
				Properties =
				{
					{ "Effect", ContentManager.Get<EffectPass>("Phong") },
					{ "ShadowEffect", ContentManager.Get<EffectPass>("ShadowArray") },
					{ "DiffuseMap", ContentManager.Get<Texture2D>("TestTexture") },
					{ "DiffuseColor", Color.LightYellow },
					{ "DiffuseIntensity", 1 },
					{ "SpecularColor", Color.White },
					{ "SpecularIntensity", 0.8f },
					{ "Shininess", 120 }
				},*/
				Effect = ContentManager.Get<EffectPass>("Phong"),
				ShadowEffect = ContentManager.Get<EffectPass>("ShadowArray"),
				DiffuseMap = ContentManager.Get<Texture2D>("TestTexture"),
				DiffuseColor = Color.LightYellow,
				DiffuseIntensity = 1,
				SpecularColor = Color.White,
				SpecularIntensity = 0.8f,
				Shininess = 120
			});

			ContentManager.Add<Mesh>("Quad", IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, MeshGenerator.GenQuad(), BufferUsage.Static));
			ContentManager.Add<Mesh>("Cube", IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, MeshGenerator.GenCube(), BufferUsage.Static));
			ContentManager.Add<Mesh>("Sphere", IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, MeshGenerator.GenUVSphere(1, 32, 16), BufferUsage.Static));


			ContentManager.Add<PrimitiveBatch>("Ortho", new PrimitiveBatch(GraphicsManager.GraphicsDevice, ContentManager.Get<EffectPass>("PrimitiveOrtho")));
			ContentManager.Add<PrimitiveBatch>("Screen", new PrimitiveBatch(GraphicsManager.GraphicsDevice, ContentManager.Get<EffectPass>("PrimitiveScreen")));
			ContentManager.Add<SpriteFont>("Arial", SpriteFont.CreateFromFile(GraphicsManager.GraphicsDevice, "arial.ttf", 32));
			ContentManager.Add<FontBatch>("Screen", new FontBatch(GraphicsManager.GraphicsDevice, ContentManager.Get<EffectPass>("FontScreen") as BasicEffectPass)); //primitive = ContentManager.Get<PrimitiveBatch>("Screen");

			//ContentManager.SerializeRegistries("content.xml");

			//------SCENE---------//

			//load via code
			ActiveScene = CreateTestScene();
			ActiveScene.Game = this;
			//load scene
			//ActiveScene = ContentManager.Serializer.Deserialize<Scene3D>("TestScene");

			//serialize scene!
			ContentManager.Serializer.Serialize<Scene3D>("TestScene", ActiveScene);
		}

		public static Scene3D CreateTestScene()
		{
			return new Scene3D()
			{
				Name = "TestScene",
				Entities =
				{

					new Entity3D() //ambientlight
					{
						Name = "Ambient",
						Behaviours =
						{
							new Light3D()
							{
								LightType = LightType.Ambient,
								Ambient = new AmbientLightData()
								{
									Down = new Vector4(0.1f, 0.1f, 0.1f, 1),
									Range = new Vector4(0.0f, 0.0f, 0.0f, 0)
								}
							}
						}
					},

					new Entity3D()
					{
						Name = "Directional",
						Transform =
						{
							Rotation = Quaternion.RotationYawPitchRoll(0, -1f, 0)
						},
						Behaviours =
						{
							new Light3D()
							{
								LightType = LightType.Directional,
								Color = Color.AntiqueWhite,
								Intensity = 1
							},
							//new LightCascadeDebug()
						}
					},

					new Entity3D()
					{
						Name = "Directional2",
						Transform =
						{
							Rotation = Quaternion.RotationYawPitchRoll(2f, -1f, 0)
						},
						Behaviours =
						{
							new Light3D()
							{
								LightType = LightType.Directional,
								Color = Color.LightYellow,
								Intensity = 1
							},
							//new LightCascadeDebug()
						}
					},

					new Entity3D()
					{
						Name = "Sphere",
						Transform =
						{
							Position = new Vector3(4, 2, 4)
						},

						Behaviours =
						{
							new MeshRenderer3D()
							{
								Mesh = ContentManager.Get<Mesh>("Sphere"),
								Material = ContentManager.Get<Material>("TestMaterial") as BasicMaterial
							},
							new Spinner()
						}
					},

					new Entity3D()
					{
						Name = "Cube",
						Transform =
						{
							Position = new Vector3(6, 2, 4)
						},

						Behaviours =
						{
							new MeshRenderer3D()
							{
								Mesh = ContentManager.Get<Mesh>("Cube"),
								Material = ContentManager.Get<Material>("TestMaterial") as BasicMaterial
							},
							new Spinner() { Speed = 0.01f }
						}
					},

					new Entity3D()
					{
						Name = "Terrain",
						Behaviours =
						{
							new HeightmapTerrain()
							{
								Material = ContentManager.Get<Material>("TestMaterial") as BasicMaterial
							}
						}
					},



					new Entity3D()
					{
						Name = "Camera",
						Transform =
						{
							Position = new Vector3(3, 1, 3)
						},
						Behaviours =
						{
							new Camera3D()
							{
								Near = 0.01f,
								Far = 100.0f,
								Fov = 1f,
								ClearColor = Color.SkyBlue
							},
							new FPSCameraController(),
							//new PostProcessingTest()
							//new DebugMenu3D()
							//{
							//	Font = ContentManager.Get<SpriteFont>("Arial")
							//}
						}
					}

				}
			};
		}



	}
}
