﻿using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Demo
{
    public class ExampleGame2D : Game3D
    {
		public override void Initialize()
		{
			base.Initialize();
		}

		public override void LoadContent()
		{
			//Could probably use graphicssettings in here for shader defines

			ContentManager.Add<EffectPass>("Sprite", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/sprite", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("Phong", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/phong", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("FontScreen", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/font", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("PrimitiveOrtho", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/primitive", VertexPC.Layout));
			ContentManager.Add<EffectPass>("PrimitiveScreen", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/screenprimitive", VertexPC.Layout));

			ContentManager.Add<PrimitiveBatch>("Ortho", new PrimitiveBatch(GraphicsManager.GraphicsDevice, ContentManager.Get<EffectPass>("PrimitiveOrtho")));

			ContentManager.Add<Texture2D>("TestTexture", Texture2D.LoadFromFile(GraphicsManager.GraphicsDevice, "Content/Textures/uvtemplate.bmp"));

			ContentManager.Add<IndexedMesh>("Quad", IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, MeshGenerator.GenQuad(), BufferUsage.Static));



			ActiveScene = CreateTestScene();

		}

		private static Scene3D CreateTestScene()
		{
			return new Scene3D()
			{
				Name = "TestScene",
				Entities =
				{
					new Entity3D()
					{
						Transform = { Position = new Core.Vector3(0, 0, 3) },
						Behaviours =
						{
							new Engine3D.Camera2D()
							{
								
							}
						}
					}
				}
			};
		}
	}
}
