﻿

using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using NetherealSharp.Engine2D;
using System.Diagnostics;
using System;

#if FORMS
using System.Windows.Forms;
#endif

#if FORMS && WPF
using System.Windows.Forms.Integration;
using System.Windows.Media;
#endif

namespace NetherealSharp.Demo
{
	public class LegacyExampleGame2D : Game2D
	{
		Stopwatch watch;


		public override void Initialize()
		{
			base.Initialize();
		}

		public override void LoadContent()
		{
			//Could probably use graphicssettings in here for shader defines

			ContentManager.Add<EffectPass>("Sprite", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/sprite", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("Phong", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/phong", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("FontScreen", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/font", VertexPNT.Layout));
			ContentManager.Add<EffectPass>("PrimitiveOrtho", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/primitive", VertexPC.Layout));
			ContentManager.Add<EffectPass>("PrimitiveScreen", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/screenprimitive", VertexPC.Layout));

			ContentManager.Add<PrimitiveBatch>("Ortho", new PrimitiveBatch(GraphicsManager.GraphicsDevice, ContentManager.Get<EffectPass>("PrimitiveOrtho")));

			ContentManager.Add<Texture2D>("TestTexture", Texture2D.LoadFromFile(GraphicsManager.GraphicsDevice, "Content/Textures/uvtemplate.bmp"));

			ContentManager.Add<IndexedMesh>("Quad", IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, MeshGenerator.GenQuad(), BufferUsage.Static));

			

			ActiveScene = CreateTestScene();


		}

		public static Scene2D CreateTestScene()
		{
			return new Scene2D()
			{
				Name = "TestScene",
				Entities =
				{
					new Entity2D()
					{
						Name =  "Camera",
						Transform = { Position = new Vector3(0, 0, 3) },
						Behaviours =
						{
							new Camera2D()
							{
								Zoom = 100.0f,
								Near = -10.0f,
								Far = 10.0f
							},
							new RTSCameraController(),
							new DebugMenu()
						}
					},
					new Entity2D()
					{
						Name = "Square",
						Behaviours =
						{
							new MeshRenderer2D()
							{

							},
							//new RigidBody() { isStatic = true} 
						}
					}
				}
			};
		}


	}
}
