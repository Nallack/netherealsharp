﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;
using NetherealSharp.Engine;
using NetherealSharp.Engine.Input;
using NetherealSharp.Core.Input;

namespace NetherealSharp.Demo
{
    public class FPSCameraController : Behaviour3D
    {
		//private float m_XSensitivity;
		//private float m_YSensitivity;

		public float XSensitivity = 2;
		public float YSensitivity = 2;
		public float RunModifier = 3.0f;

		public float Yaw = 0;
		public float Pitch = 0;

		private bool m_CursorLock = false;
		public bool CursorLock
		{
			get { return m_CursorLock; }
			set
			{
				m_CursorLock = value;
				mouse.Lock = value;
				
				//Forms doesnt handle this well atm
				//mouse.Hide = value;
			}
		}

		private InputKeyboard keyboard;
		private InputMouse mouse;

		public FPSCameraController()
		{
			//InputManager.GotFocus += () => { InputManager.LockCursor = m_CursorLock;  InputManager.HideCursor = m_CursorLock; };
			//InputManager.LostFocus += () => { InputManager.LockCursor = false; InputManager.HideCursor = false; };

			keyboard = InputManager.Keyboards[0];
			mouse = InputManager.Mice[0];

		}

		public override void Update()
		{
			

			float forwardDelta = TimeManager.DeltaTime;
			float sideDelta = TimeManager.DeltaTime;
			float turnDelta = TimeManager.DeltaTime;

            float yawDelta = -0.05f * XSensitivity * mouse.Delta.X * TimeManager.DeltaTime;
            float pitchDelta = -0.05f * YSensitivity * mouse.Delta.Y * TimeManager.DeltaTime;

			//Fast mode
			if(keyboard.GetKeyDown(Key.LeftShift))
			{
				forwardDelta *= RunModifier;
				sideDelta *= RunModifier;
			}
			

			//Keyboard Look
			/*
			if (InputManager.GetKeyDown(Keys.J))
				Entity.Transform.Rotation = Quaternion.RotationYawPitchRoll(turnDelta, 0, 0) * Entity.Transform.Rotation;
			if (InputManager.GetKeyDown(Keys.L))
				Entity.Transform.Rotation = Quaternion.RotationYawPitchRoll(-turnDelta, 0, 0) * Entity.Transform.Rotation;

			if (InputManager.GetKeyDown(Keys.I) && (up.Y > 0.0f || forward.Y < 0.0f))
				Entity.Transform.Rotation = Quaternion.RotationAxis(right, turnDelta) * Entity.Transform.Rotation;
			if (InputManager.GetKeyDown(Keys.K) && (up.Y > 0.0f || forward.Y > 0.0f))
				Entity.Transform.Rotation = Quaternion.RotationAxis(right, -turnDelta) * Entity.Transform.Rotation;
			*/


			if (mouse.GetButton(MouseButton.Right))
			{
				CursorLock = true;


				Pitch = MathUtil.Clamp(Pitch + pitchDelta, -MathUtil.PiOverTwo, MathUtil.PiOverTwo);
				Yaw = MathUtil.Wrap(Yaw + yawDelta, -MathUtil.Pi, MathUtil.Pi);
				Entity.Transform.Rotation = Quaternion.RotationYawPitchRoll(Yaw, Pitch, 0);
			}
			else
			{
				CursorLock = false;
			}

			Vector3 forward = Vector3.Transform(Vector3.ForwardRH, Entity.Transform.Rotation);
			Vector3 right = Vector3.Transform(Vector3.Right, Entity.Transform.Rotation);
			Vector3 up = Vector3.Transform(Vector3.Up, Entity.Transform.Rotation);

			//mouse Look (Quaternion Only)
			/*
			if((pitchDelta > 0.0f && (up.Y > 0.0f || forward.Y < 0.0f)) || ((pitchDelta < 0.0f && (up.Y > 0.0f || forward.Y > 0.0f))))
				Entity.Transform.Rotation = Quaternion.RotationAxis(right, pitchDelta) * Entity.Transform.Rotation;

			Entity.Transform.Rotation = Quaternion.RotationAxis(Vector3.Up, yawDelta) * Entity.Transform.Rotation;
			*/


			if (keyboard.GetKeyDown(Key.W)) Entity.Transform.Position += forward * forwardDelta;
			if (keyboard.GetKeyDown(Key.S)) Entity.Transform.Position -= forward * forwardDelta;

			if (keyboard.GetKeyDown(Key.A)) Entity.Transform.Position -= right * sideDelta;
			if (keyboard.GetKeyDown(Key.D)) Entity.Transform.Position += right * sideDelta;
		}
	}


}
