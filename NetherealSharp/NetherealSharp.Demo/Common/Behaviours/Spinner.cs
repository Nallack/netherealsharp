﻿using NetherealSharp.Core;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Engine
{
	[DataContract]
    public class Spinner : Behaviour3D
    {
		private float m_Speed = 0.1f;

		[DataMember]
		public float Speed { get {return m_Speed; } set { m_Speed = value; } }


		public override void Update()
		{
			Entity.Transform.Rotation *= Quaternion.RotationAxis(Vector3.Up, Speed);
		}
	}
}
