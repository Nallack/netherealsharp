﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace NetherealSharp.Demo
{
    public class TextureDebug3D : Behaviour3D, IDrawable3D, IDisposable
    {
		private BasicEffectPass Effect;
		private Mesh Quad;
		private UniformBuffer<ObjectData> ObjectUniform;

		public Texture2D DisplayTexture { get; set; }
		public RectangleF Rectangle = new RectangleF(0.7f, -0.7f, 0.2f, 0.2f);

		public override void Initialize()
		{
			base.Initialize();

			Effect = ContentManager.Get<EffectPass>("Sprite") as BasicEffectPass;
			Quad = ContentManager.Get<Mesh>("Quad");
			ObjectUniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		private Matrix GetMatrix()
		{
			return Matrix.Scaling(Rectangle.Width, Rectangle.Height, 1f) * 
				Matrix.Translation(Rectangle.Left, Rectangle.Top, 0f);
		}

		public void BeginDraw(DrawArgs3D args)
		{
			//throw new NotImplementedException();
		}

		public void Draw(DrawArgs3D args)
		{
			if (DisplayTexture != null)
			{
				Effect.Bind(args.Context);
				ObjectUniform.Data.WorldViewProj = GetMatrix();
				ObjectUniform.Update();

				ObjectUniform.Bind(args.Context, (int)UniformLocation.Object);
				Effect.BindTexture2D(args.Context, "DiffuseMap", DisplayTexture);

				Quad.Draw(args.Context, PrimitiveMode.TriangleList);
				Effect.Unbind(args.Context);

				args.Context.EmptyShaderResource((int)TextureLocation.Diffuse);

			}
		}

		public void Dispose()
		{
			ObjectUniform.Dispose();
		}
	}
}
