﻿
using System;
using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using NetherealSharp.Engine2D;


namespace NetherealSharp.Demo
{
    public class DebugMenu : Behaviour2D, IDrawable2D
    {
		PrimitiveBatch batch;
		UniformBuffer<ObjectData> objectuniform;

		public DebugMenu()
		{
			batch = ContentManager.Get<PrimitiveBatch>("Ortho");
			objectuniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		public override void Update()
		{
			base.Update();
		}

		public void BeginDraw(DrawArgs2D args)
		{
			//throw new NotImplementedException();
		}

		public void Draw(DrawArgs2D args)
		{
			objectuniform.Data.World = Entity.Transform.Matrix;
			objectuniform.Data.WorldViewProj = objectuniform.Data.World * args.Camera.Data.ViewProj;

			objectuniform.Update();
			objectuniform.Bind(args.Context, (int)UniformLocation.Object);

			batch.BeginDraw(PrimitiveMode.LineStrip);
			batch.Add(new VertexPC(new Vector3( 0.5f,  0.5f,  0), new Vector4(1, 0, 0, 1)));
			batch.Add(new VertexPC(new Vector3(-0.5f,  0.5f,  0), new Vector4(1, 0, 0, 1)));
			batch.Add(new VertexPC(new Vector3(-0.5f, -0.5f,  0), new Vector4(1, 0, 0, 1)));
			batch.Add(new VertexPC(new Vector3( 0.5f, -0.5f,  0), new Vector4(1, 0, 0, 1)));
			batch.Add(new VertexPC(new Vector3(0.5f, 0.5f, 0), new Vector4(1, 0, 0, 1)));
			batch.Flush();
			batch.EndDraw();
		}
	}
}
