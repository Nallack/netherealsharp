﻿using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Demo.Common
{
    public class IndexedMeshDebug : Behaviour3D, IDrawable3D
    {
		public Mesh Mesh;
		UniformBuffer<ObjectData> objectuniform;


		public IndexedMeshDebug()
		{
			Mesh = ContentManager.Get<Mesh>("Sphere");
			objectuniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		public void BeginDraw(DrawArgs3D args)
		{
			//throw new NotImplementedException();
		}

		public void Draw(DrawArgs3D args)
		{
			objectuniform.Data.World = Entity.Transform.Matrix;
			objectuniform.Data.WorldViewProj = objectuniform.Data.World * args.Camera.Data.ViewProj;
			objectuniform.Update();

			objectuniform.Bind(args.Context, (int)UniformLocation.Object);
		}
	}
}
