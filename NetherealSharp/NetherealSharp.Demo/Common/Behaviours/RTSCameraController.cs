﻿

using NetherealSharp.Core;
using NetherealSharp.Core.Input;
using NetherealSharp.Engine.Input;
using NetherealSharp.Engine2D;
using System;
using System.Diagnostics;


namespace NetherealSharp.Demo
{
	public class RTSCameraController : Behaviour2D
	{
		Camera2D camera;
		InputKeyboard keyboard;

		public override void Initialize()
		{
			base.Initialize();
			camera = Entity.GetBehaviour<Camera2D>();
			keyboard = InputManager.Keyboards[0];
		}

		public override void Update()
		{
			float speed = 2.0f;

			//Console.WriteLine(camera.Zoom);

			Vector3 translation = Vector3.Zero;
			if (keyboard.GetKeyDown(Key.W)) translation.Y += speed * TimeManager.DeltaTime;
			if (keyboard.GetKeyDown(Key.A)) translation.X -= speed * TimeManager.DeltaTime;
			if (keyboard.GetKeyDown(Key.S)) translation.Y -= speed * TimeManager.DeltaTime;
			if (keyboard.GetKeyDown(Key.D)) translation.X += speed * TimeManager.DeltaTime;
			//if (InputManager.GetKeyDown((int)Key.Z)) translation.Z -= speed * TimeManager.DeltaTime;
			//if (InputManager.GetKeyDown((int)Key.X)) translation.Z += speed * TimeManager.DeltaTime;

			if (keyboard.GetKeyDown(Key.Z)) camera.Zoom += camera.Zoom * speed * TimeManager.DeltaTime;
			if (keyboard.GetKeyDown(Key.X)) camera.Zoom -= camera.Zoom * speed * TimeManager.DeltaTime;

			Entity.Transform.Position += translation;

		}
	}
}
