﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Demo
{
    public class LightCascadeDebug : Behaviour3D, IDrawable3D, IDisposable
    {
		private BasicEffectPass SpriteEffect;
		private BasicEffectPass SpriteArrayEffect;
		private Mesh Quad;

		private UniformBuffer<ObjectData> ObjectUniform;
		private UniformBuffer<SpriteData> SpriteUniform;

		private RectangleF Rectangle;

		private Light3D Light;

		private Matrix GetMatrix(int i)
		{
			return Matrix.Scaling(Rectangle.Width, Rectangle.Height, 1f) *
				Matrix.Translation(Rectangle.Left, -0.7f + i * 0.5f, 0f);
		}

		public override void Initialize()
		{
			base.Initialize();

			SpriteEffect = ContentManager.Get<EffectPass>("Sprite") as BasicEffectPass;
			SpriteArrayEffect = ContentManager.Get<EffectPass>("SpriteArray") as BasicEffectPass;

			Quad = ContentManager.Get<Mesh>("Quad");
			//for regular sprite
			ObjectUniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
			//for better effect
			SpriteUniform = UniformBuffer<SpriteData>.Create(GraphicsManager.GraphicsDevice);

			Rectangle = new RectangleF(0.7f, -0.7f, 0.2f, 0.2f);

			Light = Entity.GetBehaviour<Light3D>();
		}

		public void BeginDraw(DrawArgs3D args) { }

		public void Draw(DrawArgs3D args)
		{
			if (Light.m_ShadowMap != null)
			{
				if (Light.m_ShadowMap.RenderTextureArray != null)
				{
					for (int i = 0; i < Light.m_ShadowMap.RenderTextureArray.ArraySize; i++)
					{
						SpriteArrayEffect.Bind(args.Context);
						SpriteUniform.Data.WorldViewProj = GetMatrix(i);
						SpriteUniform.Data.ArrayIndex = i;
						SpriteUniform.Update();

						SpriteUniform.Bind(args.Context, (int)UniformLocation.Object);
						SpriteArrayEffect.BindTexture2DArray(args.Context, "diffuseMap", Light.m_ShadowMap.RenderTextureArray);

						Quad.Draw(args.Context, PrimitiveMode.TriangleList);
						SpriteArrayEffect.Unbind(args.Context);
					}
				}


				//if (shadowmap.RenderTextures != null)
				//{
				//	for (int i = 0; i < shadowmap.RenderTextures.Length; i++)
				//	{
				//		SpriteEffect.Bind(args.Context);
				//		ObjectUniform.Data.WorldViewProj = GetMatrix(i);
				//		ObjectUniform.Update();

				//		ObjectUniform.Bind(args.Context, (int)UniformLocation.Object);
				//		SpriteEffect.ApplyTexture2D(args.Context, "", shadowmap.RenderTextures[i]);

				//		Quad.Draw(PrimitiveMode.TriangleList);
				//		SpriteEffect.Unbind(args.Context);
				//	}
				//	args.Context.EmptyShaderResource((int)TextureLocation.Diffuse);
				//}
			}
		}


		public void Dispose()
		{
			SpriteUniform.Dispose();
			ObjectUniform.Dispose();
		}
	}
}
