﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System.Xml.Serialization;

namespace NetherealSharp.Demo
{
	public class DebugMenu3D : Behaviour3D, IDrawable3D, IDisposable
	{
		public enum Direction
		{
			Left, Right, Up, Down
		}

		[XmlIgnore]
		PrimitiveBatch primitive;
		[XmlIgnore]
		FontBatch fontbatch;
		[XmlIgnore]

		public SpriteFont Font;
		public Direction Dir = Direction.Up;

		UniformBuffer<ObjectData> objectuniform;

		public DebugMenu3D()
		{

		}

		public override void Initialize()
		{
			base.Initialize();

			fontbatch = ContentManager.Get<FontBatch>("Screen");
			primitive = ContentManager.Get<PrimitiveBatch>("Ortho");
			objectuniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		public override void Update()
		{
			base.Update();
		}

		public void BeginDraw(DrawArgs3D args)
		{
			objectuniform.Data.World = Matrix.Identity;
			//throw new NotImplementedException();
		}

		public void Draw(DrawArgs3D args)
		{
			objectuniform.Data.WorldViewProj = objectuniform.Data.World * args.Camera.Data.ViewProj;

			objectuniform.Update();
			objectuniform.Bind(args.Context, (int)UniformLocation.Object);

			primitive.BeginDraw(PrimitiveMode.LineStrip);
			primitive.Add(new VertexPC(new Vector3(0.5f, 0.5f, 0), new Vector4(1, 0, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(-0.5f, 0.5f, 0), new Vector4(1, 0, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(-0.5f, -0.5f, 0), new Vector4(1, 0, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(0.5f, -0.5f, 0), new Vector4(1, 0, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(0.5f, 0.5f, 0), new Vector4(1, 0, 0, 1)));
			primitive.Flush();
			primitive.EndDraw();

			
			FontFormat format = new FontFormat()
			{
				Alignment = TextAlignment.Center
			};

			//fontbatch.DrawString("FPS: " + TimeManager.FrameRate, font, Vector2.Zero);
			fontbatch.DrawString("Draw Time: " + 1.0 / TimeManager.drawtime, Font, new Vector2(20, 20));
			//fontbatch.DrawString("Center", font, Vector2.Zero, new Vector2(300, 200), format);
			//fontbatch.DrawString("Help!", font, new Vector2(px, 0), new Vector2(100, 200), args);

			//fontbatch.Flush();
		}

		public void Dispose()
		{
			Font.Dispose();
			objectuniform.Dispose();
		}
 
	}
}
