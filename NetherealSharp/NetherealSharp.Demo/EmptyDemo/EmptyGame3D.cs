﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Demo
{
    public class EmptyGame3D : Game3D
	{
		public override void Initialize()
		{
			base.Initialize();
		}

		public override void LoadContent()
		{
			//ContentManager.Add<EffectPass>("Simple", BasicEffectPass.CompileFromFile(GraphicsManager.GraphicsDevice, "Content/Shaders/Default/Forward/simple", VertexPC.Layout));

			ActiveScene = new EmptyScene3D();
		}
	}
}
