﻿
#if OPENCL
using System;
using System.Collections.Generic;
using System.Text;

using Cloo;

using Base = NetherealSharp.Core.Compute;

namespace NetherealSharp.Core.Compute.OpenCL
{
    public class ComputeDevice : Base.ComputeDevice
    {
		//COMPUTE
		public Cloo.ComputePlatform CLPlatform { get; private set; }
		public Cloo.ComputeDevice CLDevice { get; private set; }
		public Cloo.ComputeContext CLContext { get; private set; }
		public Cloo.ComputeCommandQueue CLCommandQueue { get; private set; }


		public ComputeDevice()
		{
			CLPlatform = Cloo.ComputePlatform.Platforms[0];
			CLDevice = CLPlatform.Devices[0];

			//Move this to a context class
			//Single context can exist for multiple devices, but each device will need its own commandqueue
			//think in terms of having two of the same GPU installed.
			CLContext = new Cloo.ComputeContext(
				CLPlatform.Devices,
				new ComputeContextPropertyList(CLPlatform),
				new Cloo.Bindings.ComputeContextNotifier(HandleContextNotify),
				IntPtr.Zero);

			CLCommandQueue = new Cloo.ComputeCommandQueue(CLContext, CLDevice, Cloo.ComputeCommandQueueFlags.None);
		}

		private void HandleContextNotify(string errorInfo, IntPtr clDataPtr, IntPtr clDataSize, IntPtr userDataPtr)
		{

		}
		
	}
}

#endif