﻿
#if OPENCL
using System;
using System.Collections.Generic;
using System.Text;

using Cloo;

namespace NetherealSharp.Core.Compute.OpenCL
{
    public class ComputeProgram
    {
		Cloo.ComputeDevice Device { get; set; }
		Cloo.ComputeContext Context { get; set; }

		Cloo.ComputeKernel Kernel { get; set; }

		public ComputeProgram(ComputeDevice device, string filepath)
		{
			Device = device.CLDevice;

		}
	}
}
#endif