﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Diagnostics
{
	/// <summary>
	/// TODO: write a method that encapsulates the entire execution process to custom handle exceptions
	/// may want to make a modular callback to allow user to define how exceptions
	/// shall be shown(like window, in game console)
	/// </summary>
    public static class Debug
    {
		public enum DebugMode
		{
			MessageWindow,
			Console,
			DebugConsole,
		};

		public enum DebugFlags
		{
			None,
			WarningsAsErrors,
			ThrowException
		};

		private static DebugMode m_DebugMode = DebugMode.MessageWindow;
		private static DebugFlags m_DebugFlags = DebugFlags.None;

		public static void Init(DebugMode mode, DebugFlags flags)
		{
			m_DebugMode = mode;
			m_DebugFlags = flags;
		}

		public static void LogWarning(string message)
		{
			switch(m_DebugMode)
			{
#if FORMS
				case DebugMode.MessageWindow:
					System.Windows.Forms.MessageBox.Show(message, "Warning", System.Windows.Forms.MessageBoxButtons.AbortRetryIgnore);
					break;
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public static void LogWarning(string caption, Exception e)
		{

		}

		//Bridge this implementation
		public static void LogError(string caption, string message)
		{
			switch (m_DebugMode)
			{
#if FORMS
				case DebugMode.MessageWindow:
					var result = System.Windows.Forms.MessageBox.Show(message, caption, System.Windows.Forms.MessageBoxButtons.AbortRetryIgnore);
					switch(result)
					{
						case System.Windows.Forms.DialogResult.OK:
							return;
						case System.Windows.Forms.DialogResult.Ignore:
							return;
						case System.Windows.Forms.DialogResult.Abort:
							throw new ApplicationException();
						default:
							throw new NotImplementedException();
					}
#endif
				default:
					throw new NotImplementedException();
			}
		}
    }
}
