﻿#if ANDROID
using System;
using System.Collections.Generic;
using System.Text;

using Andrd = Android;

namespace NetherealSharp.Core.Platform
{
	/// <summary>
	/// Workaround to get a static reference to the main android activity.
	/// </summary>
    public static class AndroidManager
    {
		public static Andrd.Content.Context Activity { get; private set; }

		public static void Init(Andrd.App.Activity activity)
		{
			Activity = activity;
		}
    }
}
#endif