﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Content
{
    public interface IContentReader
    {
		object ReadContent(string name, string filepath);
    }
}
