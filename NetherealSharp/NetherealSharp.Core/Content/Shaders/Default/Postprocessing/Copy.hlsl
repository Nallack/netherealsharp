﻿
#include <StandardVertex.hlsli>

SamplerState linearSampler : register(s0);

struct PixelIn
{
	float4 Position	: SV_POSITION;
	float2 TexCoord	: TEXCOORD;
};


Texture2D DiffuseMap : register(t0);
//Texture2D DepthMap : register(t1);


PixelIn VS(VertexPNT input)
{
	PixelIn output = (PixelIn)0;
	output.Position = input.Position;
	output.TexCoord = input.TexCoord;
	return output;
}

float4 PS(PixelIn input) : SV_TARGET
{
	//return float4(input.TexCoord,0,1);
	return DiffuseMap.Sample(linearSampler, input.TexCoord);
}