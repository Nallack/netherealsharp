


uniform Object {
	mat4 World;
	mat4 WorldViewProj;
};

uniform Camera {
	mat4 View;
	mat4 Proj;
	mat4 ViewProj;
	mat4 Screen; //Pixel Cordinates for pixel alignment fonts
	
	//vec4 ePosition;
};

//uniform PrimitiveBlock {
//	vec4 Color;
//};

uniform sampler2D diffuseMap;

#ifdef VERTEX_SHADER

in vec4 vPosition;
in vec2 vTexCoord;

out vec4 fPosition;
out vec2 fTexCoord;

void main() {
	gl_Position = Proj * View * World * vPosition;
	//gl_Position = WorldViewProj * vPosition;
	//gl_Position = Proj * vPosition;
	fTexCoord = vTexCoord;
}

#endif

#ifdef FRAGMENT_SHADER

in vec2 fTexCoord;

out vec4 color;

void main() {
	color = texture(diffuseMap, fTexCoord);
}

#endif