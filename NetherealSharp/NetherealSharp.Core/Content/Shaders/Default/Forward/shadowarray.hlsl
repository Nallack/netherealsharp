﻿
//------------------------------------
//Directional Light ShadowMap Shader
//------------------------------------

#define MAXSHADOWCASCADES 3
#define SHADOWCASCADES 3

struct VertexIn
{
	float4 Position : POSITION;
};

struct GeometryIn
{
	float4 PositionW : POSITION;
};

struct PixelIn
{
	float4 PositionH : SV_POSITION;
	uint TargetIndex : SV_RenderTargetArrayIndex;
};

cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Shadow : register(b1)
{
	float4x4 ViewProj[MAXSHADOWCASCADES];
}

//May want this for partially transparent materials
//Texture2D diffuseMap : register (t0);
//SamplerState linearSampler : register(s0);

GeometryIn VS(VertexIn input)
{
	GeometryIn output = (GeometryIn)0;
	output.PositionW = mul(World, input.Position);
	return output;
}

[maxvertexcount(3 * MAXSHADOWCASCADES)]
void GS(triangle GeometryIn input[3], inout TriangleStream<PixelIn> outputStream)
{
	PixelIn output = (PixelIn)0;

	for (int i = 0; i < SHADOWCASCADES; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			output.PositionH = mul(ViewProj[i], input[j].PositionW);
			output.TargetIndex = i;
			outputStream.Append(output);
		}
		outputStream.RestartStrip();
	}
}

float4 PS(PixelIn input) : SV_TARGET
{
	float depth = input.PositionH.z;
//float dx = ddx(input.PositionH);
//float dy = ddy(input.PositionH);
//float moments = pow(depth, 2.0) * 0.25*(dx*dx + dy*dy);

//return float4(depth, depth * depth, 0.0f, 1.0f);
return float4(depth, depth * depth, 0, 1);
}