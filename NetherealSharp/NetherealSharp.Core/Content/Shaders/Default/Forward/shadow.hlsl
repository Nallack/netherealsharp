﻿
//Directional Light ShadowMap shader

struct VertexIn
{
	float4 Position	: POSITION;
	//float3 Normal	: NORMAL;
	//float2 TexCoord	: TEXCOORD;
};

struct PixelIn
{
	float4 PositionH	: SV_POSITION;
	//float2 TexCoord		: TEXCOORD;
};


cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
	float4 Eye;
};

//May want this for partially transparent materials
//Texture2D diffuseMap : register (t0);
//SamplerState linearSampler : register(s0);


PixelIn VS(VertexIn input)
{
	PixelIn output = (PixelIn)0;
	
	//output.PositionH = input.Position;
	output.PositionH = mul(WorldViewProj, input.Position);
	//output.TexCoord = input.TexCoord;

	return output;
}

float4 PS(PixelIn input) : SV_TARGET
{ 
	float depth = input.PositionH.z;
	//float dx = ddx(input.PositionH);
	//float dy = ddy(input.PositionH);
	//float moments = pow(depth, 2.0) * 0.25*(dx*dx + dy*dy);
	
	//return float4(depth, depth * depth, 0.0f, 1.0f);
	return float4(depth, depth * depth, 0, 1);
}