﻿


//http://www.marmoset.co/toolbag/learn/pbr-practice
// (PBR)
// 0 : Albedo
// 1 : Normal+Microsurface
// 2 : Reflectiviy+fresnel
//


struct VertexIn
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float2 TexCoord : TEXCOORD;
};

struct PixelIn
{
	float4 PositionW : POSITION;
	float4 PositionH : SV_POSITION;
	float3 NormalW : NORMAL;
	float3 TangentW : TANGENT;
	float2 TexCoord : TEXCOORD;
};

cbuffer Material : register(b3)
{
	float3 Albedol;
	float3 Microsurface;
	float3 Reflectivity;
}

Texture2D AlbedoMap;
Texture2D NormalMap;
Texture2D Microsurface;
Texture2D Reflectivity;
Texture2D Fresnel;

//Applied only to ambient light and not direct diffuse light
Texture2D AmbientOcclusion;

Texture2D Cavity;