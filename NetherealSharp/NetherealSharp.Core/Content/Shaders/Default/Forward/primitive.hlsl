﻿
struct VertexIn
{
	float4 position : POSITION;
	float4 color : COLOR;
};

struct PixelIn
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
};

cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
};

Texture2D diffuseMap;
SamplerState linearSampler : register(s0);

PixelIn VS(VertexIn vertex)
{
	PixelIn pixel = (PixelIn)0;
	pixel.position = vertex.position;
	//pixel.position = mul(ViewProj, vertex.position);
	pixel.color = vertex.color;
	return pixel;
}

float4 PS(PixelIn pixel) : SV_TARGET
{
	return pixel.color;
}