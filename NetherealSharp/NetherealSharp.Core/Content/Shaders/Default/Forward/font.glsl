﻿

#include <Standard.glsli>

uniform sampler2D diffuseMap;

#if VERTEX_SHADER

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec4 fPositionW;
out vec3 fNormalW;
out vec2 fTexCoord;

void main() {
	gl_Position = Screen * vPosition;
	fTexCoord = vTexCoord;
}

#elif FRAGMENT_SHADER

in vec4 fPositionW;
in vec3 fNormalW;
in vec2 fTexCoord;

out vec4 color;

void main() {
	//if(c.a == 0) { discard; }
	color = texture(diffuseMap, fTexCoord);
}

#endif