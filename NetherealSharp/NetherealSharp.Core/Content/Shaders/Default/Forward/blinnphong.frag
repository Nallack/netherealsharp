﻿#version 150

in vec4 fPositionW;
in vec3 fNormalW;
in vec2 fTexCoord;

out vec4 color;

struct AmbientLight {
	vec4 Down;
	vec4 Range;
};

uniform Camera {
	mat4 View;
	mat4 Proj;
	mat4 ViewProj;
};

uniform Light {
	AmbientLight Ambient;
};

uniform sampler2D diffuseMap;

void main() {
	
	vec3 normal = normalize(fNormalW);
	float up = normal.y * 0.5f + 0.5f;

	vec4 ambient = (Ambient.Down + up * Ambient.Range);
	vec4 diffuse = vec4(0);
	vec4 specular = vec4(0);


	vec4 tex = texture(diffuseMap, fTexCoord);
	
	color = tex * (ambient + diffuse) + specular;
}