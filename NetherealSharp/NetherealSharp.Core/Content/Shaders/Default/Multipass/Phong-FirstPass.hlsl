﻿
#define MAXSHADOWCASCADES 2

struct PixelIn
{
	float4 PositionW : POSITION;
	float4 PositionH : SV_POSITION;
	float3 NormalW	 : NORMAL;
};



PixelIn VS(VertexIn input)
{
	PixelIn output = (PixelIn)0;

	output.PositionW = mul(World, input.Position);
	output.PositionH = mul(ViewProj, output.PositionW);
	output.NormalW = mul((float3x3)World, input.Normal);
	output.TexCoord = input.TexCoord;
}

float3 PS(PixelIn input) : SV_TARGET
{

}