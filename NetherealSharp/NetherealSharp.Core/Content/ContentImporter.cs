﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Content
{
	public abstract class ContentImporter
	{
		public static ContentImporter Create()
		{
#if ASSIMP
			return new Assimp.ContentImporter();
#else
			return null;
#endif
		}
	}
}
