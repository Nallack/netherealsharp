﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Content
{
    interface IContentWriter
    {
		bool WriteContent(string filepath, object content);
    }
}
