﻿#if ASSIMP
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Content
{
	/// <summary>
	/// Importer for external scene content including:
	/// Textures, Models, Materials and Animations.
	/// (Perhaps call this externalImporter)
	/// </summary>
    public class ContentImporter
    {
		public Assimp.AssimpContext Context;

		public ContentImporter()
		{
			Context = new Assimp.AssimpContext();
			Context.SetConfig(new Assimp.Configs.NormalSmoothingAngleConfig(66.6f));
		}

    }
}
#endif