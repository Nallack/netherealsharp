﻿#if OPENAL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Audio.OpenAL;

namespace NetherealSharp.Core.Audio
{
    public static class AudioUtils
    {
		public static ALFormat GetSoundFormat(int channels, int bits)
		{
			switch(channels)
			{
				case 1: return bits == 8 ? ALFormat.Mono8 : ALFormat.Mono16;
				case 2: return bits == 8 ? ALFormat.Stereo8 : ALFormat.Stereo16;
				default: throw new NotImplementedException();
			}
		}
    }
}
#endif