﻿

using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.VR
{
    public abstract class VRMirrorTexture : IDisposable
    {

		public abstract void CopyToTarget();

		public static VRMirrorTexture Create(VRDevice device, GraphicsDevice graphics, RenderTarget target)
		{
			switch (device.Implementation)
			{
#if OCULUS && DIRECTX11
			case VRImplementation.Oculus:
				return new Oculus.DX11.VRMirrorTexture(device as Oculus.VRDevice, graphics, target);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Dispose();
    }
}