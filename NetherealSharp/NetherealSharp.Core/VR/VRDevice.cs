﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.VR
{
    public abstract class VRDevice : IDisposable
    {
		public VRImplementation Implementation;

		public static VRDevice Create(VRImplementation implementation)
		{
			switch (implementation)
			{
#if OCULUS
				case VRImplementation.Oculus:
					return new Oculus.VRDevice();
#endif
#if OSVR
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Dispose();
	}
}