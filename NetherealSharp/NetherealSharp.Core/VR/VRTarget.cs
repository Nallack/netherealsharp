﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.VR
{
    public abstract class VRTarget
    {
		public abstract void Present();
    }
}
