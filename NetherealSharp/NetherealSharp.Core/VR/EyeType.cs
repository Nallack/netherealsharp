﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.VR
{
    public enum EyeType
    {
		Left,
		Right
    }

	public static class EyeTypeConversion
	{
#if OCULUS
		public static OculusWrap.OVR32.EyeType ToOculus(this EyeType value)
		{
			return (OculusWrap.OVR32.EyeType)value;
		}
#endif
	}
}