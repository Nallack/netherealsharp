﻿#if DIRECTX && OCULUS
using OculusWrap;
using System;
using System.Collections.Generic;
using System.Text;

using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

namespace NetherealSharp.Core.VR.Oculus.DX11
{
    public static class Helper
    {

#if OCULUS
		public static OVRTypes.TextureBindFlags ConvertBindFlags(D3D11.BindFlags flags)
		{
			OVRTypes.TextureBindFlags result = 0;
			if ((flags & D3D11.BindFlags.RenderTarget) > 0)
				result |= OVRTypes.TextureBindFlags.DX_RenderTarget;
			if((flags & D3D11.BindFlags.DepthStencil) > 0)
				result |= OVRTypes.TextureBindFlags.DX_DepthStencil;
			if ((flags & D3D11.BindFlags.UnorderedAccess) > 0)
				result |= OVRTypes.TextureBindFlags.DX_UnorderedAccess;
			return result;
		}

		public static OVRTypes.TextureFormat ConvertFormat(DXGI.Format format)
		{
			switch (format)
			{
				case SharpDX.DXGI.Format.B5G6R5_UNorm: return OVRTypes.TextureFormat.B5G6R5_UNORM;
				case SharpDX.DXGI.Format.B5G5R5A1_UNorm: return OVRTypes.TextureFormat.B5G5R5A1_UNORM;
				case SharpDX.DXGI.Format.R8G8B8A8_UNorm: return OVRTypes.TextureFormat.R8G8B8A8_UNORM;
				case SharpDX.DXGI.Format.R8G8B8A8_UNorm_SRgb: return OVRTypes.TextureFormat.R8G8B8A8_UNORM_SRGB;
				case SharpDX.DXGI.Format.B8G8R8A8_UNorm: return OVRTypes.TextureFormat.B8G8R8A8_UNORM;
				case SharpDX.DXGI.Format.B8G8R8A8_UNorm_SRgb: return OVRTypes.TextureFormat.B8G8R8A8_UNORM_SRGB;
				case SharpDX.DXGI.Format.B8G8R8X8_UNorm: return OVRTypes.TextureFormat.B8G8R8X8_UNORM;
				case SharpDX.DXGI.Format.B8G8R8X8_UNorm_SRgb: return OVRTypes.TextureFormat.B8G8R8X8_UNORM_SRGB;
				case SharpDX.DXGI.Format.R16G16B16A16_Float: return OVRTypes.TextureFormat.R16G16B16A16_FLOAT;
				case SharpDX.DXGI.Format.D16_UNorm: return OVRTypes.TextureFormat.D16_UNORM;
				case SharpDX.DXGI.Format.D24_UNorm_S8_UInt: return OVRTypes.TextureFormat.D24_UNORM_S8_UINT;
				case SharpDX.DXGI.Format.D32_Float: return OVRTypes.TextureFormat.D32_FLOAT;
				case SharpDX.DXGI.Format.D32_Float_S8X24_UInt: return OVRTypes.TextureFormat.D32_FLOAT_S8X24_UINT;

				case SharpDX.DXGI.Format.R8G8B8A8_Typeless: return OVRTypes.TextureFormat.R8G8B8A8_UNORM;
				case SharpDX.DXGI.Format.R16G16B16A16_Typeless: return OVRTypes.TextureFormat.R16G16B16A16_FLOAT;

				default: return OVRTypes.TextureFormat.UNKNOWN;
			}
		}
#endif

#if OCULUS
		public static OVRTypes.MirrorTextureDesc ConvertMirrorTextureDesc(D3D11.Texture2DDescription texture2DDescription)
		{
			OVRTypes.MirrorTextureDesc mirrortexturedesc = new OVRTypes.MirrorTextureDesc()
			{
				Width = texture2DDescription.Width,
				Height = texture2DDescription.Height,
				Format = ConvertFormat(texture2DDescription.Format),
				MiscFlags = OVRTypes.TextureMiscFlags.None
			};

			return mirrortexturedesc;
		}

		public static OVRTypes.TextureSwapChainDesc ConvertTexture2DDesc(D3D11.Texture2DDescription texture2DDescription)
		{

			OVRTypes.TextureSwapChainDesc swapchaindesc = new OVRTypes.TextureSwapChainDesc()
			{
				Width = texture2DDescription.Width,
				Height = texture2DDescription.Height,
				ArraySize = texture2DDescription.ArraySize,
				BindFlags = ConvertBindFlags(texture2DDescription.BindFlags),
				Format = ConvertFormat(texture2DDescription.Format),
				MipLevels = texture2DDescription.MipLevels,
				MiscFlags = OVRTypes.TextureMiscFlags.None,
				SampleCount = texture2DDescription.SampleDescription.Count,
				Type = OVRTypes.TextureType.Texture2D,
			};

			//These dont match up anymore, will need helpers...
			//OVRTypes.TextureBindFlags.
			//D3D11.BindFlags.
			

			/*
			OVR.D3D11.D3D11_TEXTURE2D_DESC d3d11DTexture = new OVR.D3D11.D3D11_TEXTURE2D_DESC();
			d3d11DTexture.Width = (uint)texture2DDescription.Width;
			d3d11DTexture.Height = (uint)texture2DDescription.Height;
			d3d11DTexture.MipLevels = (uint)texture2DDescription.MipLevels;
			d3d11DTexture.ArraySize = (uint)texture2DDescription.ArraySize;
			d3d11DTexture.Format = (OVR.D3D11.DXGI_FORMAT)texture2DDescription.Format;
			d3d11DTexture.SampleDesc.Count = (uint)texture2DDescription.SampleDescription.Count;
			d3d11DTexture.SampleDesc.Quality = (uint)texture2DDescription.SampleDescription.Quality;
			d3d11DTexture.Usage = (OVR.D3D11.D3D11_USAGE)texture2DDescription.Usage;
			d3d11DTexture.BindFlags = (uint)texture2DDescription.BindFlags;
			d3d11DTexture.CPUAccessFlags = (uint)texture2DDescription.CpuAccessFlags;
			d3d11DTexture.MiscFlags = (uint)texture2DDescription.OptionFlags;
			return d3d11DTexture;
			*/
			return swapchaindesc;
		}
#endif

	}
}

#endif