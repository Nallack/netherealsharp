﻿
using System;
using System.Collections.Generic;
using System.Text;


namespace NetherealSharp.Core.VR
{
	//For now just assume a single HMD can be used.
    public static class VRManager
    {
		public static VRDevice VRDevice { get; private set; }

		public static bool Enabled { get; set; }

		public static void Initialize()
		{
			VRDevice = VRDevice.Create(VRImplementation.Oculus);
		}

		public static void Dispose()
		{
			VRDevice.Dispose();
		}
    }
}
