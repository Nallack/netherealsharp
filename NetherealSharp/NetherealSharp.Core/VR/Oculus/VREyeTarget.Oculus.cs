﻿#if OCULUS
using OculusWrap;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.VR;

namespace NetherealSharp.Core.VR.Oculus
{
	public abstract class VREyeTarget : Base.VREyeTarget
	{
		public OVRTypes.EyeRenderDesc EyeRenderDesc;

		public OVR32.FovPort FOV;
		public Vector3 HmdToEyeViewOffset;
		public Viewport Viewport;

		public static VREyeTarget Create(VRDevice device, GraphicsDevice graphics, EyeType eye)
		{
			switch (graphics.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.VREyeTarget(device, graphics as NetherealSharp.Core.Graphics.DX11.GraphicsDevice, eye);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.VREyeTarget(device, graphics as NetherealSharp.Core.Graphics.OpenGL.GraphicsDevice, eye);
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
#endif