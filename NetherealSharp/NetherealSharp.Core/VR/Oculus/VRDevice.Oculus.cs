﻿#if OCULUS
using System;
using System.Collections.Generic;
using System.Text;
using OculusWrap;
using System.Windows.Forms;

using Base = NetherealSharp.Core.VR;

namespace NetherealSharp.Core.VR.Oculus
{
    public class VRDevice : Base.VRDevice, IDisposable
    {
		//may want to move this elsewhere as multiple HMD
		//may use a single runtime, but not too sure
		public Wrap Runtime;
		public Hmd HMD;

		public VRDevice()
		{
			Implementation = VRImplementation.Oculus;

			Runtime = new Wrap();

			OVRTypes.InitParams initparams = new OVRTypes.InitParams();
			initparams.Flags = OVRTypes.InitFlags.Debug;
			bool success = Runtime.Initialize(initparams);
			if (!success)
			{
				MessageBox.Show("Failed to initialize the Oculus runtime library.", "Oculus", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			OVRTypes.GraphicsLuid uid;
			HMD = Runtime.Hmd_Create(out uid);
			if (HMD == null)
			{
				Diagnostics.Debug.LogError("Oculus", "Failed to initialize the Oculus runtime library.");
				return;
			}

			if (HMD.ProductName == string.Empty)
			{
				MessageBox.Show("The HMD is not enabled.", "There's a tear in the Rift", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			//Set CAPS and Tracking
			//apparently does not need to be set here anymore?

			/* 1.8
			HMD.SetEnabledCaps(OVR.HmdCaps.DebugDevice);
			HMD.ConfigureTracking(
				OVR.TrackingCaps.Orientation
				| OVR.TrackingCaps.Position
				| OVR.TrackingCaps.MagYawCorrection,
				OVR.TrackingCaps.None);
			*/
		}

		public override void Dispose()
		{
			HMD.Dispose();
		}
	}
}
#endif