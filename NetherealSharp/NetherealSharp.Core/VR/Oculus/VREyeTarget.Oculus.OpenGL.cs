﻿
#if OCULUS && OPENGL

using System;
using System.Collections.Generic;
using System.Text;

using OculusWrap;

using NetherealSharp.Core.Graphics.OpenGL;
using Base = NetherealSharp.Core.VR.Oculus;
using OpenTK.Graphics.OpenGL;

namespace NetherealSharp.Core.VR.Oculus.OpenGL
{
	public class VREyeTarget : Oculus.VREyeTarget
	{
		private Core.Graphics.OpenGL.GraphicsDevice Graphics;
		private OVRTypes.Sizei m_Size;

		public override int Width { get { return m_Size.Width; } }
		public override int Height { get { return m_Size.Height; } }

		public float PixelsPerDisplayUnit = 1.0f;

		//OculusWrap Graphics
		public TextureSwapChain TextureSwapChain;

		//OpenGL Graphics
		int RenderTextureId;
		int FramebufferId;
		Texture2D DepthTexture;

		public VREyeTarget(VRDevice device, GraphicsDevice graphics, EyeType eyeType)
		{
			Graphics = graphics;
			EyeType = eyeType;
			int eye = (int)eyeType;

			FOV = device.HMD.DefaultEyeFov[eye];
			EyeRenderDesc = device.HMD.GetRenderDesc(eyeType.ToOculus(), FOV);
			HmdToEyeViewOffset = EyeRenderDesc.HmdToEyeOffset;

			m_Size = device.HMD.GetFovTextureSize(eyeType.ToOculus(), FOV, PixelsPerDisplayUnit);
			Viewport = new Viewport(0, 0, m_Size.Width, m_Size.Height, 0.0f, 1.0f);

			var desc = new OVRTypes.TextureSwapChainDesc()
			{
				Width = m_Size.Width,
				Height = m_Size.Height,
				ArraySize = 1,
				Format = OVRTypes.TextureFormat.R8G8B8A8_UNORM_SRGB,
				BindFlags = OVRTypes.TextureBindFlags.None,
				MipLevels = 1,
				SampleCount = 1,
				MiscFlags = OVRTypes.TextureMiscFlags.None,
				StaticImage = 0,
				Type = OVRTypes.TextureType.Texture2D
			};

			//Rendertexture and depthtexture
			var result = device.HMD.CreateTextureSwapChainGL(desc, out TextureSwapChain);

			int textureSwapchainBufferCount;
			TextureSwapChain.GetLength(out textureSwapchainBufferCount);

			for(int i = 0; i < textureSwapchainBufferCount; i++)
			{
				uint texid = 0;
				result = TextureSwapChain.GetBufferGL(i, out texid);

				GL.BindTexture(TextureTarget.Texture2D, (int)texid);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Linear);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (float)TextureWrapMode.ClampToEdge);
				GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (float)TextureWrapMode.ClampToEdge);
			}
			FramebufferId = GL.GenFramebuffer();

			DepthTexture = Texture2D.LoadEmpty
				(m_Size.Width, m_Size.Height,
				TextureMinFilter.Linear,
				TextureMagFilter.Linear,
				PixelInternalFormat.DepthComponent24,
				PixelFormat.DepthComponent,
				PixelType.UnsignedInt);

		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override void Present()
		{
			var result = TextureSwapChain.Commit();
			if (result != OVRTypes.Result.Success) throw new Exception();
		}
	}
}

#endif