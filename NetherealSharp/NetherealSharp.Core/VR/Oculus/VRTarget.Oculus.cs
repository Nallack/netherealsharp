﻿
#if OCULUS
using System;
using System.Collections.Generic;
using System.Text;

using OculusWrap;
using NetherealSharp.Core.Graphics;
using Base = NetherealSharp.Core.VR;

namespace NetherealSharp.Core.VR.Oculus
{
	public class VRTarget : Base.VRTarget, IDisposable
	{
		public VRDevice m_VRDevice;

		public VREyeTarget[] EyeTargets;
		public LayerEyeFov LayerEyeFov;
		public Layers Layers;

		public VRMirrorTexture Mirror;

		public VRTarget(VRDevice device, GraphicsDevice graphics)
		{
			m_VRDevice = device;
			EyeTargets = new VREyeTarget[2];

			Layers = new Layers();
			LayerEyeFov = Layers.AddLayerEyeFov();

			EyeTargets[0] = VREyeTarget.Create(m_VRDevice, graphics, EyeType.Left);
			LayerEyeFov.ColorTexture[0] = (EyeTargets[0] as DX11.VREyeTarget).SwapTextureSet.TextureSwapChainPtr;
			LayerEyeFov.Viewport[0].Position = new OVRTypes.Vector2i(0, 0);
			LayerEyeFov.Viewport[0].Size = new OVRTypes.Sizei((EyeTargets[0] as DX11.VREyeTarget).Width, (EyeTargets[0] as DX11.VREyeTarget).Height);
			LayerEyeFov.Fov[0] = (EyeTargets[0] as DX11.VREyeTarget).FOV;
			LayerEyeFov.Header.Flags = OVRTypes.LayerFlags.None;

			EyeTargets[1] = VREyeTarget.Create(m_VRDevice, graphics, EyeType.Right);
			LayerEyeFov.ColorTexture[1] = (EyeTargets[0] as DX11.VREyeTarget).SwapTextureSet.TextureSwapChainPtr;
			LayerEyeFov.Viewport[1].Position = new OVRTypes.Vector2i(0, 0);
			LayerEyeFov.Viewport[1].Size = new OVRTypes.Sizei((EyeTargets[1] as DX11.VREyeTarget).Width, (EyeTargets[0] as DX11.VREyeTarget).Height);
			LayerEyeFov.Fov[1] = (EyeTargets[1] as DX11.VREyeTarget).FOV;
			LayerEyeFov.Header.Flags = OVRTypes.LayerFlags.None;

		}

		public override void Present()
		{
			var result = m_VRDevice.HMD.SubmitFrame(0, Layers);
			if (result != OVRTypes.Result.Success) throw new Exception();
		}

		public void Dispose()
		{
			EyeTargets[0].Dispose();
			EyeTargets[1].Dispose();
		}
	}
}
#endif