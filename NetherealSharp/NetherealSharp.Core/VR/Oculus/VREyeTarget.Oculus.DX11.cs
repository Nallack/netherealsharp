﻿
#if DIRECTX11 && OCULUS
using System;
using System.Collections.Generic;
using System.Text;


using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using OculusWrap;
using SharpDX.Direct2D1;

using NetherealSharp.Core.Graphics.DX11;
using Base = NetherealSharp.Core.VR.Oculus;

namespace NetherealSharp.Core.VR.Oculus.DX11
{
	/// <summary>
	/// Equivalent of a 'EyeTexture' in the Oculus wrap demo
	/// </summary>
	public class VREyeTarget : Base.VREyeTarget, IDX11RenderTarget2D
	{
		private GraphicsDevice Graphics;
		private OVRTypes.Sizei m_Size;

		public override int Width { get { return m_Size.Width; } }
		public override int Height { get { return m_Size.Height; } }

		public float PixelsPerDisplayUnit = 1.0f;
		public int Samples = 1;

		//Oculus View
		public OculusWrap.TextureSwapChain SwapTextureSet;

		//SharpDX View
		public D3D11.Texture2D BackBuffer { get { return BackBuffers[BackBufferIndex]; } }
		
		//might want to check whether the swapchain also updates the render targetview like DXGI does
		public D3D11.RenderTargetView RenderTargetView { get { return RenderTargets[BackBufferIndex]; } }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }
		public SharpDX.Direct2D1.RenderTarget D2DTarget { get { return null; } }

		public D3D11.Texture2D[] BackBuffers;
		public D3D11.RenderTargetView[] RenderTargets;
		public D3D11.Texture2D DepthBuffer;
		//shouldnt need a swapchain of depth stencils.

		private int BackBufferIndex
		{
			get
			{
				int index;
				SwapTextureSet.GetCurrentIndex(out index);
				return index;
			}
		}

		public VREyeTarget(VRDevice device, GraphicsDevice graphics, EyeType eyeType)
		{
			Graphics = graphics;

			EyeType = eyeType;
			int eye = (int)eyeType;
			
			

			//Base properties
			FOV = device.HMD.DefaultEyeFov[eye];
			EyeRenderDesc = device.HMD.GetRenderDesc(eyeType.ToOculus(), FOV);
			HmdToEyeViewOffset = EyeRenderDesc.HmdToEyeOffset;

			m_Size = device.HMD.GetFovTextureSize(eyeType.ToOculus(), FOV, PixelsPerDisplayUnit);

			Viewport = new Viewport(0, 0, m_Size.Width, m_Size.Height, 0.0f, 1.0f);


			using (var d = Graphics.D3D11Device.QueryInterface<DXGI.Device1>())
			{
				d.MaximumFrameLatency = 1;
			}

			

			var texDesc = new D3D11.Texture2DDescription()
			{
				Width = m_Size.Width,
				Height = m_Size.Height,
				ArraySize = 1,
				MipLevels = 1,
				Format = DXGI.Format.B8G8R8A8_UNorm,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				BindFlags = D3D11.BindFlags.ShaderResource | D3D11.BindFlags.RenderTarget
			};

			var swaptexdesc = Helper.ConvertTexture2DDesc(texDesc);

			var result = device.HMD.CreateTextureSwapChainDX(
				Graphics.D3D11Device.NativePointer,
				swaptexdesc,
				out SwapTextureSet);

			//Create SharpDX view of textures
			int textureSwapchainBufferCount;
			SwapTextureSet.GetLength(out textureSwapchainBufferCount);
			BackBuffers = new D3D11.Texture2D[textureSwapchainBufferCount];
			RenderTargets = new D3D11.RenderTargetView[textureSwapchainBufferCount];

			for (int i = 0; i < textureSwapchainBufferCount; i++)
			{
				//var texGuid = SharpDX.Utilities.GetGuidFromType(typeof(Texture2D));
				var texGuid = new Guid("6f15aaf2-d208-4e89-9ab4-489535d34f9c");
				IntPtr swapChainTextureComPtr = IntPtr.Zero;
				result = SwapTextureSet.GetBufferDX(i, texGuid, out swapChainTextureComPtr);

				//BackBuffers[i] = new D3D11.Texture2D(SwapTextureSet.TextureSwapChainPtr);
				BackBuffers[i] = new D3D11.Texture2D(swapChainTextureComPtr);
				RenderTargets[i] = new D3D11.RenderTargetView(Graphics.D3D11Device, BackBuffers[i]);

			}
			//create a depth buffer
			DepthBuffer = new D3D11.Texture2D(Graphics.D3D11Device, new D3D11.Texture2DDescription()
			{
				Format = DXGI.Format.D32_Float,
				ArraySize = 1,
				MipLevels = 1,
				Width = m_Size.Width,
				Height = m_Size.Height,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				OptionFlags = D3D11.ResourceOptionFlags.None
			});
			DepthStencilView = new D3D11.DepthStencilView(Graphics.D3D11Device, DepthBuffer);
			
			 
		}

		protected override void OnResized(System.Drawing.Size newSize)
		{
			base.OnResized(newSize);
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override void Present()
		{
			var result = SwapTextureSet.Commit();
			if (result != OVRTypes.Result.Success) throw new Exception();
		}


	}
}
#endif