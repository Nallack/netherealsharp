﻿#if DIRECTX11 && OCULUS && FORMS

using System;
using System.Collections.Generic;
using System.Text;

using OculusWrap;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

using Base = NetherealSharp.Core.VR;
using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.VR.Oculus.DX11
{
    public class VRMirrorTexture : Base.VRMirrorTexture
    {
		NetherealSharp.Core.VR.VRDevice m_VRDevice;
		NetherealSharp.Core.Graphics.DX11.GraphicsDevice m_Graphics;

		RenderTarget Target;
		NetherealSharp.Core.Graphics.DX11.IDX11RenderTarget2D DXTarget;

		//This is not properly abstracted yet
		OculusWrap.MirrorTexture OWMirrorTexture;
		D3D11.Texture2D MirrorTexture;

		public VRMirrorTexture(VRDevice device, GraphicsDevice graphics, RenderTarget target)
		{
			Target = target as RenderTarget;
			DXTarget = Target as Graphics.DX11.IDX11RenderTarget2D;
			m_Graphics = graphics as Graphics.DX11.GraphicsDevice;
			m_VRDevice = device;

			CreateSizeDependentResources();

			Target.Resized += (newSize) =>
			{
				MirrorTexture.Dispose();
				OWMirrorTexture.Dispose();

				CreateSizeDependentResources();

			};
		}

		protected void CreateSizeDependentResources()
		{
			D3D11.Texture2DDescription mirrordesc = GetMirrorDesc();
			var owdesc = Helper.ConvertMirrorTextureDesc(mirrordesc);
			var result = (m_VRDevice as VR.Oculus.VRDevice).HMD.CreateMirrorTextureDX(
				m_Graphics.D3D11Device.NativePointer,
				owdesc,
				out OWMirrorTexture);


			var uuid = new Guid("6f15aaf2-d208-4e89-9ab4-489535d34f9c");
			IntPtr mirrorTextureComPtr = IntPtr.Zero;
			OWMirrorTexture.GetBufferDX(uuid, out mirrorTextureComPtr);

			MirrorTexture = new D3D11.Texture2D(mirrorTextureComPtr);
		}

		public override void CopyToTarget()
		{
			m_Graphics.D3D11Device.ImmediateContext.CopyResource(MirrorTexture, DXTarget.BackBuffer);
		}

		protected D3D11.Texture2DDescription GetMirrorDesc()
		{
			return new D3D11.Texture2DDescription()
			{
				Width = Target.Width,
				Height = Target.Height,
				ArraySize = 1,
				MipLevels = 1,
				Format = DXTarget.RenderTargetView.Description.Format,
				SampleDescription = new DXGI.SampleDescription(
					DXTarget.BackBuffer.Description.SampleDescription.Count,
					DXTarget.BackBuffer.Description.SampleDescription.Quality),
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				BindFlags = D3D11.BindFlags.ShaderResource | D3D11.BindFlags.RenderTarget
			};
		}

		public override void Dispose()
		{
			MirrorTexture.Dispose();
			OWMirrorTexture.Dispose();
		}
	}
}
#endif