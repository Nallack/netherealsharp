﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.VR
{
    public enum VRImplementation
    {
		None,
		Oculus,
		OpenVR
    }
}
