﻿
#if RAWINPUT
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Input;
using SharpDX.Multimedia;
using RI = SharpDX.RawInput;

namespace NetherealSharp.Core.Input.RawInput
{
    /// <summary>
    /// RawInput might need a manager singleton to handle callbacks
    /// from seperate devices.
    /// </summary>
    public class InputMouse : Base.InputMouse
    {
        public InputMouse()
        {
            RI.Device.RegisterDevice(UsagePage.Generic, UsageId.GenericMouse, RI.DeviceFlags.None);

            RI.Device.MouseInput += HandleMouseInput;
        }

        public void HandleMouseInput(object sender, RI.MouseInputEventArgs args)
        {
            Console.WriteLine("Mouse Device : " + args.Device);
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}

#endif