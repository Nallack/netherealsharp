﻿#if FORMS

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Base = NetherealSharp.Core.Input;

namespace NetherealSharp.Core.Input.Forms
{
    public class InputMouse : Base.InputMouse
    {
		protected System.Windows.Forms.Control Control { get; set; }

		#region Properties
		public override bool Hide
		{
			set
			{
				base.Hide = value;
				
				//this has a 1 second delay, buggy
				if(value) System.Windows.Forms.Cursor.Hide();
				else System.Windows.Forms.Cursor.Show();
			}
		}
		#endregion

		//could add public events here

		public InputMouse(System.Windows.Forms.Control control)
		{
			Control = control;

			foreach(var flag in Enum.GetValues(typeof(MouseButton)))
			{
				try
				{
					Buttons.Add((int)flag, new InputButton { Name = flag.ToString() });
				}
				catch (Exception e) { }
			}

			Position = new Vector2(
				System.Windows.Forms.Cursor.Position.X,
				System.Windows.Forms.Cursor.Position.Y);

			control.MouseDown += HandleMouseDown;
			control.MouseUp += HandleMouseUp;

		}
		
		public override void Update()
		{
			Vector2 CurrentPos = new Vector2(
				System.Windows.Forms.Cursor.Position.X,
				System.Windows.Forms.Cursor.Position.Y);

			Delta = CurrentPos - Position;


			if (Lock)
			{
				Position = new Vector2(
					Control.TopLevelControl.Left + Control.ClientRectangle.Width / 2,
					Control.TopLevelControl.Top + Control.ClientRectangle.Height / 2);

				Cursor.Position = new System.Drawing.Point(
					(int)Position.X, 
					(int)Position.Y);
			}
			else
			{
				Position = CurrentPos;
			}

		}

		private void HandleMouseDown(object sender, MouseEventArgs e)
		{
			Buttons[(int)e.Button.FromForms()].State = ButtonState.OnDown;
		}

		private void HandleMouseUp(object sender, MouseEventArgs e)
		{
			Buttons[(int)e.Button.FromForms()].State = ButtonState.OnUp;
		}


    }
}

#endif