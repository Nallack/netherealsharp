﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
    public abstract class InputMouse : InputDevice
    {
		#region Properties
		/// <summary>
		/// Cursor position in pixel space
		/// </summary>
		public Vector2 Position { get; protected set; }

        /// <summary>
        /// Mouse movement from previous frame
        /// </summary>
        public Vector2 Delta { get; protected set; }

        /// <summary>
        /// Mouse movement scaled to fit atomically constant time increment
        /// </summary>
        public Vector2 SmoothDelta { get; protected set; }

        /// <summary>
        /// Lock the cursor to centre of screen
        /// </summary>
        public virtual bool Lock { get; set; } = false;

		/// <summary>
		/// Hide the native cursor
		/// </summary>
		public virtual bool Hide { get; set; } = false;

		#endregion
		#region Events

		#endregion


		public bool GetButton(MouseButton button)
		{
			return (Buttons[(int)button].State & ButtonState.Down) == ButtonState.Down; 
		}

        public abstract void Update();

		#region FactoryMethod
		public static InputMouse Create(InputImplementation implementation, object window)
        {
            switch(implementation)
            {
#if FORMS
                case InputImplementation.Forms:
                    return new Forms.InputMouse(window as System.Windows.Forms.Control);
#endif
#if RAWINPUT
                case InputImplementation.RawInput:
                    return new RawInput.InputMouse();
#endif
                default:
                    throw new NotImplementedException();
            }
        }
		#endregion
	}
}
