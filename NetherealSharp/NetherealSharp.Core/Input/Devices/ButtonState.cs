﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
	public enum ButtonState
	{
		Up, Down, OnUp, OnDown
	}
}
