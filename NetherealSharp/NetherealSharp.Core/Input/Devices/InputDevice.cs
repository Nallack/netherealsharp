﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
    /// <summary>
    /// An abstract interface for all input methods.
    /// May want to consider adding a touch device, tracking device,
    /// and possibly allow for virtual devices from these.
    /// </summary>
    public abstract class InputDevice
    {
		public SortedDictionary<int, InputButton> Buttons { get; set; }
		public SortedDictionary<int, InputAxis> Axis { get; set; }

		public InputDevice()
		{
			Buttons = new SortedDictionary<int, InputButton>();
			Axis = new SortedDictionary<int, InputAxis>();
		}
    }

	public class InputButton
	{
		public string Name { get; set; }
		public ButtonState State { get; set; }
	}

	public class InputAxis
	{
		public string Name { get; set; }
		public float Value { get; set; }
	}
}
