﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
    public enum InputImplementation
    {
        Native,
        Forms,
        WPF,
        OpenTK,
        DirectInput,
        XInput,
        RawInput
    }
}
