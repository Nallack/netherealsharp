﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
    /// <summary>
    /// Keyboard input device, can be handled as a raw input device,
    /// or alternatively accesses via the windowing toolkit.
    /// </summary>
    public abstract class InputKeyboard : InputDevice
    {

		public bool GetKeyDown(Key key)
		{
			return (Buttons[(int)key].State & ButtonState.Down) == ButtonState.Down;
		}

        public abstract void Update();



        public static InputKeyboard Create(InputImplementation implementation, object window)
        {
            switch (implementation)
            {
#if FORMS
                case InputImplementation.Forms:
                    return new Forms.InputKeyboard(window as System.Windows.Forms.Control);
#endif
#if WPF
#endif
#if RAWINPUT
#endif
#if XINPUT
#endif
#if OPENGL
                case InputImplementation.OpenTK:
                    return new OpenGL.InputKeyboard();
#endif
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
