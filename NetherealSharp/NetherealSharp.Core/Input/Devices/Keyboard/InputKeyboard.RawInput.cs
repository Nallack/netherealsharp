﻿#if RAWINPUT

using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.Multimedia;
using RI = SharpDX.RawInput;
using Base = NetherealSharp.Core.Input;


namespace NetherealSharp.Core.Input.WawInput
{
    /// <summary>
    /// Keyboard device for adapter classes to implement.
    /// </summary>
    public class InputKeyboard : Base.InputKeyboard, IDisposable
    {
        public InputKeyboard()
        {
            RI.Device.RegisterDevice(UsagePage.Generic, UsageId.GenericKeyboard, RI.DeviceFlags.None);

            RI.Device.KeyboardInput += HandleKeyboardInput;
        }

        public void HandleKeyboardInput(object sender, RI.KeyboardInputEventArgs args)
        {
            
        }

        public override void Update()
        {

        }

        public void Dispose()
        {
            RI.Device.RegisterDevice(UsagePage.Generic, UsageId.GenericKeyboard, RI.DeviceFlags.Remove);
        }
    }
}

#endif