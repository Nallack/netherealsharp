﻿#if FORMS
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using Base = NetherealSharp.Core.Input;

namespace NetherealSharp.Core.Input.Forms
{
    /// <summary>
    /// Windows forms based keyboard input, where input is read through
    /// OS system events. Good for typing, but not ideal for keyboard state.
    /// </summary>
    public class InputKeyboard : Base.InputKeyboard
    {
        //public event KeyEvent

        protected System.Windows.Forms.Control Control { get; set; }

        public InputKeyboard(System.Windows.Forms.Control control)
        {
            Control = control;

            foreach (var flag in Enum.GetValues(typeof(System.Windows.Forms.Keys)))
            {
				try
				{
					Buttons.Add((int)flag, new InputButton { Name = flag.ToString() });
				}
				catch (Exception) { }
            }

            //May want focus to be handled externally,
            //since it can be good in some scenarios to allow using the app
            //without window focus.
            //probably not possible with this however.
            //control.GotFocus += (sender, e) => { };
            //control.LostFocus += (sender, e) => { };

            control.KeyDown += HandleKeyDown;
            control.KeyUp += HandleKeyUp;
            control.KeyPress += HandleKeyPress;
        }

        private void HandleKeyDown(object sender, System.Windows.Forms.KeyEventArgs args)
        {
            //System doesnt seperate some keys
            if(args.KeyCode == System.Windows.Forms.Keys.ShiftKey)
            {
                if (Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.LShiftKey)))
                    Buttons[(int)Key.LeftShift].State = ButtonState.Down;

                if (Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.RShiftKey)))
                    Buttons[(int)Key.RightShift].State = ButtonState.Down;
            }
            else Buttons[(int)args.KeyCode].State = ButtonState.Down;
        }

        private void HandleKeyUp(object sender, System.Windows.Forms.KeyEventArgs args)
        {
            //System doesnt seperate some keys
            if (args.KeyCode == System.Windows.Forms.Keys.ShiftKey)
            {
                if (Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.LShiftKey)))
                    Buttons[(int)Key.LeftShift].State = ButtonState.Up;

                if (Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.RShiftKey)))
                    Buttons[(int)Key.RightShift].State = ButtonState.Up;
            }
            else Buttons[(int)args.KeyCode].State = ButtonState.Up;
        }

        private void HandleKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs args)
        {
            //Not sure about te behaviour of this
        }

        public override void Update()
        {
            //event based triggers, may not need anything here
            //throw new NotImplementedException();
        }

        [DllImport("user32.dll")]
        private static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey);
    }
}
#endif