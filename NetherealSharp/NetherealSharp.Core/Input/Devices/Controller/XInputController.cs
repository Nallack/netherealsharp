﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
    /// <summary>
    /// Class specific to XInput controllers (aka xbox 360 or One),
    /// but can be interfaced through one many API's
    /// such as XInput, DirectInput, RawInput
    /// </summary>
    public abstract class XInputController : InputController
    {
        public static InputController Create(InputImplementation implementation)
        {
            switch (implementation)
            {
#if DIRECTINPUT
                case InputImplementation.DirectInput:
                    return new DI.XInputController();
#endif
#if XINPUT
                case InputImplementation.XInput:
                    return new XInput.XInputController();
#endif
#if RAWINPUT
                case InputImplementation.RawInput:
                    return new RawInput.XInputController();
#endif
#if OPENGL
                case InputImplementation.OpenTK:
                    return new OpenGL.XInputController();
#endif
                default:
                    throw new NotImplementedException();
            }

        }
    }
}
