﻿
#if DIRECTX && XINPUT
using System;
using System.Collections.Generic;
using System.Text;

using XI = SharpDX.XInput;
using Base = NetherealSharp.Core.Input;

namespace NetherealSharp.Core.Input.XInput
{
    public class XInputController : Base.XInputController
    {
		protected XI.Controller Controller { get; private set; }
		protected XI.State State { get; set; }
		protected int LastPacket { get; set; }

		public XInputController()
		{
			Controller = new XI.Controller(XI.UserIndex.One);

			foreach(XI.GamepadButtonFlags flag in Enum.GetValues(typeof(XI.GamepadButtonFlags)))
			{
				Buttons.Add((int)flag, new InputButton() { Name = flag.ToString() });
			}

		}

		public override void Update()
		{
			State = Controller.GetState();
			int currentPacket = State.PacketNumber;

			if (currentPacket != LastPacket)
			{
				XI.GamepadButtonFlags buttons = State.Gamepad.Buttons;

				foreach (XI.GamepadButtonFlags flag in Enum.GetValues(typeof(XI.GamepadButtonFlags)))
				{
					if ((buttons & flag) != 0) Buttons[(int)flag].State = ButtonState.Down;
					else Buttons[(int)flag].State = ButtonState.Up;
				}
			}

			LastPacket = currentPacket;
			
		}

    }
}
#endif