﻿#if RAWINPUT
using System;
using System.Collections.Generic;
using System.Text;

using RI = SharpDX.RawInput;
using MM = SharpDX.Multimedia;

using Base = NetherealSharp.Core.Input;

namespace NetherealSharp.Core.Input.RawInput
{
    public class XInputController : Base.XInputController
    {
        public XInputController()
        {
            RI.Device.RegisterDevice(MM.UsagePage.Generic, MM.UsageId.GenericJoystick, RI.DeviceFlags.None);
            RI.Device.RawInput += HandleRawInput;
            
        }

        internal void HandleRawInput(object sender, RI.RawInputEventArgs rawArgs)
        {
            var args = (RI.HidInputEventArgs)rawArgs;

            //args.RawData.
            Console.WriteLine("Got Jostick input");

            throw new NotImplementedException();
        }

        public override void Update()
        {
            throw new NotImplementedException();
        }
    }
}
#endif