﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Input;

namespace NetherealSharp.Core.Input.OpenGL
{
    public class XInputController : Base.XInputController
    {
        protected int DeviceIndex { get; set; } 

        public XInputController()
        {
            DeviceIndex = 0;
        }

        public override void Update()
        {

        }
    }
}

#endif