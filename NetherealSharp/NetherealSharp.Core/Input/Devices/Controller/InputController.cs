﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
	/// <summary>
	/// A wrapper of keybindings to actions to be performed in an application.
	/// </summary>
    public abstract class InputController : InputDevice
    {

        public abstract void Update();

	}
}
