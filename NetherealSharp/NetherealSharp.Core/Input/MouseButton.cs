﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Input
{
	public enum MouseButton
	{
		Left,
		Right,
		Middle
	}

	public static class MouseButtonConversion
	{
#if FORMS
		public static MouseButton FromForms(this System.Windows.Forms.MouseButtons value)
		{
			switch(value)
			{
				case System.Windows.Forms.MouseButtons.Left: return MouseButton.Left;
				case System.Windows.Forms.MouseButtons.Right: return MouseButton.Right;
				case System.Windows.Forms.MouseButtons.Middle: return MouseButton.Middle;
				default: throw new Exception("Unknown mouse button : " + value);
			}
		}
#endif
#if WPF
		public static MouseButton FromWPF(this System.Windows.Input.MouseButton value)
		{
			switch (value)
			{
				case System.Windows.Input.MouseButton.Left: return MouseButton.Left;
				default: return MouseButton.Right;
			}
		}
#endif
	}
}
