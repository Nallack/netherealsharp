﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public struct MaterialData
    {
		public Vector4 DiffuseColor;
		public Vector4 SpecularColor;
		public float Shininess;
		public float DiffuseIntensity;
		public float SpecularIntensity;
		float pad0;
    }
}
