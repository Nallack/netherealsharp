﻿using NetherealSharp.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[DataContract, Content(RegistryType = typeof(Material))]
	public abstract class Material : IContentable, IDisposable
	{
		[DataMember]
		public string Name { get; set; }
		public Type ContentType { get { return typeof(Material); } }

		public Dictionary<string, object> Properties { get; protected set; } = new Dictionary<string, object>();


		public abstract void Update();
		public abstract void Bind(GraphicsContext context);
		public abstract void Unbind(GraphicsContext context);

		public abstract void Dispose();
		
	}
}
