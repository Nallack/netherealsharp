﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[DataContract]
    public class BasicMaterial : Material
    {
		[DataMember]
		public EffectPass Effect { get; set; }

		[DataMember]
		public EffectPass ShadowEffect { get; set; }

		[DataMember]
		public Texture2D DiffuseMap { get; set; }
		//Texture2D NormalMap;
		//Texture2D SpecularMap;

		public UniformBuffer<MaterialData> MaterialUniform { get; protected set; }

		public Color DiffuseColor;
		public Color SpecularColor;
		public float DiffuseIntensity;
		public float SpecularIntensity;
		public float Shininess;



		public BasicMaterial(GraphicsDevice device, string name)
		{
			Name = name;
			MaterialUniform = UniformBuffer<MaterialData>.Create(device);
		}

		public override void Update()
		{
			MaterialUniform.Data.DiffuseColor = DiffuseColor.ToVector4();
			MaterialUniform.Data.SpecularColor = SpecularColor.ToVector4();
			MaterialUniform.Data.Shininess = Shininess;
			MaterialUniform.Data.DiffuseIntensity = DiffuseIntensity;
			MaterialUniform.Data.SpecularIntensity = SpecularIntensity;
			MaterialUniform.Update();
		}

		public override void Bind(GraphicsContext context)
		{
			Effect.Bind(context);

			//TODO: GL doesnt like this line for some reason, probably wrong texture location
			//DiffuseMap.Apply(context, (int)TextureLocation.Diffuse);

			Effect.BindTexture2D(context, "DiffuseMap", DiffuseMap);
			MaterialUniform.Bind(context, (int)UniformLocation.Material);
		}

		public override void Unbind(GraphicsContext context)
		{
			Effect.Unbind(context);
		}


		public override void Dispose()
		{
			MaterialUniform.Dispose();
		}


	}
}
