﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// Based of the UnityEngine implementation of materials
	/// where properties seem to be stored in Maps and shader reflection
	/// determines which maps to lookup to pass in as uniforms.
	/// Would seem though you would need to use Raw Buffer objects for uniforms
	/// instead of generics so properties dont have to be predefined in structs.
	/// </summary>
    public abstract class CustomMaterial
    {


		//public void SetBuffer(string name, UniformBuffer buffer) { }
		public void SetTexture(string name, Texture2D texture) { }
		public void SetInt(string name, int value) { }
		public void SetMatrix(string name, Matrix matrix) { }
		public void SetColor(string name, Color color) { }
		public void SetVector(string name, Vector4 value) { }
		public void SetFloat(string name, float value) { }

    }
}
