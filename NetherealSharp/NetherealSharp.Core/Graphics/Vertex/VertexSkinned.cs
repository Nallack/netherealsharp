﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public struct VertexSkinned : IVertex
	{
		public Vector3 Position;
		public Vector3 Normal;
		public Vector2 TexCoord;

		public Int4 BoneIndices;
		public Vector4 BoneWeights;

		public static readonly VertexLayout Layout = new VertexLayout()
		{
			Size = 64,
			Elements = new VertexLayoutElement[]
			{
				new VertexLayoutElement("vPosition", 0, "POSITION", 3, VertexType.Float, false, 64, 0),
				new VertexLayoutElement("vNormal", 1, "NORMAL", 3, VertexType.Float, true, 64, 12),
				new VertexLayoutElement("vTexCoord", 2, "TEXCOORD", 2, VertexType.Float, false, 64, 24),
				new VertexLayoutElement("vBoneIndices", 3, "BONEINDICES", 4, VertexType.Int, false, 64, 32),
				new VertexLayoutElement("vBoneWeights", 4, "BoneWEIGHTS", 4, VertexType.Float, false, 64, 48)
			}
		};

		public VertexSkinned(Vector3 position, Vector3 normal, Vector2 texCoord, Int4 indices, Vector4 weights)
		{
			Position = position;
			Normal = normal;
			TexCoord = texCoord;
			BoneIndices = indices;
			BoneWeights = weights;
		}

	}
}
