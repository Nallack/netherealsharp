﻿using System;
using System.Collections.Generic;
using System.Text;

#if DIRECTX11
using D3D11 = SharpDX.Direct3D11;
using SharpDX.DXGI;
#endif
#if DIRECTX12
using D3D12 = SharpDX.Direct3D12;
#endif

namespace NetherealSharp.Core.Graphics
{
    public class VertexLayout
    {
		public VertexLayoutElement[] Elements;
		public int Size;

		public VertexLayout()
		{
		}

#if DIRECTX11
		public D3D11.InputElement[] ToD3D11()
		{
			D3D11.InputElement[] elements = new D3D11.InputElement[Elements.Length];
			for(int i = 0; i < Elements.Length; i++)
			{
				elements[i] = new D3D11.InputElement(
					Elements[i].Semantic,
					0,
					Elements[i].ToDXGI(),
					Elements[i].Offset,
					0);
			}
			return elements;
		}
#endif
#if DIRECTX12
		public D3D12.InputElement[] ToD3D12()
		{
			D3D12.InputElement[] elements = new D3D12.InputElement[Elements.Length];
			for(int i = 0; i < Elements.Length; i++)
			{
				elements[i] = new D3D12.InputElement(
					Elements[i].Semantic,
					0,
					Elements[i].ToDXGI(),
					Elements[i].Offset,
					0);
			}
			return elements;
		}
#endif
	}
}
