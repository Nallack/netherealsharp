﻿using System;
using System.Collections.Generic;
using System.Text;


#if DIRECTX
using SharpDX;
#elif OPENGL
using OpenTK;
using OpenTK.Graphics.OpenGL;
#endif


namespace NetherealSharp.Core.Graphics
{
    public struct VertexPNT : IVertex, IVertexPos
    {
		public Vector3 Position { get; set; }
		public Vector3 Normal;
		public Vector2 TexCoord;

		public static readonly VertexLayout Layout = new VertexLayout()
		{
			Size = 32,
			Elements = new VertexLayoutElement[]
			{
				new VertexLayoutElement("vPosition", 0, "POSITION", 3, VertexType.Float, false, 32, 0),
				new VertexLayoutElement("vNormal", 1, "NORMAL", 3, VertexType.Float, true, 32, 12),
				new VertexLayoutElement("vTexCoord", 2, "TEXCOORD", 2, VertexType.Float, false, 32, 24)
			}
		};

		public VertexPNT(Vector3 position, Vector3 normal, Vector2 texCoord)
		{
			Position = position;
			Normal = normal;
			TexCoord = texCoord;
		}

	}
}
