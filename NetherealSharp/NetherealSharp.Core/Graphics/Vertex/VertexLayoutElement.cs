﻿using System;
using System.Collections.Generic;
using System.Text;

#if DIRECTX11
using SharpDX.DXGI;
#elif OPENGL
using OpenTK.Graphics.OpenGL;
#endif

namespace NetherealSharp.Core.Graphics
{
    public struct VertexLayoutElement
    {
		public string Name;
		public int Index;
		public string Semantic;
		public int Size;
		public VertexType Type; 
		//public VertexAttribPointerType Type;
		public bool Normalized;
		public int Stride;
		public int Offset;

		public VertexLayoutElement(string name, int index, string semantic, int size, VertexType type, bool normalized, int stride, int offset)
		{
			Name = name;
			Index = index;
			Semantic = semantic;
			Size = size;
			Type = type;
			Normalized = normalized;
			Stride = stride;
			Offset = offset;
		}

#if DIRECTX11 || DIRECTX12
		public Format ToDXGI()
		{
			if (Type == VertexType.Float && Size == 4) return Format.R32G32B32A32_Float;
			if (Type == VertexType.Float && Size == 3) return Format.R32G32B32_Float;
			if (Type == VertexType.Float && Size == 2) return Format.R32G32_Float;
			return Format.Unknown;
		}
#endif
	}
}
