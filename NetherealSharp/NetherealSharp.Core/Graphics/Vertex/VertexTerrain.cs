﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    struct VertexTerrain : IVertex
    {
		public Vector3 Position;
		public Vector2 TexCoord;
		public Vector4 BlendWeight;
		public Int4    BlendIndices;

		public static readonly VertexLayout Layout = new VertexLayout()
		{
			Size = 32,
			Elements = new VertexLayoutElement[]
			{
				new VertexLayoutElement("vPosition", 0, "POSITION", 3, VertexType.Float, false, 52, 0),
				new VertexLayoutElement("vTexCoord", 1, "TEXCOORD", 2, VertexType.Float, false, 52, 12),
				new VertexLayoutElement("vBlendWeight", 2, "BLENDWEIGHT", 4, VertexType.Float, false, 52, 20),
				new VertexLayoutElement("vBlendIndices", 3, "BLENDINDICES", 4, VertexType.Int, false, 52, 36)
			}
		};

		public VertexTerrain(Vector3 position, Vector2 texcoord, Vector4 blendweight, Int4 blendindices)
		{
			Position = position;
			TexCoord = texcoord;
			BlendWeight = blendweight;
			BlendIndices = blendindices;
		}
    }
}
