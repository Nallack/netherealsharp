﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class VertexPT : IVertex, IVertexPos
    {
		public Vector3 Position { get; set; }
		public Vector3 Normal;
		public Vector2 TexCoord;

		public static readonly VertexLayout Layout = new VertexLayout()
		{
			Size = 20,
			Elements = new VertexLayoutElement[]
			{
				new VertexLayoutElement("vPosition", 0, "POSITION", 3, VertexType.Float, false, 20, 0),
				new VertexLayoutElement("vTexCoord", 1, "TEXCOORD", 2, VertexType.Float, false, 20, 12)
			}
		};
    }
}
