﻿
#if OPENGL
using OpenTK.Graphics.OpenGL;
#endif

namespace NetherealSharp.Core.Graphics
{
	public enum VertexType
	{
		Float,
		Int,
		UnsignedInt
	}

	public static class VertexTypeConversion
	{
#if DIRECTX11
#endif
#if OPENGL
		public static VertexAttribPointerType ToOpenGL(this VertexType value)
		{
			switch(value)
			{
				case VertexType.Float:
					return VertexAttribPointerType.Float;
				default:
					return VertexAttribPointerType.Float;
			}
			
		}
#endif
	}
}