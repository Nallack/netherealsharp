﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public struct VertexPC : IVertex
	{
		public Vector3 Position;
		public Vector4 Color;

		public static VertexLayout Layout = new VertexLayout()
		{
			Size = 28,
			Elements = new VertexLayoutElement[]
			{
				new VertexLayoutElement("vPosition", 0, "POSITION", 3, VertexType.Float, false, 28, 0),
				new VertexLayoutElement("vColor", 1, "COLOR", 4, VertexType.Float, false, 28, 12),
			}
		};

		public VertexPC(Vector3 position, Vector4 color)
		{
			Position = position;
			Color = color;
		}

	}
}
