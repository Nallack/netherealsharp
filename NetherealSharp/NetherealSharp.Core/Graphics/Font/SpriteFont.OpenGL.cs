﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using SharpFont;
using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.InteropServices;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	/// <summary>
	/// Stores font as atlas on GPU memory with kerning info
	/// </summary>
	public class SpriteFont : Base.SpriteFont
	{
		private GraphicsDevice Device;

		int width = 1024;
		int height = 1024;

		private uint charOffset = 0u;
		private uint charCount = 255u;

		public Face Face;

		public SpriteFont(GraphicsDevice device, string filepath, int size)
		{
			bool aliased = false;

			Device = device;

			Face = new SharpFont.Face(Device.FontLibrary, filepath);
			Face.SetCharSize(0, size, 0, (uint)size);


			Texture2D atlas;
			//if(aliased) atlas = Texture2D.LoadEmpty(width, height, TextureMinFilter.Linear, TextureMagFilter.Linear, PixelInternalFormat.Alpha, PixelFormat.Alpha);
			atlas = Texture2D.LoadEmpty(
				width, 
				height, 
				TextureMinFilter.Linear, 
				TextureMagFilter.Linear, 
				PixelInternalFormat.Alpha, 
				PixelFormat.Alpha, 
				PixelType.UnsignedByte);


			FontMetric = new FontMetric();
			Atlas = atlas;

			FontMetric.LineHeight = Face.Height / 64; // * LineSpacing
			

			int xmargin = 1;
			int ymargin = 1;
			int ystep = 0;

			System.Drawing.Point nextAtlasPos = new System.Drawing.Point(xmargin, ymargin);

			for (uint i = charOffset; i <= charOffset + charCount; i++)
			{

				Face.LoadGlyph(i, LoadFlags.Default, LoadTarget.Normal);
				Face.Glyph.RenderGlyph(RenderMode.Normal);

				FontMetric.MaxBearingY = Math.Max(FontMetric.MaxBearingY, (float)Face.Glyph.Metrics.HorizontalBearingY);

				if (nextAtlasPos.X + Face.Glyph.Bitmap.Width + xmargin > width)
				{
					nextAtlasPos.Y += ystep + ymargin;
					nextAtlasPos.X = xmargin;
					ystep = 0;
				}

				//store metrics for batch rendering
				var gm = new GlyphMetric();

				//gm.Top = (float)Face.Glyph.BitmapTop;
				//gm.Left = (float)Face.Glyph.BitmapLeft;
				gm.BearingX = (float)Face.Glyph.Metrics.HorizontalBearingX;
				gm.BearingY = (float)Face.Glyph.Metrics.HorizontalBearingY;

				if (aliased)
				{
					//add a pixel wide atlas border

					gm.XAdvance = (float)(Face.Glyph.Advance.X - 1);
					gm.YAdvance = (float)(Face.Glyph.Advance.Y);
					gm.Width = (float)(Face.Glyph.Metrics.Width + 1);
					gm.Height = (float)(Face.Glyph.Metrics.Height + 1);

					gm.TexX = (nextAtlasPos.X - 0.5f) / (float)width;
					gm.TexY = (nextAtlasPos.Y - 0.5f) / (float)height;
					gm.TexWidth = (Face.Glyph.Bitmap.Width + 1) / (float)width;
					gm.TexHeight = (Face.Glyph.Bitmap.Rows + 1) / (float)height;
				}
				else
				{

					gm.XAdvance = (float)(Face.Glyph.Advance.X);
					gm.YAdvance = (float)(Face.Glyph.Advance.Y);

					gm.Width = (float)(Face.Glyph.Metrics.Width);
					gm.Height = (float)(Face.Glyph.Metrics.Height);

					gm.TexX = (nextAtlasPos.X) / (float)width;
					gm.TexY = (nextAtlasPos.Y) / (float)height;
					gm.TexWidth = (Face.Glyph.Bitmap.Width) / (float)width;
					gm.TexHeight = (Face.Glyph.Bitmap.Rows) / (float)height;
				}

				FontMetric.GlyphMetrics.Add(i + 29, gm);

				//update texture
				atlas.UpdateRegion(
					nextAtlasPos.X, nextAtlasPos.Y, 
					Face.Glyph.Bitmap.Width, Face.Glyph.Bitmap.Rows,
					PixelFormat.Alpha,
					Face.Glyph.Bitmap.Buffer);
					

				nextAtlasPos.X += Face.Glyph.Bitmap.Width + xmargin;
				ystep = Math.Max(ystep, Face.Glyph.Bitmap.Rows);
				
			}
		}


		public override void Apply(int slot)
		{
			
		}

		public override void Dispose()
		{
			Atlas.Dispose();
			Face.Dispose();
		}
	}
}
#endif