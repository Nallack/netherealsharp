﻿
#if DIRECTX11
using SharpDX.DirectWrite;
using System;
using System.Collections.Generic;
using System.Text;

using SF = SharpFont;

using DXGI = SharpDX.DXGI;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Stores font as atlas on GPU memory with kerning info
	/// </summary>
	public class FTSpriteFont : Base.SpriteFont
	{


		public SF.Face Face;
		//internal System.Drawing.Font Font;

		int width = 1024;
		int height = 1024;

		private uint charOffset = 0u;
		private uint charCount = 255u;

		public FTSpriteFont(GraphicsDevice device, string fontname, int size)
		{
			//OpenGL has a method of TexSubImage2D to partially write to GPU for each glyph.
			//DirectX11 doesn't quite do this as nicely and will need to create a data rectangle and etc as
			//the library only returns a raw pointer to the texture data.

			//Instead we'll just generate a bitmap and copy the whole thing to CPu as a whole.


			Face = new SharpFont.Face(device.FontLibrary, fontname);
			Face.SetCharSize(0, size, 0, (uint)size);

			//var atlas = Texture2D.LoadEmpty(width, height, DXGI.Format.A8_UNorm);
			//Atlas = atlas;

			var bitmap = new System.Drawing.Bitmap(width, height);

			FontMetric = new FontMetric();

			FontMetric.LineHeight = Face.Height / 64; // * LineSpacing

			int xmargin = 1;
			int ymargin = 1;
			int ystep = 0;

			System.Drawing.Point nextAtlasPos = new System.Drawing.Point(xmargin, ymargin);

			using (var g = System.Drawing.Graphics.FromImage(bitmap))
			{
				for (uint i = charOffset; i <= charOffset + charCount; i++)
				{

					Face.LoadGlyph(i, SF.LoadFlags.Default, SF.LoadTarget.Normal);
					Face.Glyph.RenderGlyph(SF.RenderMode.Normal);

					if (nextAtlasPos.X + Face.Glyph.Bitmap.Width + xmargin > width)
					{
						nextAtlasPos.Y += ystep + ymargin;
						nextAtlasPos.X = xmargin;
					}

					FontMetric.MaxBearingY = Math.Max(FontMetric.MaxBearingY, (float)Face.Glyph.Metrics.HorizontalBearingY);

					var gm = new GlyphMetric();

					gm.BearingX = (float)Face.Glyph.Metrics.HorizontalBearingX;
					gm.BearingY = (float)Face.Glyph.Metrics.HorizontalBearingY;

					gm.XAdvance = (float)Face.Glyph.Advance.X;
					gm.YAdvance = (float)Face.Glyph.Advance.Y;

					gm.Width = (float)Face.Glyph.Metrics.Width;
					gm.Height = (float)Face.Glyph.Metrics.Height;

					gm.TexX = (nextAtlasPos.X) / (float)width;
					gm.TexY = (nextAtlasPos.Y) / (float)height;
					gm.TexWidth = (Face.Glyph.Bitmap.Width) / (float)width;
					gm.TexHeight = (Face.Glyph.Bitmap.Rows) / (float)height;

					FontMetric.GlyphMetrics.Add(i + 29, gm);

					if (Face.Glyph.Bitmap.Width > 0)
					{
						var cbitmap = Face.Glyph.Bitmap.ToGdipBitmap(System.Drawing.Color.Black);
						g.DrawImageUnscaled(cbitmap, nextAtlasPos.X, nextAtlasPos.Y);

						nextAtlasPos.X += Face.Glyph.Bitmap.Width + xmargin;
						ystep = Math.Max(ystep, Face.Glyph.Bitmap.Rows);
					}
				}
			}

			Atlas = Texture2D.CreateFromData(device, width, height, DXGI.Format.R8G8B8A8_UNorm, bitmap);
		}

		public override void Apply(int slot)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			Atlas.Dispose();
			Face.Dispose();
		}
	}
}
#endif