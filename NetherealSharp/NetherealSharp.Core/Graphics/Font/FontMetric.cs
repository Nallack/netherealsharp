﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class FontMetric
    {
		public float MaxBearingY;
		public int LineHeight;

		public Dictionary<uint, GlyphMetric> GlyphMetrics { get; set; }

		public FontMetric()
		{
			GlyphMetrics = new Dictionary<uint, GlyphMetric>();
		}
	}
}
