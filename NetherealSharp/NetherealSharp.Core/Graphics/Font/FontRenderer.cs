﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D2D1 = SharpDX.Direct2D1;
using DW = SharpDX.DirectWrite;

namespace NetherealSharp.Core.Graphics.DX11
{
	//Font Rendering using D2D1
    public class FontRenderer
    {
		public GraphicsDevice Graphics;

		//Equivelent to SpriteFont
		public DW.TextFormat Format;


		public DW.TextLayout Layout;

		public FontRenderer(GraphicsDevice device)
		{

			Format = new DW.TextFormat(
				Graphics.DWFactory,
				"Arial",
				DW.FontWeight.Normal,
				DW.FontStyle.Normal,
				32);



		}

    }
}
#endif
