﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A FontAtlas
	/// Stores font as atlas on GPU memory with kerning info
	/// </summary>
	public abstract class SpriteFont : IContentable, IDisposable
	{
		public string Name { get; set; }
		public Type ContentType { get { return typeof(SpriteFont); } }

		public Texture2D Atlas { get; protected set; }
		//public Dictionary<uint, GlyphMetric> FontMetrics;
		public FontMetric FontMetric;

		public static SpriteFont CreateFromFile(GraphicsDevice device, string filename, int size)
		{

			//TODO: in one of the config managers detect windows
#if WINDOWS
			string fontsfolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts);
#endif

			

			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.FTSpriteFont(device as DX11.GraphicsDevice, Path.Combine(fontsfolder, filename), size);
#endif
#if DIRECTX12
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.SpriteFont(device as OpenGL.GraphicsDevice, Path.Combine(fontsfolder, filename), size);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Apply(int slot);

		public abstract void Dispose();
	}
}
