﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D2D1 = SharpDX.Direct2D1;
using DW = SharpDX.DirectWrite;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class NativeFont : Base.NativeFont
    {
		private GraphicsDevice Device;

		public DW.TextFormat format;
		public DW.TextLayout layout;
		public D2D1.Brush brush;

		public NativeFont(GraphicsDevice device)
		{
			Device = device;

			format = new DW.TextFormat(Device.DWFactory, "Arial", 12)
			{
				//TextAlignment = DW.TextAlignmen
			};

			layout = new DW.TextLayout(Device.DWFactory, "hello", format, 200, 100);
			
		}

		public void DrawString(string text, Vector2 position)
		{
			using (var brush = new D2D1.SolidColorBrush((Device.ImmediateContext as DX11.GraphicsContext).D2D1Context, Color.Black))
			{
				(Device.ImmediateContext as DX11.GraphicsContext).D2D1Context.BeginDraw();
				(Device.ImmediateContext as DX11.GraphicsContext).D2D1Context.DrawTextLayout(position, layout, brush);
				//m_Graphics.D2D1Context.DrawText(D2D1.MeasuringMode.)
				//m_Graphics.DWFactory.DW.TextAlignment.Center


				(Device.ImmediateContext as DX11.GraphicsContext).D2D1Context.EndDraw();
			}
		}
    }
}
#endif