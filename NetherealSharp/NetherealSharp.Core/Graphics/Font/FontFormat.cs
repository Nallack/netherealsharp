﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public enum TextAlignment
	{
		Left,
		Center,
		Right
	}

    public struct FontFormat
    {
		public TextAlignment Alignment;
    }
}
