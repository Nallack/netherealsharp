﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class GlyphMetric
    {

		//public float Top;
		//public float Left;

		public float BearingX;
		public float BearingY;

		public float XAdvance;
		public float YAdvance;
		public float Width;
		public float Height;

		public float TexX;
		public float TexY;
		public float TexWidth;
		public float TexHeight;
    }
}
