﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using D2D1 = SharpDX.Direct2D1;
using DW = SharpDX.DirectWrite;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// DirectWrite Font
	/// </summary>
    public class RasterFont
    {
		public GraphicsDevice Graphics;

		public DW.TextFormat TextFormat;

		public RasterFont(GraphicsDevice device, string fontname, int size)
		{
			Graphics = device;

			TextFormat = new DW.TextFormat(
				Graphics.DWFactory,
				fontname,
				DW.FontWeight.Normal,
				DW.FontStyle.Normal,
				size);
		}
    }
}
#endif