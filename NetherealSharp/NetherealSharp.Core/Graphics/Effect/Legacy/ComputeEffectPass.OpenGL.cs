﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using OGL = OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class ComputeEffect : Base.ComputeEffect
    {
		GraphicsDevice m_device;
		public int shaderId;

		public ComputeEffect(GraphicsDevice device, string filepath)
		{
			m_device = device;
			CompileFromFile(filepath);
		}

		protected void CompileFromFile(string filepath)
		{
#if OPENTK1_1
			Shader computeShader = new Shader(m_device, (OGL.ShaderType)(0xFF));
#elif OPENTK2_0
			Shader computeShader = new Shader(m_device, OGL.ShaderType.ComputeShader);
#endif


		}

		public override void Bind(Base.GraphicsContext context)
		{
			
		}

		public override void BindUniform<T>(Base.UniformBuffer<T> uniform, int slot)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2D(ITexture texture, int slot)
		{
			throw new NotImplementedException();
		}

		public override void BindAccessBuffer<T>(Base.AccessBuffer<T> buffer, int slot)
		{
			throw new NotImplementedException();
		}

		public override void Execute()
		{
			//Cl.EnqueueNDRangeKernel()
		}

		public override void Unbind()
		{
			throw new NotImplementedException();
		}

	}
}
#endif