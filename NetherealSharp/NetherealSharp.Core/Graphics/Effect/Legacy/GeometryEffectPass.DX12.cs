﻿using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class GeometryEffectPass : Base.GeometryEffectPass
	{
		public static GeometryEffectPass CompileFromFile(string filename, VertexLayout layout)
		{
			return null;
		}


		public override void Bind(GraphicsContext context)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2D(GraphicsContext context, int slot, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2D(GraphicsContext context, string name, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2DArray(GraphicsContext context, int slot, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2DArray(GraphicsContext context, string name, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindUniform<T>(GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
			throw new NotImplementedException();
		}

		public override void BindUniform<T>(GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override int GetTextureLocation(string name)
		{
			throw new NotImplementedException();
		}

		public override int GetUniformLocation(string name)
		{
			throw new NotImplementedException();
		}

		public override void Unbind(GraphicsContext context)
		{
			throw new NotImplementedException();
		}
	}
}
