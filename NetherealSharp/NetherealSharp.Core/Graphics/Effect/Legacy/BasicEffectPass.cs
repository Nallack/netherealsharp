﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;

#if DIRECTX11
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.D3DCompiler;
#elif OPENGL
using OpenTK;
using OpenTK.Graphics.OpenGL;
#endif

namespace NetherealSharp.Core.Graphics
{


	//Check!!! http://stackoverflow.com/questions/1327064/serialization-of-an-abstract-class

	[DataContract]
	public abstract class BasicEffectPass : EffectPass
	{

		public BasicEffectPass()
		{

		}

		public static BasicEffectPass CompileFromFile(GraphicsDevice device, string filename, VertexLayout layout)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				//case GraphicsImplementation.DirectX11:
					//return DX11.BasicEffect.CompileFromFile(filename, layout);
					//return new DX11.BasicEffectPass(device as DX11.GraphicsDevice, filename, layout);
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return DX12.BasicEffect.CompileFromFile(device as DX12.GraphicsDevice, filename, layout);
#endif
#if OPENGL
				//case GraphicsImplementation.OpenGL:
					//return OpenGL.BasicEffectPass.CompileFromFile(device as OpenGL.GraphicsDevice, filename, layout);
#endif
#if OPENGLES
				case GraphicsImplementation.OpenGLES:
					return OpenGLES.BasicEffectPass.CompileFromFile(device as OpenGLES.GraphicsDevice, filename, layout);
#endif
#if VULKAN
//				case GraphicsImplementation.Vulkan:
//					return Vulkan.BasicEffectPass.;
#endif
				default:
					throw new NotImplementedException();
            }
		}

	}
}
