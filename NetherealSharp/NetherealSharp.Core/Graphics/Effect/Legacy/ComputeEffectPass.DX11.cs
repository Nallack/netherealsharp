﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.D3DCompiler;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// 
	/// 
	/// Usage:	this.Bind()
	///			this.BindUniform<T>()		(ConstantBuffer)
	///			this.BindAccessBuffer<T>()	(RWStructuredBuffer/UnorderedAccessView)
	///			this.Execute()
	///
	///AccessBuffer now contains the results
	/// </summary>
	public class ComputeEffect : Base.ComputeEffect
	{
		D3D11.Device Device;
		D3D11.DeviceContext Context;

		D3D11.ComputeShader ComputeShader;

		int m_ThreadGroupsX;
		int m_ThreadGroupsY;
		int m_ThreadGroupsZ;


		public ComputeEffect(Base.GraphicsDevice device)
		{
			Device = (device as GraphicsDevice).D3D11Device;

			string filepath = "";

			var csbc = ShaderBytecode.CompileFromFile(
				filepath + ".hlsl",
				"CS",
				"cs_5_0",
				ShaderFlags.Debug,
				EffectFlags.None);

			if (csbc.Bytecode == null)
			{
				Diagnostics.Debug.LogError("Compute Shader Error", "Error Compiling \"" + filepath + "\"\n" + csbc.Message);
			}
			ComputeShader = new D3D11.ComputeShader(Device, csbc);


		}

		public override void Bind(Base.GraphicsContext context)
		{
			Context = (context as GraphicsContext).D3D11Context;
			Context.ComputeShader.Set(ComputeShader);
		}

		public override void BindUniform<T>(Base.UniformBuffer<T> uniform, int slot)
		{
			Context.ComputeShader.SetConstantBuffer(slot, (uniform as DX11.UniformBuffer<T>).Buffer);
		}

		public override void BindTexture2D(ITexture texture, int slot)
		{
			Context.ComputeShader.SetShaderResource(slot, (texture as IDX11Texture).ShaderResourceView);
		}

		public override void BindAccessBuffer<T>(Base.AccessBuffer<T> buffer, int slot)
		{
			Context.ComputeShader.SetUnorderedAccessView(slot, (buffer as DX11.AccessBuffer<T>).UnorderedAccessView);
		}

		public override void Unbind()
		{
			Context.ComputeShader.Set(null);
			Context = null;
		}

		public override void Execute()
		{
			Device.ImmediateContext.Dispatch(m_ThreadGroupsX, m_ThreadGroupsY, m_ThreadGroupsZ);
		}

    }
}
#endif