﻿#if DIRECTX12

using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.D3DCompiler;
using D3D = SharpDX.Direct3D;
using DXGI = SharpDX.DXGI;
using D3D12 = SharpDX.Direct3D12;
using Base = NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Diagnostics;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class BasicEffect : Base.BasicEffectPass
	{
		GraphicsDevice Graphics;

		D3D12.ShaderBytecode VertexShader;
		D3D12.ShaderBytecode PixelShader;

		//ShaderProgram equivalent
		D3D12.RootSignature RootSignature;
		D3D12.PipelineState PipelineState;

		//Commandlists are bound to pipeline states
		D3D12.CommandAllocator CommandAllocator;
		D3D12.GraphicsCommandList EffectCommandList;


		public static new BasicEffect CompileFromFile(GraphicsDevice device, string filepath, VertexLayout layout)
		{
			BasicEffect be = new BasicEffect(device, filepath, layout);
			return be;

		}

		public BasicEffect(GraphicsDevice device, string filepath, VertexLayout layout) : base()
		{
			Graphics = device;


			var rsd = new D3D12.RootSignatureDescription(
				D3D12.RootSignatureFlags.AllowInputAssemblerInputLayout,
				new D3D12.RootParameter[]
				{
				//	new D3D12.RootParameter(
				//		D3D12.ShaderVisibility.Vertex,
				//		new D3D12.DescriptorRange()
				//		{
				//			RangeType = D3D12.DescriptorRangeType.ConstantBufferView,
				//			BaseShaderRegister = 0,
				//			OffsetInDescriptorsFromTableStart = int.MinValue,
				//			DescriptorCount = 1
				//		}),
				//	new D3D12.RootParameter(
				//		D3D12.ShaderVisibility.Pixel,
				//		new D3D12.DescriptorRange()
				//		{
				//			RangeType = D3D12.DescriptorRangeType.ConstantBufferView,
				//			BaseShaderRegister = 0,
				//			OffsetInDescriptorsFromTableStart = int.MinValue,
				//			DescriptorCount = 1
				//		})
				}
			);

			RootSignature = Graphics.D3DDevice.CreateRootSignature(rsd.Serialize());




			D3D.ShaderMacro[] macros = new D3D.ShaderMacro[1];
			//ShaderInclude i;

			var vsbc = ShaderBytecode.CompileFromFile(
				filepath + ".hlsl",
				"VS",
				"vs_4_0",
				ShaderFlags.None,
				EffectFlags.None,
				macros,
				null);
			if (vsbc.Bytecode == null)
			{
				Debug.LogError("Vertex Shader Error", "Error Compiling \"" + filepath + "\"\n" + vsbc.Message);
			}

			VertexShader = new D3D12.ShaderBytecode(vsbc);

			var psbc = ShaderBytecode.CompileFromFile(filepath + ".hlsl", "PS", "ps_4_0");
			if (psbc.Bytecode == null)
			{
				Debug.LogError("Pixel Shader Error", "Error Compiling \"" + filepath + "\"\n" + vsbc.Message);
			}

			PixelShader = new D3D12.ShaderBytecode(psbc);

			var psoDesc = new D3D12.GraphicsPipelineStateDescription()
			{
				InputLayout = new D3D12.InputLayoutDescription(layout.ToD3D12()),
				RootSignature = RootSignature,
				VertexShader = VertexShader,
				PixelShader = PixelShader,
				RasterizerState = D3D12.RasterizerStateDescription.Default(),
				BlendState = D3D12.BlendStateDescription.Default(),
				DepthStencilFormat = DXGI.Format.D32_Float,
				DepthStencilState = new SharpDX.Direct3D12.DepthStencilStateDescription()
				{
					IsDepthEnabled = false,
					IsStencilEnabled = false
				},
				SampleMask = int.MaxValue,
				PrimitiveTopologyType = D3D12.PrimitiveTopologyType.Triangle,
				RenderTargetCount = 0,
				Flags = D3D12.PipelineStateFlags.None,
				SampleDescription = new DXGI.SampleDescription(1, 0),
				StreamOutput = new D3D12.StreamOutputDescription()
			};

			PipelineState = Graphics.D3DDevice.CreateGraphicsPipelineState(psoDesc);

			CommandAllocator = Graphics.D3DDevice.CreateCommandAllocator(D3D12.CommandListType.Direct);
			EffectCommandList = Graphics.D3DDevice.CreateCommandList(
				D3D12.CommandListType.Direct,
				CommandAllocator,
				PipelineState);
		}


		public override void Bind(GraphicsContext context)
		{
			Graphics.D3D12DefaultCommandList.PipelineState = PipelineState;
		}

		public override void Unbind(GraphicsContext context)
		{
			Graphics.D3D12DefaultCommandList.PipelineState = null;
		}

		public override void Dispose()
		{
			EffectCommandList.Dispose();
			CommandAllocator.Dispose();
			PipelineState.Dispose();
		}

		public override int GetTextureLocation(string name)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2D(GraphicsContext context, string name, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2D(GraphicsContext context, int slot, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override int GetUniformLocation(string name)
		{
			throw new NotImplementedException();
		}

		public override void BindUniform<T>(GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			throw new NotImplementedException();
		}

		public override void BindUniform<T>(GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2DArray(GraphicsContext context, string name, ITexture texture)
		{
			throw new NotImplementedException();
		}

		public override void BindTexture2DArray(GraphicsContext context, int slot, ITexture texture)
		{
			throw new NotImplementedException();
		}
	}
}
#endif