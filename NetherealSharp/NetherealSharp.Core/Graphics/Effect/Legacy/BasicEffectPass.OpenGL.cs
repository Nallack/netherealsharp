﻿
#if OPENGL

using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;
using OGL = OpenTK.Graphics.OpenGL;




using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	public class BasicEffectPass2 : Base.BasicEffectPass
	{
		GraphicsDevice m_device;
			
		ShaderProgram ShaderProgram;
		int DiffuseLocation;
		int NormalLocation;

		public BasicEffectPass2(GraphicsDevice device)
		{
			m_device = device;
		}

		public static new BasicEffectPass2 CompileFromFile(GraphicsDevice device, string filepath, VertexLayout layout)
		{
			BasicEffectPass2 be = new BasicEffectPass2(device);

			string profile = be.m_device.GetHighestShaderProfile();

			be.ShaderProgram = new ShaderProgram();
			Shader vertexShader = new Shader(device, OGL.ShaderType.VertexShader);
			vertexShader.CompileFromFile(filepath + ".glsl");
			be.ShaderProgram.AttachShader(vertexShader);

			Shader fragmentShader = new Shader(device, OGL.ShaderType.FragmentShader);
			fragmentShader.CompileFromFile(filepath + ".glsl");
			be.ShaderProgram.AttachShader(fragmentShader);

			be.ShaderProgram.BindVertexLayout(layout);

			be.ShaderProgram.Link();

			be.ShaderProgram.BindUniformBlock("Object", (int)UniformLocation.Object);
			be.ShaderProgram.BindUniformBlock("Camera", (int)UniformLocation.Camera);
			be.ShaderProgram.BindUniformBlock("Light", (int)UniformLocation.Light);
			be.ShaderProgram.BindUniformBlock("Material", (int)UniformLocation.Material);

			be.DiffuseLocation = be.ShaderProgram.GetUniformLocation("DiffuseMap");
			be.NormalLocation = be.ShaderProgram.GetUniformLocation("NormalMap");

			return be;
		}

		public override void Bind(Base.GraphicsContext context)
		{
			ShaderProgram.Bind();
		}

		public override int GetTextureLocation(string name)
		{
			return ShaderProgram.GetUniformLocation(name);
		}

		public override void BindTexture2D(Base.GraphicsContext context, string name, ITexture texture)
		{
			BindTexture2D(context, ShaderProgram.GetUniformLocation(name), texture);
		}

		public override void BindTexture2D(Base.GraphicsContext context, int slot, ITexture texture)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2D, (texture as IGLTexture2D).TextureId);
			GL.Uniform1(slot, slot);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, string name, ITexture texture)
		{
			BindTexture2DArray(context, ShaderProgram.GetUniformLocation(name), texture);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, int slot, ITexture texture)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2DArray, (texture as IGLTexture2D).TextureId);
			GL.Uniform1(slot, slot);
		}

		public override int GetUniformLocation(string name)
		{
			return ShaderProgram.GetUniformLocation(name);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			BindUniform(context, ShaderProgram.GetUniformLocation(name), uniform);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
#if OPENTK1_1
			GL.BindBufferBase(BufferTarget.UniformBuffer, slot, (uniform as UniformBuffer<T>).UboId);
#elif OPENTK2_0
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, slot, (uniform as UniformBuffer<T>).UboId);
#endif
		}

		public override void Unbind(Base.GraphicsContext context)
		{
			ShaderProgram.Unbind();
		}

		public override void Dispose()
		{
			ShaderProgram.Dispose();
		}
	}
}

#endif