﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class ComputeEffect
    {

		public abstract void Bind(GraphicsContext context);

		public abstract void BindUniform<T>(UniformBuffer<T> uniform, int slot) where T : struct;

		public abstract void BindTexture2D(ITexture texture, int slot);

		public abstract void BindAccessBuffer<T>(AccessBuffer<T> buffer, int slot) where T : struct;

		public abstract void Execute();

		public abstract void Unbind();
    }
}
