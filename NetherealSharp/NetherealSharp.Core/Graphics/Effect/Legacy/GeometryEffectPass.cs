﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class GeometryEffectPass : EffectPass
    {

		public static GeometryEffectPass CompileFromFile(GraphicsDevice device, string filename, VertexLayout layout)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
			//case GraphicsImplementation.DirectX11:
				//return DX11.BasicEffect.CompileFromFile(filename, layout);
				//return new DX11.GeometryEffectPass(device as DX11.GraphicsDevice, filename, layout);
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return DX12.GeometryEffectPass.CompileFromFile(filename, layout);
#endif
#if OPENGL
				//case GraphicsImplementation.OpenGL:
					//return OpenGL.GeometryEffectPass.CompileFromFile(device as OpenGL.GraphicsDevice, filename, layout);
#endif
			default:
				throw new NotImplementedException();
			}
		}

	}
}
