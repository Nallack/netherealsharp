﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// Contains effect passes that are intended to be used together.
	/// </summary>
    public abstract class EffectTechnique : IDisposable
    {
		public List<EffectPass> Passes { get; private set; }

		public abstract void Dispose();
    }
}
