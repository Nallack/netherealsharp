﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A complete shader pipeline
	/// </summary>
	[Serializable, DataContract(IsReference = true)]
	public abstract class EffectPass : IContentable, IDisposable
	{
		public string Name { get; set; }
		public Type ContentType { get { return typeof(EffectPass); } }

		/// <summary>
		/// Automatically detect the type of EffectPass and generate it.
		/// TODO: will probably want to incorperate all of the effectpass types into single classes.
		/// </summary>
		/// <returns></returns>
		public static EffectPass CompileFromFile(GraphicsDevice device, string filename, ShaderType flags, VertexLayout layout)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return DX11.EffectPass.CompileFromFile(device as DX11.GraphicsDevice, filename, flags, layout);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return OpenGL.EffectPass.CompileFromFile(device as OpenGL.GraphicsDevice, filename, flags, layout);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Bind(GraphicsContext context);

		public abstract int GetTextureLocation(string name);

		//opengl has to call differently for each type of texture
		//dx11 simply uses the shaderresourceview
		//one option would be to just make a different call for each, or alternativly gl textures
		//store there type and make the decision within an abstraction.
		public abstract void BindTexture2D(GraphicsContext context, string name, ITexture texture);
		public abstract void BindTexture2D(GraphicsContext context, int slot, ITexture texture);

		public abstract void BindTexture2DArray(GraphicsContext context, string name, ITexture texture);
		public abstract void BindTexture2DArray(GraphicsContext context, int slot, ITexture texture);

		public abstract int GetUniformLocation(string name);
		public abstract void BindUniform<T>(GraphicsContext context, string name, UniformBuffer<T> uniform) where T : struct;
		public abstract void BindUniform<T>(GraphicsContext context, int slot, UniformBuffer<T> uniform) where T : struct;

		public abstract void Unbind(GraphicsContext context);
		public abstract void Dispose();
    }
}
