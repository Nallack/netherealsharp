﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[Serializable, DataContract(IsReference = true)]
    public class Effect : IContentable, IDisposable
    {
		public string Name { get; set; }
		public Type ContentType { get { return typeof(EffectPass); } }

		public List<EffectTechnique> Techniques { get; protected set; }
		public EffectTechnique CurrentTechnique { get; set; }

		//Still abstract...
		public Effect() { }

		/// <summary>
		/// Accept a json
		/// </summary>
		/// <param name="filepath"></param>
		public Effect(GraphicsDevice device, string filepath)
		{

		}

		public void Dispose()
		{

		}
    }
}
