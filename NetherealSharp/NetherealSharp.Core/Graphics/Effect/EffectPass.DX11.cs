﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.D3DCompiler;
using D3D11 = SharpDX.Direct3D11;
using D3D = SharpDX.Direct3D;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.Serialization;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Unify all the other implementing versions of this
	/// </summary>
	[Serializable, DataContract(IsReference = true)]
	public abstract class EffectPass : Base.EffectPass
	{
		protected GraphicsDevice Device;

		protected D3D11.VertexShader VertexShader;
		protected D3D11.GeometryShader GeometryShader;
		protected D3D11.PixelShader PixelShader;

		protected ShaderType StageFlags;

		protected ShaderReflection VSReflection;
		protected ShaderReflection GSReflection;
		protected ShaderReflection PSReflection;

		protected D3D11.InputLayout InputLayout;

		/// <summary>
		/// Overrides the base static method
		/// </summary>
		/// <param name="device"></param>
		/// <param name="filename"></param>
		/// <param name="flags"></param>
		/// <param name="layout"></param>
		/// <returns></returns>
		public static new EffectPass CompileFromFile(GraphicsDevice device, string filename, ShaderType flags, VertexLayout layout)
		{
			switch (flags)
			{
				case (ShaderType.Vertex | ShaderType.Pixel):
					return new DX11.BasicEffectPass(device, filename, flags, layout);
				case (ShaderType.Vertex | ShaderType.Geometry | ShaderType.Pixel):
					return new DX11.GeometryEffectPass(device, filename, flags, layout);
				default:
					throw new NotImplementedException();
			}
		}

		public EffectPass(GraphicsDevice device, string filepath, ShaderType flags, VertexLayout layout)
		{
			Device = device;
			StageFlags = flags;
			
			CompileFromFile(filepath, layout);
		}



		protected virtual void CompileFromFile(string filepath, VertexLayout layout)
		{

		}


		protected D3D11.VertexShader CompileVertexShader(string filepath, VertexLayout layout, D3D.ShaderMacro[] macros, ShaderInclude include, out ShaderReflection reflection)
		{
			var vsbc = ShaderBytecode.CompileFromFile(
			filepath + ".hlsl",
			"VS",
			Device.HighestShaderProfile(ShaderVersion.VertexShader),
			ShaderFlags.Debug,
			EffectFlags.None,
			macros,
			include);

			if (vsbc.Bytecode == null) Diagnostics.Debug.LogError("Vertex Shader Error", "Error Compiling \"" + filepath + "\"\n" + vsbc.Message);

			reflection = new ShaderReflection(vsbc);

			InputLayout = new SharpDX.Direct3D11.InputLayout(Device.D3D11Device, vsbc, layout.ToD3D11());

			return new D3D11.VertexShader(Device.D3D11Device, vsbc);
		}

		protected D3D11.GeometryShader CompileGeometryShader(string filepath, D3D.ShaderMacro[] macros, ShaderInclude include, out ShaderReflection reflection)
		{

			var gsbc = ShaderBytecode.CompileFromFile(
				filepath + ".hlsl",
				"GS",
				Device.HighestShaderProfile(ShaderVersion.GeometryShader),
				ShaderFlags.None,
				EffectFlags.None,
				macros,
				null);

			if (gsbc.Bytecode == null)
			{
				Diagnostics.Debug.LogError("Geometry Shader Error", "Error Compiling \"" + filepath + "\"\n" + gsbc.Message);
			}

			reflection = new ShaderReflection(gsbc);

			return new D3D11.GeometryShader(Device.D3D11Device, gsbc);
		}

		protected D3D11.PixelShader CompilePixelShader(string filepath, D3D.ShaderMacro[] macros, ShaderInclude include, out ShaderReflection reflection)
		{
			var psbc = ShaderBytecode.CompileFromFile(
				filepath + ".hlsl",
				"PS",
				Device.HighestShaderProfile(ShaderVersion.PixelShader),
				ShaderFlags.Debug,
				EffectFlags.None,
				macros,
				include);

			if (psbc.Bytecode == null) Diagnostics.Debug.LogError("Pixel Shader Error", "Error Compiling \"" + filepath + "\"\n" + psbc.Message);

			reflection = new ShaderReflection(psbc);

			return new D3D11.PixelShader(Device.D3D11Device, psbc);
		}
	}

	public class BasicEffectPass : EffectPass
	{
		public BasicEffectPass(GraphicsDevice device, string filepath, ShaderType flags, VertexLayout layout)
			: base(device, filepath, flags, layout)
		{
		}



		protected override void CompileFromFile(string filepath, VertexLayout layout)
		{
			D3D.ShaderMacro[] macros = new D3D.ShaderMacro[]
			{
				//ForwardLighting Settings
				new D3D.ShaderMacro("MAXLIGHTS", 3),
				new D3D.ShaderMacro("MAXSHADOWCASCADES", 3),
				new D3D.ShaderMacro("ENABLESHADOWS", 1),
				new D3D.ShaderMacro("ENABLECASCADES", 1),
				new D3D.ShaderMacro("SOFTSHADOWS", 1),
				new D3D.ShaderMacro("SHADOWMAPARRAY", 1),

				//StandardShadows Settings
				new D3D.ShaderMacro("VSM", 1),
				new D3D.ShaderMacro("PCF", 1),
				new D3D.ShaderMacro("BILINEAR", 1),
			};

			//TODO: Custom shader include, may want load from a database at some point
			ShaderInclude include = new ShaderInclude()
			{
				IncludeDirectories = { "Content\\Shaders\\Include" }
			};

			if ((StageFlags & ShaderType.Vertex) != 0) VertexShader = CompileVertexShader(filepath, layout, macros, include, out VSReflection);
			if ((StageFlags & ShaderType.Geometry) != 0) GeometryShader = CompileGeometryShader(filepath, macros, include, out GSReflection);
			if ((StageFlags & ShaderType.Pixel) != 0) PixelShader = CompilePixelShader(filepath, macros, include, out PSReflection);
		}

		public override void Bind(Base.GraphicsContext context)
		{
			var c = (context as DX11.GraphicsContext).D3D11Context;
			c.InputAssembler.InputLayout = InputLayout;
			c.VertexShader.Set(VertexShader);
			c.PixelShader.Set(PixelShader);
		}

		public override void BindTexture2D(Base.GraphicsContext context, int slot, ITexture texture)
		{
			var c = (context as GraphicsContext).D3D11Context;
			if (texture != null)
				c.PixelShader.SetShaderResource(slot, (texture as IDX11Texture).ShaderResourceView);
			else
				c.PixelShader.SetShaderResource(slot, null);
		}

		public override void BindTexture2D(Base.GraphicsContext context, string name, ITexture texture)
		{
			try
			{
				var desc = PSReflection.GetResourceBindingDescription(name);
				BindTexture2D(context, desc.BindPoint, texture);
			}
			catch (SharpDX.SharpDXException e)
			{
				throw new Exception("BasicEffectPass " + Name + "does not have texture named : " + name);
			}
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, int slot, ITexture texture)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.PixelShader.SetShaderResource(slot, (texture as IDX11Texture).ShaderResourceView);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, string name, ITexture texture)
		{
			try
			{
				var desc = PSReflection.GetResourceBindingDescription(name);
				BindTexture2D(context, desc.BindPoint, texture);
			}
			catch (SharpDX.SharpDXException e)
			{
				throw new Exception("BasicEffectPass " + Name + "does not have texture array named : " + name);
			}
		}

		public override void BindUniform<T>(Base.GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.VertexShader.SetConstantBuffer(slot, (uniform as UniformBuffer<T>).Buffer);
			c.PixelShader.SetConstantBuffer(slot, (uniform as UniformBuffer<T>).Buffer);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			var desc = PSReflection.GetResourceBindingDescription(name);
			BindUniform(context, desc.BindPoint, uniform);
		}

		public override int GetTextureLocation(string name)
		{
			var desc = PSReflection.GetResourceBindingDescription(name);
			if (desc.Type == ShaderInputType.Texture) return desc.BindPoint;
			else return -1;
		}

		public override int GetUniformLocation(string name)
		{
			var desc = VSReflection.GetResourceBindingDescription(name);
			if (desc.Type == ShaderInputType.ConstantBuffer) return desc.BindPoint;
			else return -1;
		}

		public override void Unbind(Base.GraphicsContext context)
		{
			var c = (context as DX11.GraphicsContext).D3D11Context;
			c.VertexShader.Set(null);
			c.PixelShader.Set(null);
		}

		public override void Dispose()
		{
			VSReflection.Dispose();
			PSReflection.Dispose();

			VertexShader.Dispose();
			PixelShader.Dispose();

			InputLayout.Dispose();
		}
	}

	public class GeometryEffectPass : EffectPass
	{
		public GeometryEffectPass(GraphicsDevice device, string filepath, ShaderType flags, VertexLayout layout)
			: base(device, filepath, flags, layout)
		{
		}

		protected override void CompileFromFile(string filepath, VertexLayout layout)
		{
			D3D.ShaderMacro[] macros = new D3D.ShaderMacro[]
			{
								//ForwardLighting Settings
				new D3D.ShaderMacro("MAXLIGHTS", 3),
				new D3D.ShaderMacro("MAXSHADOWCASCADES", 3),
				new D3D.ShaderMacro("ENABLESHADOWS", 1),
				new D3D.ShaderMacro("ENABLECASCADES", 1),
				new D3D.ShaderMacro("SOFTSHADOWS", 1),
				new D3D.ShaderMacro("SHADOWMAPARRAY", 1),

				//StandardShadows Settings
				new D3D.ShaderMacro("VSM", 1),
				new D3D.ShaderMacro("PCF", 1),
				new D3D.ShaderMacro("BILINEAR", 1),
			};

			//TODO: Custom shader include, may want load from a database at some point
			ShaderInclude include = new ShaderInclude()
			{
				IncludeDirectories = { "Content\\Shaders\\Include" }
			};

			if ((StageFlags & ShaderType.Vertex) != 0) VertexShader = CompileVertexShader(filepath, layout, macros, include, out VSReflection);
			if ((StageFlags & ShaderType.Geometry) != 0) GeometryShader = CompileGeometryShader(filepath, macros, include, out GSReflection);
			if ((StageFlags & ShaderType.Pixel) != 0) PixelShader = CompilePixelShader(filepath, macros, include, out PSReflection);
		}

		public override void Bind(Base.GraphicsContext context)
		{
			var c = (context as DX11.GraphicsContext).D3D11Context;
			c.InputAssembler.InputLayout = InputLayout;
			c.VertexShader.Set(VertexShader);
			c.GeometryShader.Set(GeometryShader);
			c.PixelShader.Set(PixelShader);
		}


		public override int GetTextureLocation(string name)
		{
			var desc = PSReflection.GetResourceBindingDescription(name);
			if (desc.Type == ShaderInputType.Texture) return desc.BindPoint;
			else return -1;
		}

		public override void BindTexture2D(Base.GraphicsContext context, string name, ITexture texture)
		{
			var desc = PSReflection.GetResourceBindingDescription(name);
			BindTexture2D(context, desc.BindPoint, texture);
		}

		public override void BindTexture2D(Base.GraphicsContext context, int slot, ITexture texture)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.PixelShader.SetShaderResource(slot, (texture as IDX11Texture).ShaderResourceView);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, string name, ITexture texture)
		{
			try
			{
				var desc = PSReflection.GetResourceBindingDescription(name);
				BindTexture2D(context, desc.BindPoint, texture);
			}
			catch (SharpDX.SharpDXException e)
			{
				throw new Exception("BasicEffectPass " + Name + "does not have texture array named : " + name);
			}
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, int slot, ITexture texture)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.PixelShader.SetShaderResource(slot, (texture as IDX11Texture).ShaderResourceView);
		}

		public override int GetUniformLocation(string name)
		{
			var desc = VSReflection.GetResourceBindingDescription(name);
			if (desc.Type == ShaderInputType.ConstantBuffer) return desc.BindPoint;
			else return -1;

		}

		public override void BindUniform<T>(Base.GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			var desc = PSReflection.GetResourceBindingDescription(name);
			BindUniform(context, desc.BindPoint, uniform);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.VertexShader.SetConstantBuffer(slot, (uniform as UniformBuffer<T>).Buffer);
			c.PixelShader.SetConstantBuffer(slot, (uniform as UniformBuffer<T>).Buffer);
		}

		public override void Unbind(Base.GraphicsContext context)
		{
			var c = (context as DX11.GraphicsContext).D3D11Context;
			c.VertexShader.Set(null);
			c.GeometryShader.Set(null);
			c.PixelShader.Set(null);
		}

		public override void Dispose()
		{
			VertexShader.Dispose();
			PixelShader.Dispose();
			InputLayout.Dispose();
		}
	}
}
#endif