﻿
#if OPENGLES

using System;
using System.Collections.Generic;
using System.Text;

#if OPENGLES_31
using OpenTK.Graphics.ES31;
#elif OPENGLES_30
using TK = OpenTK.Graphics.ES30;
using OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
#endif


using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGLES
{
	public abstract class EffectPass : Base.EffectPass
	{
		protected GraphicsDevice GraphicsDevice;
		protected ShaderProgram ShaderProgram;
		Shader VertexShader;
		Shader GeometryShader;
		Shader FragmentShader;

		protected ShaderType StageFlags;

		Dictionary<string, int> ShaderReflection;

		public static EffectPass CompileFromFile(GraphicsDevice device, string filename, ShaderType flags, VertexLayout layout)
		{

			return new CustomEffectPass(device, filename, flags, layout);
			/*
			switch(flags)
			{
				case (ShaderType.Vertex | ShaderType.Pixel):
					return new OpenGL.BasicEffectPass(device, filename, flags, layout);
				case (ShaderType.Vertex | ShaderType.Geometry | ShaderType.Pixel):
					return new OpenGL.GeometryEffectPass(device, filename, flags, layout);
				default:
					throw new NotImplementedException();
			}*/
		}

		public EffectPass(GraphicsDevice device, string filepath, ShaderType flags, VertexLayout layout)
		{
			GraphicsDevice = device;
			StageFlags = flags;

			CompileFromFile(filepath, layout);
		}



		protected abstract void CompileFromFile(string filepath, VertexLayout layout);

	}

	public class CustomEffectPass : EffectPass
	{
		public CustomEffectPass(GraphicsDevice device, string filepath, ShaderType flags, VertexLayout layout)
			: base(device, filepath, flags, layout)
		{
		}


		protected override void CompileFromFile(string filepath, VertexLayout layout)
		{
			string profile = GraphicsDevice.GetHighestShaderProfile();

			ShaderProgram = new ShaderProgram();

			if ((StageFlags & ShaderType.Vertex) != 0)
			{
				Shader VertexShader = new Shader(GraphicsDevice, TK.ShaderType.VertexShader);
				VertexShader.CompileFromFile(filepath + ".glsl");
				ShaderProgram.AttachShader(VertexShader);
			}

			if ((StageFlags & ShaderType.Pixel) != 0)
			{
				Shader FragmentShader = new Shader(GraphicsDevice, TK.ShaderType.FragmentShader);
				FragmentShader.CompileFromFile(filepath + ".glsl");
				ShaderProgram.AttachShader(FragmentShader);
			}

			ShaderProgram.BindVertexLayout(layout);

			ShaderProgram.Link();

			//TODO: alternative method would be better, perhaps using shader reflection
			ShaderProgram.BindUniformBlock("Object", (int)UniformLocation.Object);
			ShaderProgram.BindUniformBlock("Camera", (int)UniformLocation.Camera);
			ShaderProgram.BindUniformBlock("Light", (int)UniformLocation.Light);
			ShaderProgram.BindUniformBlock("Material", (int)UniformLocation.Material);

			//DiffuseLocation = be.ShaderProgram.GetUniformLocation("DiffuseMap");
			//NormalLocation = be.ShaderProgram.GetUniformLocation("NormalMap");
		}

		public override void Bind(Base.GraphicsContext context)
		{
			ShaderProgram.Bind();
		}

		public override int GetTextureLocation(string name)
		{
			return ShaderProgram.GetUniformLocation(name);
		}

		public override void BindTexture2D(Base.GraphicsContext context, string name, ITexture texture)
		{
			BindTexture2D(context, ShaderProgram.GetUniformLocation(name), texture);
		}

		public override void BindTexture2D(Base.GraphicsContext context, int slot, ITexture texture)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2D, (texture as IGLTexture2D).TextureId);
			GL.Uniform1(slot, slot);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, string name, ITexture texture)
		{
			BindTexture2DArray(context, ShaderProgram.GetUniformLocation(name), texture);
		}

		public override void BindTexture2DArray(Base.GraphicsContext context, int slot, ITexture texture)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2DArray, (texture as IGLTexture2D).TextureId);
			GL.Uniform1(slot, slot);
		}

		public override int GetUniformLocation(string name)
		{
			return ShaderProgram.GetUniformLocation(name);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, string name, Base.UniformBuffer<T> uniform)
		{
			BindUniform(context, ShaderProgram.GetUniformLocation(name), uniform);
		}

		public override void BindUniform<T>(Base.GraphicsContext context, int slot, Base.UniformBuffer<T> uniform)
		{
#if OPENTK1_1
			GL.BindBufferBase(BufferTarget.UniformBuffer, slot, (uniform as UniformBuffer<T>).UboId);
#elif OPENTK2_0
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, slot, (uniform as UniformBuffer<T>).UboId);
#else
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, slot, (uniform as UniformBuffer<T>).UboId);
#endif
		}

		public override void Unbind(Base.GraphicsContext context)
		{
			ShaderProgram.Unbind();
		}

		public override void Dispose()
		{
			ShaderProgram.Dispose();
		}

	}
}

#endif