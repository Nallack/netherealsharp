﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Model
{

	/// <summary>
	/// Part of a modelmesh that uses a single material
	/// </summary>
	public class ModelMeshPart : IDisposable
	{
		private EffectPass s_Effect;
		public EffectPass Effect { get { return s_Effect; } }

		public IndexedMesh Mesh { get; }
		
		public void Draw() {}
		
		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
