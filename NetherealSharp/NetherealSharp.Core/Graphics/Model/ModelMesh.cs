﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Model
{
	/// <summary>
	/// A collection of mesh parts that are drawn together with the same effect.
	/// </summary>
    public class ModelMesh
    {
		public ModelMaterial Material { get; }
		//public List<Effect> Effects { get; } this is covered by material
		
		public List<ModelMeshPart> Parts { get; }
		public IndexedMesh Mesh { get; }

    }
}
