﻿#if ASSIMP
using System;
using System.Collections.Generic;
using System.Text;

using Assimp;
using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Model
{
	public class Model : IDisposable
	{

		public List<ModelMesh> m_Meshes;
		public List<ModelMeshBone> m_Bones;
		public List<ModelMaterial> m_Materials;

		public Model(string filepath)
		{
			m_Materials = new List<ModelMaterial>();

			//ContentManager.Importer.Context.SetConfig(new Assimp.Configs.IFCUseCustomTriangulationConfig())
			Scene scene = ContentManager.Importer.Context.ImportFile(filepath, PostProcessSteps.Triangulate);

			//Materials
			if(scene.HasMaterials)
			{
				foreach(var material in scene.Materials)
				{
					ModelMaterial m = new ModelMaterial();

					//load textures
					if (material.HasTextureAmbient) m.Ambient = ContentManager.GetTexture2D(material.TextureAmbient.FilePath);
					if (material.HasTextureDiffuse) m.Diffuse = ContentManager.GetTexture2D(material.TextureDiffuse.FilePath);
					//if (material.HasTextureSpecular) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);

					if (material.HasTextureNormal) m.Normal = Texture2D.LoadFromFile(GraphicsManager.GraphicsDevice, material.TextureNormal.FilePath);
					//if (material.HasTextureDisplacement) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);
					//if (material.HasTextureHeight) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);

					//if (material.HasTextureReflection) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);
					//if (material.HasTextureOpacity) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);
					//if (material.HasTextureEmissive) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);
					//if (material.HasTextureLightMap) m.Ambient = Texture2D.LoadFromFile(material.TextureAmbient.FilePath);

					//load Effect
					m.Effect = ContentManager.Get<Effect>("Phong");

					m_Materials.Add(m);
				}
			}

			//meshes
			foreach(var mesh in scene.Meshes)
			{
				ModelMesh m = new ModelMesh();

				//load meshes
				if (mesh.HasVertices && mesh.HasNormals && mesh.HasTextureCoords(0) && !mesh.HasBones)
				{
					List<VertexPNT> vertices = new List<VertexPNT>(mesh.VertexCount);
					for (int i = 0; i < mesh.VertexCount; i++) vertices.Add(
						new VertexPNT(
							mesh.Vertices[i],
							mesh.Normals[i],
							mesh.TextureCoordinateChannels[0][i]));

					List<uint> indices = new List<uint>(mesh.FaceCount);
					indices.AddRange(mesh.GetUnsignedIndices());

					m.Mesh = IndexedMesh.CreateFromData(
						GraphicsManager.GraphicsDevice,
						new IndexedMeshData<VertexPNT>(VertexPNT.Layout, vertices, indices), BufferUsage.Static);
				}
				else if(mesh.HasVertices && mesh.HasNormals && mesh.HasTextureCoords(0) && mesh.HasBones)
				{
					throw new NotImplementedException();
				}
				else
				{
					throw new NotImplementedException();
				}

				m.Material = m_Materials[mesh.MaterialIndex];

				m_Meshes.Add(m);
			}

		}


		public void Draw()
		{
			foreach(var mesh in m_Meshes)
			{
				foreach(var part in mesh.Parts)
				{
					part.Effect.Bind();

				}
			}
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
#endif