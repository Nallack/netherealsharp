﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Model
{
    public class ModelMaterial
    {
		public EffectPass Effect;

		public Texture2D Ambient;
		public Texture2D Diffuse;
		public Texture2D Normal;
    }
}
