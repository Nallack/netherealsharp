﻿using System;
using System.Collections.Generic;
using System.Text;

#if DIRECTX11
using SharpDX.Direct3D;
#endif
#if OPENGL
using OpenTK.Graphics.OpenGL;
#endif
#if OPENGLES
#if OPENGLES_30
using OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.ES20;
#endif
#endif

namespace NetherealSharp.Core.Graphics
{
	public enum PrimitiveMode
	{
		TriangleList,
		LineList,
		PointList,
		TriangleStrip,
		LineStrip,
	}

	public static class PrimitiveModeConversion
	{
#if DIRECTX11
		public static PrimitiveTopology ToD3D(this PrimitiveMode value)
		{
			switch(value)
			{
				case PrimitiveMode.PointList: return PrimitiveTopology.PointList;
				case PrimitiveMode.LineList: return PrimitiveTopology.LineList;
				case PrimitiveMode.TriangleList: return PrimitiveTopology.TriangleList;
				case PrimitiveMode.LineStrip: return PrimitiveTopology.LineStrip;
				case PrimitiveMode.TriangleStrip: return PrimitiveTopology.TriangleStrip;
				default: return PrimitiveTopology.Undefined;
				
			}
		}
#endif
#if OPENGL
#if OPENTK1_1
		public static BeginMode ToGL(this PrimitiveMode value)
		{
			switch (value)
			{
				case PrimitiveMode.PointList: return BeginMode.Points;
				case PrimitiveMode.LineList: return BeginMode.Lines;
				case PrimitiveMode.TriangleList: return BeginMode.Triangles;
				case PrimitiveMode.LineStrip: return BeginMode.LineStrip;
				case PrimitiveMode.TriangleStrip: return BeginMode.TriangleStrip;
				default: return 0;

			}
		}
#elif OPENTK2_0
		public static PrimitiveType ToGL(this PrimitiveMode value)
		{
			switch (value)
			{
				case PrimitiveMode.PointList: return PrimitiveType.Points;
				case PrimitiveMode.LineList: return PrimitiveType.Lines;
				case PrimitiveMode.TriangleList: return PrimitiveType.Triangles;
				case PrimitiveMode.LineStrip: return PrimitiveType.LineStrip;
				case PrimitiveMode.TriangleStrip: return PrimitiveType.TriangleStrip;
				default: return 0;

			}
		}
#endif
#endif
#if OPENGLES
		public static All ToGLES(this PrimitiveMode value)
		{
			switch(value)
			{
				case PrimitiveMode.PointList: return All.Points;
				case PrimitiveMode.LineList: return All.Lines;
				case PrimitiveMode.TriangleList: return All.Triangles;
				case PrimitiveMode.LineStrip: return All.LineStrip;
				case PrimitiveMode.TriangleStrip: return All.TriangleStrip;
				default: return 0;
			}
		}
#endif
	}
}