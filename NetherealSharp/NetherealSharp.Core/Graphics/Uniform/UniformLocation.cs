﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public enum UniformLocation
    {
		Object = 0,
		Camera = 1,
		Light = 2,
		Material = 3
    }
}
