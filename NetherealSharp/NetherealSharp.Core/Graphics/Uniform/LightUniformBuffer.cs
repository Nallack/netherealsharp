﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// Custom uniform buffer to support arbitrary arrays.
	/// </summary>
    public abstract class LightUniformBuffer : UniformBuffer<LightData>
    {
		/// <summary>
		/// Conveniently placed here for organisation
		/// </summary>
		/// //Shader cant dynamically select texture from array
		public List<ITexture> ShadowMaps { get; set; }
		
		//public ITexture2D ShadowMapArray { get; set; }


		public static LightUniformBuffer Create(GraphicsDevice device, int maxlights)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.LightUniformBuffer(device as DX11.GraphicsDevice, 3, maxlights);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.LightUniformBuffer(3, maxlights);
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Vulkan.LightUniformBuffer(device as Vulkan.GraphicsDevice, 3, maxlights);
#endif
				default:
					throw new NotImplementedException();
			}
		}
    }
}
