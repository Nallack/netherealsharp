﻿#if DIRECTX12
using System;
using System.Collections.Generic;
using System.Text;

using D3D12 = SharpDX.Direct3D12;
using Base = NetherealSharp.Core.Graphics;
using System.Runtime.InteropServices;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class UniformBuffer<T> : Base.UniformBuffer<T> where T : struct
	{
		GraphicsDevice Device;

		//D3D12.RootSignature Signature;
		D3D12.Resource ConstantBuffer;
		IntPtr ConstantBufferPointer;

		public UniformBuffer(GraphicsDevice device)
		{
			Device = device;

			ConstantBuffer = Device.D3DDevice.CreateCommittedResource(
				new D3D12.HeapProperties(D3D12.HeapType.Upload),
				D3D12.HeapFlags.None,
				D3D12.ResourceDescription.Buffer(Marshal.SizeOf<T>()),
				D3D12.ResourceStates.GenericRead);

			var cbvd = new D3D12.ConstantBufferViewDescription()
			{
				BufferLocation = ConstantBuffer.GPUVirtualAddress,
				SizeInBytes = (Marshal.SizeOf<T>() + 255) & ~255
			};

			Device.D3DDevice.CreateConstantBufferView(cbvd, Device.CBDescriptorHeap.CPUDescriptorHandleForHeapStart);

		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			throw new NotImplementedException();
		}

		public override void Update()
		{
			ConstantBufferPointer = ConstantBuffer.Map(0);
			SharpDX.Utilities.Write(ConstantBufferPointer, ref Data);
		}

		public override void Dispose()
		{
			ConstantBuffer.Dispose();
		}
	}
}
#endif