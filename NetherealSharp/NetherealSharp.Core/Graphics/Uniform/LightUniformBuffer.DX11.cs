﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;


namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Uniform buffer used in the forward rendering pipeline. Stores
	/// an array of light uniforms and array of shadowmap transforms to texturespace.
	/// </summary>
    public class LightUniformBuffer : Base.LightUniformBuffer
    {
		private D3D11.Device Device;
		private D3D11.Buffer ConstantBuffer;
		private DataBuffer m_DataBuffer;

		private int m_cascades;

		public LightUniformBuffer(GraphicsDevice device, int cascades, int maxlights)
		{
			Device = device.D3D11Device;
			m_cascades = cascades;
			Data = new LightData(maxlights, cascades);
			ShadowMaps = new List<ITexture>(maxlights * cascades);

			//ShadowmMapArray = new RenderTexture2DArray(device, 1024, 1024, maxlights * cascades);

			ConstantBuffer = new D3D11.Buffer(
				Device,
				new D3D11.BufferDescription(
					Data.Stride,
					D3D11.ResourceUsage.Default,
					D3D11.BindFlags.ConstantBuffer,
					D3D11.CpuAccessFlags.None,
					D3D11.ResourceOptionFlags.None,
					Data.Stride
				));

			m_DataBuffer = new DataBuffer(Data.Stride);
		}

		public override void Update()
		{
			m_DataBuffer.Clear();

			//Copy Ambeint Light Source data
			int offset = 0;
			m_DataBuffer.Set(offset, Data.Ambient);


			//Copy Amorphic Light Source Info
			offset = AmbientLightData.Stride;

			//WARNING: Lights count must not exceed the maximum, as set does unsafe operations
			for (int i = 0; i < Math.Min(Data.Lights.Count, Data.MaxLights); i++)
			{
				m_DataBuffer.Set(offset + i * AmorphicLightData.Stride, Data.Lights[i]);
			}


			//Copy light source shadow transforms in seperate array
			offset = AmbientLightData.Stride + Data.MaxLights * AmorphicLightData.Stride;
			for(int i = 0; i < Data.MaxLights * Data.Cascades; i++)
			{
				if (Data.ShadowTransforms.Count > i)
					m_DataBuffer.Set(offset + i * 64, Data.ShadowTransforms[i]);
				else
					m_DataBuffer.Set(offset + i * 64, Matrix.Identity);
			}


			Device.ImmediateContext.UpdateSubresource(
				ConstantBuffer, 0, null, m_DataBuffer.DataPointer, 0, 0);
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			var c = (context as GraphicsContext).D3D11Context;

			c.VertexShader.SetConstantBuffer(index, ConstantBuffer);
			c.PixelShader.SetConstantBuffer(index, ConstantBuffer);
		}

		public override void Dispose()
		{
			m_DataBuffer.Dispose();
			ConstantBuffer.Dispose();
			//ShadowmMapArray.Dispose();
		}
	}
}
#endif