﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class ShadowUniformBuffer : UniformBuffer<ShadowData>
    {
		public static ShadowUniformBuffer Create(GraphicsDevice device, int maxCascades)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.ShadowUniformBuffer(device as DX11.GraphicsDevice, maxCascades);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.ShadowUniformBuffer(maxCascades);
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Vulkan.ShadowUniformBuffer(maxCascades);
#endif
				default:
					throw new NotImplementedException();
			}
		}
    }
}
