﻿
#if OPENGLES

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

#if OPENGLES_31
using OpenTK.Graphics.ES31;
#elif OPENGLES_30
using OpenTK.Graphics.ES30;
using OGL = OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.ES20;
#endif

using Cloo;

using Base = NetherealSharp.Core.Graphics;


namespace NetherealSharp.Core.Graphics.OpenGLES
{
    public class UniformBuffer<T> : Base.UniformBuffer<T> where T : struct
	{
		public int UboId;
		//public Cloo.ComputeBuffer<T> clBuffer;

		public UniformBuffer()
		{
			GL.GenBuffers(1, out UboId);
			//clBuffer = new ComputeBuffer<T>()
			//clBuffer = Cl.CreateBuffer()
		}

		public override void Update()
		{
			GL.BindBuffer(BufferTarget.UniformBuffer, UboId);

			GL.BufferData<T>(
				BufferTarget.UniformBuffer,
				(IntPtr)Marshal.SizeOf(Data),
				ref Data,
				OGL.BufferUsage.DynamicDraw);

			GL.BindBuffer(BufferTarget.UniformBuffer, 0);

			GL.Flush();
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, index, UboId);
		}

		public override void Dispose()
		{
			GL.DeleteBuffers(1, ref UboId);
		}

	}
}

#endif
