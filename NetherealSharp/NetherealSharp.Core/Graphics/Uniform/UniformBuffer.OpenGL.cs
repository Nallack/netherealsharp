﻿
#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using OpenTK.Graphics.OpenGL;

using Cloo;

using Base = NetherealSharp.Core.Graphics;


namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class UniformBuffer<T> : Base.UniformBuffer<T> where T : struct
	{
		public int UboId;
		//public Cloo.ComputeBuffer<T> clBuffer;

		public UniformBuffer()
		{
			//UboId = GL.GenBuffer();
			GL.GenBuffers(1, out UboId);

			//clBuffer = new ComputeBuffer<T>()

			//clBuffer = Cl.CreateBuffer()
		}

		public override void Update()
		{
			GL.BindBuffer(BufferTarget.UniformBuffer, UboId);

			GL.BufferData<T>(
				BufferTarget.UniformBuffer,
				(IntPtr)Marshal.SizeOf(Data),
				ref Data,
				BufferUsageHint.DynamicDraw);

			GL.BindBuffer(BufferTarget.UniformBuffer, 0);

			GL.Flush();
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
#if OPENTK1_1
			GL.BindBufferBase(BufferTarget.UniformBuffer, index, UboId);
#elif OPENTK2_0
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, index, UboId);
#endif
		}

		public override void Dispose()
		{
			GL.DeleteBuffer(UboId);
		}

	}
}

#endif
