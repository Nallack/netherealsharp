﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	/// <summary>
	/// A uniform buffer that extends from the LightData struct, which contains
	/// a dynamic array of light sources. This is for forward rendering with
	/// multiple light sources.
	/// </summary>
	public class LightUniformBuffer : Base.LightUniformBuffer
	{
		private int m_UboId;

		private SharpDX.DataBuffer m_DataBuffer;

		public LightUniformBuffer(int cascades, int maxlights)
		{
			Data = new LightData(maxlights, cascades);
			ShadowMaps = new List<ITexture>(maxlights * cascades);

			m_UboId = GL.GenBuffer();

			m_DataBuffer = new SharpDX.DataBuffer(Data.Stride);
		}

		public override void Update()
		{
			m_DataBuffer.Clear();

			//Copy Ambeint Light Source data
			int offset = 0;
			m_DataBuffer.Set(offset, Data.Ambient);


			//Copy Amorphic Light Source Info
			offset = AmbientLightData.Stride;

			//WARNING: Lights count must not exceed the maximum, as set does unsafe operations
			for (int i = 0; i < Math.Min(Data.Lights.Count, Data.MaxLights); i++)
			{
				m_DataBuffer.Set(offset + i * AmorphicLightData.Stride, Data.Lights[i]);
			}

			//Copy light source shadow transforms in seperate array
			offset = AmbientLightData.Stride + Data.MaxLights * AmorphicLightData.Stride;
			for (int i = 0; i < Data.MaxLights * Data.Cascades; i++)
			{
				if (Data.ShadowTransforms.Count > i)
					m_DataBuffer.Set(offset + i * 64, Data.ShadowTransforms[i]);
				else
					m_DataBuffer.Set(offset + i * 64, Matrix.Identity);
			}

			GL.BindBuffer(BufferTarget.UniformBuffer, m_UboId);
			GL.BufferData(
				BufferTarget.UniformBuffer,
				(IntPtr)m_DataBuffer.Size,
				m_DataBuffer.DataPointer,
				BufferUsageHint.DynamicDraw);
			GL.BindBuffer(BufferTarget.UniformBuffer, 0);
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
#if OPENTK1_1
			GL.BindBufferBase(BufferTarget.UniformBuffer, index, m_UboId);
#elif OPENTK2_0
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, index, m_UboId);
#endif
		}

		public override void Dispose()
		{
			GL.DeleteBuffer(m_UboId);
		}
	}
}

#endif