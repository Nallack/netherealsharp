﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;


namespace NetherealSharp.Core.Graphics
{
	public abstract class UniformBuffer : IDisposable
	{
		public abstract void Update();
		public abstract void Bind(GraphicsContext context, int index);
		public abstract void Dispose();
	}

	/// <summary>
	/// Generic overload of a uniformbuffer, subclasses should automatically bind the Data property to the GPU
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public abstract class UniformBuffer<T> : UniformBuffer where T : struct
	{
		public T Data;

		public static UniformBuffer<T> Create(GraphicsDevice device)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.UniformBuffer<T>(device as DX11.GraphicsDevice);
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return new DX12.UniformBuffer<T>(device as DX12.GraphicsDevice);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.UniformBuffer<T>();
#endif
#if OPENGLES
				case GraphicsImplementation.OpenGLES:
					return new OpenGLES.UniformBuffer<T>();
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Vulkan.UniformBuffer<T>();
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
