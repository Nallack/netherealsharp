﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	public struct CameraData
    {
		public Matrix View;
		public Matrix Project;
		public Matrix ViewProj;

		public Matrix Screen;

		public Vector3 Eye;
		float pad;
    }
}
