﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	public struct SpriteData
    {
		public Matrix World;
		public Matrix WorldViewProj;

		public int ArrayIndex;
		public Vector3 pad;
	}
}
