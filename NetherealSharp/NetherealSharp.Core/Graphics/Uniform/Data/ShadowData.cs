﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	//Data used by the shadow renderer, this is a collection of
	//data for a single light source, but variable number of cascades.
    public struct ShadowData
    {
		public int MaxCascades;
		public int Stride { get; private set; }

		public Matrix[] ViewProj { get; private set; }

		public ShadowData(int maxcascades)
		{
			MaxCascades = maxcascades;
			Stride = MaxCascades * Matrix.SizeInBytes;

			ViewProj = new Matrix[MaxCascades];
		}
    }
}
