﻿
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	public struct AmbientLightData
	{
		public Vector4 Down;
		public Vector4 Range;

		public const int Stride = 32;
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct DirectionalLightData
	{
		public Vector4 Color;
		public Vector3 Direction;
		public float Intensity;

		public int MatrixIndex;
		public int MapIndex;

		public static implicit operator AmorphicLightData(DirectionalLightData value)
		{
			return new AmorphicLightData()
			{
				Type = (int)LightType.Directional,
				Color = value.Color,
				Direction = value.Direction,
				Intensity = value.Intensity,
				MatrixIndex = value.MatrixIndex,
				MapIndex = value.MapIndex
			};
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct PointLightData
	{
		public Vector4 Color;
		public Vector3 Position;
		public float Intensity;

		public static implicit operator AmorphicLightData(PointLightData value)
		{
			return new AmorphicLightData()
			{
				Type = (int)LightType.Point,
				Color = value.Color,
				Position = value.Position,
				Intensity = value.Intensity
			};
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct SpotLightData
	{
		public Vector4 Color;
		public Vector3 Position;
		public Vector3 Direction;
		public Vector3 ThetaPhiGamma;
		public float Intensity;


		public const int Type = 4;

		public static implicit operator AmorphicLightData(SpotLightData value)
		{
			return new AmorphicLightData()
			{
				Type = 3,
				Color = value.Color,
				Position = value.Position,
				Direction = value.Direction,
				Intensity = value.Intensity,
				ThetaPhiGamma = value.ThetaPhiGamma
			};
		}
	}

	//[StructLayout(LayoutKind.Sequential, Pack = 16)]
	public struct AmorphicLightData
	{
		public int Enabled;
		public int ShadowEnabled;
		public int MatrixIndex;
		public int MapIndex;
		
		public Vector3 Position;
		public float Type;

		public Vector3 Direction;
		float pad1;

		public Vector3 ThetaPhiGamma;
		public float Intensity;

		public Vector4 Color;

		public const int Stride = 80;
	}

	/// <summary>
	/// A dynamic light data structure (make sure to write a custom writer)
	/// </summary>
	public struct LightData
	{
		public int MaxLights;
		public int Cascades;
		public int Stride;

		public AmbientLightData Ambient { get; set; }
		public List<AmorphicLightData> Lights { get; private set; }
		public List<Matrix> ShadowTransforms { get; private set; }

		public LightData(int maxlights, int cascades)
		{
			MaxLights = maxlights;
			Cascades = cascades;

			Stride = AmbientLightData.Stride + maxlights * (AmorphicLightData.Stride + cascades * Matrix.SizeInBytes);

			Ambient = new AmbientLightData() { Down = Color.Black.ToVector4() };
			Lights = new List<AmorphicLightData>(MaxLights);
			ShadowTransforms = new List<Matrix>(MaxLights * Cascades);
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct SimpleLightData
	{
		public int MaxLights;
		public int Stride;

		public AmbientLightData Ambient;
		public DirectionalLightData[] DirectionalLights;
		public PointLightData[] PointLights;
		public SpotLightData[] SpotLights;

		public SimpleLightData(int maxlights)
		{
			MaxLights = maxlights;
			Stride = 1;

			Ambient = new AmbientLightData();
			DirectionalLights = new DirectionalLightData[MaxLights];
			PointLights = new PointLightData[MaxLights];
			SpotLights = new SpotLightData[MaxLights];
		}
	}
}
