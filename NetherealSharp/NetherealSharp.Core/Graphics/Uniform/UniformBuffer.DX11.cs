﻿
#if DIRECTX11

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;


namespace NetherealSharp.Core.Graphics.DX11
{
    public class UniformBuffer<T> : Base.UniformBuffer<T> where T : struct
	{
		protected D3D11.Device Device;
		internal D3D11.Buffer Buffer;

		public UniformBuffer(GraphicsDevice device)
		{
			Device = device.D3D11Device;

			Buffer = D3D11.Buffer.Create<T>(
				Device,
				D3D11.BindFlags.ConstantBuffer,
				ref Data,
				Marshal.SizeOf(typeof(T)));

			/*
			ConstantBuffer = new D3D11.Buffer(
				GraphicsManager.D3DDevice,
				stride,
				D3D11.ResourceUsage.Default,
				D3D11.BindFlags.ConstantBuffer,
				D3D11.CpuAccessFlags.None,
				D3D11.ResourceOptionFlags.None,
				stride);
				*/
		}

		public override void Update()
		{
			Device.ImmediateContext.UpdateSubresource<T>(ref Data, Buffer);
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			var c = (context as GraphicsContext).D3D11Context;

			c.VertexShader.SetConstantBuffer(index, Buffer);
			c.PixelShader.SetConstantBuffer(index, Buffer);
		}

		public override void Dispose()
		{
			Buffer.Dispose();
		}
	}
}

#endif
