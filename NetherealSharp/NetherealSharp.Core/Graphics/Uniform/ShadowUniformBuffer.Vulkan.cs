﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{
	public class ShadowUniformBuffer : Base.ShadowUniformBuffer
	{
		private int maxCascades;

		public ShadowUniformBuffer(int maxCascades)
		{
			this.maxCascades = maxCascades;
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override void Update()
		{
			throw new NotImplementedException();
		}
	}
}
#endif