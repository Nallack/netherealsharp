﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	public class ShadowUniformBuffer : Base.ShadowUniformBuffer
	{
		private D3D11.Device Device;
		private D3D11.Buffer ConstantBuffer;
		private SharpDX.DataBuffer m_DataBuffer;

		private int m_MaxCascades;

		public ShadowUniformBuffer(GraphicsDevice device, int maxCascades)
		{
			Device = device.D3D11Device;
			m_MaxCascades = maxCascades;
			Data = new ShadowData(maxCascades);

			ConstantBuffer = new D3D11.Buffer(
				Device,
				new D3D11.BufferDescription(
					Data.Stride,
					D3D11.ResourceUsage.Default,
					D3D11.BindFlags.ConstantBuffer,
					D3D11.CpuAccessFlags.None,
					D3D11.ResourceOptionFlags.None,
					Data.Stride));

			m_DataBuffer = new SharpDX.DataBuffer(Data.Stride);
		}

		public override void Update()
		{
			m_DataBuffer.Clear();

			int offset = 0;
			for(int i = 0; i < Data.ViewProj.Length; i++)
			{
				m_DataBuffer.Set(offset + i * 64, Data.ViewProj[i]);
			}

			Device.ImmediateContext.UpdateSubresource(ConstantBuffer, 0, null, m_DataBuffer.DataPointer, 0, 0);
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			var c = (context as GraphicsContext).D3D11Context;

			c.VertexShader.SetConstantBuffer(index, ConstantBuffer);
			c.GeometryShader.SetConstantBuffer(index, ConstantBuffer);
			c.PixelShader.SetConstantBuffer(index, ConstantBuffer);
		}

		public override void Dispose()
		{
			m_DataBuffer.Dispose();
			ConstantBuffer.Dispose();
		}


	}
}
#endif