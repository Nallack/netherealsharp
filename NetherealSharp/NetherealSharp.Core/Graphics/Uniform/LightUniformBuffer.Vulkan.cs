﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{
	public class LightUniformBuffer : Base.LightUniformBuffer
	{
		private int maxlights;
		private int cascades;

		public LightUniformBuffer(GraphicsDevice device, int cascades, int maxlights)
		{
			this.cascades = cascades;
			this.maxlights = maxlights;

			Data = new LightData(maxlights, cascades);
			ShadowMaps = new List<ITexture>();
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override void Update()
		{
			throw new NotImplementedException();
		}
	}
}

#endif