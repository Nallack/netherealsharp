﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class ShadowUniformBuffer : Base.ShadowUniformBuffer
    {
		private int m_UboId;

		private SharpDX.DataBuffer m_DataBuffer;
		private int m_MaxCascades;

		public ShadowUniformBuffer(int maxCascades)
		{
			m_MaxCascades = maxCascades;
			Data = new ShadowData(maxCascades);

			m_UboId = GL.GenBuffer();
			m_DataBuffer = new SharpDX.DataBuffer(Data.Stride);
		}

		public override void Update()
		{
			m_DataBuffer.Clear();

			int offset = 0;
			for (int i = 0; i < Data.ViewProj.Length; i++)
			{
				m_DataBuffer.Set(offset + i * 64, Data.ViewProj[i]);
			}

			GL.BindBuffer(BufferTarget.UniformBuffer, m_UboId);
			GL.BufferData(
				BufferTarget.UniformBuffer,
				(IntPtr)m_DataBuffer.Size,
				m_DataBuffer.DataPointer,
				BufferUsageHint.DynamicDraw);
			GL.BindBuffer(BufferTarget.UniformBuffer, 0);
		}

		public override void Bind(Base.GraphicsContext context, int index)
		{
#if OPENTK1_1
			GL.BindBufferBase(BufferTarget.UniformBuffer, index, m_UboId);
#elif OPENTK2_0
			GL.BindBufferBase(BufferRangeTarget.UniformBuffer, index, m_UboId);
#endif
		}

		public override void Dispose()
		{
			GL.DeleteBuffer(m_UboId);
		}
	}
}
#endif