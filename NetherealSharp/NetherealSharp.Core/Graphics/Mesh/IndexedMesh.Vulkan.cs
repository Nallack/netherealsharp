﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using Vk = VulkanSharp;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{
	public class IndexedMesh : Base.IndexedMesh
	{
		protected Vk.PhysicalDevice Device;

		protected Vk.Buffer VertexBuffer;
		protected Vk.Buffer IndexBuffer;

		protected IndexedMesh() { }

		public IndexedMesh(GraphicsDevice device, VertexLayout layout, BufferUsage usage)
		{

		}

		public static IndexedMesh CreateFromData<T>(GraphicsDevice device, IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			var mesh = new IndexedMesh();
			mesh.Device = device.PhysicalDevice;

			var size = data.Layout.Size * data.Vertices.Count;

			mesh.VertexBuffer = CreateBuffer<T>(device, data.Vertices.ToArray(), Vk.BufferUsageFlags.VertexBuffer);
			mesh.IndexBuffer = CreateBuffer<T>(device, data.Vertices.ToArray(), Vk.BufferUsageFlags.IndexBuffer);

			mesh.indexOffset = 0;
			mesh.indexCount = data.Indices.Count;

			mesh.OBB = data.OBB;
			mesh.AABB = data.BoundingBox;
			mesh.BoundingSphere = data.BoundungSphere;

			return mesh;
		}

		private static Vk.Buffer CreateBuffer<T>(GraphicsDevice device, T[] data, Vk.BufferUsageFlags usageFlags) where T : struct
		{
			var size = System.Runtime.InteropServices.Marshal.SizeOf(data);
			var createBufferInfo = new Vk.BufferCreateInfo
			{
				Size = size,
				Usage = usageFlags,
				SharingMode = Vk.SharingMode.Exclusive,
				QueueFamilyIndices = new uint[] { 0 }
			};
			var buffer = device.LogicalDevice.CreateBuffer(createBufferInfo);

			//TODO more here

			//Memory
			var memoryReq = device.LogicalDevice.GetBufferMemoryRequirements(buffer);
			var allocInfo = new Vk.MemoryAllocateInfo { AllocationSize = memoryReq.Size };
			var memoryProperties = device.PhysicalDevice.GetMemoryProperties();
			bool heapIndexSet = false;

			var deviceMemory = device.LogicalDevice.AllocateMemory(allocInfo);
			IntPtr memPtr = device.LogicalDevice.MapMemory(deviceMemory, 0, size, 0);

			System.Runtime.InteropServices.Marshal.StructureToPtr(data, memPtr, false);

			device.LogicalDevice.UnmapMemory(deviceMemory);
			device.LogicalDevice.BindBufferMemory(buffer, deviceMemory, 0);

			return buffer;
		}

		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			var c = context as Vulkan.GraphicsContext;

			for(int i = 0; i < c.ActiveVKRenderTarget.DrawCmdBuffers.Length; i++)
			{
				var cmdBuffer = c.ActiveVKRenderTarget.DrawCmdBuffers[i];
				cmdBuffer.CmdBindVertexBuffers(0, 1, VertexBuffer, 0);
				cmdBuffer.CmdBindIndexBuffer(IndexBuffer, 0, Vk.IndexType.Uint32);
				cmdBuffer.CmdDrawIndexed((uint)indexCount, 1, 0, 0, 0);
			}
		}

		public override void UpdateFromData<T>(IndexedMeshData<T> data, BufferUsage usage)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}

#endif