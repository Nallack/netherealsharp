﻿
#if DIRECTX11

using System;
using SharpDX;
using D3D11 = SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Direct3D;


using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class IndexedMesh : Base.IndexedMesh
    {
		D3D11.Device Device;

		D3D11.Buffer VertexBuffer;
		D3D11.Buffer IndexBuffer;
		D3D11.VertexBufferBinding VertexBufferBinding;

		protected IndexedMesh() { }

		public IndexedMesh(GraphicsDevice device, VertexLayout layout, BufferUsage usage)
		{
			Device = device.D3D11Device;

			VertexBuffer = new D3D11.Buffer(
				Device,
				new D3D11.BufferDescription()
				{
					BindFlags = D3D11.BindFlags.VertexBuffer,
					Usage = usage.ToDX11(),
					StructureByteStride = layout.Size,
					CpuAccessFlags = D3D11.CpuAccessFlags.Write,
					OptionFlags = D3D11.ResourceOptionFlags.None,
					SizeInBytes = layout.Size * 1024
				});

			IndexBuffer = new D3D11.Buffer(
				Device,
				new D3D11.BufferDescription()
				{
					BindFlags = D3D11.BindFlags.IndexBuffer,
					Usage = usage.ToDX11(),
					StructureByteStride = layout.Size,
					CpuAccessFlags = D3D11.CpuAccessFlags.Write,
					OptionFlags = D3D11.ResourceOptionFlags.None,
					SizeInBytes = 4 * 1024
				});


			VertexBufferBinding = new D3D11.VertexBufferBinding(
				VertexBuffer,
				layout.Size,
				0);

		}

		public static IndexedMesh CreateFromData<T>(GraphicsDevice device, IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			IndexedMesh mesh = new IndexedMesh();

			mesh.Device = device.D3D11Device;

			D3D11.BufferDescription desc = new D3D11.BufferDescription()
			{
				BindFlags = D3D11.BindFlags.VertexBuffer,
				CpuAccessFlags = D3D11.CpuAccessFlags.Write,
				Usage = usage.ToDX11(),
				SizeInBytes = 0,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				StructureByteStride = 0
			};

			mesh.VertexBuffer = D3D11.Buffer.Create<T>(
				mesh.Device,
				D3D11.BindFlags.VertexBuffer,
				data.Vertices.ToArray());

			//VertexBuffer = D3D11.Buffer.Create<Vertex>(
			//	GraphicsManager.D3DDevice, 
			//	data.Vertices.ToArray(),
			//	desc);

			mesh.IndexBuffer = D3D11.Buffer.Create<uint>(
				mesh.Device,
				D3D11.BindFlags.IndexBuffer,
				data.Indices.ToArray());


			mesh.VertexBufferBinding = new D3D11.VertexBufferBinding(
				mesh.VertexBuffer, 
				VertexPNT.Layout.Size, 
				0);

			mesh.indexOffset = 0;
			mesh.indexCount = data.Indices.Count;

			mesh.OBB = data.OBB;
			mesh.AABB = data.BoundingBox;
			mesh.BoundingSphere = data.BoundungSphere;

			return mesh;
		}

		public override void UpdateFromData<T>(IndexedMeshData<T> data, BufferUsage usage)
		{
			DataStream ds;

			//Update Vertex Buffer
			Device.ImmediateContext.MapSubresource(
				VertexBuffer,
				D3D11.MapMode.WriteDiscard,
				D3D11.MapFlags.None, out ds);

			ds.WriteRange<T>(data.Vertices.ToArray());
			Device.ImmediateContext.UnmapSubresource(VertexBuffer, 0);

			//Update Index Buffer
			Device.ImmediateContext.MapSubresource(
				IndexBuffer,
				D3D11.MapMode.WriteDiscard,
				D3D11.MapFlags.None, out ds);

			ds.WriteRange<uint>(data.Indices.ToArray());
			Device.ImmediateContext.UnmapSubresource(IndexBuffer, 0);

			indexOffset = 0;
			indexCount = data.Indices.Count;

			AABB = data.BoundingBox;

		}

		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			Device.ImmediateContext.InputAssembler.SetVertexBuffers(0, VertexBufferBinding);
			Device.ImmediateContext.InputAssembler.SetIndexBuffer(IndexBuffer, Format.R32_UInt, 0);
			Device.ImmediateContext.InputAssembler.PrimitiveTopology = mode.ToD3D();
			Device.ImmediateContext.DrawIndexed(indexCount, indexOffset, 0);
        }

		public override void Dispose()
		{
			VertexBuffer.Dispose();
			IndexBuffer.Dispose();
		}


	}
}
#endif