﻿using System;
using System.Collections.Generic;
using System.Text;

#if DIRECTX11
using SharpDX;
#elif OPENGL
using OpenTK;
#endif

namespace NetherealSharp.Core.Graphics
{
	public class MeshGenerator
	{
		public static IndexedMeshData<VertexPNT> GenQuad()
		{
			return new IndexedMeshData<VertexPNT>(VertexPNT.Layout)
			{
				Vertices =
				{
					new VertexPNT(new Vector3(-1, -1, 0), new Vector3(0, 0, 1), new Vector2(0, 1)),
					new VertexPNT(new Vector3( 1, -1, 0), new Vector3(0, 0, 1), new Vector2(1, 1)),
					new VertexPNT(new Vector3(-1,  1, 0), new Vector3(0, 0, 1), new Vector2(0, 0)),
					new VertexPNT(new Vector3( 1,  1, 0), new Vector3(0, 0, 1), new Vector2(1, 0)),
				},
				Indices =
				{
					0, 2, 1, 2, 3, 1
				}
			};
		}

		public static IndexedMeshData<VertexPNT> GenCube()
		{
			float hw = 0.5f;
			float hh = 0.5f;
			float hd = 0.5f;

			return new IndexedMeshData<VertexPNT>(VertexPNT.Layout)
			{
				Vertices =
				{
					//front
					new VertexPNT(new Vector3(-hw, -hh, +hd), new Vector3(0, 0, 1), new Vector2(0, 1)),
					new VertexPNT(new Vector3(-hw, +hh, +hd), new Vector3(0, 0, 1), new Vector2(0, 0)),
					new VertexPNT(new Vector3(+hw, +hh, +hd), new Vector3(0, 0, 1), new Vector2(1, 0)),
					new VertexPNT(new Vector3(+hw, -hh, +hd), new Vector3(0, 0, 1), new Vector2(1, 1)),
					//back
					new VertexPNT(new Vector3(-hw, -hh, -hd), new Vector3(0, 0, -1), new Vector2(1, 1)),
					new VertexPNT(new Vector3(+hw, -hh, -hd), new Vector3(0, 0, -1), new Vector2(0, 1)),
					new VertexPNT(new Vector3(+hw, +hh, -hd), new Vector3(0, 0, -1), new Vector2(0, 0)),
					new VertexPNT(new Vector3(-hw, +hh, -hd), new Vector3(0, 0, -1), new Vector2(1, 0)),
					// top
					new VertexPNT(new Vector3(-hw, +hh, +hd), new Vector3(0, 1, 0), new Vector2(0, 1)),
					new VertexPNT(new Vector3(-hw, +hh, -hd), new Vector3(0, 1, 0), new Vector2(0, 0)),
					new VertexPNT(new Vector3(+hw, +hh, -hd), new Vector3(0, 1, 0), new Vector2(1, 0)),
					new VertexPNT(new Vector3(+hw, +hh, +hd), new Vector3(0, 1, 0), new Vector2(1, 1)),
					// bottom
					new VertexPNT(new Vector3(-hw, -hh, +hd), new Vector3(0, -1, 0), new Vector2(1, 1)),
					new VertexPNT(new Vector3(+hw, -hh, +hd), new Vector3(0, -1, 0), new Vector2(0, 1)),
					new VertexPNT(new Vector3(+hw, -hh, -hd), new Vector3(0, -1, 0), new Vector2(0, 0)),
					new VertexPNT(new Vector3(-hw, -hh, -hd), new Vector3(0, -1, 0), new Vector2(1, 0)),
					// left
					new VertexPNT(new Vector3(-hw, -hh, -hd), new Vector3(-1, 0, 0), new Vector2(0, 1)),
					new VertexPNT(new Vector3(-hw, +hh, -hd), new Vector3(-1, 0, 0), new Vector2(0, 0)),
					new VertexPNT(new Vector3(-hw, +hh, +hd), new Vector3(-1, 0, 0), new Vector2(1, 0)),
					new VertexPNT(new Vector3(-hw, -hh, +hd), new Vector3(-1, 0, 0), new Vector2(1, 1)),
					// right
					new VertexPNT(new Vector3(+hw, -hh, +hd), new Vector3(1, 0, 0), new Vector2(0, 1)),
					new VertexPNT(new Vector3(+hw, +hh, +hd), new Vector3(1, 0, 0), new Vector2(0, 0)),
					new VertexPNT(new Vector3(+hw, +hh, -hd), new Vector3(1, 0, 0), new Vector2(1, 0)),
					new VertexPNT(new Vector3(+hw, -hh, -hd), new Vector3(1, 0, 0), new Vector2(1, 1)),
				},
				Indices =
				{
					0,1,2,0,2,3,
					4,5,6,4,6,7,
					8,9,10,8,10,11,
					12,13,14,12,14,15,
					16,17,18,16,18,19,
					20,21,22,20,22,23
				}
			};
		}

		public static IndexedMeshData<VertexPNT> GenUVSphere(float radius, int longCount, int latCount)
		{
			var mesh = new IndexedMeshData<VertexPNT>(VertexPNT.Layout);

			float dPhi = MathUtil.Pi / latCount;
			float dTheta = MathUtil.TwoPi / longCount;

			float theta, phi;

			//Top
			for (int j = 0; j < longCount; j++)
			{
				theta = (j + 0.5f) * dTheta;
				var p = new Vector3(0, radius, 0);
				var n = Vector3.Up;
				//var t = Vector3.Right;
				var uv = new Vector2(theta / MathUtil.TwoPi, 0f);
				mesh.Vertices.Add(new VertexPNT(p, n, uv));
			}
			//Middle
			for (int i = 1; i < latCount; i++)
			{
				phi = i * dPhi;

				for (int j = 0; j <= longCount; j++)
				{
					theta = j * dTheta;

					var p = new Vector3
					(
						radius * MathF.Cos(theta) * MathF.Sin(phi),
						radius * MathF.Cos(phi),
						radius * MathF.Sin(theta) * MathF.Sin(phi)
					);
					var n = Vector3.Normalize(p);
					var t = new Vector3
					(
						-radius * MathF.Sin(phi) * MathF.Sin(theta),
						0,
						radius * MathF.Sin(phi) * MathF.Cos(theta)
					);
					t.Normalize();
					var uv = new Vector2(theta / MathUtil.TwoPi, 0.5f - 0.5f * MathF.Cos(phi));
					mesh.Vertices.Add(new VertexPNT(p, n, uv));
				}
			}
			//Bottom
			for (int j = 0; j < longCount; j++)
			{
				theta = (j + 0.5f) * dTheta;
				var p = new Vector3(0, -radius, 0);
				var n = Vector3.Down;
				//var t = Vector3.Right;
				var uv = new Vector2(theta / MathUtil.TwoPi, 1f);
				mesh.Vertices.Add(new VertexPNT(p, n, uv));
			}

			//indices
			uint ringVertexCount = (uint)longCount + 1;
			for (uint j = 0; j < longCount; j++)
			{
				mesh.Indices.Add(j);
				mesh.Indices.Add(j + ringVertexCount - 1);
				mesh.Indices.Add(j + ringVertexCount);
				
			}


			uint baseIndex = (uint)longCount;
			for (uint i = 0; i < latCount - 2; i++)
			{
				for (uint j = 0; j < longCount; j++)
				{
					mesh.Indices.Add(baseIndex + j);
					mesh.Indices.Add(baseIndex + ringVertexCount + j);
					mesh.Indices.Add(baseIndex + ringVertexCount + j + 1);
					

					mesh.Indices.Add(baseIndex + j);
					mesh.Indices.Add(baseIndex + ringVertexCount + j + 1);
					mesh.Indices.Add(baseIndex + j + 1);
					
				}
				baseIndex += ringVertexCount;
			}
			for (uint j = 0; j < longCount; j++)
			{
				mesh.Indices.Add(baseIndex + j);
				mesh.Indices.Add(baseIndex + ringVertexCount + j);
				mesh.Indices.Add(baseIndex + j + 1);
				
			}

			return mesh;
		}
	}
}
