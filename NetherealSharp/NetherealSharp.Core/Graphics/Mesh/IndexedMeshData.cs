﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public class IndexedMeshData<T> where T : IVertex
	{
		public List<T> Vertices;
		public List<uint> Indices;
		public VertexLayout Layout;

		public OrientedBoundingBox OBB
		{
			get
			{
				return new OrientedBoundingBox(
					Array.ConvertAll<T, Vector3>(
						Vertices.ToArray(), 
						new Converter<T, Vector3>((value) =>
						{
							return (value as IVertexPos).Position;
						}
					)));
			}
		}

		public BoundingBox BoundingBox
		{
			get
			{
				return BoundingBox.FromPoints(
					Array.ConvertAll<T, Vector3>(
						Vertices.ToArray(), 
						new Converter<T, Vector3>((value) =>
						{
							return (value as IVertexPos).Position;
						}
					)));

			}
		}

		public BoundingSphere BoundungSphere
		{
			get
			{
				return BoundingSphere.FromPoints(
					Array.ConvertAll<T, Vector3>(
						Vertices.ToArray(),
						new Converter<T, Vector3>((value) =>
						{
							return (value as IVertexPos).Position;
						}
					)));
			}
		}

		public IndexedMeshData(VertexLayout layout)
		{
			Vertices = new List<T>();
			Indices = new List<uint>();
			Layout = layout;
		}

		public IndexedMeshData(VertexLayout layout, List<T> vertices, List<uint> indices)
		{
			Layout = layout;
			Vertices = vertices;
			Indices = indices;
		}
	}
}
