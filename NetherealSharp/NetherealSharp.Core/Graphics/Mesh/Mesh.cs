﻿using NetherealSharp.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A drawable item that uses a single vertex buffer
	/// and the same vertex layout for all its vertices.
	/// </summary>
	[Content(RegistryType = typeof(Mesh))]
	public abstract class Mesh : IContentable, IDisposable
    {
		public string Name { get; set; }
		public Type ContentType { get { return typeof(Mesh); } }

		public OrientedBoundingBox OBB { get; protected set; }
		public BoundingBox AABB { get; protected set; }
		public BoundingSphere BoundingSphere { get; protected set; }

		public abstract void Draw(GraphicsContext context, PrimitiveMode mode);
		public abstract void Dispose();
    }
}
