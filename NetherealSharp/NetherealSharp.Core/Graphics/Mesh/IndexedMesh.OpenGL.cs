﻿
#if OPENGL
using System;
using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class IndexedMesh : Base.IndexedMesh
    {
		int vaoId;
		int vboId;
		int iboId;


		public IndexedMesh()
		{
			vaoId = GL.GenVertexArray();
			GL.BindVertexArray(vaoId);
			vboId = GL.GenBuffer();
			iboId = GL.GenBuffer();
			GL.BindVertexArray(0);
		}

		public IndexedMesh(VertexLayout layout, BufferUsage usage)
		{
			vaoId = GL.GenVertexArray();
			GL.BindVertexArray(vaoId);
			vboId = GL.GenBuffer();
			iboId = GL.GenBuffer();

			GL.BindBuffer(BufferTarget.ArrayBuffer, vboId);

			foreach (var e in layout.Elements)
			{
				GL.EnableVertexAttribArray(e.Index);
				GL.VertexAttribPointer(e.Index, e.Size, e.Type.ToOpenGL(), e.Normalized, e.Stride, e.Offset);
			}

			GL.BindVertexArray(0);

			indexOffset = 0;
			indexCount = 0;
		}


		public static IndexedMesh CreateFromData<T>(IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			IndexedMesh mesh = new IndexedMesh();

			GL.BindVertexArray(mesh.vaoId);


			//InterleavedArrayFormat.

			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vboId);

			foreach (var e in data.Layout.Elements)
			{
				GL.EnableVertexAttribArray(e.Index);
				GL.VertexAttribPointer(e.Index, e.Size, e.Type.ToOpenGL(), e.Normalized, e.Stride, e.Offset);
			}

			GL.BufferData<T>(
				BufferTarget.ArrayBuffer,
				(IntPtr)(data.Layout.Size * data.Vertices.Count),
				data.Vertices.ToArray(),
				usage.ToOpenGL());

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, mesh.iboId);
			GL.BufferData<uint>(
				BufferTarget.ElementArrayBuffer,
				(IntPtr)(4 * data.Indices.Count),
				data.Indices.ToArray(),
				usage.ToOpenGL());

			GL.BindVertexArray(0);

			mesh.indexOffset = 0;
			mesh.indexCount = data.Indices.Count;

			return mesh;
		}

		public override void UpdateFromData<T>(IndexedMeshData<T> data, BufferUsage usage)
		{
			GL.BindVertexArray(vaoId);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vboId);

			GL.BufferData<T>(
				BufferTarget.ArrayBuffer,
				(IntPtr)(data.Layout.Size * data.Vertices.Count),
				data.Vertices.ToArray(),
				usage.ToOpenGL());

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, iboId);
			GL.BufferData<uint>(
				BufferTarget.ElementArrayBuffer,
				(IntPtr)(4 * data.Indices.Count),
				data.Indices.ToArray(),
				usage.ToOpenGL());

			GL.BindVertexArray(0);

			indexOffset = 0;
			indexCount = data.Indices.Count;
		}

		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			GL.BindVertexArray(vaoId);

			GL.DrawElements(
				mode.ToGL(),
				indexCount, 
				DrawElementsType.UnsignedInt, 
				indexOffset);

			GL.BindVertexArray(0);
		}

		public override void Dispose()
		{
			GL.DeleteBuffer(vboId);
			GL.DeleteBuffer(iboId);
			GL.DeleteVertexArray(vaoId);
		}
	}
}
#endif