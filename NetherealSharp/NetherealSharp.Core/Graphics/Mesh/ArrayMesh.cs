﻿

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;


namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A type of mesh that does not use an index buffer.
	/// </summary>
    public abstract class ArrayMesh : Mesh
    {
		protected int vertexOffset = 0;
		protected int vertexCount = 0;

		public static ArrayMesh Create<T>(GraphicsDevice device, VertexLayout layout, BufferUsage usage) where T : struct, IVertex
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return DX11.ArrayMesh.Create<T>(device as DX11.GraphicsDevice, layout, usage);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return OpenGL.ArrayMesh.Create<T>(layout, usage);
#endif
				default:
					return null;
			}
		}

		public static ArrayMesh CreateFromData<T>(GraphicsDevice device, MeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return DX11.ArrayMesh.CreateFromData<T>(device as DX11.GraphicsDevice, data, usage);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return OpenGL.ArrayMesh.CreateFromData<T>(device as DX11.GraphicsDevice, data, usage);
#endif
				default:
					return null;
			}
		}

		public abstract void UpdateFromData<T>(MeshData<T> data, BufferUsage usage) where T : struct, IVertex;
	}
}
