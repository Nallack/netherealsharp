﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class ArrayMesh : Base.ArrayMesh
    {
		D3D11.Device Device;
		D3D11.Buffer VertexBuffer;
		D3D11.Buffer IndexBuffer;
		D3D11.VertexBufferBinding VertexBufferBinding;

		int offset = 0;
		int count = 0;

		private ArrayMesh() { }
		
		public static new ArrayMesh Create<T>(GraphicsDevice device, VertexLayout layout, BufferUsage usage) where T : struct, IVertex
		{
			

			ArrayMesh m = new ArrayMesh();

			m.Device = device.D3D11Device;
			//m.VertexBuffer = D3D11.Buffer.Create(GraphicsManager.D3DDevice, D3D11.BindFlags.VertexBuffer);
			m.VertexBuffer = new D3D11.Buffer(m.Device, new D3D11.BufferDescription()
			{
				BindFlags = D3D11.BindFlags.VertexBuffer,
				Usage = D3D11.ResourceUsage.Dynamic,
				StructureByteStride = layout.Size,
				CpuAccessFlags = D3D11.CpuAccessFlags.Write,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SizeInBytes = layout.Size * 1024
			});

			m.VertexBufferBinding = new D3D11.VertexBufferBinding(m.VertexBuffer, layout.Size, m.vertexOffset);
			return m;
		}

		public static new ArrayMesh CreateFromData<T>(GraphicsDevice device, MeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			/*
			D3D11.BufferDescription desc = new D3D11.BufferDescription()
			{
				BindFlags = D3D11.BindFlags.VertexBuffer,
				CpuAccessFlags = D3D11.CpuAccessFlags.Write,
				Usage = BufferUsageConversion.Convert(usage),
				SizeInBytes = 0,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				StructureByteStride = 0
			};*/

			ArrayMesh m = new ArrayMesh();

			m.vertexOffset = 0;
			m.vertexCount = data.Vertices.Count;

			m.VertexBuffer = D3D11.Buffer.Create<T>(
				device.D3D11Device,
				D3D11.BindFlags.VertexBuffer,
				data.Vertices.ToArray());

			m.VertexBufferBinding = new D3D11.VertexBufferBinding(m.VertexBuffer, Marshal.SizeOf(typeof(T)), m.vertexOffset);



			return m;
		}

		public override void UpdateFromData<T>(MeshData<T> data, BufferUsage usage)
		{
			//GraphicsManager.D3DContext.UpdateSubresource(data.Vertices.ToArray(), VertexBuffer);

			SharpDX.DataStream ds;

			Device.ImmediateContext.MapSubresource(
				VertexBuffer, 
				D3D11.MapMode.WriteDiscard, 
				D3D11.MapFlags.None, out ds);


			ds.WriteRange<T>(data.Vertices.ToArray());

			Device.ImmediateContext.UnmapSubresource(VertexBuffer, 0);

			vertexOffset = 0;
			vertexCount = data.Vertices.Count;
		}


		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			(context as DX11.GraphicsContext).D3D11Context.InputAssembler.SetVertexBuffers(0, VertexBufferBinding);
			(context as DX11.GraphicsContext).D3D11Context.InputAssembler.PrimitiveTopology = mode.ToD3D();
			(context as DX11.GraphicsContext).D3D11Context.Draw(vertexCount, vertexOffset);
		}

		public override void Dispose()
		{
			VertexBuffer.Dispose();
		}
	}
}

#endif