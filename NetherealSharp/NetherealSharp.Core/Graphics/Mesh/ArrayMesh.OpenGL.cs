﻿
#if OPENGL

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class ArrayMesh : Base.ArrayMesh
    {
		int vaoId;
		int vboId;

		int first = 0;
        int count = 0;

		public ArrayMesh()
		{
			vaoId = GL.GenVertexArray();
			GL.BindVertexArray(vaoId);
			vboId = GL.GenBuffer();
			GL.BindVertexArray(0);
		}

		public static ArrayMesh Create<T>(VertexLayout layout, BufferUsage usage) where T : struct, IVertex
		{

			ArrayMesh mesh = new ArrayMesh();

			GL.BindVertexArray(mesh.vaoId);

			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vboId);

			foreach(var e in layout.Elements)
			{
				GL.EnableVertexAttribArray(e.Index);
				GL.VertexAttribPointer(e.Index, e.Size, e.Type.ToOpenGL(), e.Normalized, e.Stride, e.Offset);
			}
			GL.EnableVertexAttribArray(0);

			return mesh;
		}

		public static ArrayMesh CreateFromData<T>(MeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			ArrayMesh mesh = new ArrayMesh();

			GL.BindVertexArray(mesh.vaoId);

			GL.BindBuffer(BufferTarget.ArrayBuffer, mesh.vboId);
			
			foreach(var e in data.Layout.Elements)
			{
				GL.EnableVertexAttribArray(e.Index);
				GL.VertexAttribPointer(e.Index, e.Size, e.Type.ToOpenGL(), e.Normalized, e.Stride, e.Offset);
			}
			GL.EnableVertexAttribArray(0);

			GL.BufferData<T>(
				BufferTarget.ArrayBuffer,
				(IntPtr)(data.Layout.Size),
				data.Vertices.ToArray(),
				usage.ToOpenGL());

			GL.BindVertexArray(0);

			mesh.count = data.Vertices.Count;

			return mesh;
		}

		public override void UpdateFromData<T>(MeshData<T> data, BufferUsage usage)
		{
			GL.BindVertexArray(vaoId);

			GL.BindBuffer(BufferTarget.ArrayBuffer, vboId);

			GL.BufferData<T>(
				BufferTarget.ArrayBuffer,
				(IntPtr)(data.Layout.Size * data.Vertices.Count),
				data.Vertices.ToArray(),
				usage.ToOpenGL());

			GL.BindVertexArray(0);

			count = data.Vertices.Count;
		}

		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			GL.BindVertexArray(vaoId);
			GL.DrawArrays(mode.ToGL(), first, count);
		}

		public override void Dispose()
		{
			GL.DeleteBuffer(vboId);
			GL.DeleteVertexArray(vaoId);
		}
	}
}

#endif