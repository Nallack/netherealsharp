﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class MeshData<T> where T : IVertex
    {
		public List<T> Vertices { get; protected set; }
		public List<uint> Indices { get; protected set; }
		public VertexLayout Layout { get; protected set; }

		public MeshData(VertexLayout layout, bool hasIndices)
		{
			Vertices = new List<T>();
			if(hasIndices) Indices = new List<uint>();
			Layout = layout;
		}
    }
}
