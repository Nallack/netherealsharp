﻿
using NetherealSharp.Core.Serialization;
using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace NetherealSharp.Core.Graphics
{
	
    public abstract class IndexedMesh : Mesh, IDisposable
    {
		protected int indexOffset = 0;
		protected int indexCount = 0;

		public static IndexedMesh Create(GraphicsDevice device, VertexLayout layout, BufferUsage usage)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.IndexedMesh(device as DX11.GraphicsDevice, layout, usage);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.IndexedMesh(layout, usage);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public static IndexedMesh CreateFromData<T>(GraphicsDevice device, IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return DX11.IndexedMesh.CreateFromData(device as DX11.GraphicsDevice, data, usage);
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return DX12.IndexedMesh.CreateFromData(device as DX12.GraphicsDevice, data, usage);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return OpenGL.IndexedMesh.CreateFromData(data, usage);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void UpdateFromData<T>(IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex;
	}
}
