﻿
#if DIRECTX12

using System;
using SharpDX;
using D3D12 = SharpDX.Direct3D12;
using SharpDX.DXGI;
using SharpDX.Direct3D;


using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class IndexedMesh : Base.IndexedMesh
	{
		GraphicsDevice Device;

		D3D12.Resource VertexBuffer;
		D3D12.VertexBufferView VertexBufferView;

		D3D12.Resource IndexBuffer;
		D3D12.IndexBufferView IndexBufferView;


		public IndexedMesh(GraphicsDevice device)
		{
			Device = device;
		}

		public IndexedMesh(GraphicsDevice device, VertexLayout layout, BufferUsage usage)
		{
			Device = device;

			var hp = new D3D12.HeapProperties(SharpDX.Direct3D12.HeapType.Upload);

			VertexBuffer = Device.D3DDevice.CreateCommittedResource(
				hp,
				D3D12.HeapFlags.None,
				D3D12.ResourceDescription.Buffer(layout.Size * 1024),
				D3D12.ResourceStates.GenericRead);

			VertexBufferView = new D3D12.VertexBufferView()
			{
				BufferLocation = VertexBuffer.GPUVirtualAddress,
				StrideInBytes = layout.Size,
				SizeInBytes = layout.Size * 1024
			};

			IndexBuffer = Device.D3DDevice.CreateCommittedResource(
				hp,
				D3D12.HeapFlags.None,
				D3D12.ResourceDescription.Buffer(1024),
				D3D12.ResourceStates.IndexBuffer);

			IndexBufferView = new D3D12.IndexBufferView()
			{
				BufferLocation = IndexBuffer.GPUVirtualAddress,
				Format = Format.R32_UInt,
				SizeInBytes = 1024 * sizeof(uint)
			};

		}

		//TODO: use a constructor instead
		public static new IndexedMesh CreateFromData<T>(GraphicsDevice device, IndexedMeshData<T> data, BufferUsage usage) where T : struct, IVertex
		{
			IndexedMesh mesh = new IndexedMesh(device);

			var hp = new D3D12.HeapProperties(SharpDX.Direct3D12.HeapType.Upload);

			mesh.VertexBuffer = mesh.Device.D3DDevice.CreateCommittedResource(
				hp,
				D3D12.HeapFlags.None,
				D3D12.ResourceDescription.Buffer(data.Layout.Size * 1024),
				D3D12.ResourceStates.GenericRead);

			IntPtr vertexbufferstart = mesh.VertexBuffer.Map(0);
			Utilities.Write<T>(vertexbufferstart, data.Vertices.ToArray(), 0, data.Vertices.Count);
			mesh.VertexBuffer.Unmap(0);

			mesh.VertexBufferView = new D3D12.VertexBufferView()
			{
				BufferLocation = mesh.VertexBuffer.GPUVirtualAddress,
				StrideInBytes = data.Layout.Size,
				SizeInBytes = data.Layout.Size * 1024
			};

			mesh.IndexBuffer = mesh.Device.D3DDevice.CreateCommittedResource(
				hp,
				D3D12.HeapFlags.None,
				D3D12.ResourceDescription.Buffer(1024),
				D3D12.ResourceStates.IndexBuffer);

			IntPtr indexbufferstart = mesh.IndexBuffer.Map(0);
			Utilities.Write<uint>(indexbufferstart, data.Indices.ToArray(), 0 , data.Indices.Count);
			mesh.IndexBuffer.Unmap(0);

			mesh.IndexBufferView = new D3D12.IndexBufferView()
			{
				BufferLocation = mesh.IndexBuffer.GPUVirtualAddress,
				Format = Format.R32_UInt,
				SizeInBytes = 1024 * sizeof(uint)
			};

			mesh.indexOffset = 0;
			mesh.indexCount = data.Indices.Count;

			return mesh;
		}

		public override void UpdateFromData<T>(IndexedMeshData<T> data, BufferUsage usage)
		{

			throw new NotImplementedException();
			//DataStream ds;

			////Update Vertex Buffer
			//Graphics.D3DContext.MapSubresource(
			//	VertexBuffer,
			//	D3D11.MapMode.WriteDiscard,
			//	D3D11.MapFlags.None, out ds);

			//ds.WriteRange<T>(data.Vertices.ToArray());
			//Graphics.D3DContext.UnmapSubresource(VertexBuffer, 0);

			////Update Index Buffer
			//Graphics.D3DContext.MapSubresource(
			//	IndexBuffer,
			//	D3D11.MapMode.WriteDiscard,
			//	D3D11.MapFlags.None, out ds);

			//ds.WriteRange<uint>(data.Indices.ToArray());
			//Graphics.D3DContext.UnmapSubresource(IndexBuffer, 0);

			//indexOffset = 0;
			//indexCount = data.Indices.Count;

		}

		public override void Draw(Base.GraphicsContext context, PrimitiveMode mode)
		{
			Device.D3D12DefaultCommandList.PrimitiveTopology = mode.ToD3D();
			Device.D3D12DefaultCommandList.SetVertexBuffer(0, VertexBufferView);
			Device.D3D12DefaultCommandList.DrawIndexedInstanced(3, 1, 0, 0, 0);

        }

		public override void Dispose()
		{
			IndexBuffer.Dispose();
			VertexBuffer.Dispose();
		}


	}
}
#endif