﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{


	/// <summary>
	/// A graphics device that can be configured to different graphics implementations such as 
	/// D3D11 and OpenGL
	/// </summary>
	public class GraphicsFactory
    {
		public readonly GraphicsImplementation Implementation;

		public GraphicsFactory(GraphicsImplementation implementation)
		{
			Implementation = implementation;
		}

		public GraphicsDevice CreateDevice()
		{
			switch (Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.GraphicsDevice();
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return new DX12.GraphicsDevice();
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.GraphicsDevice();
#endif
//#if OPENGLES
//				case GraphicsImplementation.OpenGLES:
//					return new OpenGL.GraphicsDevice();
//#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
