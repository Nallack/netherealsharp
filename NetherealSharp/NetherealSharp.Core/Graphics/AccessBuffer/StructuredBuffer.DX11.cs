﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.InteropServices;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Buffer that is accessable in compute shader, and can be cleverly used
	/// as a theoretical vertex shader by shading without vertex buffers.
	/// 
	/// TODO: On draw, set vertex buffer and vertex shader to contain SV_VertexID
	///			Access the StructuredBuffer at the VertexID and process from there!
	///
	/// 
	/// https://msdn.microsoft.com/en-us/library/windows/desktop/bb232912%28v=vs.85%29.aspx
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class StructuredBuffer<T> : Base.StructuredBuffer<T>, IDX11AccessBuffer where T : struct
	{
		protected D3D11.Device Device;
		protected D3D11.Buffer Buffer;
		public D3D11.UnorderedAccessView UnorderedAccessView { get; protected set; }
		public D3D11.ShaderResourceView ShaderResourceView { get; protected set; }

		public StructuredBuffer(GraphicsDevice device, int maxArraySize)
		{
			Device = device.D3D11Device;
			Buffer = new D3D11.Buffer(Device, new D3D11.BufferDescription()
			{
				BindFlags = D3D11.BindFlags.UnorderedAccess | D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SizeInBytes = maxArraySize,
				StructureByteStride = Marshal.SizeOf(typeof(T))
			});

			UnorderedAccessView = new D3D11.UnorderedAccessView(Device, Buffer, new D3D11.UnorderedAccessViewDescription()
			{
				Format = DXGI.Format.Unknown,
				Buffer =
				{
					FirstElement = 0,
					ElementCount = maxArraySize,
					Flags = D3D11.UnorderedAccessViewBufferFlags.None,
				},
				Dimension = D3D11.UnorderedAccessViewDimension.Buffer,
			});

			ShaderResourceView = new D3D11.ShaderResourceView(Device, Buffer, new D3D11.ShaderResourceViewDescription()
			{
				Buffer =
				{
					FirstElement = 0,
					ElementCount = maxArraySize,
					ElementOffset = 0,
					ElementWidth = Marshal.SizeOf(typeof(T))
				},
				Format = DXGI.Format.Unknown,
				Dimension = SharpDX.Direct3D.ShaderResourceViewDimension.Buffer,
				

			});
		}
	}
}
#endif