﻿#if DIRECTX11

using System;
using System.Collections.Generic;
using System.Text;

using D3D11 = SharpDX.Direct3D11;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Can be read from shader using StructuredBuffer<> type
	/// </summary>
    public interface IDX11AccessBuffer
    {
		D3D11.UnorderedAccessView UnorderedAccessView { get; }
		D3D11.ShaderResourceView ShaderResourceView { get; }
    }
}
#endif