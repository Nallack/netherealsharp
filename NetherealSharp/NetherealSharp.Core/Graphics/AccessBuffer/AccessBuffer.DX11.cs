﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;
using System.Runtime.InteropServices;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Contains an array of the specified generic,
	/// Can 
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public class AccessBuffer<T> : Base.AccessBuffer<T> where T : struct
    {
		protected D3D11.Device Device;
		protected D3D11.Buffer Buffer; //This can act as a ConstantBufferView
		public D3D11.UnorderedAccessView UnorderedAccessView; //view that allows writing to

		public AccessBuffer(GraphicsDevice device, int maxArraySize)
		{
			Device = device.D3D11Device;


			Buffer = new D3D11.Buffer(Device, new D3D11.BufferDescription()
			{
				BindFlags = D3D11.BindFlags.UnorderedAccess | D3D11.BindFlags.ShaderResource,
				OptionFlags = D3D11.ResourceOptionFlags.BufferStructured,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				SizeInBytes = Marshal.SizeOf(typeof(T)) * maxArraySize,
				StructureByteStride = Marshal.SizeOf(typeof(T)),
				Usage = D3D11.ResourceUsage.Default
			});

			var uavd = new D3D11.UnorderedAccessViewDescription()
			{
				Dimension = D3D11.UnorderedAccessViewDimension.Buffer

			};

			UnorderedAccessView = new D3D11.UnorderedAccessView(Device, Buffer);
		}

		public void Bind(Base.GraphicsContext context, int slot)
		{
			var c = context as GraphicsContext;

			c.D3D11Context.VertexShader.SetConstantBuffer(slot, Buffer);

			c.D3D11Context.ComputeShader.SetUnorderedAccessView(slot, UnorderedAccessView);
		}
    }
}

#endif