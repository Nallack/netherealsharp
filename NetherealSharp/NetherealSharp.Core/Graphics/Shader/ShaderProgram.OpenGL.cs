﻿
#if OPENGL

using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;



namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class ShaderProgram : IDisposable
    {

		int ProgramId;

		public ShaderProgram()
		{
			ProgramId = GL.CreateProgram();
		}

		public int GetUniformLocation(string name)
		{
			return GL.GetUniformLocation(ProgramId, name);
		}

		public void Bind()
		{
			GL.UseProgram(ProgramId);
		}

		public void AttachShader(Shader shader)
		{
			GL.AttachShader(ProgramId, shader.ShaderId);
		}

		public void Link()
		{
			GL.LinkProgram(ProgramId);

			int status;
#pragma warning disable CS0618 // Type or member is obsolete
			GL.GetProgram(ProgramId, ProgramParameter.LinkStatus, out status);
#pragma warning restore CS0618 // Type or member is obsolete
			if (status == 0)
			{
				string log = GL.GetProgramInfoLog(ProgramId);
				throw new ApplicationException(log);
			}
		}

		public void BindVertexLayout(VertexLayout layout)
		{
			foreach(var e in layout.Elements)
			{
				GL.BindAttribLocation(ProgramId, e.Index, e.Name);
			}
		}

		public void BindUniformBlock(string name, int binding)
		{
			int index = GL.GetUniformBlockIndex(ProgramId, name);
            if (index != (int)All.InvalidIndex)
			{
				GL.UniformBlockBinding(ProgramId, index, binding);
			}
		}

		public void Unbind()
		{
			GL.UseProgram((int)All.None);
		}

		public void Dispose()
		{
			GL.DeleteProgram(ProgramId);
		}
	}
}
#endif