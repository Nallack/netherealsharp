﻿#if OPENGL || OPENGLES
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	/// <summary>
	/// GLSL has no preprocessor library, so a preprocessor will need to be
	/// implemented to bring glsl up to hlsl features.
	/// 
	/// Must handle:
	/// shader macros
	/// shader includes
	/// repassing includes within includes
	/// 
	/// may need to inject glsl version first before all of this
	/// </summary>
    public class ShaderPreprocessor
    {
		List<string> m_include;
		//ShaderInclude m_include;

		public ShaderPreprocessor()
		{
			m_include = new List<string>()
			{
				 "Content/Shaders/Include/"
			};
		}

		public string Process(string shadersource)
		{
			var lines = shadersource.Split(
				new string[] { Environment.NewLine },
				StringSplitOptions.RemoveEmptyEntries);
			return Process(lines);
		}

		public string Process(string[] shadersorce)
		{
			StringBuilder completesource = new StringBuilder();
			foreach (var line in shadersorce)
			{
				if (line.StartsWith("#include"))
				{
					string directive = line.Split(' ')[1];
					string filepath = m_include[0] + directive.Trim('<', '>', '"');

#if DEBUG
					System.Console.WriteLine("include " + filepath);
#endif
					StreamReader s;
					try
					{

						using (var filestream = File.Open(filepath, FileMode.Open))
						{
							 s = new StreamReader(filestream);

							//will need to run this through the preprocessor too!
							//and possibly detect defines and comment blocks to treat
							//nested includes

							completesource.AppendLine(s.ReadToEnd());

							s.Close();
						}
						
					}
					catch (FileNotFoundException e)
					{
						throw new FileNotFoundException("GLSL could not find file " + filepath + " : " + e, filepath);
					}
				}
				else completesource.AppendLine(line);
			}
			return completesource.ToString();
		}



		private string ParseIncludeDirective(string include)
		{
			return "";
		}
    }
}
#endif