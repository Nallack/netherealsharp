﻿#if OPENGLES


using System;
using System.Collections.Generic;
using System.Text;
using NetherealSharp.Core.Diagnostics;

#if OPENGLES_31
using OpenTK.Graphics.ES31;
using OGL = OpenTK.Graphics.ES31;
#elif OPENGLES_30
using OpenTK.Graphics.ES30;
using OGL = OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
using OGL = OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.ES20;
using OGL = OpenTK.Graphics.ES20;
#endif

using OpenTK.Graphics;

using System.IO;

namespace NetherealSharp.Core.Graphics.OpenGLES
{
    public class Shader : IDisposable
    {
		GraphicsDevice m_device;
		public int ShaderId;
		OGL.ShaderType Type;

		public Shader(GraphicsDevice device, OGL.ShaderType type)
		{
			m_device = device;
			Type = type;
			ShaderId = GL.CreateShader(type);
		}
		 
		public void CompileFromFile(string filepath)
		{
			StringBuilder shaderbuilder = new StringBuilder();

			var v = GL.GetString(StringName.ShadingLanguageVersion);
			shaderbuilder.AppendLine("#version " + "150");
			switch(Type)
			{
				case OGL.ShaderType.VertexShader:
					shaderbuilder.AppendLine("#define VERTEX_SHADER 1");
					break;
#if OPENGL || OPENGLES_31
				case OGL.ShaderType.GeometryShader:
					shaderbuilder.AppendLine("#define GEOMETRY_SHADER 1");
					break;
#endif
				case OGL.ShaderType.FragmentShader:
					shaderbuilder.AppendLine("#define FRAGMENT_SHADER 1");
					break;
				default:
					throw new NotImplementedException();
			}

			var processed = m_device.GLSLPreprocessor.Process(File.ReadAllText(filepath));

			shaderbuilder.Append(processed);
			string shadercode = shaderbuilder.ToString();

			GL.ShaderSource(ShaderId, shadercode);

			GL.CompileShader(ShaderId);



			int status = 1;
			GL.GetShader(ShaderId, ShaderParameter.CompileStatus, out status);
			if(status == 0)
			{
				string log = GL.GetShaderInfoLog(ShaderId);

				Debug.LogError(Type + " Compile Error", "failed to compile shader at \"" + filepath + "\"\n" + log);

				//throw new InvalidDataException("Shader Compile Error " + log);
			}
		}

		public void Dispose()
		{
			GL.DeleteShader(ShaderId);
		}
	}
}
#endif