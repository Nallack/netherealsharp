﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class ShaderMacro
    {
		public string Name;
		public string Definition;

#if DIRECTX11
		public SharpDX.Direct3D.ShaderMacro ToD3D()
		{
			return new SharpDX.Direct3D.ShaderMacro(Name, Definition);
		}
#endif
	}
}
