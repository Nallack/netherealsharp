﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[FlagsAttribute]
    public enum ShaderType
    {
		Vertex = 1,
		Pixel = 2,
		Geometry = 4,
		Hull = 8,
		Domain = 16,
		Compute = 32

    }

	public static class ShaderTypeConversion
	{
#if DIRECTX
		public static SharpDX.D3DCompiler.ShaderVersion ToD3D(this ShaderType value)
		{
			return SharpDX.D3DCompiler.ShaderVersion.VertexShader;
		}
#endif
	}
}
