﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using SharpDX.D3DCompiler;
using SharpDX;

namespace NetherealSharp.Core.Graphics
{
	//looks up file system from current directory and then include directories,
	//The interface is called by the D3DCompiler, so this object has to be able to fetch
	//all possible include files. Preloading a directory might be an option, but generally
	//you would want compiling done as part of the build phase.

	//The exception could be say allowing users to mod shaders in which case the include folder
	//would need to be part of the build (unless sneakily hidden to hide shader source, but this would
	//probably be hacked way to easy and leak the code anyway).

	public class ShaderInclude : CallbackBase, Include
	{
		public readonly Stack<string> CurrentDirectory;
		public readonly Dictionary<string, FileItem> FileResolved;
		public readonly List<string> IncludeDirectories;

		public ShaderInclude()
		{
			CurrentDirectory = new Stack<string>();
			FileResolved = new Dictionary<string, FileItem>();
			IncludeDirectories = new List<string>();
		}

		public Stream Open(IncludeType type, string fileName, Stream parentStream)
		{
			var currentDirectory = Environment.CurrentDirectory;

			var selectedFile = fileName;

			if(FileResolved.ContainsKey(fileName))
			{
				selectedFile = FileResolved[fileName].FilePath;
			}
			else
			{
				if (!Path.IsPathRooted(selectedFile))
				{
					var directoryToSearch = new List<string> { currentDirectory };
					directoryToSearch.AddRange(IncludeDirectories);
					foreach (var dirPath in directoryToSearch)
					{
						var filepath = Path.Combine(dirPath, fileName);
						if (File.Exists(filepath))
						{
							selectedFile = filepath;
							break;
						}
					}

					var fullpath = Path.GetFullPath(selectedFile);
					CurrentDirectory.Push(Path.GetDirectoryName(fullpath));

					FileResolved.Add(fileName, new FileItem()
					{
						FileName = fileName,
						FilePath = fullpath
					});
				}
			}


			Stream stream = null;

			//FileResolved.Remove(selectedFile);

			if (selectedFile == null || !File.Exists(selectedFile))
				throw new FileNotFoundException();

			stream = new FileStream(selectedFile, FileMode.Open, FileAccess.Read);
			//remove BOM
			stream.Seek(3, SeekOrigin.Begin);

			return stream;
		}

		public void Close(Stream stream)
		{
			stream.Close();
			//CurrentDirectory.Pop();
		}


		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected override void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~ShaderInclude() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		//public void Dispose()
		//{
		//	// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//	Dispose(true);
		//	// TODO: uncomment the following line if the finalizer is overridden above.
		//	// GC.SuppressFinalize(this);
		//}
		#endregion

	}

	public class FileItem
	{
		public string FileName;
		public string FilePath;
	}
}
#endif