﻿

#if OPENGL

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NetherealSharp.Core.Diagnostics;

using OpenTK.Graphics.OpenGL;
using OGL = OpenTK.Graphics.OpenGL;


using OpenTK.Graphics;
using System.IO;


namespace NetherealSharp.Core.Graphics.OpenGL
{
	/// <summary>
	/// A lightweight shader class for OpenGL/ES
	/// 
	/// </summary>
    public class Shader : IDisposable
    {
		GraphicsDevice m_device;
		public int ShaderId;
		OGL.ShaderType Type;

		public Shader(GraphicsDevice device, OGL.ShaderType type)
		{
			m_device = device;
			Type = type;
			ShaderId = GL.CreateShader(type);
		}
		
		/// <summary>
		/// Compiles a single shader from a GLSL shader file
		/// To support multiple shaders per source file, shader type directives are
		/// inserted at the start of the source, along with the heighest supported shader
		/// language version.
		/// </summary>
		/// <param name="filepath"></param>
		public void CompileFromFile(string filepath)
		{
			StringBuilder shaderstring = new StringBuilder();



			//TODO: check if a specific version is chosen, either through existing version directive,
			//non-null member variable, else determine via GL context.

			var shadersource = File.ReadAllText(filepath);
			var splitshadersource = shadersource.Split('\n');

			var versionline = from line in splitshadersource
						  where line.Contains("#version")
						  select line;

			if(versionline.Count() > 1)
			{
				throw new Exception(filepath + " : Shader cannot contian multiple version definitions");
			}
			else if(versionline.Count() == 1)
			{
				shaderstring.AppendLine(versionline.ElementAt(0));
			}
			else
			{
				//begin our shader string with a calculated version
				var v = GL.GetString(StringName.ShadingLanguageVersion);
				shaderstring.AppendLine("#version " + "150");
			}


			switch (Type)
			{
				case OGL.ShaderType.VertexShader:
					shaderstring.AppendLine("#define VERTEX_SHADER 1");
					break;
#if OPENGLES_31 || OPENGL_32
				case OGL.ShaderType.GeometryShader:
					shaderstring.AppendLine("#define GEOMETRY_SHADER 1");
					break;
#endif
				case OGL.ShaderType.FragmentShader:
					shaderstring.AppendLine("#define FRAGMENT_SHADER 1");
					break;
				default:
					throw new NotImplementedException();
			}

			
			var processed = m_device.GLSLPreprocessor.Process(shadersource);

			shaderstring.Append(processed);

			string shadercode = shaderstring.ToString();
			GL.ShaderSource(ShaderId, shadercode);

			GL.CompileShader(ShaderId);



			int status = 1;
			GL.GetShader(ShaderId, ShaderParameter.CompileStatus, out status);
			if(status == 0)
			{
				string log = GL.GetShaderInfoLog(ShaderId);

				Debug.LogError(Type + " Compile Error", "failed to compile shader at \"" + filepath + "\"\n" + log);

				//throw new InvalidDataException("Shader Compile Error " + log);
			}
		}

		public void Dispose()
		{
			GL.DeleteShader(ShaderId);
		}
	}
}
#endif