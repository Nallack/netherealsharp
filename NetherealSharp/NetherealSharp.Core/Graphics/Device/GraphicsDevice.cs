﻿using System;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A DirectX style wrapper of the graphics API and device reference
	/// Treat as an abstract Factory (for now pass as parameter to static abstract constructors)
	/// </summary>
	public abstract class GraphicsDevice : IDisposable
	{
		public GraphicsImplementation Implementation;

		public IRenderTarget ActiveRenderTarget;

		public GraphicsContext ImmediateContext { get; protected set; }

		public static GraphicsDevice Create(GraphicsImplementation implementation)
		{
			switch(implementation)
			{
#if DIRECTX12
                case GraphicsImplementation.DirectX12:
                    return new DX12.GraphicsDevice();
#endif
#if DIRECTX11
                case GraphicsImplementation.DirectX11:
					return new DX11.GraphicsDevice();
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.GraphicsDevice();
#endif
#if OPENGLES
				case GraphicsImplementation.OpenGLES:
					return new OpenGLES.GraphicsDevice();
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Vulkan.GraphicsDevice();
#endif
				default:
					throw new NotImplementedException();
			}
		}


		public virtual void Begin(RenderTarget target) { }

		public virtual void End(RenderTarget target) { }



#region FactoryConstructors
		//public abstract RenderTexture2D CreateRenderTexture2D();

		//public abstract Texture2D CreateTexture2D();
		
		//public abstract UniformBuffer<T> CreateUniformBuffer();
#endregion



		public abstract void Dispose();
	}
}
