﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;


using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class GraphicsContext : Base.GraphicsContext
    {
		public GraphicsDevice Device { get; private set; }

		public D3D11.DeviceContext D3D11Context;
		public D2D1.DeviceContext D2D1Context;

		//public D2D1.RenderTarget D2D1ActiveTarget;

		/// <summary>
		/// Create a graphics context, either immediate or deffered.
		/// </summary>
		/// <param name="device"></param>
		/// <param name="immediate"></param>
		public GraphicsContext(GraphicsDevice device, bool immediate)
		{
			Device = device;


			//D3D11 Context
			/*
			using (var d = device.D3DDevice.QueryInterface<D3D11.Device2>())
			{
				D3D11Context = new D3D11.DeviceContext2(d);
			}*/

			if(immediate) D3D11Context = Device.D3D11Device.ImmediateContext;
			else
			{
				D3D11Context = new D3D11.DeviceContext(Device.D3D11Device);
			}

			//D2D1 Context
			D2D1Context = new D2D1.DeviceContext1(Device.D2D1Device, D2D1.DeviceContextOptions.None);
		}



		public override void SetRenderTarget(IRenderTarget target)
		{
			var dxtarget = target as IDX11RenderTarget2D;
			D3D11Context.OutputMerger.SetRenderTargets(dxtarget.DepthStencilView, dxtarget.RenderTargetView);
			ActiveRenderTarget = target;
		}

		public override void SetEmptyTarget()
		{
			D3D11Context.OutputMerger.SetRenderTargets((D3D11.DepthStencilView)null, (D3D11.RenderTargetView)null);
		}


		public override void SetViewport(float x, float y, float width, float height)
		{
			D3D11Context.Rasterizer.SetViewport(x, y, width, height);
		}

		public override void ClearTarget(IRenderTarget target, Color4 color)
		{
			var dxtarget = target as IDX11RenderTarget2D;
			D3D11Context.ClearRenderTargetView(dxtarget.RenderTargetView, color);
			D3D11Context.ClearDepthStencilView(
				dxtarget.DepthStencilView, 
				D3D11.DepthStencilClearFlags.Depth | D3D11.DepthStencilClearFlags.Stencil,
				1.0f, 0);
		}

		public override void ClearRenderTargetView(IRenderTarget target, Color4 color)
		{
			var t = target as IDX11RenderTarget2D;
			D3D11Context.ClearRenderTargetView(t.RenderTargetView, color);
		}

		public override void ClearDepthStencilView(IRenderTarget target)
		{
			var t = target as IDX11RenderTarget2D;
			D3D11Context.ClearDepthStencilView(t.DepthStencilView, D3D11.DepthStencilClearFlags.Depth, 1.0f, 0);
		}

		public override void SetShaderResources(int slot, ITexture[] textures)
		{
			
			var views = new D3D11.ShaderResourceView[textures.Length];
			for(int i = 0; i < textures.Length; i++)
			{
				views[i] = (textures[i] as IDX11Texture).ShaderResourceView;
			}
			
			D3D11Context.PixelShader.SetShaderResources(slot, views);
		}


		public override void EmptyShaderResources(int slot, int count)
		{
			var views = new D3D11.ShaderResourceView[count];
			D3D11Context.PixelShader.SetShaderResources(slot, views);
		}

		public override void EmptyShaderResource(int slot)
		{
			D3D11Context.PixelShader.SetShaderResource(slot, null);
		}

		public override void Dispose()
		{
			D3D11Context.Dispose();
			D2D1Context.Dispose();
		}
	}
}
#endif