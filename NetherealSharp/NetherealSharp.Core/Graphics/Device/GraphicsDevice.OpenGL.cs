﻿#if OPENGL

using System;

using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;



namespace NetherealSharp.Core.Graphics.OpenGL
{
	/// <summary>
	/// OpenGL Contexts for some reason require a default render target which must be used in context creation.
	/// </summary>
    public class GraphicsDevice : Base.GraphicsDevice
    {
		//FONT
#if OPENFONT
		public SharpFont.Library FontLibrary;
#endif

		public ShaderPreprocessor GLSLPreprocessor;

		//GRAPHICS
		public OpenTK.Graphics.IGraphicsContext GLContext { get; private set; }

#if OPENGL
		private DebugProcArb m_DebugProc;
#endif


		public GraphicsDevice()
		{
#if OPENGL
			Implementation = GraphicsImplementation.OpenGL;
#elif OPENGLES
			Implementation = GraphicsImplementation.OpenGLES;
#endif

#if OPENFONT
			FontLibrary = new SharpFont.Library();
#endif
			GLSLPreprocessor = new ShaderPreprocessor();


			//Disabling this for some reason greatly increases CPU usage...


#if OPENGL
			//------OpenGL 3.0----------//
			//GL.Enable(EnableCap.DebugOutput);
			//GL.Enable(EnableCap.DebugOutputSynchronous);

			//v1.1.2 m_DebugProc = new DebugProcArb(DebugCallback);
			GL.Arb.DebugMessageCallback(m_DebugProc, IntPtr.Zero);

			//GL.DebugMessageCallback(m_DebugProc, IntPtr.Zero);

			//DEBUG
			int[] ids = { 131185 };

			

			//GL.DebugMessageControl(DebugSourceControl.DontCare, DebugTypeControl.DontCare, DebugSeverityControl.DebugSeverityHigh, 0, ids, false);
			//GL.DebugMessageControl(DebugSourceControl.DontCare, DebugTypeControl.DontCare, DebugSeverityControl.DontCare, 0, ids, false);
			//GL.DebugMessageControl(DebugSourceControl.DontCare, DebugTypeControl.DontCare, DebugSeverityControl.DebugSeverityNotification, 0, ids, false);
#endif
			
			

			GL.Enable(EnableCap.DepthTest);
			//GL.Enable(EnableCap.CullFace);
			//GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);

			//GL.Enable(EnableCap.AlphaTest);
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);


			ImmediateContext = new GraphicsContext(this, true);

			//CreateDeviceIndependantResources();

		}

#if OPENGL
		void DebugCallback(int id, ArbDebugOutput category, ArbDebugOutput severity, IntPtr length, string message, IntPtr userParam)
		{
			System.Diagnostics.Debug.WriteLine("GL Message --- " + message);

			//System.Threading.WaitCallback callback = (obj) =>
			//{

			//};

			//System.Threading.ThreadPool.QueueUserWorkItem(callback, null);


			/*
			//new Task(() =>
			//{
			if (id == 131185) return;
			else
			{
				System.Diagnostics.Debug.WriteLine("GL Message --- " +
					"\nID: " + id +
					"\nSource: " + source +
					"\nType: " + type +
					"\nSeverity: " + severity +
					"\nMessage: " + Marshal.PtrToStringAnsi(message, length) + "\n");
			}
				//}).Start();
			*/

		}
#endif

		public string GetHighestShaderProfile()
		{
			return GL.GetString(StringName.ShadingLanguageVersion);
		}

		public override void Dispose()
		{

		}
	}
}
#endif