﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;

using Vk = VulkanSharp;
using Base = NetherealSharp.Core.Graphics;


namespace NetherealSharp.Core.Graphics.Vulkan
{
	/// <summary>
	/// Location of suitable queue families.
	/// </summary>
	public struct QueueFamilyIndices
	{
		public uint Graphics;
		public uint Compute;
		public uint Transfer;
	}

	public class GraphicsDevice : Base.GraphicsDevice
	{
		public Vk.Instance Instance;

		public Vk.PhysicalDevice PhysicalDevice;
		public Vk.PhysicalDeviceProperties Properties;
		public Vk.PhysicalDeviceFeatures Features;
		public Vk.PhysicalDeviceMemoryProperties MemoryProperties;
		public Vk.QueueFamilyProperties[] QueueFamilyProperties;

		public QueueFamilyIndices QueueFamilyIndices;

		public Vk.Device LogicalDevice;


		//public Vk.CommandPool CommandPool;

		public Vk.Queue Queue;


		public Vk.CommandBuffer SetupCommandBuffer;


		public GraphicsDevice()
		{
			CreateInstance();
			CreateDevices();

			Queue = LogicalDevice.GetQueue(0, 0);

			//Immediate Context
			ImmediateContext = new Vulkan.GraphicsContext(this);
		}

		private void CreateInstance()
		{
			Implementation = GraphicsImplementation.Vulkan;

			Vk.ApplicationInfo applicationinfo = new Vk.ApplicationInfo()
			{
				ApplicationName = "NetherealSharp.Windows",
				EngineName = "NetherealSharp.Engine",
				EngineVersion = 1,
				ApiVersion = Vk.Version.Make(1, 0, 0)
			};

			var enabledLayerNames = new string[]
			{
				"VK_LAYER_LUNARG_standard_validation"
			};

			var enabledExtensionNames = new string[]
			{
				 "VK_KHR_surface", "VK_KHR_win32_surface", "VK_EXT_debug_report"
			};

			Vk.InstanceCreateInfo instanceInfo = new Vk.InstanceCreateInfo()
			{
				ApplicationInfo = applicationinfo,
				EnabledLayerCount = 1,
				EnabledLayerNames = enabledLayerNames,
				EnabledExtensionCount = 3,
				EnabledExtensionNames = enabledExtensionNames
			};

			Instance = new Vk.Instance(instanceInfo);

		}

		private void CreateDevices()
		{
			//Devices
			Vk.PhysicalDevice[] devices = Instance.EnumeratePhysicalDevices();

			var queueInfo = new Vk.DeviceQueueCreateInfo
			{
				QueuePriorities = new float[] { 1.0f }
			};
			var deviceInfo = new Vk.DeviceCreateInfo
			{
				EnabledExtensionNames = new string[] { "VK_KHR_swapchain" },
				QueueCreateInfos = new Vk.DeviceQueueCreateInfo[] { queueInfo }
			};

			PhysicalDevice = devices[0];
			Properties = PhysicalDevice.GetProperties();
			Features = PhysicalDevice.GetFeatures();
			MemoryProperties = PhysicalDevice.GetMemoryProperties();
			QueueFamilyProperties = PhysicalDevice.GetQueueFamilyProperties();

			LogicalDevice = PhysicalDevice.CreateDevice(deviceInfo);
		}

		private void CreateQueues(Vk.QueueFlags requestFlags = Vk.QueueFlags.Graphics | Vk.QueueFlags.Compute)
		{
			//Graphics Queue

			if((requestFlags & Vk.QueueFlags.Graphics) != 0)
			{
				QueueFamilyIndices.Graphics = 0;
			}
		}

		public uint GetQueueFamilyIndex(Vk.QueueFlags queueFlags)
		{

			//dedicated queue for compute (non graphics compatible queue)
			/*
			 var index = from property in QueueFamilyProperties
						where (property.QueueFlags & queueFlags) != 0
						where (property.QueueFlags & Vk.QueueFlags.Graphics) == 0
						select index;
			*/


			//Dedicated queue for compute
			if((queueFlags & Vk.QueueFlags.Compute) != 0)
			{
				try
				{
					uint index = (uint)QueueFamilyProperties
										.Select((v, i) => new { v, i })
										.First((p) =>
										{
											return ((p.v.QueueFlags & queueFlags) != 0)
												&& ((p.v.QueueFlags & Vk.QueueFlags.Graphics) == 0);
										})
										.i;

					return index;
				}
				catch (Exception e)
				{

				}


				//for (uint i = 0; i < QueueFamilyProperties.Length; i++)
				//{
				//	if(((QueueFamilyProperties[i].QueueFlags & queueFlags) != 0) && ((QueueFamilyProperties[i].QueueFlags & Vk.QueueFlags.Graphics) == 0))
				//	{
				//		return i;
				//	}
				//}
			}

			//
			if ((queueFlags & Vk.QueueFlags.Compute) != 0)
			{
				for (uint i = 0; i < QueueFamilyProperties.Length; i++)
				{
					if ((QueueFamilyProperties[i].QueueFlags & queueFlags) != 0)
					{
						return i;
					}
				}
			}

			return 0;

		}

		public uint GetMemoryType(uint typebits, Vk.MemoryPropertyFlags properties)
		{
			for(uint i = 0; i < MemoryProperties.MemoryTypeCount; i++)
			{
				if((typebits & 1) == 1)
				{
					if((MemoryProperties.MemoryTypes[i].PropertyFlags & properties) == properties)
					{
						return i;
					}
				}
				typebits >>= 1;
			}

			throw new Exception("Mem type is not found");
		}


		public override void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}


#endif