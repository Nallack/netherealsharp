﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class BlendState : Base.BlendState
    {
		D3D11.BlendState D3D11State;

		public BlendState(GraphicsDevice device)
		{

			var blendDesc = new D3D11.BlendStateDescription()
			{
				AlphaToCoverageEnable = false,
				IndependentBlendEnable = false
			};

			var rtbd = new D3D11.RenderTargetBlendDescription()
			{
				IsBlendEnabled = true,
				SourceBlend = D3D11.BlendOption.SourceAlpha,
				DestinationBlend = D3D11.BlendOption.InverseSourceAlpha,
				BlendOperation = D3D11.BlendOperation.Add,
				SourceAlphaBlend = D3D11.BlendOption.One,
				DestinationAlphaBlend = D3D11.BlendOption.One,
				AlphaBlendOperation = D3D11.BlendOperation.Add,
				RenderTargetWriteMask = D3D11.ColorWriteMaskFlags.All
			};


			for (int i = 0; i < blendDesc.RenderTarget.Length; i++)
			{
				blendDesc.RenderTarget[i] = rtbd;
			}

			D3D11State = new D3D11.BlendState(device.D3D11Device, blendDesc);
		}

		public override void Apply(Base.GraphicsDevice device)
		{
			(device as GraphicsDevice).D3D11Device.ImmediateContext.OutputMerger.BlendState = D3D11State;
		}
	}
}

#endif