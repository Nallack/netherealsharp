﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class BlendState
    {
		public static BlendState Create(GraphicsDevice device)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.BlendState(device as DX11.GraphicsDevice);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.BlendState(device as OpenGL.GraphicsDevice);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Apply(GraphicsDevice device);
    }
}
