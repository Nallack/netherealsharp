﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class BlendState : Base.BlendState
    {
		public BlendState(GraphicsDevice device)
		{

		}

		public override void Apply(Base.GraphicsDevice device)
		{
			GL.Enable(EnableCap.AlphaTest);
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			//GL.DepthFunc
			//GL.BlendFuncSeparate()
			//GL.BlendEquation
			
			//GL.StencilFunc
			//GL.StencilFuncSeparate
			//GL.StencilOp
			//GL.StencilOpSeparate
		}
    }
}
#endif