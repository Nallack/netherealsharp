﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics.Device
{
    public struct BlendDescription
    {
		public bool IsBlendEnabled;

#if DIRECTX11
		public static implicit operator SharpDX.Direct3D11.BlendStateDescription(BlendDescription value)
		{
			return new SharpDX.Direct3D11.BlendStateDescription();
		}
#endif
    }
}
