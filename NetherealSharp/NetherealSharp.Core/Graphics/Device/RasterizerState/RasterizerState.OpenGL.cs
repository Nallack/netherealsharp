﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;

namespace NetherealSharp.Core.Graphics
{
    public class RasterizerState
    {
		public RasterizerState()
		{

		}


		public void Apply()
		{
			GL.CullFace(CullFaceMode.Back);
			
		}
    }
}
#endif