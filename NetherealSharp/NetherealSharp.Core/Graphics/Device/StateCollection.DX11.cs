﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;


using SharpDX;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class StateCollection : Base.StateCollection
    {
		public D3D11.SamplerState SamplerDefault;
		public D3D11.SamplerState SamplerComparisionShadow;
		public D3D11.SamplerState SamplerShadow;

		public D3D11.BlendState BlendDefault;
		public D3D11.RasterizerState RasterizerDefault;
		public D3D11.DepthStencilState DepthStencilDefault;

		public StateCollection(D3D11.Device device)
		{
			var samplerDesc = new D3D11.SamplerStateDescription()
			{
				ComparisonFunction = D3D11.Comparison.Less,
				BorderColor = Color.Black,
				Filter = D3D11.Filter.Anisotropic,
				MaximumAnisotropy = 8,
				AddressU = D3D11.TextureAddressMode.Mirror,
				AddressV = D3D11.TextureAddressMode.Mirror,
				AddressW = D3D11.TextureAddressMode.Mirror,
                MaximumLod = 4,
                MinimumLod = 0,
                MipLodBias = 0
			};

			var lessEqualsDesc = new D3D11.SamplerStateDescription()
			{
				ComparisonFunction = D3D11.Comparison.LessEqual,
				BorderColor = Color.White,
				Filter = D3D11.Filter.ComparisonMinMagLinearMipPoint,
				MaximumAnisotropy = 8,
				MaximumLod = 4,
				MinimumLod = 0,
				AddressU = D3D11.TextureAddressMode.Border,
				AddressV = D3D11.TextureAddressMode.Border,
				AddressW = D3D11.TextureAddressMode.Mirror,
				MipLodBias = 0
			};

			var shadowDesc = new D3D11.SamplerStateDescription()
			{
				ComparisonFunction = D3D11.Comparison.LessEqual,
				BorderColor = Color.White,
				Filter = D3D11.Filter.Anisotropic,
				//Filter = D3D11.Filter.MinMagMipLinear,
				//Filter = D3D11.Filter.MinMagPointMipLinear,
				MaximumAnisotropy = 8,
				MaximumLod = 4,
				MinimumLod = 0,
				AddressU = D3D11.TextureAddressMode.Border,
				AddressV = D3D11.TextureAddressMode.Border,
				AddressW = D3D11.TextureAddressMode.Mirror,
				MipLodBias = 0
			};

			SamplerDefault = new D3D11.SamplerState(device, samplerDesc);
			SamplerComparisionShadow = new D3D11.SamplerState(device, lessEqualsDesc);
			SamplerShadow = new D3D11.SamplerState(device, shadowDesc);

			//ShadowSampler = new D3D11.SamplerState(graphics.D3DDevice, )





			var blendDesc = new D3D11.BlendStateDescription()
			{
				AlphaToCoverageEnable = false,
				IndependentBlendEnable = false
			};

			//transparency
			//thankyou http://www.gamedev.net/topic/596801-d3d11-alpha-transparency-trouble/
			var rtbd = new D3D11.RenderTargetBlendDescription()
			{
				IsBlendEnabled = true,
				SourceBlend = D3D11.BlendOption.SourceAlpha,
				DestinationBlend = D3D11.BlendOption.InverseSourceAlpha,
				BlendOperation = D3D11.BlendOperation.Add,
				SourceAlphaBlend = D3D11.BlendOption.One,
				DestinationAlphaBlend = D3D11.BlendOption.One,
				AlphaBlendOperation = D3D11.BlendOperation.Add,
				RenderTargetWriteMask = D3D11.ColorWriteMaskFlags.All
			};

			//default
			//var rtbd = new D3D11.RenderTargetBlendDescription()
			//{
			//	IsBlendEnabled = false,
			//	SourceBlend = D3D11.BlendOption.One,
			//	DestinationBlend = D3D11.BlendOption.Zero,
			//	BlendOperation = D3D11.BlendOperation.Add,
			//	SourceAlphaBlend = D3D11.BlendOption.One,
			//	DestinationAlphaBlend = D3D11.BlendOption.Zero,
			//	AlphaBlendOperation = D3D11.BlendOperation.Add,
			//	RenderTargetWriteMask = D3D11.ColorWriteMaskFlags.All
			//};

			for (int i = 0; i < blendDesc.RenderTarget.Length; i++)
			{
				blendDesc.RenderTarget[i] = rtbd;
			}



			BlendDefault = new D3D11.BlendState(device, blendDesc);




			var rastDesc = new D3D11.RasterizerStateDescription()
			{
				FillMode = D3D11.FillMode.Solid,
				CullMode = D3D11.CullMode.Back,
				IsFrontCounterClockwise = false,
				DepthBias = 0,
				DepthBiasClamp = 0,
				SlopeScaledDepthBias = 0,
				IsDepthClipEnabled = true,
				IsScissorEnabled = false,
				IsMultisampleEnabled = true,
				IsAntialiasedLineEnabled = true
			};

			RasterizerDefault = new D3D11.RasterizerState(device, rastDesc);
		}

		public override void Dispose()
		{
			SamplerDefault.Dispose();
			SamplerComparisionShadow.Dispose();
			BlendDefault.Dispose();
			RasterizerDefault.Dispose();
			//DepthStencilDefault.Dispose();
		}
	}
}
#endif