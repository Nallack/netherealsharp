﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{

	/// <summary>
	/// A Collection of states marked at integer location
	/// </summary>
    public abstract class StateCollection : IDisposable
    {
		//SamplerState - OpenGL defines this at texture creation
		//may want allow passing a samplerstate desc with texture creation


		//BlendState

		//RasterizerState

		//DepthStencilState

		public abstract void Dispose();

    }
}
