﻿#if DIRECTX12
using System;
using System.Collections.Generic;
using System.Text;

using DXGI = SharpDX.DXGI;
using D3D = SharpDX.Direct3D;
using D3D12 = SharpDX.Direct3D12;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class GraphicsDevice : Base.GraphicsDevice
	{
		//public D3D12.DebugDevice

		public DXGI.Factory4 DXGIFactory;

		public DXGI.Adapter Adapter;
		public D3D12.Device D3DDevice;

		public D3D12.CommandAllocator D3D12CommandAllocator;
		public D3D12.CommandQueue D3D12CommandQueue;

		//Commandlist that has the default GraphicsPipeline
		public D3D12.GraphicsCommandList D3D12DefaultCommandList;

		//Commandlist of the currently bounded Effect
		public D3D12.GraphicsCommandList STActiveCommandList { get; set; }

		public D3D12.DescriptorHeap RTVDesciptorHeap { get; protected set; }
		public D3D12.DescriptorHeap CBDescriptorHeap { get; protected set; }

		public GraphicsDevice()
		{
			D3D12.DebugInterface.Get().EnableDebugLayer();

			DXGIFactory = new DXGI.Factory4();
			Adapter = DXGIFactory.Adapters[0];

			D3DDevice = new D3D12.Device(Adapter, D3D.FeatureLevel.Level_12_0);

			RTVDesciptorHeap = D3DDevice.CreateDescriptorHeap(new D3D12.DescriptorHeapDescription()
			{
				Type = D3D12.DescriptorHeapType.RenderTargetView,
				DescriptorCount = 1,
				Flags = D3D12.DescriptorHeapFlags.None
			});

			CBDescriptorHeap = D3DDevice.CreateDescriptorHeap(new D3D12.DescriptorHeapDescription()
			{
				Type = D3D12.DescriptorHeapType.ConstantBufferViewShaderResourceViewUnorderedAccessView,
				DescriptorCount = 1,
				Flags = D3D12.DescriptorHeapFlags.ShaderVisible,
			});

			D3D12CommandAllocator = D3DDevice.CreateCommandAllocator(D3D12.CommandListType.Direct);

			var cqdesc = new D3D12.CommandQueueDescription()
			{
				Type = D3D12.CommandListType.Direct
			};

			//D3D12.PipelineState d = D3DDevice.CreateGraphicsPipelineState(new D3D12.GraphicsPipelineStateDescription()
			//{
			//	s
			//});

			D3D12CommandQueue = D3DDevice.CreateCommandQueue(cqdesc);
			D3D12DefaultCommandList = D3DDevice.CreateCommandList(D3D12.CommandListType.Direct, D3D12CommandAllocator, null);
			D3D12DefaultCommandList.Close();

			//D3DDevice.CreateFence(0, D3D12.FenceFlags.None);


		}

		public override void Begin(RenderTarget target)
		{
			//This will only reset when the command has finished executing on GPU.
			D3D12CommandAllocator.Reset();
			D3D12DefaultCommandList.Reset(D3D12CommandAllocator, null);
		}

		public override void End(RenderTarget target)
		{
			var dxt = target as DX12.ControlTarget;
			D3D12DefaultCommandList.ResourceBarrierTransition(dxt.RenderTargetView, D3D12.ResourceStates.RenderTarget, D3D12.ResourceStates.Present);

			D3D12DefaultCommandList.Close();

			D3D12CommandQueue.ExecuteCommandList(D3D12DefaultCommandList);
		}

		public override void Dispose()
		{
			//throw new NotImplementedException();
		}
	}
}
#endif