﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	//A Context that is capable of executing thread safe GPU commands.
    public abstract class GraphicsContext : IDisposable
    {

		public IRenderTarget ActiveRenderTarget { get; protected set; }

		public abstract void SetRenderTarget(IRenderTarget target);

		public abstract void SetEmptyTarget();

		public abstract void SetViewport(float x, float y, float width, float height);

		public abstract void ClearTarget(IRenderTarget target, Color4 color);

		public abstract void ClearRenderTargetView(IRenderTarget target, Color4 color);

		public abstract void ClearDepthStencilView(IRenderTarget target);

		public virtual void Begin(RenderTarget target) { }

		public virtual void End(RenderTarget target) { }




		//Could alternatively make a seperate texture2Darray class and put the method there
		//public abstract void 
		public abstract void SetShaderResources(int slot, ITexture[] textures);

		public abstract void EmptyShaderResources(int slot, int count);

		public abstract void EmptyShaderResource(int slot);

		public abstract void Dispose();

    }
}
