﻿#if OPENGLES

using System;


#if   OPENGLES_31
using OpenTK.Graphics.ES31;
#elif OPENGLES_30
using OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.ES20;
#endif

using Base = NetherealSharp.Core.Graphics;
using Andrd = Android;


namespace NetherealSharp.Core.Graphics.OpenGLES
{
	/// <summary>
	/// OpenGL Contexts for some reason require a default render target which must be used in context creation.
	/// </summary>
    public class GraphicsDevice : Base.GraphicsDevice
    {
#if OPENFONT
		public SharpFont.Library FontLibrary;
#endif
		public ShaderPreprocessor GLSLPreprocessor;

		//GRAPHICS
		public OpenTK.Graphics.IGraphicsContext GLContext { get; private set; }

		public GraphicsDevice()
		{
			Implementation = GraphicsImplementation.OpenGLES;
#if OPENFONT
			FontLibrary = new SharpFont.Library();
#endif

			GLSLPreprocessor = new ShaderPreprocessor();

			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.CullFace);
			GL.CullFace(CullFaceMode.Back);
			GL.FrontFace(FrontFaceDirection.Cw);

			//GL.Enable(EnableCap.AlphaTest);
			GL.Enable(EnableCap.Blend);
			GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);


			ImmediateContext = new GraphicsContext(this, true);
			//CreateDeviceIndependantResources();

		}

		public string GetHighestShaderProfile()
		{
			return GL.GetString(StringName.ShadingLanguageVersion);
		}

		public override void Dispose()
		{

		}
	}
}
#endif