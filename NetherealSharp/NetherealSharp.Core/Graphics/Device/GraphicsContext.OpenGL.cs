﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Graphics;
using OpenTK;
using OpenTK.Graphics.OpenGL;


#if ANDROID
using OpenTK.Platform.Android;
#endif

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class GraphicsContext : Base.GraphicsContext
    {
		public OpenTK.Graphics.IGraphicsContext GLContext { get; private set; }

		public GraphicsContext(GraphicsDevice device, bool immediate)
		{
			if (immediate)
				GLContext = OpenTK.Graphics.GraphicsContext.CurrentContext;
			else
				GLContext = new OpenTK.Graphics.GraphicsContext(OpenTK.Graphics.GraphicsMode.Default, null);
		}

#if OPENGL
		public GraphicsContext(GLControl control)
		{
			GLContext = control.Context;
		}
#endif
#if ANDROID
		public GraphicsContext(AndroidGameView view)
		{
			GLContext = view.GraphicsContext;
		}
#endif

		public override void SetRenderTarget(IRenderTarget target)
		{
			//var gltarget = target as GLControlTarget;
			//var glcontrol = gltarget.Control as GLControl;
			//glcontrol.MakeCurrent();

			ActiveRenderTarget = target;
		}

		public override void SetEmptyTarget()
		{
			//var glcontrol = ActiveRenderTarget as GLControl;
			//glcontrol.Context.MakeCurrent(null);
		}

		public override void ClearTarget(IRenderTarget target, Color4 color)
		{
			//var glTarget = target as GLControlTarget;
			//GL.Viewport(glTarget.Control.ClientRectangle);

			GL.ClearColor(color.Red, color.Green, color.Blue, color.Alpha);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
		}

		public override void SetViewport(float x, float y, float width, float height)
		{
			GL.Viewport((int)x, (int)y, (int)width, (int)height);
		}

		public override void ClearRenderTargetView(IRenderTarget target, Color4 color)
		{
			GL.ClearColor(color.Red, color.Green, color.Blue, color.Alpha);
			GL.Clear(ClearBufferMask.ColorBufferBit);
		}

		public override void ClearDepthStencilView(IRenderTarget target)
		{
			GL.Clear(ClearBufferMask.DepthBufferBit);
		}

		public override void SetShaderResources(int slot, ITexture[] textures)
		{
			int n = textures.Length;
			for(int i = 0; i < n; i++)
			{
				GL.ActiveTexture(TextureUnit.Texture0 + slot);
				GL.BindTexture(TextureTarget.Texture2D, (textures[i] as IGLTexture2D).TextureId);
				GL.Uniform1(slot, slot);
			}
		}

		public override void EmptyShaderResources(int slot, int count)
		{
			for (int i = 0; i < count; i++)
			{
				GL.ActiveTexture(TextureUnit.Texture0 + slot);
				GL.BindTexture(TextureTarget.Texture2D, 0);
			}
		}

		public override void EmptyShaderResource(int slot)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}

		public override void Dispose()
		{
			GLContext.Dispose();
		}
	}
}
#endif