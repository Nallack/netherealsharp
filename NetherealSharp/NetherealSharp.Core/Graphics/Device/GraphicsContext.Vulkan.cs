﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using Vk = VulkanSharp;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{
	/// <summary>
	/// Immediate mode graphics context.
	/// Single CommandBuffer and queue.
	/// Usage: Begin -> RenderPassBegin -> BindPipeline -> BindVertex/Indexbuffers -> draw -> End
	/// </summary>
	public class GraphicsContext : Base.GraphicsContext
	{
		public GraphicsDevice Device { get; }

		public Vk.CommandPool CommandPool { get; }

		public IVKRenderTarget ActiveVKRenderTarget { get; set; }

		/// <summary>
		/// This seems to never actually get used in the examples
		/// </summary>
		protected Vk.CommandBuffer SetupCommandBuffer { get; private set; }

		public Vk.Queue	Queue { get; private set; }
		public Vk.Fence Fence { get; private set; }

		public GraphicsContext(GraphicsDevice device)
		{
			Device = device;
			Queue = device.LogicalDevice.GetQueue(0, 0);

			var createPoolInfo = new Vk.CommandPoolCreateInfo()
			{
				Flags = Vk.CommandPoolCreateFlags.ResetCommandBuffer,
				//QueueFamilyIndex = 0
				QueueFamilyIndex = Device.GetQueueFamilyIndex(Vk.QueueFlags.Compute)
			};
			CommandPool = Device.LogicalDevice.CreateCommandPool(createPoolInfo);


			//Synchorisation objects
			var fenceInfo = new Vk.FenceCreateInfo();
			Fence = Device.LogicalDevice.CreateFence(fenceInfo);
			//var semaphoreInfo = new Vk.SemaphoreCreateInfo();
			//Semaphore = Device.LogicalDevice.CreateSemaphore(semaphoreInfo);
		}

		private void CreateSetupCommandBuffer()
		{
			if (SetupCommandBuffer != null)
			{
				Device.LogicalDevice.FreeCommandBuffers(CommandPool, 1, SetupCommandBuffer);
			}

			var commandBufferAllocationInfo = new Vk.CommandBufferAllocateInfo
			{
				Level = Vk.CommandBufferLevel.Primary,
				CommandPool = CommandPool,
				CommandBufferCount = 1
			};

			SetupCommandBuffer = Device.LogicalDevice.AllocateCommandBuffers(commandBufferAllocationInfo)[0];
		}

		//TODO
		//Buffers now can be preloaded and executed many times
		//no need to call these every frame
		//may want to consider having swapCommandBuffers
		public override void Begin(Base.RenderTarget target)
		{
			ActiveVKRenderTarget = target as IVKRenderTarget;

			foreach(var cmdBuffer in ActiveVKRenderTarget.DrawCmdBuffers)
			{
				var cmdBufferBeginInfo = new Vk.CommandBufferBeginInfo()
				{
					//Flags = Vk.CommandBufferUsageFlags.OneTimeSubmit
				};

				cmdBuffer.Begin(cmdBufferBeginInfo);
			}
		}

		/// <summary>
		/// Single command that pass to CommandBuffer:
		///	Framebuffer
		/// ClearValues
		/// Viewport
		/// RenderPass
		///		
		/// </summary>
		/// <param name="target"></param>
		/// <param name="color"></param>
		public void BeginRenderPass(IRenderTarget target, Color4 color)
		{
			var vkTarget = target as IVKRenderTarget;

			for(int i = 0; i < vkTarget.DrawCmdBuffers.Length; i++)
			{
				var cmdBuffer = vkTarget.DrawCmdBuffers[i];

				var renderPassBeginInfo = new Vk.RenderPassBeginInfo()
				{
					Framebuffer = vkTarget.Framebuffers[i],
					RenderPass = vkTarget.RenderPass,
					ClearValues = new[]
					{
						//color
						new Vk.ClearValue()
						{
							Color = {
								Float32 = new[]{ 0.0f, 0.0f, 0.2f, 1.0f }
							}
						},
						//depth
						new Vk.ClearValue()
						{
							DepthStencil = new Vk.ClearDepthStencilValue
							{
								Depth = 1.0f,
								Stencil = 0u
							}
						}
					},
					RenderArea = new Vk.Rect2D()
					{
						Extent =
						{
							Width = (uint)target.Width,
							Height = (uint)target.Height
						}
					}
				};

				cmdBuffer.CmdBeginRenderPass(renderPassBeginInfo, Vk.SubpassContents.Inline);
			}
		}

		/// <summary>
		/// Binds a pipeline object with pipelinelayout and all of its descriptorsets.
		/// No dynamic offset configured
		/// </summary>
		/// <param name="pipeline"></param>
		public void BindPipeline(GraphicsPipeline pipeline)
		{
			foreach (var cmdBuffer in ActiveVKRenderTarget.DrawCmdBuffers)
			{
				cmdBuffer.CmdBindDescriptorSets(
				Vk.PipelineBindPoint.Graphics,
				pipeline.Layout,
				0,
				(uint)pipeline.DescriptorSets.Length,
				pipeline.DescriptorSets,
				0,
				0);

				cmdBuffer.CmdBindPipeline(Vk.PipelineBindPoint.Graphics, pipeline.Pipeline);
			}
		}

		public void EndRenderPass()
		{
			foreach (var cmdBuffer in ActiveVKRenderTarget.DrawCmdBuffers)
			{
				cmdBuffer.CmdEndRenderPass();
			}
		}

		public void End()
		{
			foreach (var cmdBuffer in ActiveVKRenderTarget.DrawCmdBuffers)
			{
				cmdBuffer.End();
			}
		}

		public void ClearCommandBuffer()
		{
			foreach (var cmdBuffer in ActiveVKRenderTarget.DrawCmdBuffers)
			{
				SetupCommandBuffer.Reset(Vk.CommandBufferResetFlags.ReleaseResources);
			}
		}


#region Legacy
		public override void ClearDepthStencilView(IRenderTarget target)
		{
			var vktarget = target as IVKRenderTarget;
			for (int i = 0; i < vktarget.DrawCmdBuffers.Length; i++)
			{
				var cmdBuf = vktarget.DrawCmdBuffers[i];

				cmdBuf.CmdClearDepthStencilImage(
					vktarget.DepthStencil.Image,
					Vk.ImageLayout.DepthStencilAttachmentOptimal,
					new Vk.ClearDepthStencilValue
					{
						Depth = 1.0f,
						Stencil = 0
					},
					1,
					new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Depth,
						BaseMipLevel = 0,
						LevelCount = 1,
						BaseArrayLayer = 0,
						LayerCount = 1
					});
			}
		}

		public override void ClearRenderTargetView(IRenderTarget target, Color4 color)
		{
			var vktarget = target as IVKRenderTarget;
			for (int i = 0; i < vktarget.DrawCmdBuffers.Length; i++)
			{
				var cmdBuf = vktarget.DrawCmdBuffers[i];

				cmdBuf.CmdClearColorImage(
					vktarget.RenderTargets[i].Image,
					VulkanSharp.ImageLayout.ColorAttachmentOptimal,
					new Vk.ClearColorValue(color.ToArray()),
					1,
					new Vk.ImageSubresourceRange
					{
						AspectMask = VulkanSharp.ImageAspectFlags.Color,
						BaseMipLevel = 0,
						LevelCount = 1,
						BaseArrayLayer = 0,
						LayerCount = 1
					});
			}
		}

		public override void ClearTarget(IRenderTarget target, Color4 color)
		{
			var vktarget = target as IVKRenderTarget;
			for (int i = 0; i < vktarget.DrawCmdBuffers.Length; i++)
			{
				var cmdBuf = vktarget.DrawCmdBuffers[i];

				cmdBuf.CmdClearAttachments(
					1,
					new Vk.ClearAttachment
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						//ClearValue = new Vk.ClearColorValue(color.ToArray()),
						ColorAttachment = 0
					},
					1,
					new Vk.ClearRect
					{
						LayerCount = 1,
						BaseArrayLayer = 1,
						Rect = new Vk.Rect2D
						{
							Offset = { X = 0, Y = 0 },
							Extent = { Width = (uint)target.Width, Height = (uint)target.Height }
						}
					});
			}
		}



		public override void EmptyShaderResource(int slot)
		{
			throw new NotImplementedException();
		}

		public override void EmptyShaderResources(int slot, int count)
		{
			throw new NotImplementedException();
		}

		public override void SetEmptyTarget()
		{
			throw new NotImplementedException();
		}

		public override void SetRenderTarget(IRenderTarget target)
		{
			throw new NotImplementedException();
		}

		public override void SetShaderResources(int slot, ITexture[] textures)
		{
			throw new NotImplementedException();
		}

		public override void SetViewport(float x, float y, float width, float height)
		{
			/*
			CommandBuffer.CmdSetViewport(0, 1, new Vk.Viewport()
			{
				X = x,
				Y = y,
				Width = width,
				Height = height
			});
			*/
		}


		public override void End(Base.RenderTarget target)
		{
			/*
			//Vk.DescriptorType.
			CommandBuffer.CmdEndRenderPass();
			CommandBuffer.End();
			*/
		}
		#endregion

		public override void Dispose()
		{
		}
	}
}

#endif