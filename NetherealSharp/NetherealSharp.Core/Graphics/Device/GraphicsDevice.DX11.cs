﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using SharpDX.Direct3D;

using D3D = SharpDX.Direct3D;
using D3DC = SharpDX.D3DCompiler;
using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;
using DXGI = SharpDX.DXGI;
using WIC = SharpDX.WIC;
using DW = SharpDX.DirectWrite;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Wraps an implementing graphics device and immediate context.
	/// May want to add a sepearte context class once discovered how in OpenGL.
	/// </summary>
	public class GraphicsDevice : Base.GraphicsDevice
	{

		public SharpFont.Library FontLibrary;

		public DXGI.Factory2 DXGIFactory;
		public DXGI.Adapter DXGIAdapter;
		public WIC.ImagingFactory ImagingFactory;
		public D2D1.Factory2 D2D1Factory;
		public DW.Factory1 DWFactory;

		public D3D11.Device3 D3D11Device;
		public D2D1.Device1 D2D1Device;

		public GraphicsDevice()
		{
			Implementation = GraphicsImplementation.DirectX11;
			CreateSizeIndependantResources();
		}

		private void CreateSizeIndependantResources()
		{
			DXGIFactory = new DXGI.Factory2();
			DXGIAdapter = DXGIFactory.Adapters[0];

			D2D1Factory = new D2D1.Factory2(D2D1.FactoryType.SingleThreaded, D2D1.DebugLevel.Warning);
			ImagingFactory = new WIC.ImagingFactory();
			DWFactory = new DW.Factory1(DW.FactoryType.Shared);

			//D3D
			var creationFlags = D3D11.DeviceCreationFlags.BgraSupport;
#if DEBUG
			creationFlags |= D3D11.DeviceCreationFlags.Debug;
#endif


            using (var d3d11Device = new D3D11.Device(DXGIAdapter, creationFlags))
            {
                D3D11Device = d3d11Device.QueryInterface<D3D11.Device3>();
            }

            //D3D11Device = new D3D11.Device(DriverType.Hardware, creationFlags);

            //D2D
            using (var dxgiDevice = D3D11Device.QueryInterface<DXGI.Device2>())
            {
                D2D1Device = new D2D1.Device1(D2D1Factory, dxgiDevice);
            }
				


			//May want to move this elsewhere, maybe to an abstract 2D context
			FontLibrary = new SharpFont.Library();


			//Immediate Context
			ImmediateContext = new DX11.GraphicsContext(this, true);
		}

		/// <summary>
		/// Utility method for shader compilation.
		/// </summary>
		/// <param name="version"></param>
		/// <returns></returns>
		public string HighestShaderProfile(D3DC.ShaderVersion version)
		{
			string v, lvl;
			switch(version)
			{
				case D3DC.ShaderVersion.VertexShader:
					v = "vs";
					break;
				case D3DC.ShaderVersion.GeometryShader:
					v = "gs";
					break;
				case D3DC.ShaderVersion.HullShader:
					v = "hs";
					break;
				case D3DC.ShaderVersion.DomainShader:
					v = "ds";
					break;
				case D3DC.ShaderVersion.PixelShader:
					v = "ps";
					break;
				default:
					throw new NotImplementedException();
			}
			switch(D3D11Device.FeatureLevel)
			{
				case FeatureLevel.Level_12_1:
				case FeatureLevel.Level_12_0:
				case FeatureLevel.Level_11_1:
				case FeatureLevel.Level_11_0:
					lvl = "5_0";
					break;
				case FeatureLevel.Level_10_1:
					lvl = "4_1";
					break;
				case FeatureLevel.Level_10_0:
					lvl = "4_0";
					break;
				case FeatureLevel.Level_9_3:
				case FeatureLevel.Level_9_2:
				case FeatureLevel.Level_9_1:
					lvl = "2_0";
					break;
				default:
					throw new NotImplementedException();
			}
			return string.Format("{0}_{1}", v, lvl);
		}

		public override void Dispose()
		{
			FontLibrary.Dispose();
			ImmediateContext.Dispose();

			D2D1Device.Dispose();
			D3D11Device.Dispose();

			D2D1Factory.Dispose();
			DWFactory.Dispose();
			ImagingFactory.Dispose();

			DXGIFactory.Dispose();
			DXGIAdapter.Dispose();
		}


	}
}
#endif