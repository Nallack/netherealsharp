﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics.Pipeline
{
	/// <summary>
	/// A contains a shaderpipeline, and relevent VK.RenderPass info and extra Context State settings.
	/// </summary>
    class RenderPass
    {
		public ShaderPipeline ShaderPipeline { get; set; }

		//LoadOp, etc.
    }
}
