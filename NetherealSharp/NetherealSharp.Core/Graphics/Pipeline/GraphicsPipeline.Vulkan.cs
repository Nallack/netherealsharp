﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Text;

using Vk = VulkanSharp;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{
    public class GraphicsPipeline : Base.GraphicsPipeline
    {
		public GraphicsDevice Device;
		public Vk.PipelineLayout Layout;
		public Vk.DescriptorSet[] DescriptorSets;

		public Vk.Pipeline Pipeline;

		public GraphicsPipeline(GraphicsDevice device)
		{
			Device = device;
		}

		/// <summary>
		/// Create a single graphics pipeline
		/// </summary>
		/// <param name="device"></param>
		/// <param name="target"></param>
		public GraphicsPipeline(
			GraphicsDevice device, 
			IRenderTarget target, 
			ViewportF viewport,
			UniformBuffer[] uniforms,
			RenderState renderstate,
			ShaderPipeline shaderpipeline,
			Mesh mesh)
		{
			Device = device;

			#region uniforms
			#endregion

			#region viewport
			var vktarget = (target as Vulkan.IVKRenderTarget);
			var targetViewport = new Vk.Viewport()
			{
				MinDepth = 0,
				MaxDepth = 1.0f,
				Width = target.Width,
				Height = target.Height
			};
			var scissor = new Vk.Rect2D
			{
				Extent =
				{
					Width = (uint)target.Width,
					Height = (uint)target.Height
				}
			};
			var viewportCreateInfo = new Vk.PipelineViewportStateCreateInfo()
			{
				Viewports = new Vk.Viewport[] { targetViewport },
				Scissors = new Vk.Rect2D[] { scissor }
			};
			#endregion

			#region renderstate
			//use default
			#endregion

			#region meshes
			//use non
			#endregion

			#region shaderstages
			//use non
			#endregion

			#region renderpass
			#endregion

		}

		//TODO: Demo code, need to break apart
		/// <summary>
		/// Pipeline object used by VulkanSharp.
		/// Contains GPU handles to:
		///	- Shader Stages
		///	- Meshes
		///	- Viewport Info
		///	- RenderPass
		/// </summary>
		/// <param name="device"></param>
		public GraphicsPipeline(GraphicsDevice device, IRenderTarget target, bool testingonly)
		{
			Device = device;

			#region uniforms
			var layoutBinding = new Vk.DescriptorSetLayoutBinding
			{
				DescriptorType = Vk.DescriptorType.UniformBuffer,
				DescriptorCount = 1,
				StageFlags = Vk.ShaderStageFlags.Vertex
			};
			var descriptorSetLayoutInfo = new Vk.DescriptorSetLayoutCreateInfo()
			{
				Bindings = new Vk.DescriptorSetLayoutBinding[] { layoutBinding }
			};
			var descriptorSetLayout = device.LogicalDevice.CreateDescriptorSetLayout(descriptorSetLayoutInfo);
			var pipelineLayoutCreateInfo = new Vk.PipelineLayoutCreateInfo()
			{
				SetLayouts = new Vk.DescriptorSetLayout[] { descriptorSetLayout }
			};
			var pipelineLayout = device.LogicalDevice.CreatePipelineLayout(pipelineLayoutCreateInfo);
			#endregion

			#region viewports
			var vktarget = (target as Vulkan.IVKRenderTarget);
			var targetViewport = new Vk.Viewport()
			{
				MinDepth = 0,
				MaxDepth = 1.0f,
				Width = target.Width,
				Height = target.Height
			};
			var scissor = new Vk.Rect2D
			{
				Extent =
				{
					Width = (uint)target.Width,
					Height = (uint)target.Height
				}
			};
			var viewportCreateInfo = new Vk.PipelineViewportStateCreateInfo()
			{
				Viewports = new Vk.Viewport[] { targetViewport },
				Scissors = new Vk.Rect2D[] { scissor }
			};
			#endregion

			#region renderstate
			var multisampleCreateInfo = new Vk.PipelineMultisampleStateCreateInfo()
			{
				RasterizationSamples = Vk.SampleCountFlags.Count1,
				SampleMask = new uint[] { 0u }
			};
			var colorBlendAttachmentState = new Vk.PipelineColorBlendAttachmentState
			{
				ColorWriteMask = Vk.ColorComponentFlags.R
								| Vk.ColorComponentFlags.G
								| Vk.ColorComponentFlags.B
								| Vk.ColorComponentFlags.A
			};
			var colorBlendStateCreatInfo = new Vk.PipelineColorBlendStateCreateInfo
			{
				LogicOp = Vk.LogicOp.Copy,
				Attachments = new Vk.PipelineColorBlendAttachmentState[] { colorBlendAttachmentState }
			};
			var rasterizationStateCreateInfo = new Vk.PipelineRasterizationStateCreateInfo
			{
				PolygonMode = Vk.PolygonMode.Fill,
				CullMode = (uint)Vk.CullModeFlags.None,
				FrontFace = Vk.FrontFace.Clockwise,
				LineWidth = 1.0f
			};
			#endregion

			#region meshes
			var inputAssemblyStateCreateInfo = new Vk.PipelineInputAssemblyStateCreateInfo()
			{
				Topology = Vk.PrimitiveTopology.TriangleList
			};
			var vertexInputBindingDescription = new Vk.VertexInputBindingDescription
			{
				Stride = 3 * sizeof(float),
				InputRate = Vk.VertexInputRate.Vertex
			};
			var vertexInputAttributeDescription = new Vk.VertexInputAttributeDescription
			{
				Format = Vk.Format.R32G32B32Sfloat
			};
			var vertexInputStateCreateInfo = new Vk.PipelineVertexInputStateCreateInfo
			{
				VertexBindingDescriptions = new Vk.VertexInputBindingDescription[] { vertexInputBindingDescription },
				VertexAttributeDescriptions = new Vk.VertexInputAttributeDescription[] { vertexInputAttributeDescription }
			};
			#endregion

			#region shaderpipeline
			byte[] file = new byte[] { };
			var vertexShaderModule = device.LogicalDevice.CreateShaderModule(file);
			var fragmentShaderModule = device.LogicalDevice.CreateShaderModule(file);
			var pipelineShaderStages = new Vk.PipelineShaderStageCreateInfo[]
			{
				new Vk.PipelineShaderStageCreateInfo
				{
					Stage = Vk.ShaderStageFlags.Vertex,
					Module = vertexShaderModule,
					Name = "main"
				},
				new Vk.PipelineShaderStageCreateInfo
				{
					Stage = Vk.ShaderStageFlags.Fragment,
					Module = fragmentShaderModule,
					Name = "main"
				},
			};
			#endregion

			#region renderpass
			#endregion


			var createInfo = new Vk.GraphicsPipelineCreateInfo()
			{
				Layout = pipelineLayout,
				ViewportState = viewportCreateInfo,
				Stages = pipelineShaderStages,
				MultisampleState = multisampleCreateInfo,
				ColorBlendState = colorBlendStateCreatInfo,
				RasterizationState = rasterizationStateCreateInfo,
				InputAssemblyState = inputAssemblyStateCreateInfo,
				VertexInputState = vertexInputStateCreateInfo,
				DynamicState = new Vk.PipelineDynamicStateCreateInfo()
				{
					DynamicStates = new Vk.DynamicState[]
					{
						Vk.DynamicState.BlendConstants
					}
				},
				RenderPass = vktarget.RenderPass
			};

			var cache = device.LogicalDevice.CreatePipelineCache(new Vk.PipelineCacheCreateInfo());
			Pipeline = device.LogicalDevice.CreateGraphicsPipelines(cache, 1, new Vk.GraphicsPipelineCreateInfo())[0];
		}


		public static GraphicsPipeline CreateClearOnlyPipeline(GraphicsDevice device, IRenderTarget target)
		{
			GraphicsPipeline pipeline = new GraphicsPipeline(device);

			//uniform
			var pipelineLayoutCreateInfo = new Vk.PipelineLayoutCreateInfo() { SetLayouts = new VulkanSharp.DescriptorSetLayout[0] };
			var pipelineLayout = device.LogicalDevice.CreatePipelineLayout(pipelineLayoutCreateInfo);

			//viewport
			var vktarget = (target as Vulkan.IVKRenderTarget);
			var targetViewport = new Vk.Viewport()
			{
				MinDepth = 0,
				MaxDepth = 1.0f,
				Width = target.Width,
				Height = target.Height
			};
			var scissor = new Vk.Rect2D
			{
				Extent =
				{
					Width = (uint)target.Width,
					Height = (uint)target.Height
				}
			};
			var viewportCreateInfo = new Vk.PipelineViewportStateCreateInfo()
			{
				Viewports = new Vk.Viewport[] { targetViewport },
				Scissors = new Vk.Rect2D[] { scissor }
			};

			//renderstates
			var multisampleCreateInfo = new Vk.PipelineMultisampleStateCreateInfo()
			{
				RasterizationSamples = Vk.SampleCountFlags.Count1,
				SampleMask = new uint[] { 0u }
			};

			//shader stages



			var createInfo = new Vk.GraphicsPipelineCreateInfo()
			{
				Layout = pipelineLayout,
				ViewportState = viewportCreateInfo,
				//Stages = pipelineShaderStages,
				MultisampleState = multisampleCreateInfo,
				//ColorBlendState = colorBlendStateCreatInfo,
				//RasterizationState = rasterizationStateCreateInfo,
				//InputAssemblyState = inputAssemblyStateCreateInfo,
				//VertexInputState = vertexInputStateCreateInfo,
				DynamicState = new Vk.PipelineDynamicStateCreateInfo()
				{
					DynamicStates = new Vk.DynamicState[]
					{
						Vk.DynamicState.BlendConstants
					}
				},
				RenderPass = vktarget.RenderPass
			};

			return pipeline;
		}

		public void setUniformBindings()
		{

		}


		public void setViewport(IRenderTarget target)
		{
			var vktarget = (target as Vulkan.IVKRenderTarget);
			var targetViewport = new Vk.Viewport()
			{
				MinDepth = 0,
				MaxDepth = 1.0f,
				Width = target.Width,
				Height = target.Height
			};
			var scissor = new Vk.Rect2D
			{
				Extent =
				{
					Width = (uint)target.Width,
					Height = (uint)target.Height
				}
			};
			var viewportCreateInfo = new Vk.PipelineViewportStateCreateInfo()
			{
				Viewports = new Vk.Viewport[] { targetViewport },
				Scissors = new Vk.Rect2D[] { scissor }
			};
		}

		/// <summary>
		/// Updates the pipeline object with : viewport, uniforms, shader stages, meshes, render states
		/// </summary>
		public void UpdatePipeline()
		{
			var cache = Device.LogicalDevice.CreatePipelineCache(new Vk.PipelineCacheCreateInfo());
			Pipeline = Device.LogicalDevice.CreateGraphicsPipelines(cache, 1, new Vk.GraphicsPipelineCreateInfo())[0];
		}
    }
}
#endif