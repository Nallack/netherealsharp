﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{

	public enum LightType
	{
		None = 0,
		Ambient = 1,
		Directional = 2,
		Point = 3,
		SpotLight = 4
	}
}
