﻿

#if DIRECTX11
using SharpDX.Direct3D11;
#endif
#if OPENGL
using OpenTK.Graphics.OpenGL;
#endif
#if OPENGLES
#if OPENGLES_30
using OpenTK.Graphics.ES30;
#elif OPENGLES_20
using OpenTK.Graphics.ES20;
#else
using OpenTK.Graphics.ES20;
#endif
#endif
#if VULKAN
using VulkanSharp;
#endif

namespace NetherealSharp.Core.Graphics
{
    public enum BufferUsage
    {
		Static,
		Dynamic
    }

	public static class BufferUsageConversion
	{
#if DIRECTX11
		public static ResourceUsage ToDX11(this BufferUsage value)
		{
			switch (value)
			{
				case BufferUsage.Static:
					return ResourceUsage.Default;
				case BufferUsage.Dynamic:
					return ResourceUsage.Dynamic;
				default:
					return ResourceUsage.Dynamic;
			}
		}
#endif
#if OPENGL
		public static BufferUsageHint ToOpenGL(this BufferUsage value)
		{
			switch (value)
			{
				case BufferUsage.Static:
					return BufferUsageHint.StaticDraw;
				case BufferUsage.Dynamic:
					return BufferUsageHint.DynamicDraw;
				default:
					return BufferUsageHint.DynamicDraw;
			}
		}
#endif
#if OPENGLES
		public static BufferUsage ToGLES(this BufferUsage value)
		{
			switch(value)
			{
				case BufferUsage.Static:
					return BufferUsage.Static;
				case BufferUsage.Dynamic:
					return BufferUsage.Dynamic;
				default:
					return BufferUsage.Dynamic;
			}
		}
#endif
#if VULKAN
		/*
		public static BufferUsageFlags ToVulkan(this BufferUsage value)
		{
			switch(value)
			{
				case BufferUsage.Static:
					return BufferUsageFlags.
			}
		}
		*/
#endif
	}
}
