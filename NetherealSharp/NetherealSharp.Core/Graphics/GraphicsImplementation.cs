﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public enum GraphicsImplementation
	{
		DirectX12,
		DirectX11,
		OpenGL,
		OpenGLES,
		Vulkan
	};
}
