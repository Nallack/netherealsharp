﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class Sprite
    {
		public SpriteAtlas Atlas;
		public RectangleF Rectangle;
    }
}
