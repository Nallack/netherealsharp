﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class SpriteAtlas
    {
		public Texture2D Texture;
		public List<RectangleF> SpriteCoords;


		public SpriteAtlas()
		{

		}
    }
}
