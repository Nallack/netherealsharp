﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	[DataContract]
    public abstract class Texture1D : IContentable, ITexture, IDisposable
    {
		public string Name { get; set; }
		public Type ContentType { get { return typeof(Texture1D); } }

		public static Texture1D LoadFromFile(GraphicsDevice device, string filename)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.Texture1D(device as DX11.GraphicsDevice, filename);
#endif
//#if OPENGL
//				case GraphicsImplementation.OpenGL:
//					return OpenGL.Texture1D.LoadFromFile(filename);
//#endif
				default:
					return null;
			}
		}

		public abstract void Apply(GraphicsContext context, int slot);

		public abstract void Dispose();
	}
}
