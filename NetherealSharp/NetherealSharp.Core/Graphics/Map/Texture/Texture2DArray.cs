﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public abstract class Texture2DArray : IDisposable
	{

		public static Texture2DArray LoadFromFile(GraphicsDevice device, string filename)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.Texture2DArray(device as DX11.GraphicsDevice);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Apply(int slot);

		public abstract void Dispose();
	}
}
