﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using WIC = SharpDX.WIC;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.Serialization;
namespace NetherealSharp.Core.Graphics.DX11
{
    public class Texture1D : Base.Texture1D
    {
		DX11.GraphicsDevice Device;
		public D3D11.Texture1D Texture { get; private set; }
		public D3D11.ShaderResourceView ShaderResourceView { get; private set; }
	

		public Texture1D(GraphicsDevice device)
		{
			Device = device;
		}

		//TODO finish this off
		public Texture1D(GraphicsDevice device, string filename) : this(device)
		{
			var source = LoadBitmap(device, filename);
			Texture = null;
		}

		private static WIC.BitmapSource LoadBitmap(DX11.GraphicsDevice device, string filename)
		{
			var decoder = new WIC.BitmapDecoder(
				device.ImagingFactory,
				filename,
				WIC.DecodeOptions.CacheOnDemand);

			var frame = decoder.GetFrame(0);

			var fconv = new WIC.FormatConverter(device.ImagingFactory);

			fconv.Initialize(
				frame,
				WIC.PixelFormat.Format32bppPRGBA,
				WIC.BitmapDitherType.None,
				null,
				0.0,
				WIC.BitmapPaletteType.Custom);

			return fconv;
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			ShaderResourceView.Dispose();
			Texture.Dispose();
		}
	}
}
#endif