﻿
#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
    public class Texture2D : Base.Texture2D, IGLTexture2D
    {
		public int TextureId { get; private set; }

		public Texture2D()
		{
			TextureId = GL.GenTexture();
		}

		public static new Texture2D LoadFromFile(string filename)
		{
			var bitmap = new System.Drawing.Bitmap(filename);

			var bitmapdata = bitmap.LockBits(
				new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
				System.Drawing.Imaging.ImageLockMode.ReadOnly,
				System.Drawing.Imaging.PixelFormat.Format32bppArgb
				);

			return Texture2D.LoadFromData(bitmapdata);
		}

		public static Texture2D LoadEmpty(int width, int height, TextureMinFilter minfilter, TextureMagFilter magfilter, PixelInternalFormat internalformat, PixelFormat format, PixelType type)
		{
			return LoadFromData(width, height, 1, false, minfilter, magfilter, TextureWrapMode.Clamp, internalformat, format, type, IntPtr.Zero);
		}

		public void UpdateRegion(int x, int y, int width, int height, PixelFormat format, IntPtr pixels)
		{
			GL.BindTexture(TextureTarget.Texture2D, TextureId);

			GL.TexSubImage2D(
				TextureTarget.Texture2D,
				0,
				x, y, 
				width, height,
				format,
				PixelType.UnsignedByte,
				pixels);

			GL.BindTexture(TextureTarget.Texture2D, 0);

		}

		public static Texture2D LoadFromData(System.Drawing.Imaging.BitmapData data)
		{
			return LoadFromData(
				data.Width, data.Height,
				0, false,
				TextureMinFilter.Linear, TextureMagFilter.Linear,
				TextureWrapMode.ClampToEdge,
				PixelInternalFormat.Rgba, PixelFormat.Bgra, PixelType.UnsignedByte,
				data.Scan0);

			/*
			Texture2D tex = new Texture2D();

			GL.BindTexture(TextureTarget.Texture2D, tex.TextureId);

			int aniso = 0;
			GL.GetInteger((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out aniso);
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);

			//GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (float)TextureEnvMode.Replace);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (float)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (float)TextureWrapMode.ClampToEdge);

			GL.TexImage2D(
				TextureTarget.Texture2D, 
				0, 
				PixelInternalFormat.Rgba,
				data.Width, 
				data.Height,
				0, 
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
				PixelType.UnsignedByte,
				data.Scan0);

			//GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

			GL.BindTexture(TextureTarget.Texture2D, 0);

			return tex;
			*/
		}


		public static Texture2D LoadFromData(
			int width, int height,
			int alignment, bool genmipmaps,
			TextureMinFilter minfilter,
			TextureMagFilter magfilter,
			TextureWrapMode texturewrap, 
			PixelInternalFormat internalformat,
			PixelFormat format,
			PixelType type,
			IntPtr buffer)
		{
			Texture2D tex = new Texture2D();

			GL.BindTexture(TextureTarget.Texture2D, tex.TextureId);

			float aniso = 0;
			GL.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out aniso);
			GL.TexParameter(TextureTarget.Texture2D, (TextureParameterName)ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, aniso);

			//GL.TexEnv(TextureEnvTarget.TextureEnv, TextureEnvParameter.TextureEnvMode, (float)TextureEnvMode.Modulate);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (float)minfilter);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (float)magfilter);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (float)texturewrap);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (float)texturewrap);

			if(alignment != 0) GL.PixelStore(PixelStoreParameter.UnpackAlignment, alignment);

			GL.TexImage2D(TextureTarget.Texture2D, 0, internalformat, width, height, 0, format, type, buffer);

			if(genmipmaps) GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);

			GL.BindTexture(TextureTarget.Texture2D, 0);

			return tex;
		}

		/// <summary>
		/// Applies a texture to a particular uniform slot in the pipeline.
		/// Warning: slot must match the uniform location, which is different for DX and OPENGL
		/// </summary>
		/// <param name="context"></param>
		/// <param name="slot"></param>
		public override void Apply(Base.GraphicsContext context, int slot)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2D, TextureId);
			GL.Uniform1(slot, slot);
		}

		public override void Dispose()
		{
			GL.DeleteTexture(TextureId);
		}
	}
}
#endif