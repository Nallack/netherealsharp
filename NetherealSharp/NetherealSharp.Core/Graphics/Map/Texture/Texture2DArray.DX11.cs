﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	public class Texture2DArray : Base.Texture2DArray
	{

		GraphicsDevice Device;

		D3D11.Texture2D Texture { get; set; }

		D3D11.ShaderResourceView ShaderResourceView { get; set; }

		public Texture2DArray(GraphicsDevice device)
		{
			Device = device;
		}

		public static Texture2DArray CreateFromData(GraphicsDevice device, int width, int height, int depth, DXGI.Format format)
		{
			Texture2DArray tex = new Texture2DArray(device);

			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = width,
				Height = height,
				ArraySize = depth,
				BindFlags = D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = 1,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription = { Count = 1, Quality = 0 }
			};

			tex.Texture = new D3D11.Texture2D(tex.Device.D3D11Device, desc);
			tex.ShaderResourceView = new D3D11.ShaderResourceView(tex.Device.D3D11Device, tex.Texture);

			return tex;
		}

		public override void Apply(int slot)
		{
			Device.D3D11Device.ImmediateContext.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			ShaderResourceView.Dispose();
			Texture.Dispose();
		}
	}
}
#endif