﻿
#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using WIC = SharpDX.WIC;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.Serialization;

namespace NetherealSharp.Core.Graphics.DX11
{
	[Serializable, DataContract]
	public class Texture2D : Base.Texture2D, IDX11Texture
	{
		DX11.GraphicsDevice Device;
		D3D11.Texture2D Texture { get; set; }
		public D3D11.ShaderResourceView ShaderResourceView { get; private set; }


		public Texture2D(GraphicsDevice device)
		{
			Device = device;
		}

		public Texture2D(GraphicsDevice device, string filename) : this(device)
		{
			//load source image
			var source = LoadBitmap(device, filename);

			//Texture2D tex = new Texture2D(device);
			Texture = CreateFromBitmap(device, source);
			//TODO: can potentially add a desc here
			ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, Texture);

		}

		private static WIC.BitmapSource LoadBitmap(DX11.GraphicsDevice device, string filename)
		{
			var decoder = new WIC.BitmapDecoder(
				device.ImagingFactory,
				filename,
				WIC.DecodeOptions.CacheOnDemand);

			var frame = decoder.GetFrame(0);

			var fconv = new WIC.FormatConverter(device.ImagingFactory);

			fconv.Initialize(
				frame,
				WIC.PixelFormat.Format32bppPRGBA,
				WIC.BitmapDitherType.None,
				null,
				0.0,
				WIC.BitmapPaletteType.Custom);

			return fconv;
		}

		private static D3D11.Texture2D CreateFromBitmap(GraphicsDevice device, WIC.BitmapSource source)
		{
			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = source.Size.Width,
				Height = source.Size.Height,
				ArraySize = 1,
				BindFlags = D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = DXGI.Format.R8G8B8A8_UNorm,
				MipLevels = 1,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription =
				{
					Count = 1,
					Quality = 0
				}
			};


			var s = new DataStream(source.Size.Height * source.Size.Width * 4, true, true);
			source.CopyPixels(source.Size.Width * 4, s);

			var rect = new DataRectangle(s.DataPointer, source.Size.Width * 4);

			return new D3D11.Texture2D(device.D3D11Device, desc, rect);
		}

		//TODO: only supports DXGI.R8G8B8A8
		public static Texture2D CreateFromData(GraphicsDevice device, int width, int height, DXGI.Format format, System.Drawing.Bitmap bitmap)
		{
			var data = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			return CreateFromData(device, width, height, format, data.Scan0);
		}

		public static Texture2D CreateFromData(GraphicsDevice device, int width, int height, DXGI.Format format, IntPtr buffer)
		{
			Texture2D tex = new Texture2D(device);

			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = width,
				Height = height,
				ArraySize = 1,
				BindFlags = D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = 1,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription =
				{
					Count = 1,
					Quality = 0
				}
			};

			DataRectangle r = new DataRectangle(buffer, width * 4);
			tex.Texture = new D3D11.Texture2D(tex.Device.D3D11Device, desc, r);
			tex.ShaderResourceView = new D3D11.ShaderResourceView(tex.Device.D3D11Device, tex.Texture);

			return tex;
		}



		public static Texture2D CreateEmpty(GraphicsDevice device, int width, int height, DXGI.Format format)
		{
			return CreateFromData(device, width, height, format, IntPtr.Zero);
		}

		//TODO: may want to create some sort of shader stage flags
		public override void Apply(Base.GraphicsContext context, int slot)
		{
			var c = (context as GraphicsContext).D3D11Context;
			c.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			ShaderResourceView.Dispose();
			Texture.Dispose();
		}


	}
}

#endif