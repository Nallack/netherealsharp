﻿
#if OCULUS && DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;
using OculusWrap;

using D3D11 = SharpDX.Direct3D11;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class EyeTexture : Base.EyeTexture
    {
		public TextureSwapChain SwapTextureSet { get; set; }
		public D3D11.Texture2D[] Textures;
		public D3D11.RenderTargetView[] RenderTargetViews;
		public D3D11.Texture2D DepthStencil;
		public D3D11.DepthStencilView DepthStencilView;

		public OVR32.Sizei TextureSize;
		public OVR32.Recti ViewportSize;
		public OVR32.EyeRenderDesc RenderDescription;
		public OVR32.Vector3f HmdToEyeViewOffset;

		D3D11.Texture2DDescription TextureDescription;
		D3D11.Texture2DDescription DepthBufferDescription;

    }
}
#endif