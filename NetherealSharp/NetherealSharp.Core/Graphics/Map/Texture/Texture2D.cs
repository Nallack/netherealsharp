﻿
using System;
using System.Runtime.Serialization;

namespace NetherealSharp.Core.Graphics
{
	[Serializable, DataContract]
	public abstract class Texture2D : IContentable, ITexture, IDisposable
	{
		public string Name { get; set; }
		public Type ContentType { get { return typeof(Texture2D); } }

		/// <summary>
		/// Factory method for creating a Texture from file
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static Texture2D LoadFromFile(GraphicsDevice device, string filename)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.Texture2D(device as DX11.GraphicsDevice, filename);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return OpenGL.Texture2D.LoadFromFile(filename);
#endif
				default:
					return null;
			}
		}

		//TODO: GL makes this a bit trickier as all GLSL registers are of unique location,
		//whereas HLSL registers are seperated by uniform type.

		//GL also has active texture bindings, for this id say just make them match the slot
		public abstract void Apply(GraphicsContext context, int slot);
		public abstract void Dispose();
	}
}
