﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public enum TextureLocation
    {
		Diffuse = 0,
		Normal = -1,
		ShadowMap = 1,
		//ShadowCube = 2
    }
}
