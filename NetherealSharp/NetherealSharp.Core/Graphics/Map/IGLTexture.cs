﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public interface IGLTexture2D
    {
		int TextureId { get; }
    }
}
