﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// Texture capable of being used as a shader resource. Texture may
	/// be a regular texture or a render texture.
	/// TODO: might not even need this, make rendertexture extend texture
	/// </summary>
    public interface ITexture : IDisposable
    {
		void Apply(GraphicsContext context, int slot);
    }
}
