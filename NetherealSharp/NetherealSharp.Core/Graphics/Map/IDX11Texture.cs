﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D3D11 = SharpDX.Direct3D11;

namespace NetherealSharp.Core.Graphics.DX11
{
    public interface IDX11Texture
    {
		D3D11.ShaderResourceView ShaderResourceView { get; }
	}
}
#endif