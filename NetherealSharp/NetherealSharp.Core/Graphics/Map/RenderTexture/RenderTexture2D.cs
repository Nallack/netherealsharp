﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A texture that can also serve as a rendertarget
	/// </summary>
	public abstract class RenderTexture2D : ITexture, IRenderTarget, IDisposable
	{
		public static RenderTexture2D Create(GraphicsDevice device, Size size, TextureFormat format, bool depthOnly = false)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.RenderTexture2D(device as DX11.GraphicsDevice, size.Width, size.Height, depthOnly, format.ToDXGI());
#endif
#if OPENGL
				//case GraphicsImplementation.OpenGL:
					//return new OpenGL.RenderTexture2D();
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public static RenderTexture2D Create(GraphicsDevice device, Size size, bool depthOnly = false)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.RenderTexture2D(device as DX11.GraphicsDevice, size.Width, size.Height, depthOnly, SharpDX.DXGI.Format.R32G32_Float);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract int Width { get; protected set; }
		public abstract int Height { get; protected set; }

		public abstract void GenerateMipmaps(GraphicsContext context);

		public abstract void Apply(GraphicsContext context, int slot);

		public abstract void Dispose();
	}
}
