﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class RenderTexture2DArray : IRenderTarget, ITexture, IDisposable
    {
		public static RenderTexture2DArray Create(GraphicsDevice device, int width, int height, int arraysize)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.RenderTexture2DArray(device as DX11.GraphicsDevice, width, height, arraysize, SharpDX.DXGI.Format.R32G32_Float);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new OpenGL.RenderTexture2DArray(width, height, arraysize);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract int Height { get; protected set; }
		public abstract int Width { get; protected set; }
		public abstract int ArraySize { get; protected set; }

		public abstract void Apply(GraphicsContext context, int slot);
		public abstract void GenerateMipmaps(GraphicsContext context);

		public abstract void Dispose();
	}
}
