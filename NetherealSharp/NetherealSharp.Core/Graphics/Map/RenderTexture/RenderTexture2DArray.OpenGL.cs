﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	public class RenderTexture2DArray : Base.RenderTexture2DArray, IGLTexture2D
	{
		private int m_TextureId;
		private bool m_GenerateMipmaps = false;

		public int TextureId { get { return m_TextureId; } }
		public override int Height { get; protected set; }
		public override int Width { get; protected set; }
		public override int ArraySize { get; protected set; }

		public RenderTexture2DArray(int width, int height, int arraysize)
		{
			Width = width;
			Height = height;
			ArraySize = arraysize;


			m_TextureId = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2DArray, m_TextureId);
			GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMinFilter, (float)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureMagFilter, (float)TextureMagFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureWrapS, (float)TextureWrapMode.ClampToEdge);
			GL.TexParameter(TextureTarget.Texture2DArray, TextureParameterName.TextureWrapT, (float)TextureWrapMode.ClampToEdge);

			//consider using GL.BufferStorage3D
			GL.TexImage3D(
				TextureTarget.Texture2DArray,
				0,
				PixelInternalFormat.Rgba,
				width,
				height,
				arraysize,
				0,
				PixelFormat.Bgra,
				PixelType.UnsignedByte,
				IntPtr.Zero);

			GL.BindTexture(TextureTarget.Texture1DArray, 0);
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			GL.ActiveTexture(TextureUnit.Texture0 + slot);
			GL.BindTexture(TextureTarget.Texture2DArray, m_TextureId);
			GL.Uniform1(slot, ArraySize);

		}

		public override void Dispose()
		{
			GL.DeleteTexture(m_TextureId);
		}

		public override void GenerateMipmaps(Base.GraphicsContext context)
		{
			GL.BindTexture(TextureTarget.Texture2D, m_TextureId);
			GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
			GL.BindTexture(TextureTarget.Texture2D, 0);
		}
	}
}

#endif