﻿
#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Graphics.OpenGL;

using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.OpenGL
{
	public class RenderTexture2D : Base.RenderTexture2D, IGLTexture2D
	{
		public int TextureId { get; protected set; }
		public int FboId { get; protected set; }

		public override int Height { get; protected set; }
		public override int Width { get; protected set; }

		public RenderTexture2D()
		{
			TextureId = GL.GenTexture();

			
			FboId = GL.GenFramebuffer();
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public override void GenerateMipmaps(Base.GraphicsContext context)
		{
			throw new NotImplementedException();
		}
	}
}
#endif