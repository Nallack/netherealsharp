﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class RenderTextureCube : IRenderTarget, ITexture, IDisposable
    {
		public abstract int Height { get; protected set; }
		public abstract int Width { get; protected set; }
		public abstract void Apply(GraphicsContext context, int slot);
		public abstract void Dispose();



    }
}
