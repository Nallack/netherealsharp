﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D3D = SharpDX.Direct3D;
using D2D1 = SharpDX.Direct2D1;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
	public class RenderTexture2DArray : Base.RenderTexture2DArray, IDX11RenderTarget2D, IDX11Texture
	{
		public override int Height { get; protected set; }
		public override int Width { get; protected set; }
		public override int ArraySize { get; protected set; }

		public DX11.GraphicsDevice Device { get; protected set; }
		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }

		public D3D11.ShaderResourceView ShaderResourceView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }
		public D3D11.RenderTargetView RenderTargetView { get; protected set; }
		public D2D1.RenderTarget D2DTarget { get; protected set; }

		public int MipLevels = 4;

		public RenderTexture2DArray(GraphicsDevice device, int width, int height, int arraysize, DXGI.Format format)
		{
			Device = device;
			Width = width;
			Height = height;
			ArraySize = arraysize;

			CreateBackbuffer(format); //DXGI.Format.B8G8R8A8_UNorm
			CreateDepthBuffer(DXGI.Format.R24G8_Typeless, DXGI.Format.D24_UNorm_S8_UInt);
			CreateShaderView(DXGI.Format.R24_UNorm_X8_Typeless, false);
		}

		private void CreateBackbuffer(DXGI.Format format)
		{
			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = ArraySize,
				BindFlags = D3D11.BindFlags.ShaderResource | D3D11.BindFlags.RenderTarget,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = MipLevels,
				OptionFlags = D3D11.ResourceOptionFlags.GenerateMipMaps,
				SampleDescription = { Count = 1, Quality = 0 }
			};

			BackBuffer = new D3D11.Texture2D(Device.D3D11Device, desc);
			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);
		}

		private void CreateDepthBuffer(DXGI.Format format, DXGI.Format viewformat)
		{
			D3D11.Texture2DDescription ddesc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = ArraySize,
				BindFlags = D3D11.BindFlags.ShaderResource | D3D11.BindFlags.DepthStencil,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = MipLevels,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription = { Count = 1, Quality = 0 }
			};

			D3D11.DepthStencilViewDescription dsvdesc = new D3D11.DepthStencilViewDescription()
			{
				Dimension = D3D11.DepthStencilViewDimension.Texture2DArray,
				Flags = D3D11.DepthStencilViewFlags.None,
				Texture2DArray =
				{
					ArraySize = ArraySize,
					MipSlice = 0,
					FirstArraySlice = 0
				},
				Format = viewformat
			};

			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, ddesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer, dsvdesc);
		}

		private void CreateShaderView(DXGI.Format format, bool useDepthBuffer)
		{
			if (useDepthBuffer)
			{
				D3D11.ShaderResourceViewDescription srvdesc = new D3D11.ShaderResourceViewDescription()
				{
					Dimension = D3D.ShaderResourceViewDimension.Texture2DArray,
					Format = format,
					Texture2DArray =
					{
						ArraySize = ArraySize,
						FirstArraySlice = 0,
						MipLevels = MipLevels,
						MostDetailedMip = 0
					}
				};

				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, DepthBuffer, srvdesc);
			}
			else
			{
				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, BackBuffer);
			}
		}

		public override void GenerateMipmaps(Base.GraphicsContext context)
		{
			(context as DX11.GraphicsContext).D3D11Context.GenerateMips(ShaderResourceView);
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			Device.D3D11Device.ImmediateContext.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			RenderTargetView.Dispose();
			DepthStencilView.Dispose();
			ShaderResourceView.Dispose();

			BackBuffer.Dispose();
			DepthBuffer.Dispose();
		}
	}
}

#endif