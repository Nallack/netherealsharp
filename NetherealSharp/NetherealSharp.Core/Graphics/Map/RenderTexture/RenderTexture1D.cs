﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class RenderTexture1D : ITexture, IRenderTarget, IDisposable
    {
		public static RenderTexture1D Create(GraphicsDevice device, Size size, TextureFormat format, bool depthOnly = false)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.RenderTexture1D(device as DX11.GraphicsDevice, size.Width, depthOnly, format.ToDXGI());
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract int Width { get; protected set; }
		public int Height { get { return 1; } }

		public abstract void Apply(GraphicsContext context, int slot);

		public abstract void Dispose();

    }
}
