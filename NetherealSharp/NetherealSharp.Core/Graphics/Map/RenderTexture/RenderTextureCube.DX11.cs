﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D2D1 = SharpDX.Direct2D1;
using D3D = SharpDX.Direct3D;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class RenderTextureCube : Base.RenderTextureCube, IDX11RenderTarget2D
    {
		public override int Width { get; protected set; }
		public override int Height { get; protected set; }

		public DX11.GraphicsDevice Device { get; protected set; }

		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }

		public D3D11.ShaderResourceView ShaderResourceView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }
		public D3D11.RenderTargetView RenderTargetView { get; protected set; }

		public D2D1.RenderTarget D2DTarget { get; protected set; }

		public RenderTextureCube(GraphicsDevice device, int size, DXGI.Format format)
		{
			Device = device;
			Width = size;
			Height = size;

			CreateBackbuffer(DXGI.Format.B8G8R8A8_UNorm);
			CreateDepthBuffer(DXGI.Format.R24G8_Typeless, DXGI.Format.D24_UNorm_S8_UInt);
			CreateShaderView(false);

		}

		private void CreateBackbuffer(DXGI.Format format)
		{
			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = 6,
				BindFlags = D3D11.BindFlags.ShaderResource | D3D11.BindFlags.RenderTarget,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = 1,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription = { Count = 1, Quality = 0 }
			};

			BackBuffer = new D3D11.Texture2D(Device.D3D11Device, desc);
			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);
		}

		private void CreateDepthBuffer(DXGI.Format format, DXGI.Format viewformat)
		{
			D3D11.Texture2DDescription ddesc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = 1,
				BindFlags = D3D11.BindFlags.DepthStencil | D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = 1,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription = { Count = 1, Quality = 0 }
			};

			D3D11.DepthStencilViewDescription dsvdesc = new D3D11.DepthStencilViewDescription()
			{
				Dimension = D3D11.DepthStencilViewDimension.Texture2D,
				Flags = D3D11.DepthStencilViewFlags.None,
				Format = viewformat,
				Texture2D = { MipSlice = 0 }
			};

			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, ddesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer, dsvdesc);
		}

		private void CreateShaderView(bool useDepthBuffer)
		{
			if (useDepthBuffer)
			{
				D3D11.ShaderResourceViewDescription srvdesc = new D3D11.ShaderResourceViewDescription()
				{
					Dimension = D3D.ShaderResourceViewDimension.Texture2D,
					Format = DXGI.Format.R24_UNorm_X8_Typeless,
					Texture2D = { MipLevels = 1, MostDetailedMip = 0 }
				};

				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, DepthBuffer, srvdesc);
			}
			else
			{
				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, BackBuffer);
			}
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			Device.D3D11Device.ImmediateContext.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			BackBuffer.Dispose();
			RenderTargetView.Dispose();
			ShaderResourceView.Dispose();
		}
	}
}
#endif