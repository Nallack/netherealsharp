﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using DXGI = SharpDX.DXGI;
using D2D1 = SharpDX.Direct2D1;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using Base = NetherealSharp.Core.Graphics;
using SharpDX.Direct2D1;

namespace NetherealSharp.Core.Graphics.DX11
{
    public class RenderTexture2D : Base.RenderTexture2D, IDX11Texture, IDX11RenderTarget2D
    {
		public override int Width { get; protected set; }
		public override int Height { get; protected set; }

		public int Samples = 1;
		//1 required for SampleCmpLevelZero
		public int Miplevels = 4;

		public DX11.GraphicsDevice Device { get; protected set; }
		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }

		public D3D11.ShaderResourceView ShaderResourceView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }
		public D3D11.RenderTargetView RenderTargetView { get; protected set; }
		public D2D1.RenderTarget D2DTarget { get; protected set; }


		//TODO:
		//need options for { backbuffer  and/or depthbuffer }
		//Shader resource view is backbuffer or depthbuffer
		//mip levels


		/// <summary>
		/// Create an empty RendertTexture2D
		/// </summary>
		/// <param name="device"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="format"></param>
		public RenderTexture2D(GraphicsDevice device, int width, int height, bool depthOnly, DXGI.Format format)
		{
			Device = device;
			Width = width;
			Height = height;

			

			CreateBackBuffer(format);
			CreateDepthBuffer(DXGI.Format.D32_Float_S8X24_UInt, DXGI.Format.D32_Float_S8X24_UInt);
			CreateShaderView(format, depthOnly);
			
			//TODO: shadows used this
			//CreateDepthBuffer(DXGI.Format.R24G8_Typeless, DXGI.Format.D24_UNorm_S8_UInt);
			//CreateShaderView(DXGI.Format.R24_UNorm_X8_Typeless, depthOnly);

		}

		private void CreateBackBuffer(DXGI.Format format)
		{
			D3D11.Texture2DDescription desc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = 1,
				BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = Miplevels,
				OptionFlags = D3D11.ResourceOptionFlags.GenerateMipMaps,
				SampleDescription = { Count = Samples, Quality = 0 }
			};

			BackBuffer = new D3D11.Texture2D(Device.D3D11Device, desc);
			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);
		}

		private void CreateDepthBuffer(DXGI.Format format, DXGI.Format viewformat)
		{
			var ddesc = new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				ArraySize = 1,
				BindFlags = D3D11.BindFlags.DepthStencil,
				//BindFlags = D3D11.BindFlags.DepthStencil | D3D11.BindFlags.ShaderResource,
				Usage = D3D11.ResourceUsage.Default,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				Format = format,
				MipLevels = Miplevels,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				SampleDescription = { Count = Samples, Quality = 0 }
			};

			D3D11.DepthStencilViewDescription dsvdesc = new D3D11.DepthStencilViewDescription()
			{
				Dimension = D3D11.DepthStencilViewDimension.Texture2D,
				Flags = D3D11.DepthStencilViewFlags.None,
				Format = viewformat,
				Texture2D = { MipSlice = 0 }
			};

			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, ddesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer, dsvdesc);
		}

		private void CreateShaderView(DXGI.Format format, bool useDepthBuffer)
		{


			if (useDepthBuffer)
			{
				var srvdesc = new D3D11.ShaderResourceViewDescription()
				{
					Dimension = D3D.ShaderResourceViewDimension.Texture2D,
					Format = format,
					Texture2D = { MipLevels = Miplevels, MostDetailedMip = 0 }
				};

				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, DepthBuffer, srvdesc);
			}
			else
			{
				ShaderResourceView = new D3D11.ShaderResourceView(Device.D3D11Device, BackBuffer);
			}
		}

		public override void GenerateMipmaps(Base.GraphicsContext context)
		{
			(context as DX11.GraphicsContext).D3D11Context.GenerateMips(ShaderResourceView);
		}

		public override void Apply(Base.GraphicsContext context, int slot)
		{
			var c = context as DX11.GraphicsContext;
			c.D3D11Context.PixelShader.SetShaderResource(slot, ShaderResourceView);
		}

		public override void Dispose()
		{
			BackBuffer.Dispose();
			DepthBuffer.Dispose();

			DepthStencilView.Dispose();
			RenderTargetView.Dispose();
			ShaderResourceView.Dispose();
		}
	}
}
#endif