﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public interface IRenderTarget
    {
		int Width { get; }
		int Height { get; }
    }
}
