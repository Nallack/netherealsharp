﻿#if VULKAN
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using Vk = VulkanSharp;

namespace NetherealSharp.Core.Graphics.Vulkan
{


	/// <summary>
	/// Helper container class for managing a swapchain target
	/// TODO: look at example to get an ideal number of images
	/// </summary>
    public class SwapChain
    {
		public GraphicsDevice Device { get; private set; }
		public Vk.SwapchainKhr Swapchain { get; private set; }

		public RenderTarget[] RenderTargets { get; private set; }

		public Vk.SurfaceKhr Surface { get; private set; }
		public Vk.SurfaceCapabilitiesKhr SurfaceCapabilities { get; private set; }
		public Vk.SurfaceFormatKhr SurfaceFormat { get; private set; }

		//public Vk.Format ColorFormat { get; private set; }
		//public Vk.ColorSpaceKhr ColorSpace { get; private set; }

		public SwapChain(GraphicsDevice device, Control control)
		{
			Device = device;

			CreateWindowsSurface(control);

			//get formats dependant on this swapchain/surface
			/*
			var formats = Device.PhysicalDevice.GetSurfaceFormatsKHR(Surface);
			if((formats.Length == 1) && (formats[0].Format == Vk.Format.Undefined))
			{
				ColorFormat = Vk.Format.B8G8R8A8Unorm;
			}
			else
			{
				ColorFormat = formats[0].Format;
			}
			ColorSpace = formats[0].ColorSpace;
			*/


			CreateSwapchain();

			
		}

		private void CreateWindowsSurface(Control control)
		{

			// TODO: TRY
			/*
			Vk.DisplaySurfaceCreateInfoKhr d = new Vk.DisplaySurfaceCreateInfoKhr()
			{
			}
			device.Instance.CreateDisplayPlaneSurfaceKHR()
			*/

			ulong surface = 0;

			unsafe
			{
				Vk.Windows.Interop.Win32SurfaceCreateInfoKhr surfaceCreateInfo =
					new Vk.Windows.Interop.Win32SurfaceCreateInfoKhr()
					{
						SType = Vk.StructureType.Win32SurfaceCreateInfoKhr,
						//Hinstance = Process.GetCurrentProcess().Handle,
						Hwnd = control.Handle
					};

				Vk.Result result =
					Vk.Windows.Interop.NativeMethods.vkCreateWin32SurfaceKHR(
						Device.Instance.Handle,
						&surfaceCreateInfo,
						null,
						&surface);

				if (result != Vk.Result.Success)
					throw new Vk.ResultException(result);
			}

			Surface = new Vk.SurfaceKhr() { _handle = surface };
			if (!Device.PhysicalDevice.GetSurfaceSupportKHR(0, Surface))
			{
				throw new ArgumentException("Surface not compatible");
			}

			SurfaceCapabilities = Device.PhysicalDevice.GetSurfaceCapabilitiesKHR(Surface);
			SurfaceFormat = SelectFormat(Device.PhysicalDevice, Surface);
		}

		private Vk.SurfaceFormatKhr SelectFormat(Vk.PhysicalDevice physicalDevice, Vk.SurfaceKhr surface)
		{
			var formats = physicalDevice.GetSurfaceFormatsKHR(surface);
			foreach (var f in formats)
			{
				if (f.Format == Vk.Format.R8G8B8A8Unorm || f.Format == Vk.Format.B8G8R8A8Unorm) return f;
			}
			throw new System.Exception("Cannot find the Vulkan r8g8b8a8unorm format.");
		}


		private void CreateSwapchain()
		{
			var swapchainInfo = GetSwapchainInfo(Surface, SurfaceFormat, SurfaceCapabilities);
			Swapchain = Device.LogicalDevice.CreateSwapchainKHR(swapchainInfo);
		}

		private Vk.SwapchainCreateInfoKhr GetSwapchainInfo(Vk.SurfaceKhr surface, Vk.SurfaceFormatKhr format, Vk.SurfaceCapabilitiesKhr capabilities)
		{

			//TODO: check sample for getting the recomended image count

			return new Vk.SwapchainCreateInfoKhr()
			{
				Surface = Surface,
				MinImageCount = capabilities.MinImageCount,
				ImageFormat = format.Format,
				ImageColorSpace = format.ColorSpace,
				ImageExtent = capabilities.CurrentExtent,
				ImageUsage = Vk.ImageUsageFlags.ColorAttachment | Vk.ImageUsageFlags.TransferDst,
				PreTransform = Vk.SurfaceTransformFlagsKhr.Identity,
				ImageArrayLayers = 1,
				ImageSharingMode = Vk.SharingMode.Exclusive,
				QueueFamilyIndices = new uint[] { 0 },
				PresentMode = Vk.PresentModeKhr.Fifo,
				CompositeAlpha = Vk.CompositeAlphaFlagsKhr.Opaque
			};
		}


	}
}


#endif