﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public abstract class RenderTarget : IRenderTarget, IDisposable
    {
		public abstract int Width { get; }
		public abstract int Height { get; }

		//When the target has resized, not presenting surface.
		public event Action<System.Drawing.Size> Resized;

		public bool ResizePending = false;

		public virtual void CheckForResize() { }

		protected virtual void OnResized(System.Drawing.Size newSize)
		{
			var handler = Resized;
			if (handler != null)
			{
				Resized(newSize);
			}
		}

		public virtual void BeginDraw() { }
		public virtual void EndDraw() { }

		public abstract void Present();

		public abstract void Dispose();
    }
}
