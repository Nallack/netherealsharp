﻿#if WPF
using NetherealSharp.Core.Windows;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	public abstract class PanelTarget : RenderTarget
	{
		public static PanelTarget Create(GraphicsDevice device, RenderPanel panel)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.PanelTarget(device as DX11.GraphicsDevice, panel as Core.Windows.DX11.RenderPanel);
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
#endif