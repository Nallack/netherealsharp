﻿#if DIRECTX11 && WPF
using NetherealSharp.Core.Windows;
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.DX11
{

	public class PanelTarget : Base.PanelTarget, IDX11RenderTarget2D
	{
		internal GraphicsDevice Device;
		internal Windows.DX11.RenderPanel Panel;

		internal DXGI.SwapChain SwapChain;

		//internal D3D11.Texture2D MSBackBuffer;
		//internal D3D11.Texture2D FinalBackBuffer;


		public D3D11.Texture2D BackBuffer { get; private set; } 
		public D3D11.Texture2D DepthBuffer { get; private set; }
		public D3D11.RenderTargetView RenderTargetView { get; private set; }
		public D3D11.DepthStencilView DepthStencilView { get; private set; }
		public D2D1.RenderTarget D2DTarget { get; private set; }

		public override int Width { get { return BackBuffer.Description.Width; } }
		public override int Height { get { return BackBuffer.Description.Height; } }


		public int Samples = 1;
		public int BufferCount = 2;

		public PanelTarget(GraphicsDevice device, Windows.DX11.RenderPanel panel)
		{
			Device = device;
			Panel = panel;

			panel.Loaded += (sender, e) =>
			{
				panel.D3D11Image.WindowOwner = (new System.Windows.Interop.WindowInteropHelper(
					System.Windows.Window.GetWindow(panel))).Handle;

				panel.D3D11Image.OnRender += (surface, newsurface) =>
				{
					if (newsurface)
					{
						CreateSizeIndependantResources();
						CreateSizeDependantResources(surface);
					}
				};

				panel.D3D11Image.RequestRender();
			};

		}

		private void CreateSizeIndependantResources()
		{
			var swapDesc = GetSwapChainDescription1();
			//SwapChain = new DXGI.SwapChain1(
			//	Device.DXGIFactory,
			//	Device.D3DDevice,
			//	null,
			//	ref swapDesc);
		}

		private void CreateSizeDependantResources(IntPtr surface)
		{
			ComObject co = new ComObject(surface);
			DXGI.Resource res = co.QueryInterface<DXGI.Resource>();
			DXGI.Resource sres = Device.D3D11Device.OpenSharedResource<DXGI.Resource>(res.SharedHandle);
			BackBuffer = sres.QueryInterface<D3D11.Texture2D>();

			res.Dispose();
			sres.Dispose();

			//BackBuffer = D3D11.Texture2D.FromSwapChain<D3D11.Texture2D>(SwapChain, 0);

			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);
			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, GetDepthBufferDescription());
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer);

		}

		protected DXGI.SwapChainDescription1 GetSwapChainDescription1()
		{
			return new DXGI.SwapChainDescription1()
			{
				BufferCount = BufferCount,
				Width = (int)Panel.ActualWidth,
				Height = (int)Panel.ActualHeight,
				Format = DXGI.Format.B8G8R8A8_UNorm,
				AlphaMode = DXGI.AlphaMode.Ignore,
				SwapEffect = DXGI.SwapEffect.Discard,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = DXGI.Usage.BackBuffer | DXGI.Usage.RenderTargetOutput,
				Scaling = DXGI.Scaling.Stretch,
				Stereo = false,
				Flags = DXGI.SwapChainFlags.None
			};
		}

		protected D3D11.Texture2DDescription GetDepthBufferDescription()
		{
			return new D3D11.Texture2DDescription()
			{
				Width = BackBuffer.Description.Width,
				Height = BackBuffer.Description.Height,
				Format = DXGI.Format.D32_Float_S8X24_UInt,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				ArraySize = 1,
				MipLevels = 1,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None
			};
		}

		public override void Present()
		{
			Device.D3D11Device.ImmediateContext.Flush();

			//SwapChain.Present(0, DXGI.PresentFlags.None);

			Panel.D3D11Image.RequestRender();
		}

		public override void Dispose()
		{
			DepthStencilView.Dispose();
			RenderTargetView.Dispose();
			DepthBuffer.Dispose();
			BackBuffer.Dispose();
		}
	}
}
#endif