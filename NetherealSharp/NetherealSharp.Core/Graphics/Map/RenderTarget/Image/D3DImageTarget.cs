﻿#if WPF && DIRECTX11
using Microsoft.Wpf.Interop.DirectX;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Interop;

using D3D11 = SharpDX.Direct3D11;
using D3D9 = SharpDX.Direct3D9;

namespace NetherealSharp.Core.Graphics
{
    public abstract class D3DImageTarget : RenderTarget
    {
		protected D3D11Image D3DImage;

		public static D3DImageTarget Create(GraphicsDevice device, D3DImage image, IntPtr surface, int swapInterval)
		{
			switch (device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.D3DImageTarget(device as DX11.GraphicsDevice, image, surface);
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
#endif