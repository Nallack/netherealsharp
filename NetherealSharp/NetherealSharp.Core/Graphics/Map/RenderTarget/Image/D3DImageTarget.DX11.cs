﻿
#if DIRECTX && WPF

using System;
using System.Collections.Generic;
using System.Text;

using D3D9 = SharpDX.Direct3D9;
using D2D1 = SharpDX.Direct2D1;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

using Microsoft.Wpf.Interop.DirectX;
using System.Windows.Interop;

using Base = NetherealSharp.Core.Graphics;
using SharpDX;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Allocates the d3dimage source as the primary output target,
	/// however this means no double buffering and no multisampling.
	/// </summary>
    public class D3DImageTarget : Base.D3DImageTarget, IDX11RenderTarget2D
	{
		GraphicsDevice Device;

		public override int Width { get { return BackBuffer.Description.Width; } }
		public override int Height { get { return BackBuffer.Description.Height; } }

		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }
		public D3D11.RenderTargetView RenderTargetView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }

		public D2D1.RenderTarget D2DTarget { get; protected set; }

		public int Samples = 1;
		public int BufferCount = 2;

		public D3DImageTarget(GraphicsDevice device, D3DImage image, IntPtr surface)
		{
			Device = device;
			D3DImage = image as D3D11Image;

			ComObject co = new SharpDX.ComObject(surface);


			DXGI.Resource res = co.QueryInterface<SharpDX.DXGI.Resource>();
			DXGI.Resource sres = Device.D3D11Device.OpenSharedResource<DXGI.Resource>(res.SharedHandle);

			
			BackBuffer = sres.QueryInterface<D3D11.Texture2D>();
			//BackBuffer.Description.SampleDescription.Count = Samples;

			res.Dispose();

			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);


			//WARNING: retrieving Width Height now results in error

			//var depthbufferdesc = GetDepthBufferDescription();
			var depthbufferdesc = new D3D11.Texture2DDescription()
			{
				Width = BackBuffer.Description.Width,
				Height = BackBuffer.Description.Height,
				Format = DXGI.Format.D32_Float_S8X24_UInt,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				OptionFlags = D3D11.ResourceOptionFlags.None,
				ArraySize = 1,
				MipLevels = 1,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None
			};


			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, depthbufferdesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer);


			/*
			Device.ImmediateContext.D3D11Context.OutputMerger.SetRenderTargets(RenderTargetView);
			Device.ImmediateContext.D3D11Context.ClearRenderTargetView(RenderTargetView, Color.Blue);
			Device.ImmediateContext.D3D11Context.Flush();
			*/
		}

		private void CreateSizeDependantResources()
		{
			//BackBuffer
			var backbufferdesc = GetBackBufferDescription();
			BackBuffer = new D3D11.Texture2D(Device.D3D11Device, backbufferdesc);
			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);

			//DepthBuffer
			var depthbufferdesc = GetDepthBufferDescription();
			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, depthbufferdesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer);
		}

		protected D3D11.Texture2DDescription GetBackBufferDescription()
		{
			return new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				Format = DXGI.Format.B8G8R8A8_UNorm,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				OptionFlags = D3D11.ResourceOptionFlags.Shared,
				ArraySize = 1,
				MipLevels = 1,
				BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource,
				CpuAccessFlags = D3D11.CpuAccessFlags.None
			};
		}

		protected D3D11.Texture2DDescription GetDepthBufferDescription()
		{
			return new D3D11.Texture2DDescription()
			{
				Format = DXGI.Format.D32_Float_S8X24_UInt,
				ArraySize = 1,
				MipLevels = 1,
				Width = Width,
				Height = Height,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				OptionFlags = D3D11.ResourceOptionFlags.None
			};
		}

		public override void Present()
		{
			Device.D3D11Device.ImmediateContext.Flush();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
#endif