﻿#if VULKAN

using System;
using System.Collections.Generic;
using System.Text;

using Vk = VulkanSharp;

namespace NetherealSharp.Core.Graphics.Vulkan
{
	public interface IVKRenderTarget
	{
		GraphicsDevice Device { get; }

		//Vk.SurfaceKhr Surface { get; }
		//Vk.SurfaceCapabilitiesKhr SurfaceCapabilities { get; }
		//Vk.SwapchainKhr SwapChain { get; }

		//These should abstract the swapchain details
		Vk.RenderPass RenderPass { get; }
		Vk.Framebuffer Framebuffer { get; }
		Vk.CommandBuffer DrawCmdBuffer { get; }

		RenderTarget[] RenderTargets { get; }
		DepthStencil DepthStencil { get; }
		Vk.Framebuffer[] Framebuffers { get; }
		Vk.CommandBuffer[] DrawCmdBuffers { get; }


		Vk.Semaphore PresentComplete { get; }
		Vk.Semaphore RenderComplete { get; }
		Vk.Semaphore TexOverlayComplete { get; }

		void PrepareFrame();
		void Submit();
	}
}
#endif