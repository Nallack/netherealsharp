﻿#if WPF && DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D3D9 = SharpDX.Direct3D9;
using D2D1 = SharpDX.Direct2D1;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

using Base = NetherealSharp.Core.Graphics;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using NetherealSharp.Core.Windows;

namespace NetherealSharp.Core.Graphics.DX11
{
	/// <summary>
	/// Host Element Target which uses a D3D9 Surface
	/// </summary>
    public class HostElementTarget : Base.ElementTarget, IDX11RenderTarget2D
    {
		[DllImport("user32.dll")]
		private static extern IntPtr GetDesktopWindow();

		GraphicsDevice Device;

		//D3D9 Host
		private static int ActiveClients = 0;
		private static D3D9.Direct3DEx D3D9Factory;
		private static D3D9.DeviceEx D3D9Device;

		protected D3D9.Texture m_D3D9Texture;
		protected D3DImage m_D3DImage;

		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }
		public D3D11.RenderTargetView RenderTargetView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }

		public D2D1.RenderTarget D2DTarget { get; protected set; }

		public int Samples = 1;
		public int BufferCount = 2;

		private int lastWidth;
		private int lastHeight;

		//public D2D1.Bitmap1 BackBufferBitmap;
		//public D2D1.RenderTarget D2DTarget;


		public HostElementTarget(GraphicsDevice device, System.Windows.FrameworkElement element)
		{
			Device = device;

			if(ActiveClients == 0) StartD3D9();
			ActiveClients += 1;

			Element = element;

			CreateSizeDependantResources();

			Element.SizeChanged += HandleResize;
		}

		private void StartD3D9()
		{
			D3D9.PresentParameters presentParams = new D3D9.PresentParameters()
			{
				Windowed = true,
				SwapEffect = D3D9.SwapEffect.Discard,
				DeviceWindowHandle = GetDesktopWindow(),
				PresentationInterval = D3D9.PresentInterval.Default
			};

			D3D9Factory = new D3D9.Direct3DEx();
			D3D9Device = new D3D9.DeviceEx(
				D3D9Factory,
				0,
				D3D9.DeviceType.Hardware,
				IntPtr.Zero,
				D3D9.CreateFlags.HardwareVertexProcessing | D3D9.CreateFlags.Multithreaded | D3D9.CreateFlags.FpuPreserve,
				presentParams);
		}


		protected void CreateSizeDependantResources()
		{

			//BackBuffer
			var backbufferdesc = GetBackBufferDescription();
			BackBuffer = new D3D11.Texture2D(Device.D3D11Device, backbufferdesc);
			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);

			//DepthBuffer
			var depthbufferdesc = GetDepthBufferDescription();
			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, depthbufferdesc);
			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer);

			//D3D9 shared backbuffer
			D3D9.Format format = HostElementTarget.ToD3D9(backbufferdesc.Format);
			IntPtr handle = GetSharedHandle(BackBuffer);
			m_D3D9Texture = new D3D9.Texture(
				D3D9Device,
				backbufferdesc.Width,
				backbufferdesc.Height,
				1,
				D3D9.Usage.RenderTarget,
				format,
				D3D9.Pool.Default,
				ref handle);


			m_D3DImage = (Element as Windows.DX.RenderElement).D3DImage;

			using (D3D9.Surface surface = m_D3D9Texture.GetSurfaceLevel(0))
			{
				m_D3DImage.Lock();
				m_D3DImage.SetBackBuffer(D3DResourceType.IDirect3DSurface9, surface.NativePointer);
				m_D3DImage.Unlock();
			}

			lastWidth = Width;
			lastHeight = Height;

			ResizePending = false;
		}

		protected D3D11.Texture2DDescription GetBackBufferDescription()
		{
			return new D3D11.Texture2DDescription()
			{
				Width = Width,
				Height = Height,
				Format = DXGI.Format.B8G8R8A8_UNorm,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				OptionFlags = D3D11.ResourceOptionFlags.Shared,
				ArraySize = 1,
				MipLevels = 1,
				BindFlags = D3D11.BindFlags.RenderTarget | D3D11.BindFlags.ShaderResource,
				CpuAccessFlags = D3D11.CpuAccessFlags.None
			};
		}

		protected D3D11.Texture2DDescription GetDepthBufferDescription()
		{
			return new D3D11.Texture2DDescription()
			{
				Format = DXGI.Format.D32_Float_S8X24_UInt,
				ArraySize = 1,
				MipLevels = 1,
				Width = Width,
				Height = Height,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				OptionFlags = D3D11.ResourceOptionFlags.None
			};
		}

		private IntPtr GetSharedHandle(D3D11.Texture2D texture)
		{
			using (DXGI.Resource resource = texture.QueryInterface<DXGI.Resource>())
			{
				return resource.SharedHandle;
			}
		}

		public static D3D9.Format ToD3D9(DXGI.Format value)
		{
			switch (value)
			{
				case DXGI.Format.R8G8B8A8_UNorm:
					return D3D9.Format.A8B8G8R8;
				case DXGI.Format.B8G8R8A8_UNorm:
					return D3D9.Format.A8R8G8B8;
				default:
					return D3D9.Format.Unknown;
			}
		}

		public override void Present()
		{
			//as we arent using a swapchain here but drawing straight to
			//a shared d3d9 texture, we must create a dirty rectangle on the
			//d3dimage for WPF to know to refresh.

			Device.D3D11Device.ImmediateContext.Flush();

			m_D3DImage.Lock();
			m_D3DImage.AddDirtyRect(new System.Windows.Int32Rect(0, 0, lastWidth, lastHeight));
			m_D3DImage.Unlock();

			//throw new NotImplementedException();
		}


		private void HandleResize(object sender, EventArgs e)
		{
			ResizePending = true;
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

		public void Resize()
		{
			DepthStencilView.Dispose();
			DepthBuffer.Dispose();
			RenderTargetView.Dispose();
			BackBuffer.Dispose();

			CreateSizeDependantResources();

		}
	}
}
#endif