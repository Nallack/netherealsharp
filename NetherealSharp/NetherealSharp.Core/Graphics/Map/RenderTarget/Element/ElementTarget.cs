﻿#if WPF
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace NetherealSharp.Core.Graphics
{
	public abstract class ElementTarget : RenderTarget
	{
		public System.Windows.FrameworkElement Element;

		public override int Width { get { return (int)Element.RenderSize.Width; } }
		public override int Height { get { return (int)Element.RenderSize.Height; } }

		public static ElementTarget Create(GraphicsDevice device, FrameworkElement element, int swapInterval)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.HostElementTarget(device as DX11.GraphicsDevice, element);
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
#endif