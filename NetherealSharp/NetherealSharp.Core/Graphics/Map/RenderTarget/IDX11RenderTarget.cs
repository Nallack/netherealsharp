﻿#if DIRECTX11
using System;
using System.Collections.Generic;
using System.Text;

using D2D1 = SharpDX.Direct2D1;
using D3D11 = SharpDX.Direct3D11;

namespace NetherealSharp.Core.Graphics.DX11
{
	public interface IDX11RenderTarget1D
	{
		D3D11.Texture1D BackBuffer { get; }
		D3D11.RenderTargetView RenderTargetView { get; }
		D3D11.DepthStencilView DepthStencilView { get; }
	}

	public interface IDX11RenderTarget2D
	{
		D3D11.Texture2D BackBuffer { get; }
		D3D11.RenderTargetView RenderTargetView { get; }
		D3D11.DepthStencilView DepthStencilView { get; }

		D2D1.RenderTarget D2DTarget { get; }
	}
}
#endif