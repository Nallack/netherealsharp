﻿#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;

namespace NetherealSharp.Core.Graphics
{
	public class GLControlTarget : ControlTarget//, IGLTarget
	{
		public bool UseFrameBuffering = false;


		public override int Width { get { return Control.ClientSize.Width; } }
		public override int Height { get { return Control.ClientSize.Height; } }

		public GLControlTarget(Control control, int swapInterval)
		{
			Control = control;

			var glControl = Control as OpenTK.GLControl;
			glControl.MakeCurrent();

			glControl.Context.SwapInterval = swapInterval;
			//glControl.Context.VSync = true;

		}


		public override void Present()
		{
			var glControl = Control as OpenTK.GLControl;

			glControl.SwapBuffers();
		}

		protected override void OnResized(System.Drawing.Size newSize)
		{
			base.OnResized(newSize);
			//GL control seems to already handle resizes
			ResizePending = false;
		}

		public override void Dispose()
		{
			Control.Dispose();
		}
	}
}
#endif