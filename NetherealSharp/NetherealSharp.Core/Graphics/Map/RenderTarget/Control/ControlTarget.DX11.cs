﻿
#if FORMS && DIRECTX11

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using D2D1 = SharpDX.Direct2D1;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace NetherealSharp.Core.Graphics.DX11
{
	//Swapchain based Control target for DirectX 11.
	public class ControlTarget : Graphics.ControlTarget, IDX11RenderTarget2D
	{
		public GraphicsDevice Device { get; private set; }

		protected DXGI.SwapChain SwapChain { get; set; }

		public D3D11.Texture2D BackBuffer { get; protected set; }
		public D3D11.Texture2D DepthBuffer { get; protected set; }

		public D3D11.RenderTargetView RenderTargetView { get; protected set; }
		public D3D11.DepthStencilView DepthStencilView { get; protected set; }

		//public D2D1.RenderTarget RenderTarget { get { return null; } }

		public D2D1.Bitmap1 BackBufferBitmap;
		public D2D1.RenderTarget D2DTarget { get; protected set; }

		//TODO: VR blit requires samples set to 1!
		int SwapInterval = 0;
		public int BufferCount = 2;
		public int Samples = 1;

		public ControlTarget(GraphicsDevice device, Control control, int swapInterval)
		{
			Device = device;
			Control = control;
			SwapInterval = swapInterval;

			CreateSizeIndependantResources();
			CreateSizeDependantResources();

			Control.Resize += HandleResize;
		}

		/// <summary>
		/// Sets a variable indicating to resize for next frame, as resizing must only
		/// be performed outside of a frame drawing.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleResize(object sender, EventArgs e)
		{
			ResizePending = true;
		}

		protected void CreateSizeIndependantResources()
		{
			try
			{
				//SwapChain = new SwapChain(device.DXGIFactory, device.D3DDevice, swapChainDesc);
				var swap = GetSwapChainDescription1();
				SwapChain = new DXGI.SwapChain1(Device.DXGIFactory, Device.D3D11Device, Control.Handle, ref swap);

				//this handles window shortcuts
				Device.DXGIFactory.MakeWindowAssociation(Control.Handle, DXGI.WindowAssociationFlags.IgnoreAll);

			} catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
		}

		protected void CreateSizeDependantResources()
		{

			BackBuffer = D3D11.Texture2D.FromSwapChain<D3D11.Texture2D>(SwapChain, 0);

			RenderTargetView = new D3D11.RenderTargetView(Device.D3D11Device, BackBuffer);

			var width = Math.Max(Width, 1);
			var height = Math.Max(Height, 1);

			DepthBuffer = new D3D11.Texture2D(Device.D3D11Device, new D3D11.Texture2DDescription()
			{
				Format = DXGI.Format.D32_Float_S8X24_UInt,
				ArraySize = 1,
				MipLevels = 1,
				Width = width,
				Height = height,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = D3D11.ResourceUsage.Default,
				BindFlags = D3D11.BindFlags.DepthStencil,
				CpuAccessFlags = D3D11.CpuAccessFlags.None,
				OptionFlags = D3D11.ResourceOptionFlags.None
			});

			DepthStencilView = new D3D11.DepthStencilView(Device.D3D11Device, DepthBuffer);
			//ShaderView = new D3D11.ShaderResourceView(graphics.D3DDevice, BackBuffer);

			//D2D
			using (var surface = SwapChain.GetBackBuffer<DXGI.Surface>(0))
			{
				var bmp = new D2D1.BitmapProperties1()
				{
					//ColorContext = new D2D1.ColorContext()
					BitmapOptions = D2D1.BitmapOptions.Target
				};


				BackBufferBitmap = new D2D1.Bitmap1((Device.ImmediateContext as DX11.GraphicsContext).D2D1Context, surface);

				var rtp = new D2D1.RenderTargetProperties(
					D2D1.RenderTargetType.Hardware,
					new D2D1.PixelFormat(DXGI.Format.B8G8R8A8_UNorm, D2D1.AlphaMode.Ignore),
					100, 100,
					D2D1.RenderTargetUsage.None,
					D2D1.FeatureLevel.Level_10);

				D2DTarget = new D2D1.RenderTarget(Device.D2D1Factory, surface, rtp);
			}

			ResizePending = false;
		}

		public override void CheckForResize()
		{
			if (ResizePending) OnResized(Control.ClientSize);
		}

		protected override void OnResized(System.Drawing.Size newSize)
		{
			D2DTarget.Dispose();
			BackBufferBitmap.Dispose();

			//ShaderView.Dispose();

			DepthStencilView.Dispose();
			DepthBuffer.Dispose();

			RenderTargetView.Dispose();
			BackBuffer.Dispose();

			SwapChain.ResizeBuffers(
				BufferCount,
				Control.ClientSize.Width,
				Control.ClientSize.Height,
				SharpDX.DXGI.Format.B8G8R8A8_UNorm,
				SharpDX.DXGI.SwapChainFlags.None);

			CreateSizeDependantResources();

			base.OnResized(newSize);
		}

		protected DXGI.SwapChainDescription1 GetSwapChainDescription1()
		{
			return new DXGI.SwapChainDescription1()
			{
				BufferCount = BufferCount,
				Width = Control.Width,
				Height = Control.Height,
				Format = DXGI.Format.B8G8R8A8_UNorm,
				AlphaMode = DXGI.AlphaMode.Ignore,
				SwapEffect = DXGI.SwapEffect.Discard,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = DXGI.Usage.BackBuffer | DXGI.Usage.RenderTargetOutput,
				Scaling = DXGI.Scaling.Stretch,
				Stereo = false,
				Flags = DXGI.SwapChainFlags.None
			};
		}

		protected DXGI.SwapChainDescription GetSwapChainDescription()
		{
			return new DXGI.SwapChainDescription()
			{
				BufferCount = BufferCount,
				ModeDescription = new DXGI.ModeDescription()
				{
					Width = Control.Width,
					Height = Control.Height,
					Format = DXGI.Format.B8G8R8A8_UNorm,
					RefreshRate = new DXGI.Rational(60, 1),
					Scaling = DXGI.DisplayModeScaling.Stretched,
					ScanlineOrdering = DXGI.DisplayModeScanlineOrder.Unspecified
				},

				IsWindowed = true,
				OutputHandle = Control.Handle,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				SwapEffect = DXGI.SwapEffect.Discard,
				Usage = DXGI.Usage.RenderTargetOutput,
				Flags = DXGI.SwapChainFlags.None
			};
		}

		public override void Present()
		{
			SwapChain.Present(SwapInterval, DXGI.PresentFlags.None);
		}

		public override void Dispose()
		{
			D2DTarget.Dispose();
			BackBufferBitmap.Dispose();

			//ShaderView.Dispose();

			DepthStencilView.Dispose();
			DepthBuffer.Dispose();

			RenderTargetView.Dispose();
			BackBuffer.Dispose();

			SwapChain.Dispose();
		}

	}
}

#endif