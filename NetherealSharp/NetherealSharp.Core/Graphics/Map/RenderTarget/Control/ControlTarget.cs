﻿#if FORMS
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A renderable target that wraps a Windows Forms Control. Write Only.
	/// For a readable target use RenderTexture instead.
	/// </summary>
    public abstract class ControlTarget : RenderTarget
    {
		public override int Width { get { return Control.ClientSize.Width; } }
		public override int Height { get { return Control.ClientSize.Height; } }

		public Control Control;


#if FORMS
		public static ControlTarget Create(GraphicsDevice device, Control control, int swapInterval)
		{
			switch(device.Implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX11.ControlTarget(device as DX11.GraphicsDevice, control, swapInterval);
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return new DX12.ControlTarget(device as DX12.GraphicsDevice, control, swapInterval);
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					return new GLControlTarget(control, swapInterval);
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Vulkan.ControlTarget(device as Vulkan.GraphicsDevice, control, swapInterval);
#endif
				default:
					throw new NotImplementedException();
			}
		}
#endif

	}
}
#endif