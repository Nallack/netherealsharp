﻿#if WINDOWS && VULKAN

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Forms;

using Vk = VulkanSharp;
using Base = NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Graphics.Vulkan
{


	public class ControlTarget : Base.ControlTarget, IVKRenderTarget
	{
		private bool useFences = false;

		public GraphicsDevice Device { get; }


		//public Vk.SwapchainKhr SwapChain { get; private set; }
		//public Vk.SurfaceKhr Surface { get; private set; }
		//public Vk.SurfaceCapabilitiesKhr SurfaceCapabilities { get; private set; }
		//public Vk.SurfaceFormatKhr SurfaceFormat { get; private set; }


		public SwapChain SwapChain { get; private set; }
		private uint ImageCount = 0;
		public RenderTarget[] RenderTargets { get; private set; }
		public DepthStencil DepthStencil { get; private set; }

		public Vk.Framebuffer[] Framebuffers { get; }

		
		public uint CurrentBuffer { get; private set; } = 0;
		public Vk.Framebuffer Framebuffer { get { return Framebuffers[CurrentBuffer]; } }

		//public Vk.Format ColorFormat { get; private set; }
		public Vk.Format DepthFormat { get; private set; }



		public Vk.RenderPass RenderPass { get; }
		public Vk.CommandBuffer[] DrawCmdBuffers { get; protected set; }
		public Vk.CommandBuffer DrawCmdBuffer { get { return DrawCmdBuffers[CurrentBuffer]; } }


		Vk.Fence[] WaitFences;
		//ensures image is displayed before submitting new commands to the queue
		public Vk.Semaphore PresentComplete { get; }
		//ensures the image waits until all commands have executed before presenting
		public Vk.Semaphore RenderComplete { get; }
		public Vk.Semaphore TexOverlayComplete { get; }


		public ControlTarget(GraphicsDevice device, Control control, int swapInterval)
		{
			Control = control;
			Device = device;



			//swapchain
			SwapChain = new SwapChain(Device, Control);

			DepthFormat = FindDepthFormat();
			CreateDepthStencil();

			//TODO: renderpass? might need to be stored elsewhere
			CreateRenderTargets();
			//needed to create framebuffers
			RenderPass = CreateRenderPass();
			//framebuffers
			Framebuffers = CreateFramebuffers();
			//TODO: not sure if we ever need more than one command pool
			DrawCmdBuffers = CreateDrawCommandBuffers(Device.LogicalDevice, (Device.ImmediateContext as GraphicsContext).CommandPool);

			var semaphoreInfo = new Vk.SemaphoreCreateInfo();
			PresentComplete = Device.LogicalDevice.CreateSemaphore(semaphoreInfo);
			RenderComplete = Device.LogicalDevice.CreateSemaphore(semaphoreInfo);
			TexOverlayComplete = Device.LogicalDevice.CreateSemaphore(semaphoreInfo);

			if (useFences)
			{
				WaitFences = new Vk.Fence[ImageCount];
				for (int i = 0; i < ImageCount; i++)
				{
					var fenceinfo = new Vk.FenceCreateInfo
					{
						Flags = Vk.FenceCreateFlags.Signaled
					};
					WaitFences[i] = Device.LogicalDevice.CreateFence(fenceinfo);
				}
			}
		}

		private void CreateDepthStencil()
		{
			//Image
			var image = new Vk.ImageCreateInfo
			{
				ImageType = Vk.ImageType.Image2D,
				Format = DepthFormat,
				Extent = new Vk.Extent3D
				{
					Width = (uint)Width,
					Height = (uint)Height,
					Depth = 1
				},
				MipLevels = 1,
				ArrayLayers = 1,
				Samples = Vk.SampleCountFlags.Count1,
				Tiling = Vk.ImageTiling.Optimal,
				Usage = Vk.ImageUsageFlags.DepthStencilAttachment | Vk.ImageUsageFlags.TransferSrc
			};


			var ds = new DepthStencil();
			ds.Image = Device.LogicalDevice.CreateImage(image);

			//memory
			var memreqs = Device.LogicalDevice.GetImageMemoryRequirements(ds.Image);
			var mem = new Vk.MemoryAllocateInfo
			{
				AllocationSize = memreqs.Size,
				MemoryTypeIndex = Device.GetMemoryType(memreqs.MemoryTypeBits, Vk.MemoryPropertyFlags.DeviceLocal)
			};
			ds.Memory = Device.LogicalDevice.AllocateMemory(mem);
			Device.LogicalDevice.BindImageMemory(ds.Image, ds.Memory, 0);

			//ImageView
			var view = new Vk.ImageViewCreateInfo
			{
				ViewType = Vk.ImageViewType.View2D,
				Format = DepthFormat,
				Flags = 0,
				SubresourceRange = new Vk.ImageSubresourceRange
				{
					AspectMask = Vk.ImageAspectFlags.Depth | Vk.ImageAspectFlags.Stencil,
					BaseMipLevel = 0,
					LevelCount = 1,
					BaseArrayLayer = 0,
					LayerCount = 1
				},
				Image = ds.Image
			};
			ds.ImageView = Device.LogicalDevice.CreateImageView(view);

			DepthStencil = ds;
		}


		private void CreateRenderTargets()
		{
			var images = Device.LogicalDevice.GetSwapchainImagesKHR(SwapChain.Swapchain);

			ImageCount = (uint)images.Length;
			RenderTargets = new RenderTarget[images.Length];
			for(int i = 0; i < ImageCount; i++)
			{
				var colorAttachmentViewInfo = new Vk.ImageViewCreateInfo
				{
					Format = SwapChain.SurfaceFormat.Format,
					Components = new Vk.ComponentMapping
					{
						R = Vk.ComponentSwizzle.R,
						G = Vk.ComponentSwizzle.G,
						B = Vk.ComponentSwizzle.B,
						A = Vk.ComponentSwizzle.A
					},
					SubresourceRange = new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						BaseMipLevel = 0,
						LevelCount = 1,
						BaseArrayLayer = 0,
						LayerCount = 1
					},
					ViewType = Vk.ImageViewType.View2D,
					Image = images[i]
				};

				RenderTargets[i] = new RenderTarget();
				RenderTargets[i].Image = images[i];
				RenderTargets[i].ImageView = Device.LogicalDevice.CreateImageView(colorAttachmentViewInfo);
			}
		}


		//TODO: maybe call this one "DefaultRenderPass"
		//TODO: will probably want to move this elsewhere, probably with a new Renderpass/ShaderPipeline class
		//These are additional settings needed to describe a single renderpass
		//In DX11, these are implicit or stored with the context state.
		Vk.RenderPass CreateRenderPass()
		{
			var attDesc = new Vk.AttachmentDescription[2];


			//Color Attachment
			attDesc[0] = new Vk.AttachmentDescription
			{
				Format = SwapChain.SurfaceFormat.Format,
				Samples = Vk.SampleCountFlags.Count1,
				LoadOp = Vk.AttachmentLoadOp.Clear,
				StoreOp = Vk.AttachmentStoreOp.Store,
				StencilLoadOp = Vk.AttachmentLoadOp.DontCare,
				StencilStoreOp = Vk.AttachmentStoreOp.DontCare,
				InitialLayout = Vk.ImageLayout.Undefined,
				FinalLayout = Vk.ImageLayout.PresentSrcKhr
			};
			//Depth Attachment
			attDesc[1] = new Vk.AttachmentDescription
			{
				Format = DepthFormat,
				Samples = Vk.SampleCountFlags.Count1,
				LoadOp = Vk.AttachmentLoadOp.Clear,
				StoreOp = Vk.AttachmentStoreOp.Store,
				StencilLoadOp = Vk.AttachmentLoadOp.DontCare,
				StencilStoreOp = Vk.AttachmentStoreOp.DontCare,
				InitialLayout = Vk.ImageLayout.Undefined,
				FinalLayout = Vk.ImageLayout.DepthStencilAttachmentOptimal
			};

			//color reference
			var colorAttRef = new Vk.AttachmentReference()
			{
				Attachment = 0,
				Layout = Vk.ImageLayout.ColorAttachmentOptimal
			};
			//depth reference
			var depthAttRef = new Vk.AttachmentReference()
			{
				Attachment = 1,
				Layout = Vk.ImageLayout.DepthStencilAttachmentOptimal
			};

			//subpass Desc
			var subpassDesc = new Vk.SubpassDescription()
			{
				PipelineBindPoint = Vk.PipelineBindPoint.Graphics,
				ColorAttachmentCount = 1,
				ColorAttachments = new Vk.AttachmentReference[] { colorAttRef },
				DepthStencilAttachment = depthAttRef,
				InputAttachmentCount = 0,
				InputAttachments = null,
				PreserveAttachmentCount = 0,
				PreserveAttachments = null,
				ResolveAttachments = null
			};

			//Subpass Dependencies
			var dependencies = new Vk.SubpassDependency[2];
			dependencies[0] = new Vk.SubpassDependency()
			{
				SrcSubpass = ~0U,
				DstSubpass = 0,
				SrcStageMask = Vk.PipelineStageFlags.BottomOfPipe,
				DstStageMask = Vk.PipelineStageFlags.ColorAttachmentOutput,
				SrcAccessMask = Vk.AccessFlags.MemoryRead,
				DstAccessMask = Vk.AccessFlags.ColorAttachmentRead | Vk.AccessFlags.ColorAttachmentWrite,
				DependencyFlags = Vk.DependencyFlags.ByRegion
			};
			dependencies[1] = new Vk.SubpassDependency()
			{
				SrcSubpass = 0,
				DstSubpass = ~0U,
				SrcStageMask = Vk.PipelineStageFlags.ColorAttachmentOutput,
				DstStageMask = Vk.PipelineStageFlags.BottomOfPipe,
				SrcAccessMask = Vk.AccessFlags.ColorAttachmentRead | Vk.AccessFlags.ColorAttachmentWrite,
				DstAccessMask = Vk.AccessFlags.MemoryRead,
				DependencyFlags = Vk.DependencyFlags.ByRegion
			};

			var renderPassCreateInfo = new Vk.RenderPassCreateInfo()
			{
				AttachmentCount = (uint)attDesc.Length,
				Attachments = attDesc,
				SubpassCount = 1,
				Subpasses = new Vk.SubpassDescription[] { subpassDesc },
				DependencyCount = (uint)dependencies.Length,
				Dependencies = dependencies
			};


			return Device.LogicalDevice.CreateRenderPass(renderPassCreateInfo);

		}



		/// <summary>
		/// Creates framebuffers using swapchain images and single depthstecil.
		/// Depends on Redner
		/// </summary>
		/// <param name="device"></param>
		/// <param name="images"></param>
		/// <param name="surfaceFormat"></param>
		/// <returns></returns>
		private Vk.Framebuffer[] CreateFramebuffers()
		{
			uint nAttachments = 2;
			var attachments = new Vk.ImageView[nAttachments];

			var framebuffers = new Vk.Framebuffer[RenderTargets.Length];
			for (int i = 0; i < RenderTargets.Length; i++)
			{
				attachments[0] = RenderTargets[i].ImageView;
				attachments[1] = DepthStencil.ImageView;

				var framebufferInfo = new Vk.FramebufferCreateInfo()
				{
					Layers = 1,
					RenderPass = RenderPass,
					AttachmentCount = nAttachments,
					Attachments = attachments,
					//Width = (uint)Width,
					//Height = (uint)Height,
					Width = SwapChain.SurfaceCapabilities.CurrentExtent.Width,
					Height = SwapChain.SurfaceCapabilities.CurrentExtent.Height
				};
				framebuffers[i] = Device.LogicalDevice.CreateFramebuffer(framebufferInfo);
			}
			return framebuffers;
		}

		private Vk.CommandBuffer[] CreateDrawCommandBuffers(Vk.Device device, Vk.CommandPool commandPool)
		{
			var allocateInfo = new Vk.CommandBufferAllocateInfo()
			{
				CommandPool = commandPool,
				Level = Vk.CommandBufferLevel.Primary,
				CommandBufferCount = ImageCount
			};

			return device.AllocateCommandBuffers(allocateInfo);
		}



		private Vk.Format FindDepthFormat()
		{
			var formats = new Vk.Format[]
			{
				Vk.Format.D32SfloatS8Uint,
				Vk.Format.D32Sfloat,
				Vk.Format.D24UnormS8Uint,
				Vk.Format.D16UnormS8Uint,
				Vk.Format.D16Unorm
			};

			foreach(var format in formats)
			{
				var formatProps = Device.PhysicalDevice.GetFormatProperties(format);
				if((formatProps.OptimalTilingFeatures & Vk.FormatFeatureFlags.DepthStencilAttachment) != 0)
				{
					return format;
				}
			}
			throw new Exception("Depth format not found!");
		}



		public void PrepareFrame()
		{
			if (useFences)
			{
				CurrentBuffer = Device.LogicalDevice.AcquireNextImageKHR(
					SwapChain.Swapchain,
					TimeSpan.MaxValue,
					PresentComplete,
					WaitFences[CurrentBuffer]);

				Device.LogicalDevice.WaitForFences(1, WaitFences[CurrentBuffer], true, uint.MaxValue);
				Device.LogicalDevice.ResetFences(1, WaitFences[CurrentBuffer]);
			}
			else
			{
				CurrentBuffer = Device.LogicalDevice.AcquireNextImageKHR(
					SwapChain.Swapchain,
					TimeSpan.MaxValue,
					PresentComplete);
			}

		}

		public void Submit()
		{
			var submitInfo = new[]
				{
					new VulkanSharp.SubmitInfo()
					{
						WaitDstStageMask = new[] { Vk.PipelineStageFlags.ColorAttachmentOutput },
						WaitSemaphoreCount = 1,
						WaitSemaphores = new[] { PresentComplete },
						SignalSemaphoreCount = 1,
						SignalSemaphores = new[] { RenderComplete },
						CommandBufferCount = 1,
						CommandBuffers = new[] { DrawCmdBuffer }
					}
				};

			if(useFences)
			{
				Device.Queue.Submit(submitInfo, WaitFences[CurrentBuffer]);
			}
			else
			{
				Device.Queue.Submit(submitInfo);
			}
		}

		public override void Present()
		{			
			var presentInfo = new Vk.PresentInfoKhr()
			{
				SwapchainCount = 1,
				Swapchains = new[] { SwapChain.Swapchain },
				ImageIndices = new[] { CurrentBuffer },
				WaitSemaphoreCount = 1,
				WaitSemaphores = new[] { RenderComplete }
			};

			Device.Queue.PresentKHR(presentInfo);
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}

	}
}

#endif