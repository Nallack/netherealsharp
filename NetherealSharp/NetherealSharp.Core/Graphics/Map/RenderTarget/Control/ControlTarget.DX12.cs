﻿#if DIRECTX12 && FORMS
using System;
using System.Collections.Generic;
using System.Text;

using DXGI = SharpDX.DXGI;
using D3D = SharpDX.Direct3D;
using D3D12 = SharpDX.Direct3D12;

using Base = NetherealSharp.Core.Graphics;
using System.Windows.Forms;
using System.Threading;

namespace NetherealSharp.Core.Graphics.DX12
{
	public class ControlTarget : Base.ControlTarget
	{
		protected GraphicsDevice Graphics;


		protected DXGI.SwapChain SwapChain { get; set; }
		private D3D12.Fence Fence;
		private long currentFence;
		private AutoResetEvent eventHandle;

		public D3D12.Resource RenderTargetView { get; protected set; }
		public D3D12.Resource DepthBufferView { get; protected set; }


		//public D3D11.ShaderResourceView ShaderView { get; protected set; }

		////public D2D1.RenderTarget RenderTarget { get { return null; } }

		//public D2D1.Bitmap1 BackBufferBitmap;
		//public D2D1.RenderTarget D2DTarget { get; protected set; }

		DXGI.Format Format = DXGI.Format.R8G8B8A8_UNorm;
		private int BackBufferIndex = 0;
		int SwapInterval = 0;
		public int BufferCount = 2;
		public int Samples = 1;

		public ControlTarget(GraphicsDevice device, Control control, int swapInterval)
		{
			Graphics = device;
			Control = control;
			SwapInterval = swapInterval;

			CreateSizeIndependantResources();
			CreateSizeDependantResources();

			//Control.Resize += (sender, e) => { ResizePending = true; };
		}

		protected void CreateSizeIndependantResources()
		{
			try
			{
				//WARNING: must use FlipDiscard, samples set to 1
				var swap = GetSwapChainDescription1();
				SwapChain = new DXGI.SwapChain1(Graphics.DXGIFactory, Graphics.D3D12CommandQueue, Control.Handle, ref swap);
				Fence = Graphics.D3DDevice.CreateFence(0, D3D12.FenceFlags.None);
				currentFence = 1;
				eventHandle = new AutoResetEvent(false);

				//SwapChain = new DXGI.SwapChain(Graphics.DXGIFactory, Graphics.D3D12CommandQueue, GetSwapChainDescription());


				//this handles window shortcuts
				Graphics.DXGIFactory.MakeWindowAssociation(Control.Handle, DXGI.WindowAssociationFlags.IgnoreAll);


			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
		}

		protected void CreateSizeDependantResources()
		{

			RenderTargetView = SwapChain.GetBackBuffer<D3D12.Resource>(BackBufferIndex);
			Graphics.D3DDevice.CreateRenderTargetView(RenderTargetView, null, Graphics.RTVDesciptorHeap.CPUDescriptorHandleForHeapStart);
			ResizePending = false;
		}

		protected override void OnResized(System.Drawing.Size newSize)
		{
			//D2DTarget.Dispose();
			//BackBufferBitmap.Dispose();

			////ShaderView.Dispose();

			//DepthStencilView.Dispose();
			//DepthBuffer.Dispose();

			//RenderTargetView.Dispose();
			//BackBuffer.Dispose();

			//for (int i = 0; i < BufferCount; i++)
			//{
			//	BackBuffers[i].Dispose();
			//}

			WaitForPrevFrame();

			DisposeSizeDependantResources();

			SwapChain.ResizeBuffers(
				BufferCount,
				Control.ClientSize.Width,
				Control.ClientSize.Height,
				Format,
				SharpDX.DXGI.SwapChainFlags.None);

			CreateSizeDependantResources();



			base.OnResized(newSize);
		}

		protected DXGI.SwapChainDescription1 GetSwapChainDescription1()
		{
			return new DXGI.SwapChainDescription1()
			{
				BufferCount = BufferCount,
				Width = Control.Width,
				Height = Control.Height,
				Format = Format,
				AlphaMode = DXGI.AlphaMode.Ignore,
				SwapEffect = DXGI.SwapEffect.FlipDiscard,
				SampleDescription = new DXGI.SampleDescription(Samples, 0),
				Usage = DXGI.Usage.RenderTargetOutput,
				Scaling = DXGI.Scaling.Stretch,
				Stereo = false,
				Flags = DXGI.SwapChainFlags.None
			};
		}

		protected DXGI.SwapChainDescription GetSwapChainDescription()
		{
			return new DXGI.SwapChainDescription()
			{
				BufferCount = BufferCount,
				ModeDescription = new DXGI.ModeDescription()
				{
					Width = Control.Width,
					Height = Control.Height,
					Format = Format,
					RefreshRate = new DXGI.Rational(60, 1),
					Scaling = DXGI.DisplayModeScaling.Stretched,
					ScanlineOrdering = DXGI.DisplayModeScanlineOrder.Unspecified
				},

				IsWindowed = true,
				OutputHandle = Control.Handle,
				SampleDescription = new DXGI.SampleDescription(1, 0),
				SwapEffect = DXGI.SwapEffect.FlipDiscard,
				Usage = DXGI.Usage.RenderTargetOutput,
				Flags = DXGI.SwapChainFlags.None
			};
		}

		public override void Present()
		{
			

			SwapChain.Present(SwapInterval, DXGI.PresentFlags.None);
			BackBufferIndex = (BackBufferIndex + 1) % BufferCount;


			RenderTargetView.Dispose();
			RenderTargetView = SwapChain.GetBackBuffer<D3D12.Resource>(BackBufferIndex);
			Graphics.D3DDevice.CreateRenderTargetView(RenderTargetView, null, Graphics.RTVDesciptorHeap.CPUDescriptorHandleForHeapStart);

			WaitForPrevFrame();

		}

		private void WaitForPrevFrame()
		{
			long localFence = currentFence;
			Graphics.D3D12CommandQueue.Signal(Fence, localFence);
			currentFence++;

			if(Fence.CompletedValue < localFence)
			{
				Fence.SetEventOnCompletion(localFence, eventHandle.SafeWaitHandle.DangerousGetHandle());
				eventHandle.WaitOne();
			}
		}

		private void DisposeSizeDependantResources()
		{
			//D2DTarget.Dispose();
			//BackBufferBitmap.Dispose();

			////ShaderView.Dispose();

			//DepthStencilView.Dispose();
			//DepthBuffer.Dispose();
			//BackBuffer.Dispose();

			RenderTargetView.Dispose();
			RenderTargetView = null;
		}

		public override void Dispose()
		{

			DisposeSizeDependantResources();
			SwapChain.Dispose();
		}
	}
}
#endif