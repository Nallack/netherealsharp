﻿#if VULKAN

using System;
using System.Collections.Generic;
using System.Text;

using Vk = VulkanSharp;


namespace NetherealSharp.Core.Graphics.Vulkan
{
    public struct DepthStencil
    {
		public Vk.Image Image;
		public Vk.DeviceMemory Memory;
		public Vk.ImageView ImageView;
    }

	public struct RenderTarget
	{
		public Vk.Image Image;
		public Vk.ImageView ImageView;
	}
}

#endif