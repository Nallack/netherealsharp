﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public class SpriteBatch
    {
		GraphicsDevice Device;
		EffectPass Effect;
		IndexedMesh mesh;
		IndexedMeshData<VertexPNT> meshdata;

		public SpriteBatch(GraphicsDevice device, EffectPass effect)
		{
			Device = device;
			Effect = effect;
			meshdata = new IndexedMeshData<VertexPNT>(VertexPNT.Layout);
			mesh = IndexedMesh.Create(device, VertexPNT.Layout, BufferUsage.Dynamic);
		}

		public void Begin()
		{
			Effect.Bind(Device.ImmediateContext);
		}

		public void Add(VertexPNT vertex)
		{
			meshdata.Vertices.Add(vertex);
		}

		public void Add(Texture2D texture, RectangleF rectangle, float layer)
		{
			uint c = (uint)meshdata.Vertices.Count;

			meshdata.Vertices.AddRange(new VertexPNT[]
			{
				new VertexPNT(new Vector3(rectangle.TopLeft, layer), Vector3.ForwardLH, Vector2.Zero),
				new VertexPNT(new Vector3(rectangle.BottomLeft, layer), Vector3.ForwardLH, Vector2.Zero),
				new VertexPNT(new Vector3(rectangle.BottomRight, layer), Vector3.ForwardLH, Vector2.Zero),
				new VertexPNT(new Vector3(rectangle.TopRight, layer), Vector3.ForwardLH, Vector2.Zero)
			});

			meshdata.Indices.AddRange(new uint[] { c, c+2, c+1, c, c+3, c+2 });
		}

		public void Flush()
		{
			mesh.UpdateFromData(meshdata, BufferUsage.Dynamic);
			mesh.Draw(Device.ImmediateContext, PrimitiveMode.TriangleList);
			meshdata.Vertices.Clear();
			meshdata.Indices.Clear();
		}

		public void End()
		{
			Effect.Unbind(Device.ImmediateContext);
		}
    }
}
