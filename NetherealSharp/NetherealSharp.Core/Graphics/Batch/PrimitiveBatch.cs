﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    /// <summary>
    /// An object used for batching together multiple primitive draw calls.
    /// Accepts externally defined Effects (Recommended is color primitive or screenprimitive)
    /// TODO: allow user defined vertex layout.
    /// </summary>
    public class PrimitiveBatch : IContentable, IDisposable
    {
		GraphicsDevice Device;
		EffectPass Effect;
		ArrayMesh mesh;
		MeshData<VertexPC> meshdata;
		PrimitiveMode Mode;

		public string Name { get; set; }
		public Type ContentType { get { return typeof(PrimitiveBatch); } }

		public PrimitiveBatch(GraphicsDevice device, EffectPass effect)
		{
			Device = device;
			Effect = effect;

			meshdata = new MeshData<VertexPC>(VertexPC.Layout, false);
			mesh = ArrayMesh.Create<VertexPC>(device, VertexPC.Layout, BufferUsage.Dynamic);
		}

		public void BeginDraw(PrimitiveMode mode)
		{
			Mode = mode;
			Effect.Bind(Device.ImmediateContext);
		}

		public void setLineWidth(float width)
		{

		}

		public void Add(VertexPC vertex)
		{
			meshdata.Vertices.Add(vertex);
		}

		public void Add(IEnumerable<VertexPC> vertices)
		{
			meshdata.Vertices.AddRange(vertices);
		}

		public void Flush()
		{
			mesh.UpdateFromData<VertexPC>(meshdata, BufferUsage.Dynamic);
			mesh.Draw(Device.ImmediateContext, Mode);
			meshdata.Vertices.Clear();
		}

		public void EndDraw()
		{
			Effect.Unbind(Device.ImmediateContext);
		}

		public void Dispose()
		{
			mesh.Dispose();
		}
	}
}
