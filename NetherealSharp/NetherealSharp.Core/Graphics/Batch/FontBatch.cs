﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// Font rendering by generating a dynamic mesh from a Font Atlas.
	/// </summary>
    public class FontBatch : IContentable, IDisposable
    {
		public string Name { get; set; }
		public Type ContentType { get { return typeof(FontBatch); } }

		protected GraphicsDevice Device;
		protected BasicEffectPass FontEffect;

		protected IndexedMeshData<VertexPNT> meshdata;
		protected IndexedMesh mesh;

		private PrimitiveBatch primitive;

        public FontBatch(GraphicsDevice device, BasicEffectPass fontEffect)
        {
            Device = device;
            FontEffect = fontEffect;

			meshdata = new IndexedMeshData<VertexPNT>(VertexPNT.Layout);
            mesh = IndexedMesh.Create(device, VertexPNT.Layout, BufferUsage.Dynamic);
        }

		public FontBatch(GraphicsDevice device, BasicEffectPass fontEffect, PrimitiveBatch primitiveBatch)
		{
			Device = device;
            FontEffect = fontEffect;
            //PrimitiveBatch is for debugging glyphs only!
            //TODO: could use this instead if PB can accept generic vertex type
            primitive = primitiveBatch;

			meshdata = new IndexedMeshData<VertexPNT>(VertexPNT.Layout);
			mesh = IndexedMesh.Create(device, VertexPNT.Layout, BufferUsage.Dynamic);
		}


		//Equivelent of Unity UI with anchor at 0.5, 0.5, 0.5, 0.5
		public void DrawString(string text, SpriteFont font, Vector2 position, Vector2 size, FontFormat format)
		{

			//canvas size
			Vector2 canvas = new Vector2(Device.ActiveRenderTarget.Width, Device.ActiveRenderTarget.Height);

			Vector2 topleft = new Vector2(
						canvas.X / 2f + position.X + size.X * -0.5f,
						canvas.Y / 2f + position.Y + size.Y * -0.5f);

			Vector2 botright = new Vector2(topleft.X + size.X, topleft.Y + size.Y);

			Vector2 cursor = topleft;

			float aw = 0;
			float w = 0;
			switch (format.Alignment)
			{
				case TextAlignment.Left:
					//Left align
					//botleft = new Vector2(canvas.X / 2f + position.X + size.X * -0.5f, canvas.Y / 2 + position.Y + size.Y * 0.5f);
					//topright = new Vector2(botleft.X + size.X, botleft.Y - size.Y);
					break;
				case TextAlignment.Center:

					foreach(char c in text)
					{
						var metric = font.FontMetric.GlyphMetrics[c];
						w += metric.XAdvance;
					}
					w += font.FontMetric.GlyphMetrics[text[0]].BearingX;
					w -= font.FontMetric.GlyphMetrics[text[text.Length - 1]].XAdvance;
					w += font.FontMetric.GlyphMetrics[text[text.Length - 1]].Width;

					float xoffset = (size.X - w) / 2;
					cursor = topleft + new Vector2(xoffset, font.FontMetric.MaxBearingY);
					break;

			}

			Vector2 min = cursor;// - Vector2.UnitY * topalignoffset;
			Vector2 max = min;


			//float topalignoffset = font.FontMetric.LineHeight * 3f / 2f;
			//cursor.Y -= topalignoffset;



			uint n = 0;
			foreach (char c in text)
			{

				var metric = font.FontMetric.GlyphMetrics[c];
				//cast to integer here in case of odd resolution and cursor is at half pixels
				float x0 = (int)(cursor.X + metric.BearingX);
				float x1 = (int)(cursor.X + metric.BearingX + metric.Width);
				float y1 = (int)(cursor.Y + (metric.Height - metric.BearingY));
				float y0 = (int)(cursor.Y - metric.BearingY);

				float tx0 = metric.TexX;
				float tx1 = metric.TexX + metric.TexWidth;
				float ty0 = metric.TexY;
				float ty1 = metric.TexY + metric.TexHeight;

				min.X = Math.Min(min.X, x0);
				min.Y = Math.Min(min.Y, y0);
				max.X = Math.Max(max.X, x1);
				max.Y = Math.Max(max.Y, y1);
				aw = max.X - min.X;

				meshdata.Vertices.AddRange(new VertexPNT[] {
					new VertexPNT(new Vector3(x0, y0, 0), Vector3.BackwardRH, new Vector2(tx0, ty0)),
					new VertexPNT(new Vector3(x1, y0, 0), Vector3.BackwardRH, new Vector2(tx1, ty0)),
					new VertexPNT(new Vector3(x0, y1, 0), Vector3.BackwardRH, new Vector2(tx0, ty1)),
					new VertexPNT(new Vector3(x1, y1, 0), Vector3.BackwardRH, new Vector2(tx1, ty1)),
				});

				meshdata.Indices.AddRange(new uint[]
				{
					n + 0, n + 1, n + 2,
					n + 1, n + 3, n + 2
				});


				cursor.X += metric.XAdvance;
				cursor.Y += metric.YAdvance;
				n += 4;
			}

			//DEBUG
			primitive.BeginDraw(PrimitiveMode.LineStrip);
			primitive.Add(new VertexPC(new Vector3(topleft.X, topleft.Y, 0), new Vector4(0, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(botright.X, topleft.Y, 0), new Vector4(0, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(botright.X, botright.Y, 0), new Vector4(0, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(topleft.X, botright.Y, 0), new Vector4(0, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(topleft.X, topleft.Y, 0), new Vector4(0, 1, 0, 1)));

			primitive.Add(new VertexPC(new Vector3(min.X, min.Y, 0), new Vector4(1, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(max.X, min.Y, 0), new Vector4(1, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(max.X, max.Y, 0), new Vector4(1, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(min.X, max.Y, 0), new Vector4(1, 1, 0, 1)));
			primitive.Add(new VertexPC(new Vector3(min.X, min.Y, 0), new Vector4(1, 1, 0, 1)));
			primitive.Flush();
			primitive.EndDraw();
			
			//DRAW
			//FontEffect.Bind(Device.ImmediateContext);
			//FontEffect.ApplyTexture2D(Device.ImmediateContext, "diffuseMap", font.Atlas);
			//mesh.UpdateFromData<VertexPNT>(meshdata, BufferUsage.Dynamic);
			//mesh.Draw(PrimitiveMode.TriangleList);
			//meshdata.Vertices.Clear();
			//meshdata.Indices.Clear();
			//FontEffect.Unbind(Device.ImmediateContext);
		}

		public void DrawString(string text, SpriteFont font, Vector2 position)
		{


			Vector2 cursor = position;
			cursor.Y += font.FontMetric.LineHeight * 3f/ 2f;

			uint n = 0;
			foreach (char c in text)
			{
				GlyphMetric metric;
				if(font.FontMetric.GlyphMetrics.ContainsKey(c))
				{
					metric = font.FontMetric.GlyphMetrics[c];
				}
				else
				{
					metric = font.FontMetric.GlyphMetrics[29];
				}

				float x0 = (int)(cursor.X + metric.BearingX);
				float x1 = (int)(cursor.X + metric.BearingX + metric.Width);
				float y1 = (int)(cursor.Y + (metric.Height - metric.BearingY));
				float y0 = (int)(cursor.Y - metric.BearingY);

				float tx0 = metric.TexX;
				float tx1 = metric.TexX + metric.TexWidth;
				float ty0 = metric.TexY;
				float ty1 = metric.TexY + metric.TexHeight;


				meshdata.Vertices.AddRange(new VertexPNT[] {
					new VertexPNT(new Vector3(x0, y0, 0), Vector3.BackwardRH, new Vector2(tx0, ty0)),
					new VertexPNT(new Vector3(x1, y0, 0), Vector3.BackwardRH, new Vector2(tx1, ty0)),
					new VertexPNT(new Vector3(x0, y1, 0), Vector3.BackwardRH, new Vector2(tx0, ty1)),
					new VertexPNT(new Vector3(x1, y1, 0), Vector3.BackwardRH, new Vector2(tx1, ty1)),
				});


				meshdata.Indices.AddRange(new uint[]
				{
					n + 0, n + 1, n + 2,
					n + 1, n + 3, n + 2
				});


				cursor.X += metric.XAdvance;
				cursor.Y += metric.YAdvance;
				n += 4;
			}

			FontEffect.Bind(Device.ImmediateContext);
			FontEffect.BindTexture2D(Device.ImmediateContext, "diffuseMap", font.Atlas);

			mesh.UpdateFromData<VertexPNT>(meshdata, BufferUsage.Dynamic);
			mesh.Draw(Device.ImmediateContext, PrimitiveMode.TriangleList);
			meshdata.Vertices.Clear();
			meshdata.Indices.Clear();

			FontEffect.Unbind(Device.ImmediateContext);
		}

		public void Flush()
		{

		}

		public void Dispose()
		{
			mesh.Dispose();
		}
	}
}
