﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    public enum TextureFormat
    {
		R_32_Float,
		RG_32_Float,
		RGB_32_Float,
		RGBA_32_Float,
		BGR_32_Float,
		BGRA_32_Float,

		RGBA_8_UNorm
    };

	/// <summary>
	/// Helper class to handle implicit conversion
	/// Alternative methods can be found here:
	/// http://stackoverflow.com/questions/261663/can-we-define-implicit-conversions-of-enums-in-c
	/// </summary>
	public static class TextureFormatConversion
	{
		public static TextureFormat FromImaging(this System.Drawing.Imaging.PixelFormat value)
		{
			switch(value)
			{
				//this may require swizzling
				//these seem to work, almost as if Imaging pixel format is written in reverse...
				case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
					return TextureFormat.BGR_32_Float;

				case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
					return TextureFormat.BGRA_32_Float;

				//case System.Drawing.Imaging.PixelFormat.Format32bppPArgb:
				//	return TextureFormat.RGB_32_Float;

				default:
					throw new Exception("unsupported format : " + value);
			}
		}

#if DIRECTX11
		public static SharpDX.DXGI.Format ToDXGI(this TextureFormat value)
		{
			switch(value)
			{
				//normal layout
				case TextureFormat.R_32_Float:
					return SharpDX.DXGI.Format.R32_Float;
				case TextureFormat.RG_32_Float:
					return SharpDX.DXGI.Format.R32G32_Float;
				case TextureFormat.RGB_32_Float:
					return SharpDX.DXGI.Format.R32G32B32_Float;
				case TextureFormat.RGBA_32_Float:
					return SharpDX.DXGI.Format.R32G32B32A32_Float;
				case TextureFormat.RGBA_8_UNorm:
					return SharpDX.DXGI.Format.R8G8B8A8_UNorm;

				//reverse layout
				//DX11 Does not support this it seems
				//try swizzling on load, check http://www.gamedev.net/topic/643673-sharpdxloading-multiple-images-into-a-texture-array/

				//case TextureFormat.BGR_32_Float:
				//	return SharpDX.DXGI.Format.
				//case TextureFormat.BGRA_32_Float:


				default:
					throw new Exception("unsupported format : " + value);
					//return SharpDX.DXGI.Format.Unknown;
			}
		}
#endif

#if OPENGL
		public static OpenTK.Graphics.OpenGL.PixelFormat ToOpenGL(this TextureFormat value)
		{
			switch(value)
			{
				case TextureFormat.RGB_32_Float:
					return OpenTK.Graphics.OpenGL.PixelFormat.Rgb;
				case TextureFormat.RGBA_32_Float:
					return OpenTK.Graphics.OpenGL.PixelFormat.Rgba;

				case TextureFormat.BGR_32_Float:
					return OpenTK.Graphics.OpenGL.PixelFormat.Bgr;
				case TextureFormat.BGRA_32_Float:
					return OpenTK.Graphics.OpenGL.PixelFormat.Bgra;

				default:
					throw new Exception("Unsupported format: " + value);
			}
		}
#endif
	}
}
