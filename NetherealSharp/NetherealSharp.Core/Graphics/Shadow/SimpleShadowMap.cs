﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
    /// <summary>
    /// Single Shadow Map which consists of a single shadow map and shadowViewProj.
    /// A simple shadow map to sample from (will want to replace with a class that
    /// supports cascading shadow maps)
    /// </summary>
    public class SimpleShadowMap
    {
		public RenderTexture2D RenderTexture;
		public Matrix ShadowViewProj;

		public SimpleShadowMap(GraphicsDevice device)
		{
			RenderTexture = RenderTexture2D.Create(device, new Size(2048, 2048), true);
		}
    }
}
