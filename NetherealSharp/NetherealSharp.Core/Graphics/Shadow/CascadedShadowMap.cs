﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A collection of rendertextures and respective viewProj matrices for shadowmapping.
	/// </summary>
    public class CascadedShadowMap : IDisposable
    {
		public RenderTexture2D[] RenderTextures;
		public Matrix[] ShadowViewProj;

		public int Cascades { get; private set; }

		public CascadedShadowMap(GraphicsDevice device, int cascades, int resolution)
		{
			Cascades = cascades;
			RenderTextures = new RenderTexture2D[Cascades];

			ShadowViewProj = new Matrix[Cascades];

			for (int i = 0; i < Cascades; i++)
			{
				RenderTextures[i] = RenderTexture2D.Create(
					device, 
					new Size(resolution, resolution),
					false);
			}
		}

		public void Dispose()
		{
			for(int i = 0; i < Cascades; i++)
			{
				RenderTextures[i].Dispose();
			}
		}
	}
}
