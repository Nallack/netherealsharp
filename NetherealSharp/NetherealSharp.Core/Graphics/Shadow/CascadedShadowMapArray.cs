﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NetherealSharp.Core.Graphics
{
	/// <summary>
	/// A collection of rendertextures and respective viewProj matrices for shadowmapping.
	/// </summary>
	public class CascadedShadowMapArray : IDisposable
	{
		public RenderTexture2DArray RenderTextureArray;
		public Matrix[] ShadowViewProj;

		public int Cascades { get; private set; }

		public CascadedShadowMapArray(GraphicsDevice device, int cascades, int resolution)
		{
			Cascades = cascades;

			RenderTextureArray = RenderTexture2DArray.Create(
				device,
				resolution, resolution,
				cascades);
			ShadowViewProj = new Matrix[Cascades];

		}

		public void Dispose()
		{
			RenderTextureArray.Dispose();
		}
	}
}
