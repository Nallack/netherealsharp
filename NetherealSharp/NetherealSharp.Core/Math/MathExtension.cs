﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public static class MathF
    {

		public static float Cos(float v)
		{
			return (float)Math.Cos(v);
		}

		public static float Sin(float v)
		{
			return (float)Math.Sin(v);
		}
    }
}
