﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace NetherealSharp.Core
{
    public struct Complex : IEquatable<Complex>, IFormattable
    {
		public static readonly int SizeInBytes = Marshal.SizeOf(typeof(Complex));

		public static readonly Complex Zero = new Complex();

		public static readonly Complex Identity = new Complex(0.0f, 1.0f);

		public float X;

		public float Y;

		public Complex(float value)
		{
			X = value;
			Y = value;
		}

		public Complex(Vector2 value)
		{
			X = value.X;
			Y = value.Y;
		}

		public Complex(float x, float y)
		{
			X = x;
			Y = x;
		}

		public Complex(float[] values)
		{
			if (values == null)
				throw new ArgumentNullException("values");
			if (values.Length != 2)
				throw new ArgumentOutOfRangeException("values", "There must be two and only two input values for Complex.");

			X = values[0];
			Y = values[1];
		}

		public bool IsIdentity
		{
			get { return this.Equals(Identity); }
		}

		public bool IsNormalized
		{
			get { return MathUtil.IsOne((X * X) + (Y * Y)); }
		}

		public float Angle
		{
			get
			{
				float length = (X * X) + (Y * Y);
				if (MathUtil.IsZero(length))
					return 0.0f;

				return (float)Math.Acos(MathUtil.Clamp(Y, -1f, 1f));
			}
		}

		public float this[int index]
		{
			get
			{
				switch(index)
				{
					case 0: return X;
					case 1: return Y;
				}
				throw new ArgumentOutOfRangeException("index", "Indices for Complex run from 0 to 1, inclusive.");
			}

			set
			{
				switch (index)
				{
					case 0: X = value; break;
					case 1: Y = value; break;
					default: throw new ArgumentOutOfRangeException("index", "Indices for Complex run from 0 to 1, inclusive.");
				}
			}
		}

		public void Conjugate()
		{
			X = -X;
		}

		public void Invert()
		{
			float lengthSq = LengthSquared();
			if (!MathUtil.IsZero(lengthSq))
			{
				lengthSq = 1.0f / lengthSq;

				X = -X * lengthSq;
				Y = Y * lengthSq;
			}
		}

		public float Length()
		{
			return (float)Math.Sqrt((X * X) + (Y * Y));
		}

		public float LengthSquared()
		{
			return (X * X) + (Y * Y);
		}

		public void Normalize()
		{
			float length = Length();
			if (!MathUtil.IsZero(length))
			{
				float inverse = 1.0f / length;
				X *= inverse;
				Y *= inverse;
			}
		}

		public float[] ToArray()
		{
			return new float[] { X, Y };
		}

		public static void Add(ref Complex left, ref Complex right, out Complex result)
		{
			result.X = left.X + right.X;
			result.Y = left.Y + right.Y;
		}

		public static Complex Add(Complex left, Complex right)
		{
			Complex result;
			Add(ref left, ref right, out result);
			return result;
		}

		public static void Subtract(ref Complex left, ref Complex right, out Complex result)
		{
			result.X = left.X - right.X;
			result.Y = left.Y - right.Y;
		}

		public static Complex Subtract(Complex left, Complex right)
		{
			Complex result;
			Subtract(ref left, ref right, out result);
			return result;
		}

		public static void Multiply(ref Complex value, float scale, out Complex result)
		{
			result.X = value.X * scale;
			result.Y = value.Y * scale;
		}

		public static Complex Multiply(Complex left, float scale)
		{
			Complex result;
			Multiply(ref left, scale, out result);
			return result;
		}

		public static void Multiply(ref Complex left, ref Complex right, out Complex result)
		{
			result.X = left.X * right.Y + left.Y * right.Y;
			result.Y = left.Y * right.Y - left.X * right.X;
		}

		public static Complex Multiply(Complex left, Complex right)
		{
			Complex result;
			Multiply(ref left, ref right, out result);
			return result;
		}

		public static void Negate(ref Complex value, out Complex result)
		{
			result.X = -value.X;
			result.Y = -value.Y;
		}

		public static Complex Negate(Complex value)
		{
			Complex result;
			Negate(ref value, out result);
			return result;
		}

		public static void Barycentric(ref Complex value1, ref Complex value2, ref Complex value3, float amount1, float amount2, out Complex result)
		{
			Complex start, end;
			Slerp(ref value1, ref value2, amount1 + amount2, out start);
			Slerp(ref value1, ref value3, amount1 + amount2, out end);
			Slerp(ref start, ref end, amount2 / (amount1 + amount2), out result);
		}

		public static Complex Barycentric(Complex value1, Complex value2, Complex value3, float amount1, float amount2)
		{
			Complex result;
			Barycentric(ref value1, ref value2, ref value3, amount1, amount2, out result);
			return result;
		}

		public static void Conjugate(ref Complex value, out Complex result)
		{
			result.X = -value.X;
			result.Y = value.Y;
		}

		public static Complex Conjugate(Complex value)
		{
			Complex result;
			Conjugate(ref value, out result);
			return result;
		}

		public static void Dot(ref Complex left, ref Complex right, out float result)
		{
			result = (left.X * right.X) + (left.Y * right.Y);
		}

		public static float Dot(Complex left, Complex right)
		{
			return (left.X * right.X) + (left.Y + right.Y);
		}

		public static void Exponential(ref Complex value, out Complex result)
		{
			float angle = value.X;
			float sin = (float)Math.Sin(angle);

			if (!MathUtil.IsZero(sin))
			{
				float coeff = sin / angle;
				result.X = coeff * value.X;
			}
			else
			{
				result = value;
			}

			result.Y = (float)Math.Cos(angle);
		}

		public static Complex Exponential(Complex value)
		{
			Complex result;
			Exponential(ref value, out result);
			return result;
		}

		public static void Invert(ref Complex value, out Complex result)
		{
			result = value;
			result.Invert();
		}

		public static Complex Invert(Complex value)
		{
			Complex result;
			Invert(ref value, out result);
			return result;
		}


		public static void Lerp(ref Complex start, ref Complex end, float amount, out Complex result)
		{
			float inverse = 1.0f - amount;

			result.X = (inverse * start.X) + (amount * end.X);
			result.Y = (inverse * start.Y) + (amount * end.Y);
		}

		public static Complex Lerp(Complex start, Complex end, float amount)
		{
			Complex result;
			Lerp(ref start, ref end, amount, out result);
			return result;

		}

		/*
		public static void Logarithm(ref Complex value, out Complex result)
		{

		}






		*/

		public static void Normalize(ref Complex value, out Complex result)
		{
			Complex temp = value;
			result = temp;
			result.Normalize();
		}
		

		public static Complex Normalize(Complex value)
		{
			value.Normalize();
			return value;
		}

		public static void RotationMatrix(ref Matrix3x3 matrix, out Complex result)
		{
			throw new NotImplementedException();
		}


		public static void Slerp(ref Complex start, ref Complex end, float amount, out Complex result)
		{
			float opposite;
			float inverse;
			float dot = Dot(start, end);

			if (Math.Abs(dot) > 1.0f - MathUtil.ZeroTolerance)
			{
				inverse = 1.0f - amount;
				opposite = amount * Math.Sign(dot);
			}
			else
			{
				float acos = (float)Math.Acos(Math.Abs(dot));
				float invSin = (float)(1.0 / Math.Sin(acos));

				inverse = (float)Math.Sin((1.0f - amount) * acos) * invSin;
				opposite = (float)Math.Sin(amount * acos) * invSin * Math.Sign(dot);
			}

			result.X = (inverse * start.X) + (opposite * end.X);
			result.Y = (inverse * start.Y) + (opposite * end.Y);
		}


		public static Complex operator +(Complex left, Complex right)
		{
			Complex result;
			Add(ref left, ref right, out result);
			return result;
		}

		public static Complex operator -(Complex left, Complex right)
		{
			Complex result;
			Subtract(ref left, ref right, out result);
			return result;
		}

		public bool Equals(Complex other)
		{
			throw new NotImplementedException();
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			throw new NotImplementedException();
		}
	}
}
