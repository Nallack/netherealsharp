﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class RigidDynamic
    {
		public PhysicsContext3D Context { get; protected set; }

		public abstract bool IsKinematic { get; set; }

		public abstract Matrix Transform { get; }

		public abstract void AddToWorld(PhysicsWorld3D world);
		public abstract void RemoveFromWorld();

		public static RigidDynamic Create(PhysicsContext3D context, Matrix transform)
		{
			switch(context.Implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return new PhysX.RigidDynamic(context as PhysX.PhysicsContext3D, transform);
#endif
#if BULLET
				case PhysicsImplementation3D.Bullet:
					return new Bullet.RigidDynamic(context as Bullet.PhysicsContext3D, transform);
#endif
				default:
					throw new NotImplementedException();
			}


		}

    }
}
