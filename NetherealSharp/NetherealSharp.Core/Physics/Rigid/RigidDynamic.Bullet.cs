﻿#if BULLET
using System;
using System.Collections.Generic;
using System.Text;

using Bp = BulletSharp;
using Base = NetherealSharp.Core.Physics;


namespace NetherealSharp.Core.Physics.Bullet
{
    public class RigidDynamic : Base.RigidDynamic
    {
		public Bp.RigidBody Rigidbody { get; private set; }

		public Bp.DiscreteDynamicsWorld World { get; private set; }

		public override Matrix Transform { get { return Rigidbody.WorldTransform; } }

		public override bool IsKinematic
		{
			get
			{
				return (Rigidbody.CollisionFlags & Bp.CollisionFlags.KinematicObject) == Bp.CollisionFlags.KinematicObject;
			}

			set
			{
				//TEST
				World.RemoveRigidBody(Rigidbody);

				if (value)
				{
					Rigidbody.CollisionFlags |= Bp.CollisionFlags.KinematicObject | Bp.CollisionFlags.StaticObject;
					Rigidbody.SetMassProps(0, Vector3.Zero);
				}
				else Rigidbody.CollisionFlags &= ~Bp.CollisionFlags.KinematicObject;
				Rigidbody.SaveKinematicState(0);

				//TEST
				World.AddRigidBody(Rigidbody);
			}
		}

		public RigidDynamic(PhysicsContext3D context, Matrix transform)
		{

			var rbci = new Bp.RigidBodyConstructionInfo(
				1,
				new Bp.DefaultMotionState(transform),
				null);
			
			Rigidbody = new Bp.RigidBody(rbci);
			//Rigidbody.CollisionShape = new Bp.BoxShape(new BulletSharp.Vector3(1, 1, 1));
			//Rigidbody.CollisionFlags = Bp.CollisionFlags.KinematicObject;


			//Add after colliders get attached
			//world.World.AddRigidBody(Rigidbody);
			//world.World.AddRigidBody(Rigidbody)
		}

		public override void AddToWorld(Base.PhysicsWorld3D world)
		{
			(world as PhysicsWorld3D).World.AddRigidBody(this.Rigidbody);
			this.World = (world as PhysicsWorld3D).World;
		}


		public override void RemoveFromWorld()
		{
			World.RemoveRigidBody(this.Rigidbody);
			this.World = null;
		}
	}
}
#endif