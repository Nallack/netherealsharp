﻿#if PHYSX
using System;
using System.Collections.Generic;
using System.Text;

using Px = PhysX;

using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.PhysX
{
    public class RigidDynamic : Base.RigidDynamic
    {
		public PhysicsContext3D context;

		public Px.RigidDynamic rigidbody;

		public Px.Scene Scene { get; private set; }

		public override Matrix Transform { get { return rigidbody.GlobalPose; } }

		public override bool IsKinematic
		{
			get
			{
				return (rigidbody.RigidBodyFlags & Px.RigidBodyFlag.Kinematic) == Px.RigidBodyFlag.Kinematic;
			}
			set
			{
				if (value) rigidbody.RigidBodyFlags |= Px.RigidBodyFlag.Kinematic;
				else rigidbody.RigidBodyFlags &= ~Px.RigidBodyFlag.Kinematic;
			}
		}


		public RigidDynamic(PhysicsContext3D context, Matrix transform)
		{
			rigidbody = context.Physics.CreateRigidDynamic();

			//Px.Geometry boxgeom = new Px.BoxGeometry()
			//{
			//	Size = new Px.Math.Vector3(1, 1, 1)
			//};

			//Px.Material material = context.Physics.CreateMaterial(0.7f, 0.7f, 0.1f);

			//rigidbody.CreateShape(boxgeom, material);

			rigidbody.GlobalPose = transform;
			//rigidbody.Mass = 1;
			rigidbody.SetMassAndUpdateInertia(10);

			//add to scene after collision objects are attached
			//world.Scene.AddActor(rigidbody);


		}

		public override void AddToWorld(Base.PhysicsWorld3D world)
		{
			(world as PhysicsWorld3D).Scene.AddActor(this.rigidbody);
			this.Scene = (world as PhysicsWorld3D).Scene;
		}

		public override void RemoveFromWorld()
		{
			Scene.RemoveActor(rigidbody);
			this.Scene = null;
		}
	}
}
#endif