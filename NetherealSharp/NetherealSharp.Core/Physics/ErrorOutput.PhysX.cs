﻿#if PHYSX
using System;
using System.Collections.Generic;
using System.Text;

using PhysX;

namespace NetherealSharp.Core.Physics.PhysX
{
	public class ErrorOutput : ErrorCallback
	{
		public override void ReportError(ErrorCode errorCode, string message, string file, int lineNumber)
		{
			Console.WriteLine("PhysX: " + message);
		}
	}
}
#endif