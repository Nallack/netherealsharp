﻿
#if PHYSX
using System;
using System.Collections.Generic;
using System.Text;

using Px = PhysX;
using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.PhysX
{
    public class PhysicsContext3D : Base.PhysicsContext3D
    {
		public Px.Foundation Foundation;
		public Px.Physics Physics;
		public Px.CudaContextManager CudaContext;

		public PhysicsContext3D()
		{
			Px.ErrorCallback errorOutput = new ErrorOutput();
			Foundation = new Px.Foundation(errorOutput);

			Physics = new Px.Physics(Foundation, checkRuntimeFiles: true);

            try
            {
                CudaContext = new Px.CudaContextManager(Foundation);
            }
            catch(Exception e)
            {
                
            }
		}

		public override void Dispose()
		{
			if(CudaContext != null) CudaContext.Dispose();
			Physics.Dispose();
			Foundation.Dispose();
		}
	}
}
#endif