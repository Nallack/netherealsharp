﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class PhysicsContext2D
    {
		public PhysicsImplementation2D Implementation { get; protected set; }

		public static PhysicsContext2D Create(PhysicsImplementation2D implementation)
		{
			switch (implementation)
			{
#if BOX2D
				case PhysicsImplementation2D.Box2D:
					return new Box2D.PhysicsContext2D();
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract PhysicsWorld2D CreateWorld();

		public abstract void Dispose();
	}
}
