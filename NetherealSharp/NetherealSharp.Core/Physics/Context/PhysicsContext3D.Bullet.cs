﻿#if BULLET
using System;
using System.Collections.Generic;
using System.Text;

using BulletSharp;
using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.Bullet
{
    public class PhysicsContext3D : Base.PhysicsContext3D
    {
		public CollisionConfiguration CollisionConfiguration;
		public Dispatcher Dispatcher;
		public BroadphaseInterface Broadphase;

		public PhysicsContext3D()
		{
			CollisionConfiguration = new DefaultCollisionConfiguration();
			Dispatcher = new CollisionDispatcher(CollisionConfiguration);
			Broadphase = new DbvtBroadphase();
		}

		public override void Dispose()
		{
			Broadphase.Dispose();
			Dispatcher.Dispose();
			CollisionConfiguration.Dispose();
		}
	}
}
#endif