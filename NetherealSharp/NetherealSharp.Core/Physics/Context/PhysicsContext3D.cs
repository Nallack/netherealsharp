﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class PhysicsContext3D : IDisposable
    {
		public PhysicsImplementation3D Implementation { get; protected set; }


		public static PhysicsContext3D Create(PhysicsImplementation3D implementation)
		{
			switch(implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return new PhysX.PhysicsContext3D();
#endif
#if BULLET
				case PhysicsImplementation3D.Bullet:
					return new Bullet.PhysicsContext3D();
#endif
#if BEPU
#endif
#if JITTER
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public abstract void Dispose();
	}
}
