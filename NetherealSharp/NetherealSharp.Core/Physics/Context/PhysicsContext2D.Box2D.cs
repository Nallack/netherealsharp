﻿#if BOX2D
using System;
using System.Collections.Generic;
using System.Text;


using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.Box2D
{
    public class PhysicsContext2D : Base.PhysicsContext2D
    {


		public override Base.PhysicsWorld2D CreateWorld()
		{
			return new PhysicsWorld2D(this);
		}

		public override void Dispose()
		{

		}
    }
}
#endif