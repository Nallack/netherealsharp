﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
	public enum PhysicsImplementation2D
	{
		None,
		Box2D,
		Farseer
	};

	public enum PhysicsImplementation3D
	{
		None,
		PhysX,
		Bullet,
		BEPU,
		Jitter
	};
}
