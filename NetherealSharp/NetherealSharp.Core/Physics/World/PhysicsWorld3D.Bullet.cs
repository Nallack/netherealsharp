﻿#if BULLET
using System;
using System.Collections.Generic;
using System.Text;

using BulletSharp;

using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.Bullet
{
    public class PhysicsWorld3D : Base.PhysicsWorld3D
    {
		public DiscreteDynamicsWorld World;

		public PhysicsWorld3D(PhysicsContext3D context)
		{ 

			World = new DiscreteDynamicsWorld(
				context.Dispatcher, 
				context.Broadphase, 
				null, 
				context.CollisionConfiguration);

			World.Gravity = new Vector3(1f, -9.8f, 0);
		}

		public override void Update(float dt)
		{
			World.StepSimulation(dt);
		}
	}
}
#endif