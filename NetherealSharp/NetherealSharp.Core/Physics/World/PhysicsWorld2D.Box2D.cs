﻿
#if BOX2D
using System;
using System.Collections.Generic;
using System.Text;

using Box2DX;

using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.Box2D
{
    public class PhysicsWorld2D : Base.PhysicsWorld2D
    {
		Box2DX.Collision.AABB AABB;
		Box2DX.Dynamics.World World;

		public PhysicsWorld2D(PhysicsContext2D context)
		{
			AABB = new Box2DX.Collision.AABB();
			World = new Box2DX.Dynamics.World(
				AABB, 
				new Box2DX.Common.Vec2(0, 1),
				true);

		}

		public override void Update(float dt)
		{
			World.Step(dt, 1, 1);
		}
	}
}
#endif