﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class PhysicsWorld2D
    {


		public abstract void Update(float dt);

		public static PhysicsWorld2D Create(PhysicsContext2D context)
		{
			switch(context.Implementation)
			{
#if BOX2D
				case PhysicsImplementation2D.Box2D:
					return new Box2D.PhysicsWorld2D(context as Box2D.PhysicsContext2D);
#endif
#if FARSEER
				case PhysicsManager
#endif
				default:
					throw new NotImplementedException();
			}


		}
    }
}
