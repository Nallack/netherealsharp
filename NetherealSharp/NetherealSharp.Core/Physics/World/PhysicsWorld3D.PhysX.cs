﻿#if PHYSX
using System;
using System.Collections.Generic;
using System.Text;

using Px = PhysX;

using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.PhysX
{
    public class PhysicsWorld3D : Base.PhysicsWorld3D
    {
		public Px.Scene Scene;

		public PhysicsWorld3D(PhysicsContext3D context)
		{

			var sceneDesc = new Px.SceneDesc()
			{
				Gravity = new Px.Math.Vector3(0.0f, -9.8f, 0),
				FrictionType = Px.FrictionType.TwoDirectional,
				GpuDispatcher = context.CudaContext.GpuDispatcher
			};

			Scene = context.Physics.CreateScene(sceneDesc);
		}


		public override void Update(float dt)
		{
			
			Scene.Simulate(dt);
			Scene.FetchResults(true);
		}
	}
}
#endif