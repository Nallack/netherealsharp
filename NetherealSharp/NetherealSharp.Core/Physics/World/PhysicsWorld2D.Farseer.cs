﻿#if FARSEER
using System;
using System.Collections.Generic;
using System.Text;

using FarseerPhysics;
using FarseerPhysics.Dynamics;
using XNA = Microsoft.Xna.Framework;

namespace NetherealSharp.Core.Physics.Farseer
{
    public class PhysicsWorld2D
    {
		public World World { get; private set; }

		public PhysicsWorld2D()
		{
			World = new World(new XNA.Vector2(0f, -1.1f));

			//ConvertUnits.SetDisplayUnitToSimUnitRatio(64.0f);

		}

		public void Update()
		{
			World.Step(TimeManager.DeltaTime);
		}

	}
}
#endif