﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class PhysicsWorld3D
    {
		public List<RigidDynamic> Bodies;

		public PhysicsWorld3D()
		{
			Bodies = new List<RigidDynamic>(); //track all bodies, allow for turning on debug lines etc.
		}

		public abstract void Update(float dt);


		public static PhysicsWorld3D Create(PhysicsContext3D context)
		{
			switch(context.Implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return new PhysX.PhysicsWorld3D(context as PhysX.PhysicsContext3D);
#endif
#if BULLET
				case PhysicsImplementation3D.Bullet:
					return new Bullet.PhysicsWorld3D(context as Bullet.PhysicsContext3D);
#endif
#if BEPU
				
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
