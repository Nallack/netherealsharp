﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Physics
{
    public abstract class Collider
	{
		public static Collider CreateBox(RigidDynamic rigiddynamic, Vector3 size)
		{
			switch(rigiddynamic.Context.Implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return PhysX.Collider.CreateBox(rigiddynamic as PhysX.RigidDynamic, size);
#endif
#if BULLET
				case PhysicsImplementation3D.Bullet:
					return Bullet.Collider.CreateBox(rigiddynamic as Bullet.RigidDynamic, size);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public static Collider CreateSphere(RigidDynamic rigiddynamic, float radius)
		{
			switch (rigiddynamic.Context.Implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return PhysX.Collider.CreateSphere(rigiddynamic as PhysX.RigidDynamic, radius);
#endif
#if BULLET
				case PhysicsImplementation3D.Bullet:
					return Bullet.Collider.CreateSphere(rigiddynamic as Bullet.RigidDynamic, radius);
#endif
				default:
					throw new NotImplementedException();
			}
		}

		public static Collider CreateMesh<T>(RigidDynamic rigiddynamic, IndexedMeshData<T> data) where T : struct, IVertex
		{
			switch(rigiddynamic.Context.Implementation)
			{
#if PHYSX
				case PhysicsImplementation3D.PhysX:
					return PhysX.Collider.CreateMesh<T>(rigiddynamic, data);
#endif
				default:
					throw new NotImplementedException();
			}
		}
	}
}
