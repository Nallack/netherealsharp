﻿#if BULLET
using System;
using System.Collections.Generic;
using System.Text;

using Bp = BulletSharp;
using Base = NetherealSharp.Core.Physics;

namespace NetherealSharp.Core.Physics.Bullet
{
    public class Collider : Base.Collider
    {
		private RigidDynamic m_RigidBody;

		private Collider(RigidDynamic body)
		{
			m_RigidBody = body;
		}

		public static new Collider CreateBox(RigidDynamic body, Vector3 size)
		{
			Collider c = new Collider(body);
			c.m_RigidBody.Rigidbody.CollisionShape = new Bp.BoxShape(size/2);
			return c;
		}

		public static new Collider CreateSphere(RigidDynamic body, float radius)
		{
			Collider c = new Collider(body);
			c.m_RigidBody.Rigidbody.CollisionShape = new Bp.SphereShape(radius);
			return c;
		}
	}
}
#endif