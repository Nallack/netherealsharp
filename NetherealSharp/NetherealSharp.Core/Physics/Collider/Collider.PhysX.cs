﻿#if PHYSX
using System;
using System.Collections.Generic;
using System.Text;

using Px = PhysX;
using Base = NetherealSharp.Core.Physics;
using NetherealSharp.Core.Graphics;
using System.IO;

namespace NetherealSharp.Core.Physics.PhysX
{
    public class Collider : Base.Collider
    {
		private PhysicsContext3D m_Context;
		private RigidDynamic m_RigidBody;

		public Px.Shape Shape { get; private set; }


		private Collider(RigidDynamic body)
		{
			m_RigidBody = body;
			m_Context = (body as PhysX.RigidDynamic).context;
		}


		public static new Collider CreateSphere(RigidDynamic rigiddynamic, float radius)
		{
			Collider c = new Collider(rigiddynamic);

			Px.Geometry spheregeom = new Px.SphereGeometry()
			{
				Radius = radius
			};

			Px.Material material = c.m_Context.Physics.CreateMaterial(1, 1, 0.1f);
			c.Shape = c.m_RigidBody.rigidbody.CreateShape(spheregeom, material);
			return c;
		}

		public static new Collider CreateBox(RigidDynamic rigiddynamic, Vector3 size)
		{
			Collider c = new Collider(rigiddynamic);
			Px.Geometry boxgeom = new Px.BoxGeometry()
			{
				Size = new Px.Math.Vector3(size.X, size.Y, size.Z)
			};

			Px.Material material = c.m_Context.Physics.CreateMaterial(1, 1, 0.1f);
			c.Shape = c.m_RigidBody.rigidbody.CreateShape(boxgeom, material);

			return c;
		}

		public static new Collider CreateMesh<T>(RigidDynamic rigiddynamic, IndexedMeshData<T> data) where T : struct, IVertex, IVertexPos
		{
			Collider c = new Collider(rigiddynamic);

			Converter<T, Px.Math.Vector3> converter = (input) => { return input.Position; };

			var desc = new Px.TriangleMeshDesc()
			{
				Flags = (Px.MeshFlag)0,
				Triangles = data.Indices.ToArray(),
				Points = data.Vertices.ConvertAll<Px.Math.Vector3>(converter).ToArray()
			};

			var cooking = c.m_Context.Physics.CreateCooking();

			var stream = new MemoryStream();
			bool cookresult = cooking.CookTriangleMesh(desc, stream);
			stream.Position = 0;

			var trianglemesh = c.m_Context.Physics.CreateTriangleMesh(stream);
			var geom = new Px.TriangleMeshGeometry(trianglemesh);

			Px.Material material = c.m_Context.Physics.CreateMaterial(1, 1, 0.1f);

			c.Shape = c.m_RigidBody.rigidbody.CreateShape(geom, material);

			return c;
		}
    }
}
#endif