﻿#if DIRECTX11 && WPF
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Base = NetherealSharp.Core.Windows;

namespace NetherealSharp.Core.Windows.DX11
{


    public class RenderElement : Base.RenderElement
    {
		public Microsoft.Wpf.Interop.DirectX.D3D11Image D3D11Image;

		public event Action<IntPtr> SurfaceChanged;

		public RenderElement()
		{
			if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
			{

				D3D11Image = new Microsoft.Wpf.Interop.DirectX.D3D11Image();

				this.AddVisualChild(new System.Windows.Controls.Image()
				{
					Source = D3D11Image
				});

				SizeChanged += (sender, e) =>
				{
					D3D11Image.SetPixelSize((int)ActualWidth, (int)ActualHeight);
				};

				Loaded += (sender, e) =>
				{
					D3D11Image.WindowOwner = (new System.Windows.Interop.WindowInteropHelper(
						System.Windows.Window.GetWindow(this))).Handle;

					D3D11Image.OnRender = (surface, resized) =>
					{
						if (resized) if (SurfaceChanged != null) SurfaceChanged(surface);
					};

				};
			}
		}

		public void RequestRender()
		{
			D3D11Image.RequestRender();
		}
    }
}
#endif