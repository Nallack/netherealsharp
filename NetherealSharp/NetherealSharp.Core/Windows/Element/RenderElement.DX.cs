﻿#if WPF
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

using Base = NetherealSharp.Core.Windows;

namespace NetherealSharp.Core.Windows.DX
{
    public class RenderElement : Base.RenderElement
    {
		public D3DImage D3DImage { get; protected set; }

		public RenderElement()
		{
			SnapsToDevicePixels = true;
			D3DImage = new D3DImage();
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			drawingContext.DrawImage(D3DImage, new Rect(RenderSize));
		}
	}
}
#endif