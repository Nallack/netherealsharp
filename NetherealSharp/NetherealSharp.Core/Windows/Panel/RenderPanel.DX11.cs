﻿#if DIRECTX11 && WPF
using System;
using System.Collections.Generic;
using System.Text;

using Base = NetherealSharp.Core.Windows;

namespace NetherealSharp.Core.Windows.DX11
{
    public class RenderPanel : Base.RenderPanel
	{
		public Microsoft.Wpf.Interop.DirectX.D3D11Image D3D11Image;

		public RenderPanel()
		{
			D3D11Image = new Microsoft.Wpf.Interop.DirectX.D3D11Image();

			Children.Add(new System.Windows.Controls.Image()
			{
				Source = D3D11Image
			});

			SizeChanged += (sender, e) =>
			{
				D3D11Image.SetPixelSize((int)ActualWidth, (int)ActualHeight);
			};
		}

	}
}
#endif