﻿#if FORMS

using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using System.Windows.Forms;

namespace NetherealSharp.Core.Windows
{
	/// <summary>
	/// Class for creating a Windows Forms Render Control
	/// RenderControls currently don't inherit from this as they already inherit other classes
	/// May want to consider following and adpater pattern or similar.
	/// </summary>
    public static class RenderControl
    {
		public static Control CreateRenderControl(GraphicsImplementation implementation)
		{
			switch (implementation)
			{
#if DIRECTX11
				case GraphicsImplementation.DirectX11:
					return new DX.RenderControl();
#endif
#if DIRECTX12
				case GraphicsImplementation.DirectX12:
					return new DX.RenderControl();
#endif
#if OPENGL
				case GraphicsImplementation.OpenGL:
					var control = new OpenGL.RenderControl();

					//getting transparent format issues here...

					//Configures a maximized glControl
					//var flags = OpenTK.Graphics.GraphicsContextFlags.Debug;

					//var control = new OpenTK.GLControl(
					//	new OpenTK.Graphics.GraphicsMode(
					//		new OpenTK.Graphics.ColorFormat(8, 8, 8, 8),
					//		8,
					//		8,
					//		4,
					//		new OpenTK.Graphics.ColorFormat(8, 8, 8, 8),
					//		2,
					//		false),
					//	3,
					//	2,
					//	flags);

					control.MakeCurrent();
					return control;
#endif
#if VULKAN
				case GraphicsImplementation.Vulkan:
					return new Control();
#endif
				default:
					throw new NotImplementedException();
			}
		}
    }
}
#endif