﻿
#if OPENGL
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using OpenTK;

namespace NetherealSharp.Core.Windows.OpenGL
{
    public class RenderControl : OpenTK.GLControl
    {

		protected override bool IsInputKey(Keys keyData)
		{
			//Don't process any keys here
			//Need to return true allow framework to hear modifier keypresses
			return true;
			//return base.IsInputKey(keyData);
		}
	}
}
#endif