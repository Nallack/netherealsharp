﻿
#if FORMS
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;


namespace NetherealSharp.Core.Windows.DX
{
    public class RenderControl : SharpDX.Windows.RenderControl
    {

		protected override bool IsInputKey(Keys keyData)
		{
			//Don't process any keys automatically
			//Need to do this to get Modifier keys for form control


			return true;
			//return base.IsInputKey(keyData);
		}
	}
}
#endif