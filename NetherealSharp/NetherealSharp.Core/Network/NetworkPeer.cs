﻿#if LIDGREN
using System;
using System.Collections.Generic;
using System.Text;

using Lidgren.Network;

namespace NetherealSharp.Core.Network
{
    class NetworkPeer
    {
		public NetPeer Peer;

		public NetworkPeer()
		{
			NetPeerConfiguration config = new NetPeerConfiguration("netherealapp");

			Peer = new NetPeer(config);
		}
    }
}
#endif