﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetherealSharp.Core
{
	/// <summary>
	/// Interface used to allow content to be serialized to a name
	/// and then looked up upon deserialization.
	/// </summary>
    public interface IContentable
    {
		string Name { get; set; }

		Type ContentType { get; }

		//void Serialize(Stream stream, SerializationSettings settings);
		//object Deserialize(Stream stream, SerializationSettings settings);
    }
}
