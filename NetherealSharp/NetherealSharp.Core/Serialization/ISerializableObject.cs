﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Serialization
{
    public interface ISerializableObject
    {
		XmlObjectSerializer Serializer { get; }
    }
}
