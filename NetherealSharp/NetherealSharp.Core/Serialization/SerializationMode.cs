﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Serialization
{
    public enum SerializationMode
    {
		XML,
		DataContractXML,
		DataContractJSON,
		YAML,
		Binary
    }
}
