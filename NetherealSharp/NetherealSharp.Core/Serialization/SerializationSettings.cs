﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace NetherealSharp.Core.Serialization
{
    public class SerializationSettings
    {
		public SerializationMode Mode { get; set; }

		public IEnumerable<Type> AssemblyBehaviours { get; set; }

		//avoid recreating these and share a single instance apparently?

		public XmlObjectSerializer SceneSerializer { get; set; }
    }
}
