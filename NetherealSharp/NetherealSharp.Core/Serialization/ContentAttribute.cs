﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Serialization
{
	/// <summary>
	/// Use this attribute when you want the serializer
	/// to serialize this object under a different class,
	/// (normally an abstract class of this object where implementation
	/// does not matter)
	/// </summary>
    public class ContentAttribute : Attribute
    {
		public Type RegistryType;
    }
}
