﻿using System;
using System.Collections.Generic;
using System.Text;

//http://stackoverflow.com/questions/1744120/datacontractserializer-with-multiple-namespaces/6574024#6574024

namespace NetherealSharp.Core.Serialization
{
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public sealed class NamespaceAttribute : Attribute
    {
		public string Prefix { get; set; }
		public string Uri { get; set; }
		public NamespaceAttribute() { }
		public NamespaceAttribute(string prefix, string uri)
		{
			Prefix = prefix;
			Uri = uri;
		}
    }
}
