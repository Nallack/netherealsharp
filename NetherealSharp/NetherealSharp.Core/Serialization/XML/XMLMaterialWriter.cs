﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

using NetherealSharp.Core.Graphics;
using System.IO;

namespace NetherealSharp.Core.Content
{
    public class XMLMaterialWriter : IContentWriter
    {
		XmlSerializer Serializer;

		public XMLMaterialWriter()
		{
			Serializer = new XmlSerializer(typeof(Material));
		}


		public bool WriteContent(string filepath, object content)
		{
			try
			{
				var mat = content as Material;

				using (TextWriter writer = new StreamWriter(filepath))
				{
					Serializer.Serialize(writer, mat);
				}
			}
			catch(Exception)
			{
				return false;
			}

			return true;
		}
	}
}
