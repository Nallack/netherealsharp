﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

#if DIRECTX11
using D3D = SharpDX.Direct3D;
using D3DC = SharpDX.D3DCompiler;
#endif

using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Core.Content.Binary
{
	/// <summary>
	/// Compile shader data to binary stream
	/// </summary>
    public class ShaderCompiler
    {

#if DIRECTX11
		public byte[] CompileShader(string filepath, ShaderType type)
		{
			D3D.ShaderMacro[] macros = new D3D.ShaderMacro[1];
			ShaderInclude include = new ShaderInclude();

			var result = D3DC.ShaderBytecode.CompileFromFile(
				filepath,
				"VS",
				"vs_4_0",
				D3DC.ShaderFlags.None,
				D3DC.EffectFlags.None,
				macros,
				include);

			return result.Bytecode.Data;

		}
#endif

	}
}
