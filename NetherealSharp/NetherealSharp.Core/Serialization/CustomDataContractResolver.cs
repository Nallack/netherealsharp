﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace NetherealSharp.Core.Serialization
{
	/// <summary>
	/// Custom resolver that can serialize particular abstract classes and deserialize with a suitable overriding class.
	/// </summary>
	public class CustomDataContractResolver : DataContractResolver
	{
		//private Dictionary<string, XmlDictionaryString> m_Dictionary;

		private string defualtNS = "http://schemas.datacontract.org/2004/07/";

		public CustomDataContractResolver(Assembly assembly)
		{
			//m_Dictionary = new Dictionary<string, XmlDictionaryString>();
		}

		public override Type ResolveName(string typeName, string typeNamespace, Type declaredType, DataContractResolver knownTypeResolver)
		{
			if(typeNamespace == defualtNS + "NetherealSharp.Core.Graphics")
			{

				//use surrogate here instead
				//if (declaredType == typeof(Graphics.Effect))
				//{
				//	return typeof(Graphics.DX11.BasicEffect);
				//}

			}

			return knownTypeResolver.ResolveName(typeName, typeNamespace, declaredType, knownTypeResolver);
		}

		public override bool TryResolveType(Type type, Type declaredType, DataContractResolver knownTypeResolver, out XmlDictionaryString typeName, out XmlDictionaryString typeNamespace)
		{

			string name = type.Name;
			string namesp;

			//if (declaredType.Namespace == "NetherealSharp.Core.Graphics") 
			//	namesp = defualtNS + declaredType.Namespace;
			//else

			namesp = defualtNS + type.Namespace;

			typeName = new XmlDictionaryString(XmlDictionary.Empty, name, 0);
			typeNamespace = new XmlDictionaryString(XmlDictionary.Empty, namesp, 0);
			return true;


		}
	}
}
