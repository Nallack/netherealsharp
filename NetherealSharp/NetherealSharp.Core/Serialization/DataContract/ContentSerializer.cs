﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;

namespace NetherealSharp.Core.Serialization
{
	public abstract class ContentSerializer
	{
		public SerializationSettings Settings;

		public abstract void Serialize<T>(string filename, T data) where T : class;
		public abstract void Serialize<T>(Stream stream, T data) where T : class;

		public abstract T Deserialize<T>(string filename) where T : class;
		public abstract T Deserialize<T>(Stream stream) where T : class;
	}
}
