﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace NetherealSharp.Core.Serialization
{
    public static class DataContractExtensions
    {
		public static void WriteObject(
			this XmlObjectSerializer serializer,
			XmlWriter writer, object data,
			Dictionary<string,string> namespaces)
		{
			serializer.WriteStartObject(writer, data);
			foreach(var pair in namespaces)
			{
				writer.WriteAttributeString("xmlns", pair.Key, null, pair.Value);
			}
			serializer.WriteObjectContent(writer, data);
			serializer.WriteEndObject(writer);
		}

		public static void WriteObject(
			this XmlObjectSerializer serializer,
			XmlDictionaryWriter writer,
			DataContractResolver resolver,
			object data,
			Dictionary<string, string> namespaces)
		{

			serializer.WriteStartObject(writer, data);

			foreach(var pair in namespaces)
				{
				writer.WriteAttributeString("xmlns", pair.Key, null, pair.Value);
			}

			serializer.WriteObjectContent(writer, data);
			serializer.WriteEndObject(writer);
		}
    }
}
