﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Core.Serialization
{
	/// <summary>
	/// A surrogate that can be for objects that are intended to be loaded via the content manager.
	/// </summary>
	[DataContract]
	public class ContentSurrogate
    {
		[DataMember]
		public string Name { get; set; }

		//[DataMember]
		public Type Type { get; set; }


		public ContentSurrogate(string name, Type type)
		{
			Name = name;
			Type = type;
		}
	}
}
