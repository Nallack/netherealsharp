﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using X3D = SharpDX.X3DAudio;

namespace NetherealSharp.Core.Audio
{
    public class AudioListener
    {
		public X3D.Listener Listener;

		public AudioListener()
		{
			Listener = new X3D.Listener()
			{
				OrientFront = new Vector3(0, 0, 1),
				OrientTop = new Vector3(0, 1, 0),
				Position = new Vector3(0, 0, 0),
				Velocity = new Vector3(0, 0, 0)
			};

		}
    }
}

#endif