﻿#if DIRECTX

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using XAudio2 = SharpDX.XAudio2;
using MM = SharpDX.Multimedia;

namespace NetherealSharp.Core.Audio.XA2
{
    public class AudioBuffer : Audio.AudioBuffer, IDisposable
    {

		//This will need to be moved to a sound emmiter class
		public XAudio2.SourceVoice Voice;

		public MM.SoundStream SoundStream;
		public MM.WaveFormat Format;

		public XAudio2.AudioBuffer Buffer;
		public uint[] DecodedPacketsInfo;

		public void LoadFromFile(AudioDevice device, string filename)
		{
			var SoundStream = new MM.SoundStream(File.OpenRead(filename));

			Format = SoundStream.Format;

			Buffer = new XAudio2.AudioBuffer()
			{
				Stream = SoundStream.ToDataStream(),
				AudioBytes = (int)SoundStream.Length,
				Flags = XAudio2.BufferFlags.EndOfStream
			};

			SoundStream.Close();

			DecodedPacketsInfo = SoundStream.DecodedPacketsInfo;

			Voice = new XAudio2.SourceVoice(device.Device, Format);
			//Voice.BufferEnd += (context) =>
			Voice.SubmitSourceBuffer(Buffer, SoundStream.DecodedPacketsInfo);
		}

		public void Play()
		{
			Voice.Start();
		}

		public void Dispose()
		{
			Buffer.Stream.Dispose();
		}
    }
}
#endif