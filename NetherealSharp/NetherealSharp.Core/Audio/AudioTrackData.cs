﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace NetherealSharp.Core.Audio
{
    public class AudioTrackData
    {
		public int[] data;

		public int Channels;
		public int Bits;
		public int SampleRate;


		/// <summary>
		/// A custom audio file interpreter for wav only...
		/// https://github.com/opentk/opentk/blob/develop/Source/Examples/OpenAL/1.1/Playback.cs
		/// </summary>
		/// <param name="stream"></param>
		public AudioTrackData(Stream stream)
		{
			using (BinaryReader reader = new BinaryReader(stream))
			{
				string signature = new string(reader.ReadChars(4));

				if (signature != "RIFF") throw new NotSupportedException("stream is not a .wav file");

				int riff_chunk_size = reader.ReadInt32();

				string format = new string(reader.ReadChars(4));

				if (format != "WAVE") throw new NotSupportedException("stream is not a .wave file");

				string format_signature = new string(reader.ReadChars(4));

				if (format_signature != "fmt ") throw new NotSupportedException("wave file not supported");

				int format_chunk_size = reader.ReadInt32();
				int audio_format = reader.ReadInt16();
				int num_channels = reader.ReadInt16();
				int sample_rate = reader.ReadInt32();
				int byte_rate = reader.ReadInt32();
				int block_align = reader.ReadInt16();
				int bits_per_sample = reader.ReadInt16();

				string data_signature = new string(reader.ReadChars(4));
				if(data_signature != "data") throw new NotSupportedException("wave file not supported");

				int data_chunk_size = reader.ReadInt32();

				Channels = num_channels;
				Bits = bits_per_sample;
				SampleRate = sample_rate;
			}
		}

	}
}
