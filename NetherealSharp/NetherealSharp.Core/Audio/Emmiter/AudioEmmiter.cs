﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using X3D = SharpDX.X3DAudio;

namespace NetherealSharp.Core.Audio
{
    public class AudioEmitter
    {
		X3D.Emitter Emitter;

		public AudioEmitter()
		{
			Emitter = new X3D.Emitter()
			{
				ChannelCount = 1,
				CurveDistanceScaler = float.MinValue,
				OrientFront = new Vector3(0, 0, 1),
				OrientTop = new Vector3(0, 1, 0),
				Position = new Vector3(0, 0, 0),
				Velocity = new Vector3(0, 0, 0)
			};
		}
    }
}
#endif