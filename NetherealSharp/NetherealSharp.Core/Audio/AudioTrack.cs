﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using XAudio2 = SharpDX.XAudio2;
using MM = SharpDX.Multimedia;

using System.IO;
using SharpDX.Multimedia;

namespace NetherealSharp.Core.Audio.XA2
{
    public class AudioTrack : IDisposable
    {
		public XAudio2.SourceVoice SourceVoice;


		public AudioTrack(AudioDevice device, Stream stream)
		{
			var soundstream = new SoundStream(stream);
			var format = soundstream.Format;

			var buffer = new XAudio2.AudioBuffer()
			{
				Stream = soundstream.ToDataStream(),
				AudioBytes = (int)soundstream.Length,
				Flags = XAudio2.BufferFlags.EndOfStream
			};

			soundstream.Close();

			SourceVoice = new XAudio2.SourceVoice(device.Device, format, true);

			SourceVoice.SubmitSourceBuffer(buffer, soundstream.DecodedPacketsInfo);

			//SourceVoice = new SourceVoice(device, Stream stream)
		}

		public void Dispose()
		{
			SourceVoice.Dispose();
		}
    }
}
#endif