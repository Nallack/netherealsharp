﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.XAudio2;
using Base = NetherealSharp.Core.Audio;

namespace NetherealSharp.Core.Audio.XA2
{
    public class AudioVoice : Base.AudioVoice
    {
		SharpDX.XAudio2.SourceVoice Voice;

		public AudioVoice(AudioDevice device)
		{
			var format = new SharpDX.Multimedia.WaveFormat();
			Voice = new SourceVoice(device.Device, format);
		}

		public override void SetSource(Base.AudioBuffer buffer)
		{
			var b = buffer as AudioBuffer;
			Voice.SubmitSourceBuffer(b.Buffer, b.DecodedPacketsInfo);

			//Voice.
		}

	}
}
#endif