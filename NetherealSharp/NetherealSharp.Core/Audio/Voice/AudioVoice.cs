﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Audio
{
    public abstract class AudioVoice
    {
		public static AudioVoice Create(AudioDevice device)
		{
			switch(device.Implementation)
			{
#if DIRECTX
				case AudioImplementation.XAudio2:
				return new XA2.AudioVoice(device as XA2.AudioDevice);
#endif
				default:
				throw new NotImplementedException();
			}
		}

		public abstract void SetSource(AudioBuffer buffer);
    }
}
