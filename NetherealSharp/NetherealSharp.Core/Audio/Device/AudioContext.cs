﻿using System;
using System.Collections.Generic;
using System.Text;


namespace NetherealSharp.Core.Audio
{
    public abstract class AudioContext
    {
		public AudioImplementation Implementation;

		public static AudioContext Create(AudioImplementation implementation)
		{
			switch (implementation)
			{
#if DIRECTX
				case AudioImplementation.XAudio2:
					return new DX.AudioContext();
#endif
#if OPENAL
				case AudioImplementation.OpenAL:
					return new OpenAL.AudioContext();
#endif
			default:
					throw new NotImplementedException();
			}
		}
    }
}
