﻿#if DIRECTX
using System;
using System.Collections.Generic;
using System.Text;

using SharpDX.XAudio2;
using SharpDX.X3DAudio;
using SharpDX.Multimedia;

using Base = NetherealSharp.Core.Audio;

//https://msdn.microsoft.com/en-us/library/windows/desktop/ee415741%28v=vs.85%29.aspx

namespace NetherealSharp.Core.Audio.XA2
{
    public class AudioDevice : Base.AudioDevice
    {
		public XAudio2 Device;
		public X3DAudio SpacialDevice;

		public MasteringVoice Master;
		public WaveFormat WaveFormat;

		public AudioDevice()
		{
			Implementation = AudioImplementation.XAudio2;

			Device = new XAudio2(XAudio2Flags.DebugEngine, ProcessorSpecifier.DefaultProcessor, XAudio2Version.Default);
			SpacialDevice = new X3DAudio(Speakers.Stereo);

			Master = new MasteringVoice(Device, XAudio2.DefaultChannels, XAudio2.DefaultSampleRate);
		}
    }
}
#endif