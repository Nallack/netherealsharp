﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Audio
{
    public enum AudioImplementation
    {
		Native,
		OpenAL,
		DirectAudio,
		XAudio2,
		XACT3
    }
}
