﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Core.Audio
{
    public abstract class AudioDevice
    {
		public AudioImplementation Implementation { get; protected set; }

		public static AudioDevice Create(AudioImplementation implementation)
		{
			switch(implementation)
			{
#if DIRECTX
				case AudioImplementation.XAudio2:
				return new XA2.AudioDevice();
			case AudioImplementation.XACT3:
				return new XACT3.AudioDevice();
#endif
			default:
				throw new NotImplementedException();
			}
		}
    }
}