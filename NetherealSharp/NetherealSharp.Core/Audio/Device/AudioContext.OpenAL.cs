﻿#if OPENAL
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Audio.OpenAL;

using Base = NetherealSharp.Core.Audio;
using System.IO;

namespace NetherealSharp.Core.Audio.OpenAL
{
    public class AudioContext : Base.AudioContext
    {
		public OpenTK.Audio.AudioContext Context;
		public XRamExtension XRAM = new XRamExtension();

		public AudioContext()
		{
			//Change constructor for more options
			Context = new OpenTK.Audio.AudioContext();


			int[] abos = AL.GenBuffers(2);

			//set buffer storage to GPU hardware
			if(XRAM.IsInitialized)
			{
				XRAM.SetBufferMode(1, ref abos[0], XRamExtension.XRamStorage.Hardware);
			}

			int channels, bits_per_sample, sample_rate;
			var sound_data = new AudioTrackData(File.Open("Content/Audio/test.wav", FileMode.Open));

			var format = AudioUtils.GetSoundFormat(sound_data.Channels, sound_data.Bits);
			AL.BufferData(abos[0], format, sound_data.data, sound_data.data.Length, sound_data.SampleRate);

			if(AL.GetError() != ALError.NoError)
			{
				Console.WriteLine("Error");
			}


		}
	}
}
#endif