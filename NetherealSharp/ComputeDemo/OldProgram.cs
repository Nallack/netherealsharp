﻿/*

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenCL.Net;
using OpenCL.Net.Extensions;
using System.IO;


namespace ComputeDemo
{
	public class Program
	{



		static void Main(string[] args)
		{
			ClooDemo();
		}

		static void ClooDemo()
		{
			string filepath = "Kernel.cl";

			var platforms = Cloo.ComputePlatform.Platforms;
			var platform = platforms[0];

			//look at http://stackoverflow.com/questions/26802905/getting-opengl-buffers-using-opencl

			var properties = new Cloo.ComputeContextPropertyList(new[]
			{
				new Cloo.ComputeContextProperty(Cloo.ComputeContextPropertyName.Platform, platform.Handle.Value),
				//new Cloo.ComputeContextProperty(Cloo.ComputeContextPropertyName.CL_GL_CONTEXT_KHR, ) wglGetCurrentContext
				//
			});

			var context = new Cloo.ComputeContext(Cloo.ComputeDeviceTypes.Gpu, properties, null, IntPtr.Zero);
			var devices = context.Devices;

			var source = File.ReadAllText(filepath);
			var program = new Cloo.ComputeProgram(context, source);

		}

		static void OpenCLNetDemo()
		{
			string filepath = "Kernel.cl";

			int arrayLength = 1024;

			ErrorCode error;

			//string platform = "*Intel*";
			//var env = platform.CreateCLEnvironment();
			//var device = env.Devices[0];
			//var context = env.Context;

			//-----------Setup Device and Context ---------//

			var platforms = Cl.GetPlatformIDs(out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			var platform = platforms[0];

			var devices = Cl.GetDeviceIDs(platform, DeviceType.Gpu, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			var device = devices[0];

			Cl.ContextNotify n = (s, d, cd, userdata) =>
			{
				Console.WriteLine("Notify!");
			};

			var context = Cl.CreateContext(null, 1, devices, n, IntPtr.Zero, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());


			//--------Build program and kernels-----------//

			var source = File.ReadAllText(filepath);

			var program = Cl.CreateProgramWithSource(context, 1, new[] { source }, null, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			var builderror = Cl.BuildProgram(program, 1, new[] { device }, string.Empty, null, IntPtr.Zero);
			if (builderror != ErrorCode.Success)
			{
				Console.WriteLine(Cl.GetProgramBuildInfo(program, device, ProgramBuildInfo.Log, out error));

				Cl.GetProgramBuildInfo(program, device, ProgramBuildInfo.Status, out error);
				if (error != ErrorCode.Success) throw new Exception(error.ToString());
			}



			var kernel = Cl.CreateKernel(program, "add_array", out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());


			//------FILL DATA BUFFERS-----//
			var random = new Random();


			int blockSize = 4;
			int blocks = 3;
			IntPtr dimension = new IntPtr(blocks * blockSize);

			
			//var a = context.CreateBuffer(
			//	(from i in Enumerable.Range(0, arrayLength)
			//	 select (float)random.NextDouble()).ToArray(),
			//	MemFlags.WriteOnly);

			//var b = context.CreateBuffer(
			//	(from i in Enumerable.Range(0, arrayLength)
			//	 select (float)random.NextDouble()).ToArray(),
			//	MemFlags.WriteOnly);
				
			

			float[] A = new float[dimension.ToInt32()];
			float[] B = new float[dimension.ToInt32()];
			float[] C = new float[dimension.ToInt32()];

			for (int i = 0; i < A.Length; i++)
			{
				A[i] = random.Next() % 265;
				B[i] = random.Next() % 265;
			}

			var readflags = MemFlags.CopyHostPtr | MemFlags.ReadOnly;
			var writeflags = MemFlags.WriteOnly;

			var memA = Cl.CreateBuffer(context, readflags, A, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());
			var memB = Cl.CreateBuffer(context, readflags, B, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());
			var memC = Cl.CreateBuffer(context, writeflags, (IntPtr)(sizeof(float) * dimension.ToInt32()), IntPtr.Zero, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());


			//----SET PARAMETER VALUES-------//
			error = Cl.SetKernelArg(kernel, 0, memA);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());
			error = Cl.SetKernelArg(kernel, 1, memB);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());
			error = Cl.SetKernelArg(kernel, 2, memC);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());


			//----WRITE DATA TO DEVICE---------//

			Event clEvent;
			CommandQueue cmdQueue = Cl.CreateCommandQueue(context, device, CommandQueueProperties.None, out error);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());


			error = Cl.EnqueueWriteBuffer(
				cmdQueue,
				memA,
				Bool.True,
				IntPtr.Zero,
				new IntPtr(dimension.ToInt32() * sizeof(float)),
				A, 0, null, out clEvent);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			error = Cl.EnqueueWriteBuffer(
				cmdQueue,
				memB,
				Bool.True,
				IntPtr.Zero,
				new IntPtr(dimension.ToInt32() * sizeof(float)),
				B, 0, null, out clEvent);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			//--------EXECUTE KERNEL---------//
			error = Cl.EnqueueNDRangeKernel(cmdQueue, kernel, 1, null, new[] { dimension }, null, 0, null, out clEvent);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			//-------COPY RESULTS BACK TO MEMORY-----//
			error = Cl.EnqueueReadBuffer(cmdQueue, memC, Bool.True, 0, 4, C, 0, null, out clEvent);
			if (error != ErrorCode.Success) throw new Exception(error.ToString());

			Console.WriteLine(C.ToString());



			Cl.Finish(cmdQueue);
			Cl.ReleaseMemObject(memA);
			Cl.ReleaseMemObject(memB);
			Cl.ReleaseMemObject(memC);
		}
	}
}

	*/