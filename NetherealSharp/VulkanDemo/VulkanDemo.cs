﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vk = VulkanSharp;

namespace VulkanDemo
{
	public class VulkanDemo
	{
		System.Windows.Forms.Control Control;
		Vk.Instance Instance;
		Vk.PhysicalDevice PhysicalDevice;
		Vk.Device Device;

		Vk.Queue Queue;
		Vk.CommandBuffer[] CommandBuffers;

		Vk.Fence Fence;
		Vk.Semaphore ImageAvailableSemaphore;
		Vk.Semaphore RenderFinishedSemaphore;

		Vk.SurfaceKhr Surface;
		Vk.SwapchainKhr Swapchain;
		Vk.Image[] Images;

		bool initialized = false;

		public VulkanDemo(MainWindow window)
		{
			Control = window.GamePanel;
			Instance = CreateInstance();

		}

		public void Initialize()
		{
			Instance = CreateInstance();
			PhysicalDevice = Instance.EnumeratePhysicalDevices()[0];
			var surfaceInfo = new Vk.Windows.Win32SurfaceCreateInfoKhr()
			{
				Hinstance = Process.GetCurrentProcess().Handle,
				Hwnd = Control.Handle
			};
			Surface = Vk.Windows.InstanceExtension.CreateWin32SurfaceKHR(Instance, surfaceInfo);
			var queueProperties = PhysicalDevice.GetQueueFamilyProperties();
			if(!PhysicalDevice.GetSurfaceSupportKHR(0, Surface))
			{
				throw new Exception("Surface creation failed");
			}


			var queueInfo = new Vk.DeviceQueueCreateInfo { QueuePriorities = new float[] { 1.0f } };
			var deviceInfo = new Vk.DeviceCreateInfo
			{
				EnabledExtensionNames = new string[] { "VK_KHR_swapchain" },
				QueueCreateInfos = new Vk.DeviceQueueCreateInfo[] { queueInfo }

			};

			Device = PhysicalDevice.CreateDevice(deviceInfo);
			Queue = Device.GetQueue(0, 0);

			var surfaceCapabilities = PhysicalDevice.GetSurfaceCapabilitiesKHR(Surface);
			var surfaceFormat = SelectFormat(PhysicalDevice, Surface);
			Swapchain = CreateSwapchain(Device, Surface, surfaceCapabilities, surfaceFormat);
			Images = Device.GetSwapchainImagesKHR(Swapchain);
			var renderPass = CreateRenderPass(Device, surfaceFormat);
			var framebuffers = CreateFramebuffers(Device, Images, surfaceFormat, surfaceCapabilities, renderPass);
			CommandBuffers = CreateCommandBuffers(Device, Images, framebuffers, renderPass, surfaceCapabilities);

			var fenceInfo = new Vk.FenceCreateInfo() { Flags = (Vk.FenceCreateFlags)0 };
			Fence = Device.CreateFence(fenceInfo);
			var semaphoreInfo = new Vk.SemaphoreCreateInfo();
			ImageAvailableSemaphore = Device.CreateSemaphore(semaphoreInfo);
			RenderFinishedSemaphore = Device.CreateSemaphore(semaphoreInfo);

			Control.Paint += (sender, args) => {
				Draw();
			};

			Application.Idle += (sender, e) => {
				Thread.Sleep(100);
				Control.Invalidate();
			};

			initialized = true;
		}

		void Draw()
		{
			
			Device.ResetFences(1, Fence);

			uint nextIndex = Device.AcquireNextImageKHR(Swapchain, ulong.MaxValue, ImageAvailableSemaphore, null);

			var submitInfo = new Vk.SubmitInfo
			{
				WaitDstStageMask = new Vk.PipelineStageFlags[] { Vk.PipelineStageFlags.BottomOfPipe },
				WaitSemaphores = new Vk.Semaphore[] { ImageAvailableSemaphore },
				CommandBuffers = new Vk.CommandBuffer[] { CommandBuffers[nextIndex] }
			};

			Queue.Submit(new Vk.SubmitInfo[] { submitInfo }, Fence);

			var presentInfo = new Vk.PresentInfoKhr
			{
				Swapchains = new Vk.SwapchainKhr[] { Swapchain },
				ImageIndices = new uint[] { nextIndex },
				//WaitSemaphores = new Vk.Semaphore[] { ImageAvailableSemaphore }
			};


			Queue.PresentKHR(presentInfo);
			Queue.WaitIdle();

			Device.WaitForFences(1, Fence, false, uint.MaxValue);
		}


		#region Helpers
		static Vk.Instance CreateInstance()
		{
			var applicationInfo = new Vk.ApplicationInfo()
			{
				ApplicationName = "VulkanDemo",
				EngineName = "NetherealSharp.Engine",
				EngineVersion = 1,
				ApiVersion = Vk.Version.Make(1, 0, 0)
			};

			var enabledLayerNames = new string[]
			{
				"VK_LAYER_LUNARG_standard_validation"
			};

			var enabledExtensionNames = new string[]
			{
				 "VK_KHR_surface", "VK_KHR_win32_surface", "VK_EXT_debug_report"
			};

			var instanceInfo = new Vk.InstanceCreateInfo()
			{
				ApplicationInfo = applicationInfo,
				EnabledLayerCount = 1,
				EnabledLayerNames = enabledLayerNames,
				EnabledExtensionCount = 3,
				EnabledExtensionNames = enabledExtensionNames
			};

			return new Vk.Instance(instanceInfo);
		}

		static Vk.SurfaceFormatKhr SelectFormat(Vk.PhysicalDevice physicalDevice, Vk.SurfaceKhr surface)
		{
			foreach (var f in physicalDevice.GetSurfaceFormatsKHR(surface))
				if (f.Format == Vk.Format.R8G8B8A8Unorm || f.Format == Vk.Format.B8G8R8A8Unorm)
					return f;

			throw new Exception("didn't find the R8G8B8A8Unorm format");
		}

		static Vk.SwapchainKhr CreateSwapchain(Vk.Device device, Vk.SurfaceKhr surface, Vk.SurfaceCapabilitiesKhr surfaceCapabilities, Vk.SurfaceFormatKhr surfaceFormat)
		{
			var swapchainInfo = new Vk.SwapchainCreateInfoKhr
			{
				Surface = surface,
				MinImageCount = surfaceCapabilities.MinImageCount,
				ImageFormat = surfaceFormat.Format,
				ImageColorSpace = surfaceFormat.ColorSpace,
				ImageExtent = surfaceCapabilities.CurrentExtent,
				ImageUsage = Vk.ImageUsageFlags.ColorAttachment,
				PreTransform = Vk.SurfaceTransformFlagsKhr.Identity,
				ImageArrayLayers = 1,
				ImageSharingMode = Vk.SharingMode.Exclusive,
				QueueFamilyIndices = new uint[] { 0 },
				PresentMode = Vk.PresentModeKhr.Fifo,
				CompositeAlpha = Vk.CompositeAlphaFlagsKhr.Opaque
			};
			return device.CreateSwapchainKHR(swapchainInfo);
		}

		static Vk.RenderPass CreateRenderPass(Vk.Device device, Vk.SurfaceFormatKhr surfaceFormat)
		{
			var attDesc = new Vk.AttachmentDescription
			{
				Format = surfaceFormat.Format,
				Samples = Vk.SampleCountFlags.Count1,
				LoadOp = Vk.AttachmentLoadOp.Clear,
				StoreOp = Vk.AttachmentStoreOp.Store,
				StencilLoadOp = Vk.AttachmentLoadOp.Clear,
				StencilStoreOp = Vk.AttachmentStoreOp.Store,
				InitialLayout = Vk.ImageLayout.ColorAttachmentOptimal,
				FinalLayout = Vk.ImageLayout.ColorAttachmentOptimal
			};
			var attRef = new Vk.AttachmentReference { Layout = Vk.ImageLayout.ColorAttachmentOptimal };
			var subpassDesc = new Vk.SubpassDescription
			{
				PipelineBindPoint = Vk.PipelineBindPoint.Graphics,
				ColorAttachments = new Vk.AttachmentReference[] { attRef }
			};
			var renderPassCreateInfo = new Vk.RenderPassCreateInfo
			{
				Attachments = new Vk.AttachmentDescription[] { attDesc },
				Subpasses = new Vk.SubpassDescription[] { subpassDesc }
			};
			return device.CreateRenderPass(renderPassCreateInfo);
		}

		static Vk.Framebuffer[] CreateFramebuffers(Vk.Device device, Vk.Image[] images, Vk.SurfaceFormatKhr surfaceFormat, Vk.SurfaceCapabilitiesKhr surfaceCapabilities, Vk.RenderPass renderPass)
		{
			var displayViews = new Vk.ImageView[images.Length];
			for (int i = 0; i < images.Length; i++)
			{
				var viewCreateInfo = new Vk.ImageViewCreateInfo
				{
					Image = images[i],
					ViewType = Vk.ImageViewType.View2D,
					Format = surfaceFormat.Format,
					Components = new Vk.ComponentMapping
					{
						R = Vk.ComponentSwizzle.R,
						G = Vk.ComponentSwizzle.G,
						B = Vk.ComponentSwizzle.B,
						A = Vk.ComponentSwizzle.A
					},
					SubresourceRange = new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						LevelCount = 1,
						LayerCount = 1
					}
				};
				displayViews[i] = device.CreateImageView(viewCreateInfo);
			}
			var framebuffers = new Vk.Framebuffer[images.Length];
			for (int i = 0; i < images.Length; i++)
			{
				var frameBufferCreateInfo = new Vk.FramebufferCreateInfo
				{
					Layers = 1,
					RenderPass = renderPass,
					Attachments = new Vk.ImageView[] { displayViews[i] },
					Width = surfaceCapabilities.CurrentExtent.Width,
					Height = surfaceCapabilities.CurrentExtent.Height
				};
				framebuffers[i] = device.CreateFramebuffer(frameBufferCreateInfo);
			}
			return framebuffers;
		}

		static Vk.CommandBuffer[] CreateCommandBuffers(Vk.Device device, Vk.Image[] images, Vk.Framebuffer[] framebuffers, Vk.RenderPass renderPass, Vk.SurfaceCapabilitiesKhr surfaceCapabilities)
		{
			var createPoolInfo = new Vk.CommandPoolCreateInfo { Flags = Vk.CommandPoolCreateFlags.ResetCommandBuffer };
			var commandPool = device.CreateCommandPool(createPoolInfo);
			var commandBufferAllocateInfo = new Vk.CommandBufferAllocateInfo
			{
				Level = Vk.CommandBufferLevel.Primary,
				CommandPool = commandPool,
				CommandBufferCount = (uint)images.Length
			};
			var buffers = device.AllocateCommandBuffers(commandBufferAllocateInfo);

			


			for (int i = 0; i < images.Length; i++)
			{
				var commandBufferBeginInfo = new Vk.CommandBufferBeginInfo();

				var renderPassBeginInfo = new Vk.RenderPassBeginInfo
				{
					Framebuffer = framebuffers[i],
					RenderPass = renderPass,
					ClearValues = new Vk.ClearValue[] { new Vk.ClearValue { Color = new Vk.ClearColorValue(new float[] { 0.1f, 0.7f, 0.9f, 1.0f }) } },
					RenderArea = new Vk.Rect2D { Extent = surfaceCapabilities.CurrentExtent }
				};

				var presentToClear = new Vk.ImageMemoryBarrier()
				{
					SrcAccessMask = Vk.AccessFlags.MemoryRead,
					DstAccessMask = Vk.AccessFlags.TransferWrite,
					OldLayout = Vk.ImageLayout.Undefined,
					NewLayout = Vk.ImageLayout.TransferDstOptimal,
					SrcQueueFamilyIndex = (uint)i,
					DstQueueFamilyIndex = (uint)i,
					Image = images[i],
					SubresourceRange = new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						LevelCount = 1,
						LayerCount = 1
					}
				};

				var clearToPresent = new Vk.ImageMemoryBarrier()
				{
					SrcAccessMask = Vk.AccessFlags.TransferWrite,
					DstAccessMask = Vk.AccessFlags.MemoryRead,
					OldLayout = Vk.ImageLayout.TransferDstOptimal,
					NewLayout = Vk.ImageLayout.PresentSrcKhr,
					SrcQueueFamilyIndex = (uint)i,
					DstQueueFamilyIndex = (uint)i,
					Image = images[i],
					SubresourceRange = new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						LevelCount = 1,
						LayerCount = 1
					}
				};

				var mb = new Vk.MemoryBarrier();
				var bmb = new Vk.BufferMemoryBarrier();

				buffers[i].Begin(commandBufferBeginInfo);
				buffers[i].CmdPipelineBarrier(Vk.PipelineStageFlags.Transfer, Vk.PipelineStageFlags.Transfer, (Vk.DependencyFlags)0, 0, mb, 0, bmb, 1, presentToClear);

				buffers[i].CmdClearColorImage(
					images[i], 
					Vk.ImageLayout.TransferDstOptimal, 
					new Vk.ClearColorValue(new float[] { 0.1f, 0.7f, 0.9f, 1.0f }), 
					1, 
					new Vk.ImageSubresourceRange
					{
						AspectMask = Vk.ImageAspectFlags.Color,
						LevelCount = 1,
						LayerCount = 1
					});

				//buffers[i].CmdBeginRenderPass(renderPassBeginInfo, Vk.SubpassContents.Inline);
				//buffers[i].CmdEndRenderPass();
				buffers[i].CmdPipelineBarrier(Vk.PipelineStageFlags.Transfer, Vk.PipelineStageFlags.BottomOfPipe, (Vk.DependencyFlags)0, 0, mb, 0, bmb, 1, clearToPresent);
				/*
				buffers[i].CmdPipelineBarrier(
					Vk.PipelineStageFlags.AllGraphics,
					Vk.PipelineStageFlags.AllGraphics,
					Vk.DependencyFlags.ByRegion);
				*/
				buffers[i].End();
			}
			return buffers;
		}

		#endregion
	}
}
