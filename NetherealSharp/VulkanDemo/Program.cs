﻿using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VulkanDemo
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			

			var mainWindow = new MainWindow();

			/*
			Game3D game = new BasicGame();

			game.Implementor = new FormsGameImplementor(
				game,
				mainWindow.GamePanel,
				GraphicsImplementation.Vulkan,
				PhysicsImplementation3D.PhysX,
				false);

			game.ActiveScene = new Scene3D();

			game.Init();
			*/

			VulkanDemo demo = new VulkanDemo(mainWindow);
			demo.Initialize();


			mainWindow.Paint += Draw;

			Application.Run(mainWindow);
		}

		public static void Draw(object sender, PaintEventArgs e)
		{

		}
	}
}
