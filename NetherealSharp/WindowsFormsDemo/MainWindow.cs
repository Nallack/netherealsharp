﻿
using System;
using System.Windows.Forms;

using OpenTK.Graphics.OpenGL;

namespace WindowsFormsDemo
{

	public partial class MainWindow : Form
	{
		public Panel GamePanel { get { return panel1;} }

		public MainWindow()
		{
			InitializeComponent();
		}

		private void HandleWireframeToggle(object sender, EventArgs e)
		{
			wireframeToolStripMenuItem.Checked ^= true;
			if(wireframeToolStripMenuItem.Checked)
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
			else
				GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
		}
	}
}
