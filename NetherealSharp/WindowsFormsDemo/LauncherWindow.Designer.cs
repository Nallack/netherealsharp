﻿namespace WindowsFormsDemo
{
	partial class LauncherWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.scene = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.graphicsLabel = new System.Windows.Forms.Label();
			this.comboBoxScene = new System.Windows.Forms.ComboBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.comboBoxGraphics = new System.Windows.Forms.ComboBox();
			this.comboBoxShadows = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBoxRenderer = new System.Windows.Forms.ComboBox();
			this.panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.scene.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.button1.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.button1.Location = new System.Drawing.Point(10, 364);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(202, 27);
			this.button1.TabIndex = 1;
			this.button1.Text = "Start";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.tabControl1);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(10);
			this.panel1.Size = new System.Drawing.Size(222, 401);
			this.panel1.TabIndex = 2;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.scene);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(10, 10);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(202, 354);
			this.tabControl1.TabIndex = 2;
			// 
			// scene
			// 
			this.scene.BackColor = System.Drawing.Color.WhiteSmoke;
			this.scene.Controls.Add(this.tableLayoutPanel2);
			this.scene.Location = new System.Drawing.Point(4, 22);
			this.scene.Name = "scene";
			this.scene.Size = new System.Drawing.Size(194, 328);
			this.scene.TabIndex = 2;
			this.scene.Text = "Settings";
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.graphicsLabel, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxScene, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.checkBox1, 1, 5);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxGraphics, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxShadows, 1, 3);
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.label3, 0, 4);
			this.tableLayoutPanel2.Controls.Add(this.comboBoxRenderer, 1, 4);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 5;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 328);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Location = new System.Drawing.Point(3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 27);
			this.label2.TabIndex = 0;
			this.label2.Text = "Scene";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// graphicsLabel
			// 
			this.graphicsLabel.AutoSize = true;
			this.graphicsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.graphicsLabel.Location = new System.Drawing.Point(3, 27);
			this.graphicsLabel.Name = "graphicsLabel";
			this.graphicsLabel.Size = new System.Drawing.Size(51, 27);
			this.graphicsLabel.TabIndex = 2;
			this.graphicsLabel.Text = "Graphics";
			this.graphicsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBoxScene
			// 
			this.comboBoxScene.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxScene.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxScene.FormattingEnabled = true;
			this.comboBoxScene.Items.AddRange(new object[] {
            "Empty",
            "3D First Person Demo",
            "2D Platformer Demo"});
			this.comboBoxScene.Location = new System.Drawing.Point(60, 3);
			this.comboBoxScene.Name = "comboBoxScene";
			this.comboBoxScene.Size = new System.Drawing.Size(131, 21);
			this.comboBoxScene.TabIndex = 4;
			this.comboBoxScene.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(60, 111);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(71, 17);
			this.checkBox1.TabIndex = 3;
			this.checkBox1.Text = "VR Mode";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// comboBoxGraphics
			// 
			this.comboBoxGraphics.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxGraphics.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxGraphics.FormattingEnabled = true;
			this.comboBoxGraphics.Items.AddRange(new object[] {
            "DirectX11",
            "DirectX12",
            "OpenGL",
            "Vulkan"});
			this.comboBoxGraphics.Location = new System.Drawing.Point(60, 30);
			this.comboBoxGraphics.Name = "comboBoxGraphics";
			this.comboBoxGraphics.Size = new System.Drawing.Size(131, 21);
			this.comboBoxGraphics.TabIndex = 5;
			this.comboBoxGraphics.SelectedIndexChanged += new System.EventHandler(this.comboBoxGraphics_SelectedIndexChanged);
			// 
			// comboBoxShadows
			// 
			this.comboBoxShadows.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxShadows.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxShadows.FormattingEnabled = true;
			this.comboBoxShadows.Items.AddRange(new object[] {
            "None",
            "Low",
            "Medium",
            "High"});
			this.comboBoxShadows.Location = new System.Drawing.Point(60, 57);
			this.comboBoxShadows.Name = "comboBoxShadows";
			this.comboBoxShadows.Size = new System.Drawing.Size(131, 21);
			this.comboBoxShadows.TabIndex = 6;
			this.comboBoxShadows.SelectedIndexChanged += new System.EventHandler(this.comboBoxShadows_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 54);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 27);
			this.label1.TabIndex = 7;
			this.label1.Text = "Shadows";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Location = new System.Drawing.Point(3, 81);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 27);
			this.label3.TabIndex = 8;
			this.label3.Text = "Renderer";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBoxRenderer
			// 
			this.comboBoxRenderer.AutoCompleteCustomSource.AddRange(new string[] {
            "Forward",
            "ClearOnly"});
			this.comboBoxRenderer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBoxRenderer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxRenderer.FormattingEnabled = true;
			this.comboBoxRenderer.Items.AddRange(new object[] {
            "Forward",
            "ClearOnly",
            "Deffered"});
			this.comboBoxRenderer.Location = new System.Drawing.Point(60, 84);
			this.comboBoxRenderer.Name = "comboBoxRenderer";
			this.comboBoxRenderer.Size = new System.Drawing.Size(131, 21);
			this.comboBoxRenderer.TabIndex = 9;
			this.comboBoxRenderer.SelectedIndexChanged += new System.EventHandler(this.comboBoxRenderer_SelectedIndexChanged);
			// 
			// LauncherWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(222, 401);
			this.Controls.Add(this.panel1);
			this.Name = "LauncherWindow";
			this.Text = "Demo Setup";
			this.panel1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.scene.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage scene;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label graphicsLabel;
		private System.Windows.Forms.ComboBox comboBoxScene;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.ComboBox comboBoxGraphics;
		private System.Windows.Forms.ComboBox comboBoxShadows;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboBoxRenderer;
	}
}