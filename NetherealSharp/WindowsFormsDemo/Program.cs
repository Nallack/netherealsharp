﻿
using System;
using System.Windows.Forms;
using NetherealSharp.Engine;
using NetherealSharp.Core.Physics;

namespace WindowsFormsDemo
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);


			var selectionwindow = new LauncherWindow();

			Application.Run(selectionwindow);

			var graphics = selectionwindow.Graphics;
			var physics3d = PhysicsImplementation3D.PhysX;
			var vrmode = selectionwindow.VRMode;
			var run = selectionwindow.Run;
			var shadowquality = selectionwindow.ShadowQuality;

			selectionwindow.Dispose();

			if (run)
			{
				var gamewindow = new MainWindow();

				Game game = null;

				//TODO
				//game and implementor referencing one another done a bit odd here. choose which one
				//manages the other...
				//implementor needs game to add it to the window redraw callback.

				//After APIs are picked
				var implementor = new FormsGameImplementor(gamewindow.GamePanel, new GameDescriptor
				{
					Graphics = graphics,
					Physics2D = PhysicsImplementation2D.None,
					Physics3D = physics3d,
					RenderMode = RenderMode.Forward
				});
				
				//select a scene
				switch (selectionwindow.SceneIndex)
				{
					case (int)LauncherWindow.SceneType.Empty:
						game = new NetherealSharp.Demo.EmptyGame3D()
						{
							Implementor = implementor
						};
						game.Initialize();

						GraphicsManager.GraphicsSettings.ShadowQuality = shadowquality;
						break;

					case (int)LauncherWindow.SceneType.FirstPerson3D:

						game = new NetherealSharp.Demo.FirstPersonGame()
						{
							Implementor = implementor
						};
						game.Initialize();

						GraphicsManager.GraphicsSettings.ShadowQuality = shadowquality;
						break;

					case (int)LauncherWindow.SceneType.Platformer2D:
						game = new NetherealSharp.Demo.LegacyExampleGame2D()
						{
							Implementor = implementor
						};

						game.Initialize();

						GraphicsManager.GraphicsSettings.ShadowQuality = shadowquality;
						break;
				}

				

				Application.Run(gamewindow);

				if(game != null) game.Dispose();
			}
		}
	}
}
