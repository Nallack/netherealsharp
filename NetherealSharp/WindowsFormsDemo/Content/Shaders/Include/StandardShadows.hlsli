﻿
#ifndef STANDARD_SHADOWS
#define STANDARD_SHADOWS


#if SHADOWMAPARRAY

/// <summary>
/// Performs variance shadow mapping which doesn't have a huge performance hit, but still worth testing.
/// </summary>
float Texture2DVSM(Texture2DArray map, SamplerState samplerstate, float2 uv, float cascadeIndex, float compare)
{
#if VSM
	float2 moments = map.SampleLevel(samplerstate, float3(uv, cascadeIndex), 2).rg;
	float psmooth = 0; // 0.02;

	float p = smoothstep(compare - psmooth, compare, moments.x);
	float variance = max(moments.y - moments.x * moments.x, -0.001);
	float d = compare - moments.x;
	float p_max = smoothstep(0.2, 1.0, variance / (variance + d * d));
	return clamp(max(p, p_max), 0.0, 1.0);
#else
	return step(compare, map.SampleLevel(samplerstate, float3(uv, cascadeIndex), 0).r);
#endif
}

/// <summary>
/// Peforms percentage closer filtering, which increases time complexity by scale factor of the PCF count.
/// Not efficient, but does produce smooth shadows.
/// </summary>
float Texture2DPCF(Texture2DArray map, SamplerState samplerstate, float2 uv, float cascadeIndex, float compare)
{
	float width, height, elements, levels;
	map.GetDimensions(cascadeIndex, width, height, elements, levels);
	float scale = 1.0f; //change this to blur more
	float2 texelsize = float2(scale / width, scale / height);
	

#if PCF == 16
	float sum = 0.0f;
	for (float x = -1.5f; x < 2.0f; x++)
		for (float y = -1.5f; y < 2.0f; y++)
			sum += Texture2DVSM(map, samplerstate, uv + texelsize * float2(x, y), cascadeIndex, compare);
	return sum / 16.0f;
#elif PCF == 4
	float sum = 0.0f;
	for (float x = -0.5f; x < 1.0f; x++)
		for (float y = -0.5f; y < 1.0f; y++)
			sum += Texture2DVSM(map, samplerstate, uv + texelsize * float2(x, y), cascadeIndex, compare);
	return sum / 4.0f;
#elif PCF == 6 //dithered
	float2 offset = (float)(frac(uv.xy * 0.5) > 0.25);
	offset.y += offset.x;
	
	if (offset.y > 1.1) offset.y = 0;

	return ((Texture2DVSM(map, samplerstate, uv + texelsize * (offset + float2(-1.5, 0.5)), cascadeIndex, compare)) +
			(Texture2DVSM(map, samplerstate, uv + texelsize * (offset + float2(0.5, 0.5)), cascadeIndex, compare)) +
			(Texture2DVSM(map, samplerstate, uv + texelsize * (offset + float2(-1.5, -1.5)), cascadeIndex, compare)) +
			(Texture2DVSM(map, samplerstate, uv + texelsize * (offset + float2(0.5, -1.5)), cascadeIndex, compare))) * 0.25f;
#else
	return Texture2DVSM(map, samplerstate, uv, cascadeIndex, compare);
#endif
}

/// <summary>
/// Performs manual bilinear filtering on the shadowmap.
/// Enabling Bilinear this will increase time complexity by a factor of 4.
/// </summary>
float Texture2DBilinear(Texture2DArray map, SamplerState samplerstate, float2 uv, float cascadeIndex, float compare)
{
#if BILINEAR
	float width, height, elements, levels;
	map.GetDimensions(cascadeIndex, width, height, elements, levels);
	float2 texelsize = float2(1.0f / width, 1.0f / height);

	float scale = 1.0f; //change this to blur more
	float dimension = width / scale;

	float2 fuv = frac(uv * dimension + 0.5f); //fractional value between 0.0-1.0 from lower left corner
	float2 centroiduv = floor(uv * dimension + 0.5f) / dimension - (texelsize / 2.0f);

	float lb = Texture2DPCF(map, samplerstate, centroiduv + texelsize * float2(0, 0), cascadeIndex, compare);
	float lt = Texture2DPCF(map, samplerstate, centroiduv + texelsize * float2(0, 1), cascadeIndex, compare);
	float rb = Texture2DPCF(map, samplerstate, centroiduv + texelsize * float2(1, 0), cascadeIndex, compare);
	float rt = Texture2DPCF(map, samplerstate, centroiduv + texelsize * float2(1, 1), cascadeIndex, compare);

	float a = lerp(lb, lt, fuv.y);
	float b = lerp(rb, rt, fuv.y);
	float c = lerp(a, b, fuv.x);
	return c;
#else
	return Texture2DPCF(map, samplerstate, uv, cascadeIndex, compare);
#endif
}


///
// Use this function to calculate the shadow factor of a pixel.
// Needs cascade computed in advance, and does not interpolate cascades (yet)
///
float ComputeShadowFactor(Texture2DArray map, SamplerState samplerstate, int cascadeIndex, float4 PositionS, float scale)
{
	if (PositionS.x < 0 || PositionS.x > 1 || PositionS.y < 0 || PositionS.y > 1 || PositionS.z >= 1) return 1.0f;

	float bias = 0.002f;
	float compareDepth = PositionS.z - bias;
	
	return Texture2DBilinear(map, samplerstate, PositionS.xy, cascadeIndex, compareDepth);
}



#else


//http://codeflow.org/entries/2013/feb/15/soft-shadow-mapping/

//R32 shadowmap format
float Texture2DShadow(Texture2D map, SamplerComparisonState samplerstate, float2 uv, float compare)
{
	return map.SampleCmpLevelZero(samplerstate, uv, compare);
}

//R32 shadowmap format (PCF)
float Texture2DShadowLerp(Texture2D map, SamplerComparisonState samplerstate, float2 uv, float compare)
{

	float sum = 0.0f;
	for (int x = -2; x < 2; x++)
		for (int y = -2; y < 2; y++)
		{
			sum += saturate(map.SampleCmp(samplerstate, uv, compare, int2(x, y)));
		}

	return sum / 16.0f;
}


float ComputeShadowFactor(Texture2D map, SamplerComparisonState samplerstate, float4 PositionS)
{
	if (PositionS.x < 0 || PositionS.x > 1 || PositionS.y < 0 || PositionS.y > 1) return 0.0f;

	float bias = 0.001f;
	float depth = PositionS.z - bias;

	//return Texture2DShadowLerp(map, samplerstate, PositionS.xy, depth);

	//Binary sample
	return map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth);

	//interpolated
	//float illumination = 
	//	map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(0, 0)) + 
	//	map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(0, 1)) +
	//	map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(1, 1)) +
	//	map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(1, 0));

	//return illumination / 4.0f;

	//soft sample
	//use a jittermap and sample around position xy in polar coordinates 

	//float vis = 2 * map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(0, 0)) +
	//			map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(1, 1)) +
	//			map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(1, -1)) +
	//			map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(-1, 1)) +
	//			map.SampleCmpLevelZero(samplerstate, PositionS.xy, depth, int2(-1, -1));

	//return vis / 6.0f;
}
 








//----------FORMAT R32G32----------------//

//R32G32 shadowmap format
float Texture2DShadowVSM(Texture2D map, SamplerState samplerstate, float2 uv, float compare, int2 offset)
{
	//Simple Filtering
	//return step(compare, map.SampleLevel(samplerstate, uv, 0, offset).r);

	//VSM Filtering
	//float2 depthzero = map.SampleLevel(samplerstate, uv, 0, offset).r;


	float2 moments = map.SampleLevel(samplerstate, uv, 1, offset).rg;

	float psmooth = 0; // 0.02;

	float p = smoothstep(compare - psmooth, compare, moments.x);
	float variance = max(moments.y - moments.x * moments.x, -0.001);
	float d = compare - moments.x;

	float p_max = smoothstep(0.2, 1.0, variance / (variance + d * d));

	//return clamp( p_max, 0.0, 1.0); //shadows at object edges
	//return clamp( p, 0.0, 1.0); //simple filtering (with little bit of smooth)
	return clamp(max(p, p_max), 0.0, 1.0);

}


float Texture2DShadowVSMLerp(Texture2D map, SamplerState samplerstate, float2 uv, float compare, float scale)
{
	//No Lerp
	//return step(compare, map.SampleLevel(samplerstate, uv, 0).r);

	//Lerp

	float2 texelsize = float2(1.0f, 1.0f) / (1024.0f / scale);

	float2 fuv = frac(uv * (1024.0f / scale) + 0.5f);
	float2 centroiduv = floor(uv * (1024.0f / scale) + 0.5f) / (1024.0f / scale);


	float lb = Texture2DShadowVSM(map, samplerstate, centroiduv + texelsize * float2(0, 0), compare, int2(0, 0));
	float lt = Texture2DShadowVSM(map, samplerstate, centroiduv + texelsize * float2(0, 1), compare, int2(0, 0));
	float rb = Texture2DShadowVSM(map, samplerstate, centroiduv + texelsize * float2(1, 0), compare, int2(0, 0));
	float rt = Texture2DShadowVSM(map, samplerstate, centroiduv + texelsize * float2(1, 1), compare, int2(0, 0));

	/*
	float lb = Texture2DShadowVSM(map, samplerstate, centroiduv, compare, int2(0, 0));
	float lt = Texture2DShadowVSM(map, samplerstate, centroiduv, compare, int2(0, 1));
	float rb = Texture2DShadowVSM(map, samplerstate, centroiduv, compare, int2(1, 0));
	float rt = Texture2DShadowVSM(map, samplerstate, centroiduv, compare, int2(1, 1));
	*/

	//float lb = step(compare, map.SampleLevel(samplerstate, centroiduv + texelsize * float2(0, 0), 0).r);
	//float lt = step(compare, map.SampleLevel(samplerstate, centroiduv + texelsize * float2(0, 1), 0).r);
	//float rb = step(compare, map.SampleLevel(samplerstate, centroiduv + texelsize * float2(1, 0), 0).r);
	//float rt = step(compare, map.SampleLevel(samplerstate, centroiduv + texelsize * float2(1, 1), 0).r);

	float a = lerp(lb, lt, fuv.y);
	float b = lerp(rb, rt, fuv.y);
	float c = lerp(a, b, fuv.x);

	//return (lb + lt + rb + rt)/4.0f;
	return c;
}




float Texture2DShadowVSMPCF(Texture2D map, SamplerState samplerstate, float2 uv, float compare, float scale)
{
	//Simple
	//return Texture2DShadowVSMLerp(map, samplerstate, uv + float2((1 / off) / 1024.0f, (1 / off) / 1024.0f), compare);


	//TODO: This is PCF, not lerp

	float sum = 0.0f;
	for (float x = -1.5f; x < 2.0f; x++)
		for (float y = -1.5f; y < 2.0f; y++)
		{
			sum += Texture2DShadowVSMLerp(map, samplerstate, uv + float2(x * scale / 1024.0f, y * scale / 1024.0f), compare, scale);
		}

	return sum / 16.0f;
}


float ComputeShadowFactorVSM(Texture2D map, SamplerState samplerstate, float4 PositionS, float off)
{
	if (PositionS.x < 0 || PositionS.x > 1 || PositionS.y < 0 || PositionS.y > 1 || PositionS.z >= 1) return 1.0f;

	float bias = 0.002f;
	float depth = PositionS.z - bias;

	//Simple Algorithm
	//return step(depth, map.SampleLevel(samplerstate, PositionS.xy, 0).r);


	//PCF + VSM
	return Texture2DShadowVSMPCF(map, samplerstate, PositionS.xy, depth, off);
}



#endif

#endif