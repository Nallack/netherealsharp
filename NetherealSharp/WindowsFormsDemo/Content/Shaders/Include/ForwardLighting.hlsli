﻿
#ifndef FORWARD_LIGHTING
#define FORWARD_LIGHTING


#include <StandardLights.hlsli>
#include <StandardShadows.hlsli>

//TEXTURE REGISTERS
// 0 : Diffuse (rgba)
// 1 : Normal (rgb)
// 2 : Specular (rgb, nuetral would be inverse of diffuse)
// 3 : ShadowMaps (rg)
// 4 : ShadowCubes (rg)
// 3 : ShadowTextureArray (direction, calculated using camera. no mipping? check PCSS)
// 4 : ShadowCubeArray (point + spot light, calculated without camera, no mipping)
//
// (additive, assuming we dont have different additive pass shader for different light source 
// 3 : ShadowMap
// 4 : ShadowCube
//
// 5 : ReflectionMap (planear reflection, calculated using camera, mipped for blur)
// 6 : ReflectionCube (reflection probe, calculated without camera, mipped for blur)

cbuffer Light : register(b2)
{
	//LIGHTS
	AmbientLight Ambient;
	AmorphicLight Lights[MAXLIGHTS];
	
	//SHADOWS
	//float DepthBias;
	float4x4 ShadowTransforms[MAXLIGHTS * MAXSHADOWCASCADES];
	
	
	//float CascadeDistance[MAXLIGHTS * MAXSHADOWCASCADES];
	//float CascadeBlur[MAXLIGHTS * MAXSHADOWCASCADES];
};

SamplerComparisonState shadowComparisonSampler : register(s1);
SamplerState shadowSampler : register(s2);

#if SHADOWMAPARRAY
Texture2DArray shadowMap[MAXLIGHTS] : register(t1);
TextureCubeArray shadowCube[MAXLIGHTS] : register(t2);
#else
Texture2D shadowMap[MAXLIGHTS * MAXSHADOWCASCADES] : register (t1);
TextureCube shadowCube[MAXLIGHTS] : register(t2);
#endif


void ComputeHemisphereAmbient(in float3 n, out float4 ambient)
{
	ambient = (Ambient.Down + (n.y * 0.5f + 0.5f) * Ambient.Range);
}

float ComputeShadowCascadeVisibility(in int mapIndex, in int matrixIndex, in float d, in float3 l, in float3 e, in float4 p)
{
#if ENABLECASCADES
	float scale = 1 / 2.0f;
	float exp = 0.25f;
	float casDist = scale * d * pow(1 - dot(l, e), exp);

	//Need a better algorithm for cascading, and frustum binding,
	//maybe worth actually multiplying shadowmatrices and use the first matrix
	//that computes within a texture region.

	int cascade = 0;
	//if (casDist < 8.0f) { cascade = 0; scale = 2.0f; }
	//if (casDist < 18.0f) { cascade = 1; scale = 1.0f; }
	//else { cascade = 2; scale = 1.0f; }
	
	#if SHADOWMAPARRAY
		return ComputeShadowFactor(
			shadowMap[mapIndex], 
			shadowSampler, 
			cascade, 
			mul(ShadowTransforms[matrixIndex + cascade], p), 
			2.0f);
	#else
		return ComputeShadowFactor(
			shadowMap[i + cascade], 
			shadowSampler, 
			mul(ShadowTransforms[i + cascade], p), 
			scale);
	#endif
#else
	cascade = 0;
	#if SHADOWMAPARRAY
		return ComputeShadowFactor(
			shadowMap[mapIndex],
			shadowSampler,
			cascade,
			mul(ShadowTransforms[matrixIndex + cascade], p),
			2.0f);
	#else
		return ComputeShadowFactor(
			shadowMap[i + cascade],
			shadowSampler,
			mul(ShadowTransforms[i + cascade], p),
			2.0f);
	#endif
#endif
}

void ComputeForwardLightPhong(
	in AmorphicLight light,
	in int i, //index of base shadowmap
	in float d, //eye distance to fragment
	in float4 p, //frag pos in worldspace
	in float3 n, //frag normal
	in float3 e, //frag to eye
	in float shininess,
	inout float4 diffuse, 
	inout float4 specular)
{
	switch (light.Type)
	{
	case 2: //directional
	{
		float3 l = -light.Direction.xyz;
		float3 r = reflect(l, n);

#if ENABLESHADOWS //cant use uniform variable to access block array, using i instead of light.MapIndex
	float visibility = ComputeShadowCascadeVisibility(i, light.MatrixIndex, d, l, e, p);
#else
	float visibility = 1.0f;
#endif

		float dFactor = max(dot(l, n), 0.0f);
		float sFactor = pow(max(dot(r, -e), 0.0f), shininess);

		diffuse += saturate(dFactor * light.Color) * visibility;
		specular += saturate(sFactor * light.Color) * visibility;

		return;
	}
	case 3: //pointlight
	{
		float3 tolight = light.Position - p.xyz;
		float3 l = normalize(tolight);
		float3 r = reflect(l, n);

		float dist = length(tolight);
		float attenuation = 10.0f / dist / dist;

		float visibility = 1.0f;

		float dFactor = max(dot(l, n), 0.0f);
		float sFactor = pow(max(dot(r, -e), 0.0f), 60.0f);

		diffuse += saturate(dFactor * light.Color) * attenuation * visibility;
		specular += saturate(sFactor * light.Color) * attenuation * visibility;


		break;
	}
	case 4: //spotlight
	default:
		return;
	}
}


void ComputeForwardLightBlinnPhong(
	in AmorphicLight light,
	in int i,
	in float d, //eye distance to fragment
	in float4 p, //frag pos
	in float3 n, //frag normal
	in float3 e, //frag to eye
	in float shininess,
	inout float4 diffuse,
	inout float4 specular)
{
	switch (light.Type)
	{
	case 2: //directional
	{
		float3 l = -light.Direction.xyz;
		float3 h = normalize(l + e);

#if ENABLESHADOWS
	//cant use uniform variable to access texture array, using i instead of light.MapIndex
	float visibility = ComputeShadowCascadeVisibility(i, light.MatrixIndex, d, l, e, p);
#else
	float visibility = 1.0f;
#endif

		float dFactor = max(dot(l, n), 0.0f);
		float sFactor = pow(max(dot(n, h), 0.0f), shininess);

		diffuse += saturate(dFactor * light.Color) * visibility;
		specular += saturate(sFactor * light.Color) * visibility;

		return;
	}
	case 3: //pointlight
	{
		float3 tolight = light.Position - p.xyz;
		float3 l = normalize(tolight);
		float3 h = normalize(l + e);

		float dist = length(tolight);
		float attenuation = 10.0f / dist / dist;

		float visibility = 1.0f;

		float dFactor = max(dot(l, n), 0.0f);
		float sFactor = pow(max(dot(n, h), 0.0f), shininess);

		diffuse += saturate(dFactor * light.Color) * attenuation * visibility;
		specular += saturate(sFactor * light.Color) * attenuation * visibility;


		return;
	}
	case 4: //spotlight
	default:
		return;
	}
}


#endif