﻿
#ifndef STANDARD
#define STANDARD

cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
}

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
	float4 Eye;
};

#endif