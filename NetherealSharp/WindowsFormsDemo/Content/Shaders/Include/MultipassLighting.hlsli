﻿
#ifndef MULTIPASS_LIGHTING
#define MULITPASS_LIGHTING

#include <StandardLights.hlsli>

cbuffer Light : register(b2)
{
	AmbientLight AmbientLight;
	AmorphicLight LightPass;
}

void ComputeHemisphereAmbient(in AmbientLight light, in float3 n, out float4 ambient)
{
	ambient = light.Down + (n.y * 0.5f + 0.5f) * light.Range;
}

void ComputeLightAddPass(
	in float d,
	in float4 p,
	in float3 n,
	in float3 e,
	in float shininess,
	inout float4 diffuse,
	inout float4 specular)
{

}