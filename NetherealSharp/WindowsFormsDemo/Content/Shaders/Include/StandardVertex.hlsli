﻿
#ifndef STANDARD_VERTEX
#define STANDARD_VERTEX

struct VertexIn
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float2 TexCoord	: TEXCOORD;
};

struct VertexPT
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD;
};

struct VertexPNT
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float2 TexCoord	: TEXCOORD;
};

struct Vertex
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float3 Tangent	: TANGENT0;
	float3 Bitangent: TANGENT1;
	float2 TexCoord	: TEXCOORD;
};

#endif