﻿

uniform sampler2D diffuseMap;

#ifdef VERTEX_SHADER
in vec4 vPosition;
//in vec3 vNormal;
in vec2 vTexCoord;

out vec4 fPosition;
out vec2 fTexCoord;

void main() {
	fPosition = vPosition;
	fTexCoord = vTexCoord;
}

#endif

#ifdef FRAGMENT_SHADER
in vec4 fPosition;
in vec2 fTexCoord;

out vec4 color;

void main() {
	color = texture(diffuseMap, fTexCoord);
}
#endif