﻿
#include <StandardVertex.hlsl>

struct PixelIn
{
	float4 Position	: SV_POSITION;
	float2 TexCoord	: TEXCOORD;
};

cbuffer SSAO : register(b0)
{
	float cameraNear;
	float cameraFar;

	float2 size;

};

Texture2D DiffuseMap : register(t0);
Texture2D DepthMap : register(t1);
SamplerState linearSampler : register(s0);

PixelIn VS(VertexPT input)
{
	PixelIn output = (PixelIn)0;
	output.Position = input.Position;
	output.TexCoord = input.TexCoord;
}

float4 PS(PixelIn input)
{
	return DiffuseMap.Sample(linearSampler, input.TexCoord);
}