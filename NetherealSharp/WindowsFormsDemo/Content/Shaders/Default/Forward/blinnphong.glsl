﻿
#define MAXLIGHTS 3
#define MAXSHADOWCASCADES 3

struct AmbientLight {
	vec4 Down;
	vec4 Range;
};

struct AmorphicLight
{
	bool Enabled;
	bool ShadowEnabled;
	int MatrixIndex;
	int MapIndex;

	vec3 Position;
	float Type;

	vec3 Direction;
	float pad1;

	vec3 ThetaPhiGamma;
	float Intensity;

	vec4 Color;
};

///////////////////

uniform Object{
	mat4 World;
mat4 WorldViewProj;
};

uniform Camera{
	mat4 View;
mat4 Proj;
mat4 ViewProj;
mat4 Screen;
vec4 Eye;
};

uniform Light{
	AmbientLight Ambient;
AmorphicLight Lights[MAXLIGHTS];
mat4 ShadowTransforms[MAXLIGHTS * MAXSHADOWCASCADES];
};

uniform Material{
	vec4 DiffuseColor;
vec4 SpecularColor;

float Shininess;
float DiffuseIntensity;
float SpecularIntensity;
};

uniform sampler2D DiffuseMap;

#if VERTEX_SHADER

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec4 fPositionW;
out vec3 fNormalW;
out vec2 fTexCoord;


void main() {
	fPositionW = World * vPosition;
	gl_Position = WorldViewProj * vPosition;

	fNormalW = mat3(World) * vNormal;
	fTexCoord = vTexCoord;
}


#endif

#if FRAGMENT_SHADER

in vec4 fPositionW;
in vec3 fNormalW;
in vec2 fTexCoord;

out vec4 color;

void ComputeForwardLightBlinnPhong(
	AmorphicLight light,
	in int i,
	in float d,
	in vec4 p,
	in vec3 n,
	in vec3 e,
	in float shininess,
	inout vec4 diffuse,
	inout vec4 specular)
{
	//switch (light.Type)
	//{
	//case 2:
	//{
	vec3 l = -light.Direction.xyz;
	vec3 h = normalize(l + n);

	float dFactor = max(dot(l, n), 0.0f);
	float sFactor = pow(max(dot(n, h), 0.0f), shininess);

	diffuse += clamp(dFactor * light.Color, 0.0, 1.0);
	specular += clamp(sFactor * light.Color, 0.0, 1.0);
	return;
	//}
	//default:
	//	return;
	//}
}


void main() {

	vec3 normal = normalize(fNormalW);
	vec3 toeye = normalize(Eye.xyz - fPositionW.xyz);
	float dist = length(Eye.xyz - fPositionW.xyz);

	float up = normal.y * 0.5f + 0.5f;
	vec4 ambient = (Ambient.Down + up * Ambient.Range);
	vec4 diffuse = vec4(0);
	vec4 specular = vec4(0);

	ComputeForwardLightPhong(Lights[0], 0, dist, fPositionW, normal, toeye, Shininess, diffuse, specular);

	vec4 tex = texture(DiffuseMap, fTexCoord);
	color = tex * (ambient + diffuse) + specular;
}

#endif