﻿
struct VertexIn
{
	float4 Position	: POSITION;
	//float3 Normal	: NORMAL;
	//float2 TexCoord	: TEXCOORD;
};

struct GoemIn
{

};

struct PixelIn
{
	float4 PositionH	: SV_POSITION;
	uint TargetIndex	: SV_RenderTargetArrayIndex;
	//float2 TexCoord		: TEXCOORD;
};


cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
	float4 Eye;
};

cbuffer Light : register(b2)
{

}

GeomIn VS(VertexIn input)
{
	GeomIn output;
	return output;
}

[maxvertexcount(4)]
void GS(triangle GeomIn input[3], inout TriangleStream<PixelIn> outputStream)
{
	PixelIn output = (pixelIn)0;

}

float4 PixelIn PS()
{

}