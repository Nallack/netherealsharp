﻿
struct VertexIn
{
	float4 position : POSITION;
	float2 texcoord : TEXCOORD;
};

struct PixelIn
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
	int ArrayIndex;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
};

Texture2DArray diffuseMap : register (t0);

SamplerState linearSampler : register(s0);

PixelIn VS(VertexIn vertex)
{
	PixelIn pixel = (PixelIn)0;
	pixel.position = mul(WorldViewProj, vertex.position);
	//pixel.position = vertex.position;
	pixel.texcoord = vertex.texcoord;
	return pixel;
}

float4 PS(PixelIn pixel) : SV_TARGET
{
	return diffuseMap.Sample(linearSampler, float3(pixel.texcoord, ArrayIndex));
}