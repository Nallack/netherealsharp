﻿

struct VertexIn
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float2 TexCoord	: TEXCOORD;
};

struct PixelIn
{
	float4 PositionH	: SV_POSITION;
	float2 TexCoord		: TEXCOORD;
};


cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
};


Texture2D diffuseMap : register (t0);
SamplerState linearSampler : register(s0);

PixelIn VS(VertexIn input)
{
	PixelIn output = (PixelIn)0;
	output.PositionH = mul(Screen, input.Position);
	output.TexCoord = input.TexCoord;

	return output;
}

float4 PS(PixelIn input) : SV_TARGET
{
	//return float4(1, 0, 0, 1);
	return diffuseMap.Sample(linearSampler, input.TexCoord);
	//return float4(c.a, 0, 0, 1);
}