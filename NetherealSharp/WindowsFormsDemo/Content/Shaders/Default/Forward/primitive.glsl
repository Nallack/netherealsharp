﻿
#if VERTEX_SHADER

in vec4 vPosition;
in vec4 vColor;

out vec4 fColor;

void main() {
	gl_Position = vPosition;
	fColor = vColor;
}

#endif

#if FRAGMENT_SHADER
in vec4 fColor;
out vec4 color;

void main() {
	color = fColor;
}
#endif