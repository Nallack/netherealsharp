

#include "Standard.glsli"
#include "StandardLights.glsli"

//Might want to try a few things with the preprocessor
//such as nested includes and detecting comments

#include "ForwardLighting.glsli"

//layoutlocation = 3
uniform Material
{
	vec4 DiffuseColor;
	vec4 SpecularColor;

	float Shininess;
	float DiffuseIntensity;
	float SpecularIntensity;
};

uniform sampler2D DiffuseMap;

#ifdef VERTEX_SHADER

in vec4 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec4 fPositionW;
out vec3 fNormalW;
out vec2 fTexCoord;

void main() {
	fPositionW = World * vPosition;
	gl_Position = WorldViewProj * vPosition;

	fNormalW = mat3(World) * vNormal;
	fTexCoord = vTexCoord;
}

#elif FRAGMENT_SHADER

in vec4 fPositionW;
in vec3 fNormalW;
in vec2 fTexCoord;

out vec4 color;

void main() {

	vec3 normal = normalize(fNormalW);
	vec3 toeye = normalize(Eye.xyz - fPositionW.xyz);
	float dist = length(Eye.xyz - fPositionW.xyz);

	float up = normal.y * 0.5f + 0.5f;
	vec4 ambient = (Ambient.Down + up * Ambient.Range);
	vec4 diffuse = vec4(0);
	vec4 specular = vec4(0);

	ComputeForwardLightPhong(Lights[0], 0, dist, fPositionW, normal, toeye, Shininess, diffuse, specular);

	vec4 tex = texture(DiffuseMap, fTexCoord);
	color = tex * (ambient + diffuse) + specular;
}

#endif