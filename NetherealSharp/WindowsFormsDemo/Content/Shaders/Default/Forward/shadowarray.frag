﻿

in vec4 fPosition;

out vec4 color;

void main() {
	float depth = fPosition.z;
	color = vec4(depth, depth * depth, 0, 1);
}