﻿
#define MAXLIGHTS 3
#define MAXSHADOWCASCADES 3

SamplerState linearSampler : register(s0);

#include "Standard.hlsli"
#include "StandardVertex.hlsli"
#include "ForwardLighting.hlsli"


struct PixelIn
{
	float4 PositionW	: POSITION0;
	float4 PositionH	: SV_POSITION;
	float3 NormalW		: NORMAL;
	float2 TexCoord		: TEXCOORD;
};



cbuffer Material : register(b3)
{
	float4 DiffuseColor;
	float4 SpecularColor;

	float Shininess;
	float DiffuseIntensity;
	float SpecularIntensity;
};

Texture2D diffuseMap : register (t0);



PixelIn VS(VertexIn input)
{
	PixelIn output = (PixelIn)0;
	output.PositionW = mul(World, input.Position);
	
	//output.PositionH = mul(WorldViewProj, input.Position);
	output.PositionH = mul(ViewProj, output.PositionW);

	output.NormalW = mul((float3x3)World, input.Normal);
	output.TexCoord = input.TexCoord;

	return output;
}

float4 PS(PixelIn input) : SV_TARGET
{
	float3 normal = normalize(input.NormalW);
	float3 toeye = normalize(Eye.xyz - input.PositionW.xyz);
	float dist = length(Eye.xyz - input.PositionW.xyz); //could use dot(disp,disp) for length squared

	float4 ambient = (float4)0;
	float4 diffuse = (float4)0;
	float4 specular = (float4)0;

	ComputeHemisphereAmbient(normal, ambient);

	for (int i = 0; i < MAXLIGHTS; i++) 
		ComputeForwardLightBlinnPhong(Lights[i], i, dist, input.PositionW, normal, toeye, Shininess, diffuse, specular);


	float4 tex = diffuseMap.Sample(linearSampler, input.TexCoord);
	return tex * ((ambient + diffuse) * DiffuseColor * DiffuseIntensity) + specular * SpecularColor * SpecularIntensity;
}