﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsDemo
{
	public partial class LauncherWindow : Form
	{
		public enum SceneType
		{
			Empty = 0,
			FirstPerson3D = 1,
			Platformer2D = 2
		}

		public int SceneIndex = 0;
		public GraphicsImplementation Graphics { get; private set; }
		public PhysicsImplementation3D Physics { get; private set; }
		public QualityLevel ShadowQuality { get; private set; }

		public RenderMode RenderMode { get; private set; }

		public bool VRMode = false;

		public bool Run = false;

		public LauncherWindow()
		{
			InitializeComponent();
			comboBoxGraphics.SelectedIndex = 0;
			comboBoxShadows.SelectedIndex = 0;
			comboBoxScene.SelectedIndex = 0;
			comboBoxRenderer.SelectedIndex = 0;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Run = true;
			this.Close();
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			VRMode = checkBox1.Checked;
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			SceneIndex = comboBoxScene.SelectedIndex;
		}

		private void comboBoxGraphics_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = comboBoxGraphics.SelectedItem as string;
			switch (text)
			{
				case "DirectX11":
					Graphics = GraphicsImplementation.DirectX11;
					break;
				case "DirectX12":
					Graphics = GraphicsImplementation.DirectX12;
					break;
				case "OpenGL":
					Graphics = GraphicsImplementation.OpenGL;
					break;
				case "Vulkan":
					Graphics = GraphicsImplementation.Vulkan;
					break;
				default:
					break;

			}
		}

		private void comboBoxShadows_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = comboBoxShadows.SelectedItem as string;
			switch (text)
			{
				case "None":
					ShadowQuality = QualityLevel.None;
					break;
				case "Low":
					ShadowQuality = QualityLevel.Low;
					break;
				case "Medium":
					ShadowQuality = QualityLevel.Medium;
					break;
				case "High":
					ShadowQuality = QualityLevel.High;
					break;
				default:
					break;

			}
		}

		private void comboBoxRenderer_SelectedIndexChanged(object sender, EventArgs e)
		{
			string text = comboBoxRenderer.SelectedItem as string;
			switch(text)
			{
				case "Forward":
					RenderMode = RenderMode.Forward;
					break;
				case "ClearOnly":
					RenderMode = RenderMode.ClearOnly;
					break;
				case "Deffered":
					RenderMode = RenderMode.Deffered;
					break;
				default:
					break;
			}
		}
	}
}
