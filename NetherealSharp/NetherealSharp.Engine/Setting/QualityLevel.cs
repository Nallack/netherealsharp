﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public enum QualityLevel
    {
		None = 0,
		Low = 1,
		Medium = 2,
		High = 3
    }
}
