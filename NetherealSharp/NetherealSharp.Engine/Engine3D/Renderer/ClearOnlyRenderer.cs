﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public class ClearOnlyRenderer : SceneRenderer
    {
		public DrawArgs3D drawArgs;

		public GraphicsPipeline ClearPipeline;

		public ClearOnlyRenderer(GraphicsDevice device, IRenderTarget target)
		{
			drawArgs = new DrawArgs3D();

			if (device.Implementation == GraphicsImplementation.Vulkan)
			{
				//ClearPipeline = new Core.Graphics.Vulkan.GraphicsPipeline(device as Core.Graphics.Vulkan.GraphicsDevice, target as RenderTarget, new Core.ViewportF(0, 0, 1, 1), null, null, null, null);
			}
		}


		bool once = false;
		/// <summary>
		/// Fills a command buffer
		/// </summary>
		/// <param name="scene"></param>
		/// <param name="renderArgs"></param>
		public override void Render(Scene3D scene, RenderArgs renderArgs)
		{


			if (renderArgs.Device.Implementation == GraphicsImplementation.Vulkan)
			{
				//Vulkan Version

				//TODO: main difference compared to other API's is we don't actually need to fill the drawCmdBuffers every frame
				//triangle demo calls the exact same Commands every frame, only change is updating a uniform buffer occasionally.

				var Device = renderArgs.Device as NetherealSharp.Core.Graphics.Vulkan.GraphicsDevice;
				var Context = renderArgs.Context as NetherealSharp.Core.Graphics.Vulkan.GraphicsContext;
				var vktarget = renderArgs.Target as NetherealSharp.Core.Graphics.Vulkan.IVKRenderTarget;


				//Context.Device.LogicalDevice.WaitForFences(1, )

				vktarget.PrepareFrame();


				if (!once)
				{
					Context.Begin(renderArgs.Target);


					Context.ClearDepthStencilView(renderArgs.Target);
					Context.ClearRenderTargetView(renderArgs.Target, Core.Color.Green);

					Context.BeginRenderPass(renderArgs.Target, Core.Color.Blue);


					//Context.ClearTarget(renderArgs.Target, Core.Color.Blue);

					//Context.BindPipeline();
					//Context.Begin(renderArgs.Target);
					//Context.SetRenderTarget(renderArgs.Target);
					//Context.End(renderArgs.Target);
					//renderArgs.Target.Present();

					Context.EndRenderPass();


					Context.End();
					once = true;
				}

				vktarget.Submit();

				renderArgs.Target.Present();

				//Context.Present(renderArgs.Target);

				Device.Queue.WaitIdle();
			}
			else
			{
				var Device = renderArgs.Device;
				var Context = renderArgs.Context;

				Context.Begin(renderArgs.Target);
				Context.SetRenderTarget(renderArgs.Target);

				//scene.Draw();

				Context.End(renderArgs.Target);

				renderArgs.Target.Present();
			}


			

		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
