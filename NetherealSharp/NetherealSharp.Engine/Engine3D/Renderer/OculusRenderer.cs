﻿#if OCULUS
using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.VR;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Rename to VRRenderPipeline or SceneRenderer
	/// </summary>
	public class OculusRenderer : SceneRenderer
	{
		public LightUniformBuffer lightUniform;
		public UniformBuffer<CameraData> cameraUniform;
		public ShadowUniformBuffer shadowUniform;

		public DrawArgs3D drawArgs;

		public OculusRenderer(GraphicsDevice device)
		{
			cameraUniform = UniformBuffer<CameraData>.Create(device);
			lightUniform = LightUniformBuffer.Create(device, 3);
			//shadowUniform = ShadowUniformBuffer.Create(device, 3);

			drawArgs = new DrawArgs3D();
		}

		public override void Render(Scene3D scene, RenderArgs renderArgs)
		{
			var Graphics = renderArgs.Device;
			var Context = renderArgs.Context;

			drawArgs.Device = Graphics;
			drawArgs.Context = Graphics.ImmediateContext;

			drawArgs.Camera = cameraUniform;
			drawArgs.Lights = lightUniform;
			drawArgs.Shadows = shadowUniform;

			scene.BeginDraw(drawArgs);

			lightUniform.Data.Lights.Clear();
			lightUniform.Data.ShadowTransforms.Clear();
			lightUniform.ShadowMaps.Clear();

			foreach (var light in scene.Lights)
			{
				if (light.LightType == LightType.Ambient) lightUniform.Data.Ambient = (light as IAmbientLight3D).Ambient;

				else
				{
					switch (light.LightType)
					{
						case LightType.Directional:
							var dlight = light as IDirectionalLight3D;
							var directionaldata = dlight.Directional;

							light.DrawShadows(drawArgs);

							//index references starting transform and shadowmap resource
							int shadowindex = lightUniform.Data.ShadowTransforms.Count;

							//Set the current shadow index to the next available slot
							directionaldata.MatrixIndex = lightUniform.Data.ShadowTransforms.Count;
							lightUniform.Data.Lights.Add(directionaldata);
							lightUniform.Data.ShadowTransforms.AddRange(dlight.ShadowTransforms);
							//lightUniform.ShadowMaps.AddRange(dlight.ShadowMap);
							lightUniform.ShadowMaps.Add(dlight.ShadowMap);
							break;

						case LightType.Point:
							var plight = light as IPointLight3D;
							var pointlightdata = plight.Point;

							light.DrawShadows(drawArgs);

							lightUniform.Data.Lights.Add(pointlightdata);
							//lightUniform.Data.ShadowTransforms.Add(plight.ShadowTransforms);

							break;
					}
				}
			}

			//set all shadowmaps to an array!
			//args.Context.SetShaderResources((int)TextureLocation.ShadowMap, lightUniform.ShadowMaps.ToArray());
			//lightUniform.ShadowMaps[0].Apply(args.Context, 1);


			//NEED TO BIND SHADOW TEXTURE IN EFFECT
			//ActiveScene.Lights[1].ShadowMap.Target.Apply(Graphics.ImmediateContext, 1);

			drawArgs.ShadowMaps = lightUniform.ShadowMaps.ToArray();

			if (scene.Lights.Count > 0)
			{
				lightUniform.Update();
				lightUniform.Bind(drawArgs.Context, (int)UniformLocation.Light);
			}


			//Draw to Output
			var vrtarget = renderArgs.VRTarget as Core.VR.Oculus.VRTarget;

			OculusWrap.OVRTypes.Vector3f[] hmdtoeyeviewoffsets =
			{
					vrtarget.EyeTargets[0].HmdToEyeViewOffset,
					vrtarget.EyeTargets[1].HmdToEyeViewOffset
				};

			double displaymidpoint = vrtarget.m_VRDevice.HMD.GetPredictedDisplayTime(0);
			var trackingstate = vrtarget.m_VRDevice.HMD.GetTrackingState(displaymidpoint, true);

			var eyeposes = new OculusWrap.OVRTypes.Posef[2];
			vrtarget.m_VRDevice.Runtime.CalcEyePoses(trackingstate.HeadPose.ThePose, hmdtoeyeviewoffsets, ref eyeposes);

			//multithread this
			for (int i = 0; i < 2; i++)
			{

				var eyetarget = vrtarget.EyeTargets[i];

				//MAKE SURE TO SET THIS OR WONT RENDER!!!
				vrtarget.LayerEyeFov.RenderPose[i] = eyeposes[i];

				Context.SetRenderTarget(eyetarget);

				Context.SetViewport(
					eyetarget.Viewport.X,
					eyetarget.Viewport.Y,
					eyetarget.Viewport.Width,
					eyetarget.Viewport.Height);

				Context.ClearTarget(eyetarget, Color.SkyBlue);

				foreach (var camera in scene.Cameras)
				{

					Context.SetViewport(
							camera.Viewport.X * eyetarget.Width,
							camera.Viewport.Y * eyetarget.Height,
							(camera.Viewport.X + camera.Viewport.Width) * eyetarget.Width,
							(camera.Viewport.Y + camera.Viewport.Height) * eyetarget.Height);


					cameraUniform.Data.Screen = Matrix.OrthoOffCenterRH(
						-eyetarget.Width / 2f,
						eyetarget.Width / 2f,
						-eyetarget.Height / 2f,
						eyetarget.Height / 2f,
						-1f,
						1f);

					cameraUniform.Data.Screen = Matrix.OrthoOffCenterRH(0, eyetarget.Width, eyetarget.Height, 0, -1f, 1f);

					cameraUniform.Data.View = camera.ViewMatrix;


					//TODO: Overriding view matrix atm to incorperate head tracking (will want to move this to camera class)
					var q = eyeposes[i].Orientation;
					Quaternion rot = q;
					Vector3 pos = eyeposes[i].Position;

					Vector3 from = camera.Transform.Position + pos;

					Vector3 forward = Vector3.Transform(Vector3.ForwardRH, rot);
					//Vector3 forward = Vector3.Transform(Vector3.Transform(Vector3.ForwardRH, rot), camera.Entity.Transform.Rotation);
					Vector3 to = from + forward;

					Vector3 up = Vector3.Transform(Vector3.Up, rot);
					//Vector3 up = Vector3.Transform(Vector3.Transform(Vector3.Up, rot), camera.Entity.Transform.Rotation);

					cameraUniform.Data.View = Matrix.LookAtRH(from, to, up);

					/*
					cameraUniform.Data.Project = Matrix.PerspectiveFovRH(
						camera.Fov,
						renderArgs.Target.Width / (float)renderArgs.Target.Height,
						camera.Near,
						camera.Far);
					*/


					var wrap = (VRManager.VRDevice as Core.VR.Oculus.VRDevice).Runtime;

					cameraUniform.Data.Project = wrap.Matrix4f_Projection(
						eyetarget.FOV,
						camera.Near,
						camera.Far,
						OculusWrap.OVRTypes.ProjectionModifier.None); //right hand

					cameraUniform.Data.Project.Transpose();

					cameraUniform.Data.ViewProj = cameraUniform.Data.View * cameraUniform.Data.Project;

					cameraUniform.Data.Eye = camera.Transform.Position + eyeposes[i].Position;

					cameraUniform.Update();
					cameraUniform.Bind(Graphics.ImmediateContext, 1);

					drawArgs.Camera = cameraUniform;
					drawArgs.Lights = lightUniform;
					scene.Draw(drawArgs);

					eyetarget.Present(); //draw to next texture in sequence
				}
			}

			renderArgs.VRTarget.Present();

			
			if (renderArgs.VRMirror != null)
			{
				renderArgs.VRMirror.CopyToTarget();
				renderArgs.Target.Present();
			}
		}

		public override void Dispose()
		{
			cameraUniform.Dispose();
			lightUniform.Dispose();
		}
	}
}
#endif