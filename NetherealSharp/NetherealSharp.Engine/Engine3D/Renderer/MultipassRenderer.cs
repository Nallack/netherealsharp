﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine.Engine3D
{
	/// <summary>
	/// Multiple passes for each light source.
	/// Pros: 
	///		Arbitrary number of lights can be added at runtime
	///		Object culling for lights is simplified
	/// Cons:
	///		Vertex and rasteriser stages are repeated for each light source
	///		Draw calls increase
	///		
	/// Good for large scenes with small objects and lights
	/// </summary>
	public class MultipassRenderer : SceneRenderer
	{
		public LightUniformBuffer lightUniform;
		public UniformBuffer<CameraData> cameraUniform;
		public DrawArgs3D drawArgs;

		public MultipassRenderer(GraphicsDevice device)
		{
			cameraUniform = UniformBuffer<CameraData>.Create(device);
			lightUniform = LightUniformBuffer.Create(device, 1);
			drawArgs = new DrawArgs3D();
		}

		public override void Render(Scene3D scene, RenderArgs renderArgs)
		{
			throw new NotImplementedException();
		}

		public override void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
