﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public class LightPassRenderPipeline : SceneRenderer
    {
		public LightUniformBuffer lightUniform;
		public UniformBuffer<CameraData> cameraUniform;
		public ShadowUniformBuffer shadowUniform;

		public DrawArgs3D drawArgs;

		public LightPassRenderPipeline(GraphicsDevice device)
		{
			cameraUniform = UniformBuffer<CameraData>.Create(device);
			lightUniform = LightUniformBuffer.Create(device, 1);
			shadowUniform = ShadowUniformBuffer.Create(device, 1);

			drawArgs = new DrawArgs3D();
		}

		public override void Render(Scene3D scene, RenderArgs renderArgs)
		{
			var Context = renderArgs.Context;
			drawArgs.Device = renderArgs.Device;
			drawArgs.Context = renderArgs.Context;

			drawArgs.Camera = cameraUniform;
			drawArgs.Lights = lightUniform;
			drawArgs.Shadows = shadowUniform;

			scene.BeginDraw(drawArgs);

			//----LIGHTS AND SHADOWS -----//
			lightUniform.Data.Lights.Clear();
			lightUniform.Data.ShadowTransforms.Clear();
			lightUniform.ShadowMaps.Clear();

			foreach(var light in scene.Lights)
			{
				switch(light.LightType)
				{
					case LightType.Ambient:
						lightUniform.Data.Ambient = (light as IAmbientLight3D).Ambient;
						break;
					case LightType.Directional:
						var dlight = light as IDirectionalLight3D;
						dlight.BeginDraw(drawArgs);
						light.DrawShadows(drawArgs);
						break;
				}
			}

			lightUniform.Update();
			lightUniform.Bind(drawArgs.Context, (int)UniformLocation.Light);


			Context.Begin(renderArgs.Target);
			Context.SetRenderTarget(renderArgs.Target);
			Context.ClearTarget(renderArgs.Target, Color.SkyBlue);

			foreach(var camera in scene.Cameras)
			{

			}

		}

		public override void Dispose()
		{

		}
    }
}
