﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public abstract class SceneRenderer : IDisposable
    {


		public abstract void Render(Scene3D scene, RenderArgs renderArgs);

		public abstract void Dispose();
    }
}
