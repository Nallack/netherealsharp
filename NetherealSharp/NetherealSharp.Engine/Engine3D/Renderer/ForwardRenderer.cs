﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Forward lighting pipeline with a fixed muber of light sources.
	/// Pros:
	///		Single pass for each object
	///		Light uniform can be set once for entire scene
	///		can handle transparency
	///	Cons:
	///		lighting calculated for every primitive
	///		Hard limit on maximum lights
	///		
	///	 Good for large or small scene with an ambient and directional light,
	///	 Predetermined number of additional lights (such as ones bound to the player).
	///		
	/// </summary>
	public class ForwardSceneRenderer : SceneRenderer
	{
		public LightUniformBuffer lightUniform;
		public UniformBuffer<CameraData> cameraUniform;
		public ShadowUniformBuffer shadowUniform;

		public DrawArgs3D drawArgs;

		public ForwardSceneRenderer(GraphicsDevice device)
		{
			cameraUniform = UniformBuffer<CameraData>.Create(device);
			lightUniform = LightUniformBuffer.Create(device, 3);

			//TODO: should reflect settings, maybe constructor could automatically detect them
			shadowUniform = ShadowUniformBuffer.Create(device, 3);

			drawArgs = new DrawArgs3D();
		}

		/// <summary>
		/// TODO: might want want to store VR flag with renderArgs or the target itself, and combine vr and screen rendering into this class.
		/// </summary>
		/// <param name="scene"></param>
		/// <param name="renderArgs"></param>
		public override void Render(Scene3D scene, RenderArgs renderArgs)
		{
			var Graphics = renderArgs.Device;
			var Context = renderArgs.Context;


			drawArgs.Device = Graphics;
			drawArgs.Context = Graphics.ImmediateContext;
			drawArgs.OutputTarget = renderArgs.Target;

			drawArgs.Camera = cameraUniform;
			drawArgs.Lights = lightUniform;
			drawArgs.Shadows = shadowUniform;

			//BeginDraw sets up each object for future draws
			scene.BeginDraw(drawArgs);

			//----------LIGHTS AND SHADOWS------------//

			lightUniform.Data.Lights.Clear();
			lightUniform.Data.ShadowTransforms.Clear();
			lightUniform.ShadowMaps.Clear();

			foreach (var light in scene.Lights)
			{
				if (light.LightType == LightType.Ambient)
				{
					lightUniform.Data.Ambient = (light as IAmbientLight3D).Ambient;
				}
				else
				{
					switch (light.LightType)
					{
						case LightType.Directional:
							var dlight = light as IDirectionalLight3D;

							//---------UPDATE LIGHT UNIFORMS-------------//

							dlight.BeginDraw(drawArgs);
							var directionaldata = dlight.Directional;

							if (GraphicsManager.GraphicsSettings.ShadowQuality != QualityLevel.None)
							{
								//----------DRAW SHADOWS------------------------//

								light.DrawShadows(drawArgs);

								//----------SET UP SHADOW TRANSFORMS FOR SHADOW SAMPLING---------//

								directionaldata.MatrixIndex = lightUniform.Data.ShadowTransforms.Count;
								directionaldata.MapIndex = lightUniform.ShadowMaps.Count;
		
								lightUniform.Data.ShadowTransforms.AddRange(dlight.ShadowTransforms);
								lightUniform.ShadowMaps.Add(dlight.ShadowMap); //Texture2DArray shadowmaps

								//lightUniform.ShadowMaps.AddRange(dlight.ShadowMaps); //Texture2D shadowmaps
							}

							lightUniform.Data.Lights.Add(directionaldata);


							break;

						case LightType.Point:
							var plight = light as IPointLight3D;
							var pointlightdata = plight.Point;

							//TODO: maybe pass in the texture array here, as light may not be responsible for creating its own shadowmaps
							light.DrawShadows(drawArgs);

							lightUniform.Data.Lights.Add(pointlightdata);
							//lightUniform.Data.ShadowTransforms.Add(plight.ShadowTransforms);

							break;

						case LightType.SpotLight:
							break;
					}
				}
			}

			if (scene.Lights.Count > 0)
			{
				lightUniform.Update();
				lightUniform.Bind(drawArgs.Context, (int)UniformLocation.Light);
			}

			//bind shadowmaps for all shaders
			drawArgs.ShadowMaps = lightUniform.ShadowMaps.ToArray();
			drawArgs.Context.SetShaderResources((int)TextureLocation.ShadowMap, lightUniform.ShadowMaps.ToArray());



			//----START DRAWING--------//

			Context.Begin(renderArgs.Target);
			Context.SetRenderTarget(renderArgs.Target);
			//Context.ClearTarget(renderArgs.Target, Color.SkyBlue);

			foreach (ICamera camera in scene.Cameras)
			{

				Context.SetViewport(
					camera.Viewport.X * renderArgs.Target.Width,
					camera.Viewport.Y * renderArgs.Target.Height,
					(camera.Viewport.X + camera.Viewport.Width) * renderArgs.Target.Width,
					(camera.Viewport.Y + camera.Viewport.Height) * renderArgs.Target.Height);

				Context.ClearTarget(renderArgs.Target, camera.ClearColor);

				cameraUniform.Data.Screen = Matrix.OrthoOffCenterRH(
					0,
					renderArgs.Target.Width,
					renderArgs.Target.Height,
					0,
					-1f,
					1f);

				cameraUniform.Data.View = camera.ViewMatrix;

				//cameraUniform.Data.Project = camera.ProjectMatrix()
				cameraUniform.Data.Project = camera.ProjectMatrix(renderArgs.Target.Width, renderArgs.Target.Height);

				/*
				cameraUniform.Data.Project = Matrix.PerspectiveFovRH(
					camera.Fov,
					renderArgs.Target.Width / (float)renderArgs.Target.Height,
					camera.Near,
					camera.Far);
				*/

				cameraUniform.Data.ViewProj = cameraUniform.Data.View * cameraUniform.Data.Project;
				cameraUniform.Data.Eye = camera.Transform.Position;
				cameraUniform.Update();
				cameraUniform.Bind(drawArgs.Context, (int)UniformLocation.Camera);

				drawArgs.CameraFrustum = new BoundingFrustum(cameraUniform.Data.ViewProj);
				drawArgs.Camera = cameraUniform;
				drawArgs.Lights = lightUniform;

				camera.PreDraw(drawArgs);
				scene.Draw(drawArgs);
				camera.PostDraw(drawArgs);

			}

			Graphics.End(renderArgs.Target);

			drawArgs.Context.EmptyShaderResources((int)TextureLocation.ShadowMap, drawArgs.ShadowMaps.Length);

			renderArgs.Target.Present();
		}

		public override void Dispose()
		{
			cameraUniform.Dispose();
			lightUniform.Dispose();
		}
	}
}
