﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
	public enum RenderMode
	{
		ClearOnly,
		Forward,
		MultipassForward,
		Deffered
	}
}
