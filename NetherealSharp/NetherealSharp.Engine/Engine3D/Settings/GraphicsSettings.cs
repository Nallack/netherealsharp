﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public class GraphicsSettings
    {
		public QualityLevel ShadowQuality = QualityLevel.None;

		public int ShadowCascades = 3;
		public int ShadowResolution = 1024;
		public float MaxShadowDistance = 100; //TODO: maybe make this a scene property
		public bool SoftShadows = true;

		//Forward Rendering Only
		public int MaxLights = 3;


		public bool FixedInterval = false;

		public bool MultiPassForward = false;
		public bool DeferredRendering = false;
		public bool HighDynamicRange = false;
		public bool Reflections; //world, sky, none

        public int TextureFiltering = 0;
        public int MaximumAnistropy = 8;

        public int AntiAliasing = 4;
        public int LevelOfDetail = 10;

		public QualityLevel ModelQuality = QualityLevel.High;
		public QualityLevel ParticleQuantity = QualityLevel.Medium;



		public bool FocalLense = false;
		public bool MotionBlur = false;
		public bool Bloom = false;
        public bool SSAO = false;
		public bool HDAO = false;
		public bool HBAO = false;
        public bool EdgeTesselation = false;
        
        public GraphicsSettings() { }
    }
}
