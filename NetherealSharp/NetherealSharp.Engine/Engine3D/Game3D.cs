﻿
using System;
using System.Collections.Generic;
using System.Text;


#if FORMS
using NetherealSharp.Core.Windows;
#endif

using NetherealSharp.Core.VR;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core;
using NetherealSharp.Core.Physics;
using NetherealSharp.Engine.Input;

namespace NetherealSharp.Engine
{
	public class Game3D : Game
	{

		protected SceneRenderer renderer;

		public RenderTarget Target { get; protected set; }

		public VRTarget VRTarget { get; protected set; }
		public VRMirrorTexture VRMirror { get; protected set; }

		protected bool Oculusmode = false;
		protected bool OculusMirror = false;

		public Scene3D ActiveScene { get; set; }

		RenderArgs renderargs = new RenderArgs();
		DrawArgs3D args = new DrawArgs3D();

		public Game3D() { }


		/// <summary>
		/// Override this to load scene, gets called by init
		/// </summary>
		public virtual void LoadContent() { }

		//TODO: possibly load managers here
		//(may be challenging as manager initializers have
		// different methods per implementation)
		// In the old days I casted to object and switched through casts (probably not very good)
		public override void Initialize()
		{
			Implementor.Initialize();

			Target = Implementor.WindowTarget;
#if VR
			VRTarget = Implementor.VRTarget;
			VRMirror = Implementor.VRMirror;
#endif

			LoadContent();

#if VR && OCULUS
			if (VRManager.Enabled)
			{
				renderer = new OculusRenderPipeline(GraphicsManager.GraphicsDevice);
			}
			else
#endif
			{
				switch(Implementor.RenderMode)
				{
					case RenderMode.ClearOnly:
						renderer = new ClearOnlyRenderer(GraphicsManager.GraphicsDevice, Target);
						break;
					case RenderMode.Forward:
						renderer = new ForwardSceneRenderer(GraphicsManager.GraphicsDevice);
						break;
					default:
						throw new NotImplementedException();
				}
				
			}

			renderargs.Device = GraphicsManager.GraphicsDevice;
			renderargs.Context = GraphicsManager.GraphicsDevice.ImmediateContext;
			renderargs.Target = Target;
#if OCULUS
			renderargs.VRTarget = VRTarget;
			renderargs.VRMirror = VRMirror;
#endif

			ActiveScene.Initialize();
		}

		public override void Loop()
		{
			//Handle resize at start of frames
			Target.CheckForResize();

			//Update frame time
			TimeManager.Update();

			//refresh input
			InputManager.Update();

			//Update (with timer debug)
			TimeManager.updatewatch.Restart();
			ActiveScene.Update();
			TimeManager.updatewatch.Stop();
			TimeManager.updatetime = TimeManager.updatewatch.Elapsed.TotalSeconds;

			//Render (with timer debug)
			TimeManager.drawwatch.Restart();
			renderer.Render(ActiveScene, renderargs);
			TimeManager.drawwatch.Stop();
			TimeManager.drawtime = TimeManager.drawwatch.Elapsed.TotalSeconds;

		}

		public override void Dispose()
		{

			ActiveScene.Dispose();

			renderer.Dispose();
			Target.Dispose();

			PhysicsManager.Dispose();
			ContentManager.Dispose();
			GraphicsManager.Dispose();
		}
	}
}
