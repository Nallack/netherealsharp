﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using NetherealSharp.Core;

namespace NetherealSharp.Engine
{
    public class DrawArgs3D
    {
		public GraphicsDevice Device;
		public GraphicsContext Context;
		public RenderTarget OutputTarget;

		public BoundingFrustum CameraFrustum;

		public UniformBuffer<CameraData> Camera;
		public LightUniformBuffer Lights;
		public ShadowUniformBuffer Shadows;

		public ITexture[] ShadowMaps;

		public int Pass;

    }
}
