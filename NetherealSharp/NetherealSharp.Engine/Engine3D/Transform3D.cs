﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace NetherealSharp.Engine
{
	[Serializable][DataContract]
    public class Transform3D
    {
		[DataMember]
		public Vector3 Position;
		[DataMember]
		public Quaternion Rotation;
		[DataMember]
		public Vector3 Scale;

		public Matrix Matrix
		{
			get
			{
				return Matrix.Scaling(Scale) *
					   Matrix.RotationQuaternion(Rotation) *
					   Matrix.Translation(Position);
			}
		}

		public Transform3D() : this(Vector3.Zero) { }

		public Transform3D(Vector3 position) : this(position, Quaternion.Identity, Vector3.One) { }

		public Transform3D(Vector3 position, Quaternion rotation) : this(position, rotation, Vector3.One) { }

		public Transform3D(Vector3 position, Quaternion rotation, Vector3 scale)
		{
			Position = position;
			Rotation = rotation;
			Scale = scale;
		}
    }
}
