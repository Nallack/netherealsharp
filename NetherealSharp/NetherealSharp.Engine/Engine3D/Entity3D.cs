﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Xml.Serialization;
using System.Linq;
using System.Runtime.Serialization;

namespace NetherealSharp.Engine
{
	[Serializable, DataContract]
    public class Entity3D : IInitializable, IUpdatable, IDrawable3D, IShadowDrawable, IDisposable
	{
		private bool m_Initialized = false;

		public Scene3D Scene { get; internal set; }

		[DataMember(Order = 0)]
		public string Name { get; set; }

		[DataMember(Order = 1)]
		public Transform3D Transform { get; set; }


		[DataMember(Order = 2)]
		public ObservableCollection<Behaviour3D> Behaviours
		{
			get { return m_Behaviors; }
			private set
			{
				m_Behaviors = value;
				
				//add behaviours already in collection of deserializer
				foreach (var b in m_Behaviors) AddBehaviour(b);

				//add future added behaviours
				m_Behaviors.CollectionChanged += (sender, e) =>
				{
					foreach (var b in e.NewItems)
					{
						AddBehaviour(b as Behaviour3D);
					}
				};
			}
		}

		#region private
		ObservableCollection<Behaviour3D> m_Behaviors;
		Dictionary<Type, Behaviour3D> BehaviourRegistry { get; set; }

		List<IUpdatable> Updatables { get; set; }
		List<IDrawable3D> Drawables { get; set; }
		List<IShadowDrawable> ShadowDrawables { get; set; }
		
		#endregion

		public Entity3D()
		{
			Transform = new Transform3D();
			Behaviours = new ObservableCollection<Behaviour3D>();

			BehaviourRegistry = new Dictionary<Type, Behaviour3D>();
			Updatables = new List<IUpdatable>();
			Drawables = new List<IDrawable3D>();
			ShadowDrawables = new List<IShadowDrawable>();
		}

		[OnDeserializing]
		public void Deserializing(StreamingContext context)
		{
			BehaviourRegistry = new Dictionary<Type, Behaviour3D>();
			Updatables = new List<IUpdatable>();
			Drawables = new List<IDrawable3D>();
			ShadowDrawables = new List<IShadowDrawable>();
		}

		public void AddBehaviour(Behaviour3D behaviour)
		{
			behaviour.Entity = this;
			if (behaviour as IUpdatable != null) Updatables.Add(behaviour as IUpdatable);
			if (behaviour as IDrawable3D != null) Drawables.Add(behaviour as IDrawable3D);
			if (behaviour as IShadowDrawable != null) ShadowDrawables.Add(behaviour as IShadowDrawable);

			BehaviourRegistry.Add(behaviour.GetType(), behaviour);

			if (m_Initialized) behaviour.Initialize();
		}

		public T GetBehaviour<T>() where T : Behaviour3D
		{
			return BehaviourRegistry[typeof(T)] as T;
		}

		public void Initialize()
		{
#if DEBUG
			if (Scene == null) throw new MissingMemberException();
#endif
			foreach (var b in Behaviours) b.Initialize();
			m_Initialized = true;
		}

		public void Update()
		{
			foreach (var b in Updatables) b.Update();
		}

		public void BeginDraw(DrawArgs3D args)
		{
			foreach(var b in Drawables) b.BeginDraw(args);
		}

		public void ShadowDraw(DrawArgs3D args)
		{
			foreach (var b in ShadowDrawables) b.ShadowDraw(args);
		}

		public void Draw(DrawArgs3D args)
		{
			foreach (var b in Drawables) b.Draw(args);
		}



		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				foreach (Behaviour3D b in Behaviours)
				{
					var d = b as IDisposable;
					if (d != null) d.Dispose();
				}

				disposedValue = true;
			}
		}

		~Entity3D() {
		   Dispose(false);
		}

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
