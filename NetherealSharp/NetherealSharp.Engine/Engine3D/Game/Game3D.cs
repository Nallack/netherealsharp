﻿
using System;
using System.Collections.Generic;
using System.Text;


#if FORMS
using NetherealSharp.Core.Windows;
#endif

using NetherealSharp.Core.VR;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core;
using NetherealSharp.Core.Physics;

namespace NetherealSharp.Engine
{
	public class Game3D : Game
	{

		protected SceneRenderer renderer;

		public RenderTarget Target { get; protected set; }

		public VRTarget VRTarget { get; protected set; }
		public VRMirrorTexture VRMirror { get; protected set; }

		protected bool Oculusmode = false;
		protected bool OculusMirror = false;

		protected Scene3D ActiveScene;

		RenderArgs renderargs = new RenderArgs();
		DrawArgs3D args = new DrawArgs3D();

		public Game3D() { }


		public override void Init()
		{
			Implementor.Initialize();

			Target = Implementor.WindowTarget;
			VRTarget = Implementor.VRTarget;
			VRMirror = Implementor.VRMirror;

			LoadContent();
			Initialize();

		}

		/// <summary>
		/// Override this to load scene, gets called by init
		/// </summary>
		public virtual void LoadContent() { }

		//TODO: possibly load managers here
		//(may be challenging as manager initializers have
		// different methods per implementation)
		// In the old days I casted to object and switched through casts (probably not very good)
		public virtual void Initialize()
		{
#if OCULUS
			if (VRManager.Enabled)
			{
                renderer = new OculusRenderPipeline(GraphicsManager.GraphicsDevice);
			}
			else
			{
				renderer = new ForwardRenderer(GraphicsManager.GraphicsDevice);
			}
#else
            renderer = new ForwardRenderer(GraphicsManager.GraphicsDevice);
#endif


            renderargs.Device = GraphicsManager.GraphicsDevice;
			renderargs.Context = GraphicsManager.GraphicsDevice.ImmediateContext;
			renderargs.Target = Target;

#if OCULUS
			renderargs.VRTarget = VRTarget;
			renderargs.VRMirror = VRMirror;
#endif

			ActiveScene.Initialize();
		}

		public override void Loop()
		{
			//Handle resize at start of frames
			if (Target.ResizePending) Target.Resize();

			//Update frame time
			TimeManager.Update();

			//refresh input
			InputManager.Update();

			//Update (with timer debug)
			TimeManager.updatewatch.Restart();
			ActiveScene.Update();
			TimeManager.updatewatch.Stop();
			TimeManager.updatetime = TimeManager.updatewatch.Elapsed.TotalSeconds;

			//Render (with timer debug)
			TimeManager.drawwatch.Restart();
			renderer.Render(ActiveScene, renderargs);
			TimeManager.drawwatch.Stop();
			TimeManager.drawtime = TimeManager.drawwatch.Elapsed.TotalSeconds;

		}

		public override void Dispose()
		{

			ActiveScene.Dispose();

			renderer.Dispose();
			Target.Dispose();

			PhysicsManager.Dispose();
			ContentManager.Dispose();
			GraphicsManager.Dispose();
		}
	}
}
