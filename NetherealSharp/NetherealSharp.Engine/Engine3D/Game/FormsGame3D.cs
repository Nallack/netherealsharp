﻿#if WINDOWS && FORMS
using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Core.Windows;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NetherealSharp.Engine
{


	/// <summary>
	/// Fuck!!, already use inheritence for demo creation...
	/// 
	/// Will need to decide who gets to inherit...
	/// Look at using bridge pattern with platformImplementor
	/// 
	/// </summary>
    public class FormsGame3D : Game3D
    {

		/// <summary>
		/// Creates a full size control within the panel passed in.
		/// </summary>
		/// <param name="panel"></param>
		/// <param name="graphics"></param>
		/// <param name="physics"></param>
		/// <param name="vrmode"></param>
		public FormsGame3D(
			Panel panel,
			GraphicsImplementation graphics = GraphicsImplementation.DirectX11,
			PhysicsImplementation3D physics = PhysicsImplementation3D.PhysX,
			bool vrmode = false)
		{

			Control control = RenderControl.CreateRenderControl(graphics);
			control.Dock = DockStyle.Fill;
			control.Name = "NetherealSharpControl";

			GraphicsManager.Init(graphics);
			PhysicsManager.Init(physics);
			ContentManager.Init();
			TimeManager.Init();
			AudioManager.Init();

			//Headset needs to refresh faster than the control handle rate of 60
			//int swapinterval = 1;
			//if (vrmode) swapinterval = 0;

			int swapinterval = vrmode ? 0 : 1;

			Target = ControlTarget.Create(GraphicsManager.GraphicsDevice, control, 1);



			panel.Controls.Add(control);

			//Always update and draw for paint
			control.Paint += (sender, e) => { this.Loop(); };
			//Redraw control when idle
			Application.Idle += (sender, e) => { control.Invalidate(); };


		}

	}
}
#endif