﻿using NetherealSharp.Core;
using NetherealSharp.Core.Physics;
using NetherealSharp.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NetherealSharp.Engine
{
	[Serializable, DataContract]
    public class Scene3D : IInitializable, IUpdatable, IDrawable3D, IShadowDrawable, IDisposable
	{
		private bool m_Initialized = false;
		protected ObservableCollection<Entity3D> m_Entities;

		public Game3D game { get; internal set; }

		[DataMember(Order = 0)]
		public string Name { get; set; }

		[DataMember(Order = 1)]
		public ObservableCollection<Entity3D> Entities
		{
			get { return m_Entities; }
			private set
			{
				m_Entities = value;

				//bind entities already in deserialized collection
				foreach (var e in m_Entities) BindEntity(e);

				//bind future entities
				m_Entities.CollectionChanged += (sender, e) =>
				{
					foreach (var obj in e.NewItems)
					{
						BindEntity(obj as Entity3D);
					}
				};
			}
		}

		
		public List<Camera3D> Cameras { get; protected set; }
		public List<ILight3D> Lights { get; protected set; }
		public PhysicsWorld3D PhysicsWorld { get; protected set; }

		public Scene3D()
		{
			Entities = new ObservableCollection<Entity3D>();
			Cameras = new List<Camera3D>();
			Lights = new List<ILight3D>();
			//PhysicsWorld = PhysicsWorld3D.Create();
		}

		[OnDeserializing]
		private void Deserializing(StreamingContext context)
		{
			Cameras = new List<Camera3D>();
			Lights = new List<ILight3D>();
		}

		//refactor to Start
		public virtual void Initialize()
		{
			foreach (var e in Entities) e.Initialize();
		}

		private void BindEntity(Entity3D entity)
		{
			entity.Scene = this;
			if (m_Initialized) entity.Initialize();
		}
		
		public void Update()
		{
			foreach (var e in Entities) e.Update();

			if(PhysicsWorld != null) PhysicsWorld.Update(TimeManager.DeltaTime);
		}


		public void BeginDraw(DrawArgs3D args)
		{
			foreach (var e in Entities) e.BeginDraw(args);
		}

		public void ShadowDraw(DrawArgs3D args)
		{
			foreach (var e in Entities) e.ShadowDraw(args);
		}

		public void Draw(DrawArgs3D args)
		{
			foreach (var e in Entities) e.Draw(args);
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				//Dispose hasn't been called by user or finalizer

				if (disposing)
				{
					//User has called dispose, free managed objects early!
				}
				//Free unmanaged stuff here!
				foreach(var e in Entities)
				{
					e.Dispose();
				}

				disposedValue = true;
			}
		}

		~Scene3D() {
		   Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			//No need for finalizer as managed resources have been disposed early!
			GC.SuppressFinalize(this);
		}
		#endregion

	}
}
