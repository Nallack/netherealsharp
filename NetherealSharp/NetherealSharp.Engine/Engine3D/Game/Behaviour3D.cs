﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace NetherealSharp.Engine
{
	[DataContract] //maybe IExtensibleDataObject for shared resources
    public abstract class Behaviour3D : IInitializable, IUpdatable
    {
		public Entity3D Entity { get; internal set; }

		public Behaviour3D()
		{
		}

		public virtual void Initialize()
		{
#if DEBUG
			if (Entity == null) throw new MissingMemberException();
#endif
		}

		public virtual void OnEnable() { }

		public virtual void OnDisable() { }

		public virtual void Update() { }

		//public virtual void Draw(DrawArgs3D args) { }
	}
}
