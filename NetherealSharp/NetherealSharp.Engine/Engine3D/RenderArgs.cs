﻿using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.VR;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public class RenderArgs
    {
		public GraphicsDevice Device;
		public GraphicsContext Context;

		public RenderTarget Target;
#if OCULUS
		public VRTarget VRTarget;
		public VRMirrorTexture VRMirror;
#endif
    }
}
