﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;

namespace NetherealSharp.Engine
{
	public class SphereCollider : Behaviour3D
	{
		RigidDynamic3D RigidDynamic;
		Collider Collider;

		public float Radius = 1;

		public SphereCollider()
		{
			//RigidDynamic = Entity.GetBehaviour<RigidDynamic3D>();
			//Collider = Collider.CreateBox(RigidDynamic.RigidDynamic, new Vector3(1, 1, 1));
		}

		public void Init()
		{
			RigidDynamic = Entity.GetBehaviour<RigidDynamic3D>();
			Collider = Collider.CreateSphere(RigidDynamic.RigidDynamic, Radius);
		}

	}
}
