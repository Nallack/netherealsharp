﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;
using NetherealSharp.Core;

namespace NetherealSharp.Engine
{
    public class BoxCollider : Behaviour3D
    {
		RigidDynamic3D RigidDynamic;
		Collider Collider;

		public Vector3 Size = Vector3.One;

		public BoxCollider()
		{
			//RigidDynamic = Entity.GetBehaviour<RigidDynamic3D>();
			//Collider = Collider.CreateBox(RigidDynamic.RigidDynamic, new Vector3(1, 1, 1));
		}

		public void Init()
		{
			RigidDynamic = Entity.GetBehaviour<RigidDynamic3D>();
			Collider = Collider.CreateBox(RigidDynamic.RigidDynamic, Size);
		}

    }
}
