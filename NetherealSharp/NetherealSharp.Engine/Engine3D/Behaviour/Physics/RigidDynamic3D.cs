﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;
using NetherealSharp.Core;

namespace NetherealSharp.Engine
{
    public class RigidDynamic3D : Behaviour3D
    {
		public RigidDynamic RigidDynamic;

		public bool IsKinematic { get { return RigidDynamic.IsKinematic; } set { RigidDynamic.IsKinematic = value; } }

		public RigidDynamic3D()
		{
			RigidDynamic = RigidDynamic.Create(PhysicsManager.Context3D, Entity.Transform.Matrix);
			RigidDynamic.AddToWorld(Entity.Scene.PhysicsWorld);
		}

		public void Init()
		{
			//Bullet requires all colliders to be attached before adding to scene
			RigidDynamic.AddToWorld(Entity.Scene.PhysicsWorld);
			//Entity.Scene.PhysicsWorld.AddRigidBody(RigidDynamic);
		}

		public override void Update()
		{
			Matrix m = RigidDynamic.Transform;
			Vector3 pos;
			Quaternion rot;
			Vector3 scale;
			m.Decompose(out scale, out rot, out pos);

			Entity.Transform.Position = pos;
			Entity.Transform.Rotation = rot;
			Entity.Transform.Scale = scale;
		}

	}
}
