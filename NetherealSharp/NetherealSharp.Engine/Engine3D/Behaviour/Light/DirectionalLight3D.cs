﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace NetherealSharp.Engine
{
	public class DirectionalLight3D : Behaviour3D, ILight3D, IDirectionalLight3D, IDrawable3D
	{
		public LightType LightType { get { return LightType.Directional; } }

		[XmlIgnore]
		public DirectionalLightData Directional { get; protected set; }
		[XmlIgnore]
		public Matrix[] ShadowTransforms { get; protected set; }

		[XmlIgnore]
		//will want to upgrade to a cascading shadowmap
		public SimpleShadowMap m_ShadowMap;

		[XmlIgnore]
		public RenderTexture2D[] ShadowMaps { get { return new RenderTexture2D[] { m_ShadowMap.RenderTexture }; } }

		public RenderTexture2DArray ShadowMap { get; }

		public DirectionalLight3D()
		{
			ShadowTransforms = new Matrix[1];
			m_ShadowMap = new SimpleShadowMap(GraphicsManager.GraphicsDevice);

		}

		public override void Initialize()
		{
			Entity.Scene.Lights.Add(this);
		}

		/// <summary>
		/// need args as we reuse the camera uniform
		/// </summary>
		/// <param name="args"></param>
		public void DrawShadows(DrawArgs3D args)
		{
			args.Context.SetRenderTarget(m_ShadowMap.RenderTexture);
			args.Context.ClearRenderTargetView(m_ShadowMap.RenderTexture, Color.Blue);
			args.Context.ClearDepthStencilView(m_ShadowMap.RenderTexture);
			args.Context.SetViewport(0, 0, m_ShadowMap.RenderTexture.Width, m_ShadowMap.RenderTexture.Height);
			//args.Device.ClearTarget(ShadowMap.Target, Color.Red);

			//TODO: atm, directional light is centered on origin
			args.Camera.Data.View = Matrix.LookAtRH(Vector3.Zero, Directional.Direction, Vector3.ForwardRH);

			//Maybe do foreach cascadingshadowmap here
			//{
				//TODO: These would be set by the cascading shadow maps settings
				args.Camera.Data.Project = Matrix.OrthoRH(20f, 20f, -20f, 20f);
				args.Camera.Data.ViewProj = args.Camera.Data.View * args.Camera.Data.Project;
				ShadowTransforms[0] = args.Camera.Data.ViewProj * MathExtension.ScreenToTexture;

				args.Camera.Update();
				args.Camera.Bind(args.Context, (int)UniformLocation.Camera);

				Entity.Scene.ShadowDraw(args);
			//}
		}

		public void BeginDraw(DrawArgs3D args)
		{

		}

		public void Draw(DrawArgs3D args)
		{

		}
	}
}
