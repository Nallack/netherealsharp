﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Light Behaviour that can work as all light types.
	/// (Will need to be passed as a draw arg for shadow draw for the light view proj)
	/// (assuming we still want WorldViewProj calculated on CPU)
	/// </summary>
	[DataContract]
	public class Light3D : Behaviour3D, ILight3D, IAmbientLight3D, IDirectionalLight3D, IPointLight3D, ISpotLight3D, IDisposable
	{
		[DataMember]
		public LightType LightType { get; set; }

		[DataMember]
		public Color Color;
		[DataMember]
		public float Intensity;
		[DataMember]
		public Vector3 PhiThetaGamma;

		[DataMember]
		public AmbientLightData Ambient { get; set; }


		//TODO: this needs to be determined by quality settings
		public int ShadowCascades { get; protected set; }

		[XmlIgnore]
		private DirectionalLightData m_Directional;
		[XmlIgnore]
		public DirectionalLightData Directional { get { return m_Directional; } }

		[XmlIgnore]
		private PointLightData m_PointLightData;
		[XmlIgnore]
		public PointLightData Point { get { return m_PointLightData; } }
		[XmlIgnore]
		public SpotLightData Spot { get; }


		//public SimpleShadowMap ShadowMap;
		//public CascadedShadowMap ShadowMap;
		public CascadedShadowMapArray m_ShadowMap;

		//TODO: problem here is there can only be one cascadedshadowmaparray, not one for each light source.
		//will also mean that every shadowmap must be the same resolution.
		//public CascadedShadowMapArray ShadowMap;


		//Texture2D Shadows
		//public RenderTexture2D[] ShadowMaps { get { return ShadowMap.RenderTextures; } }

		//Texture2DArray Shadows
		public RenderTexture2DArray ShadowMap { get { return m_ShadowMap.RenderTextureArray; } }




		/// <summary>
		/// Shadow view proj matrix used to convert world coordinates into shadow space coordinates
		/// </summary>
		[XmlIgnore]
		public Matrix[] ShadowTransforms { get; private set; }

		public override void Initialize()
		{
			base.Initialize();

			ShadowCascades = GraphicsManager.GraphicsSettings.ShadowCascades;
			int ShadowResolution = GraphicsManager.GraphicsSettings.ShadowResolution;

			//m_ShadowMap = new CascadedShadowMap(GraphicsManager.GraphicsDevice, ShadowCascades, ShadowResolution);
			m_ShadowMap = new CascadedShadowMapArray(GraphicsManager.GraphicsDevice, ShadowCascades, ShadowResolution);

			ShadowTransforms = new Matrix[ShadowCascades];

			Entity.Scene.Lights.Add(this);
		}

		public void BeginDraw(DrawArgs3D args)
		{
			switch (LightType)
			{
				case LightType.Directional:
					{
						//Update Directional Data
						m_Directional.Direction = Vector3.Transform(Vector3.ForwardRH, Entity.Transform.Rotation);
						m_Directional.Color = Color.ToVector4();
						m_Directional.Intensity = Intensity;
						break;
					}
				case LightType.Point:
					{
						m_PointLightData.Position = Entity.Transform.Position;
						m_PointLightData.Color = Color.ToVector4();
						m_PointLightData.Intensity = Intensity;

						break;
					}
			}
		}

		//Render the shadows that this light source casts
		public void DrawShadows(DrawArgs3D args)
		{
			switch (LightType)
			{
				case LightType.Directional:
					{
						//Update Directional Data
						m_Directional.Direction = Vector3.Transform(Vector3.ForwardRH, Entity.Transform.Rotation);
						m_Directional.Color = Color.ToVector4();
						m_Directional.Intensity = Intensity;

						Vector3 up = Vector3.Transform(Vector3.Up, Entity.Transform.Rotation);
						Vector3 to = args.Camera.Data.Eye + m_Directional.Direction;

						Matrix.LookAtRH(ref args.Camera.Data.Eye, ref to, ref up, out args.Camera.Data.View);

						if (GraphicsManager.GraphicsSettings.ShadowQuality != QualityLevel.None)
						{
							
							//#region BadWay
							//for (int i = 0; i < ShadowCascades; i++)
							//{
							//	//TODO: use variables from a LOD/Quality setting
							//	//Maybe the first shadow map should be the lowest quality
							//	//and matches the shadow distance

							//	////float dFactor = (float)Math.Pow((i + 1), 3);

							//	float dFactor = 3 * (float)Math.Pow(2, 2 * i);


							//	////------MULTIPLE RENDER TARGETS-----------//
							//	var shadowTarget = ShadowMap.RenderTextures[i];

							//	////---Set and clear target-----//
							//	args.Context.SetRenderTarget(shadowTarget);
							//	args.Context.ClearRenderTargetView(shadowTarget, Color.White);
							//	args.Context.ClearDepthStencilView(shadowTarget);
							//	args.Context.SetViewport(0, 0, shadowTarget.Width, shadowTarget.Height);

							//	////----Camera Uniform-----//

							//	////TODO: These would be set by the cascading shadow maps settings
							//	////Matrix.OrthoOffCenterRH(-10f, 10f, -10f, 10f, -20f, 20f, out args.Camera.Data.Project);
							//	////Matrix.OrthoRH(dFactor * 2f, dFactor * 2f, -20f * (i + 1), 20f * (i + 1), out args.Camera.Data.Project);
								
							//	//Matrix.OrthoRH(dFactor * 2f, dFactor * 2f, -40f, 40f, out args.Camera.Data.Project);

							//	//Matrix.Multiply(ref args.Camera.Data.View, ref args.Camera.Data.Project, out args.Camera.Data.ViewProj);
							//	//ShadowTransforms[i] = args.Camera.Data.ViewProj * MathExtension.ScreenToTexture;
							//	//args.Camera.Update();
							//	//args.Camera.Bind(args.Context, (int)UniformLocation.Camera);

							//	////---Draw----//
							//	////(object uniform applied in here, no need for light uniform)
							//	//Entity.Scene.ShadowDraw(args);
							//	shadowTarget.GenerateMipmaps(args.Context);
							//	args.Context.SetEmptyTarget();

							//}
							//#endregion

							//----- RENDER TARGET ARRAY -------//
							args.Context.SetRenderTarget(m_ShadowMap.RenderTextureArray);
							args.Context.ClearRenderTargetView(m_ShadowMap.RenderTextureArray, Color.White);
							args.Context.ClearDepthStencilView(m_ShadowMap.RenderTextureArray);
							args.Context.SetViewport(0, 0, m_ShadowMap.RenderTextureArray.Width, m_ShadowMap.RenderTextureArray.Height);

							for (int i = 0; i < ShadowCascades; i++)
							{
								float dFactor = 3 * (float)Math.Pow(2, 2 * i);

								//Matrix.OrthoRH(dFactor * 2f, dFactor * 2f, -40f, 40f, out args.Camera.Data.Project);
								//Matrix.Multiply(ref args.Camera.Data.View, ref args.Camera.Data.Project, out args.Camera.Data.ViewProj);
								Matrix project = Matrix.OrthoRH(dFactor * 2f, dFactor * 2f, -40f, 40f);
								Matrix view = args.Camera.Data.View;
								args.Shadows.Data.ViewProj[i] = view * project;


								//keep this value for sample lookup
								ShadowTransforms[i] = args.Shadows.Data.ViewProj[i] * MathExtension.ScreenToTexture;

								//args.Camera.Update();
								//args.Camera.Bind(args.Context, (int)UniformLocation.Camera);
								args.Shadows.Update();
								args.Shadows.Bind(args.Context, (int)UniformLocation.Camera);


							}

							//---Draw----//
							//(object uniform applied in here, no need for light uniform)
							Entity.Scene.ShadowDraw(args);
							m_ShadowMap.RenderTextureArray.GenerateMipmaps(args.Context);
							args.Context.SetEmptyTarget();
						}
						break;
					}
				case LightType.Point:
					{
						m_PointLightData.Position = Entity.Transform.Position;
						m_PointLightData.Color = Color.ToVector4();
						m_PointLightData.Intensity = Intensity;

						break;
					}
			}
		}

		public void Dispose()
		{
			m_ShadowMap.Dispose();
		}
	}
}
