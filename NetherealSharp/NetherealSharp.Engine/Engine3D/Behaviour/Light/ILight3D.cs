﻿using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public interface ILight3D
    {
		LightType LightType { get; }

		void BeginDraw(DrawArgs3D args);
		void DrawShadows(DrawArgs3D args);
    }

	interface IAmbientLight3D : ILight3D
	{
		AmbientLightData Ambient { get; }
	}

	interface IDirectionalLight3D : ILight3D
	{
		DirectionalLightData Directional { get; }
		Matrix[] ShadowTransforms { get; }

		//RenderTexture2D[] ShadowMaps { get; }
		RenderTexture2DArray ShadowMap { get; }

	}

	interface IPointLight3D : ILight3D
	{
		PointLightData Point { get; }
	}

	interface ISpotLight3D : ILight3D
	{
		SpotLightData Spot { get; }
	}
}
