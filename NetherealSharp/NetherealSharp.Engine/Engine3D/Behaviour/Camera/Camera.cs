﻿using NetherealSharp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public interface ICamera : IPreDraw, IPostDraw
    {
		Rectangle Viewport { get; }

		Color ClearColor { get; }

		float Near { get; }
		float Far { get; }

		Matrix ViewMatrix { get; }

		Matrix ProjectMatrix(float targetWidth, float targetHeight);

		Transform3D Transform { get; }

		List<IPreDraw> PreDrawables { get; }
		List<IPostDraw> PostDrawables { get; }
    }
}
