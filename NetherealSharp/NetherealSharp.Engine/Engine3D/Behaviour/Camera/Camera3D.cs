﻿using NetherealSharp.Core;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Engine
{
	[DataContract]
	public class Camera3D : Behaviour3D, ICamera, IPreDraw, IPostDraw
	{
		private float m_fov = 1f;

		[DataMember]
		public float Fov { get { return m_fov; } set { m_fov = value; } }
		[DataMember]
		public float Near { get; set; }
		[DataMember]
		public float Far { get; set; }

		[DataMember]
		public Color ClearColor { get; set; }

		[DataMember]
		public Rectangle Viewport { get; set; }

		public Transform3D Transform { get { return Entity.Transform; } }

		public List<IPreDraw> PreDrawables { get; private set; }
		public List<IPostDraw> PostDrawables { get; private set; }


		public Matrix ViewMatrix
		{
			get
			{
				Vector3 to = Entity.Transform.Position + Vector3.Transform(Vector3.ForwardRH, Entity.Transform.Rotation);
				Vector3 up = Vector3.Transform(Vector3.Up, Entity.Transform.Rotation);
				return Matrix.LookAtRH(Entity.Transform.Position, to, up);
			}
		}

		public Matrix ProjectMatrix(float targetWidth, float targetHeight)
		{
			return Matrix.PerspectiveFovRH(
				Fov,
				targetWidth / targetHeight,
				Near,
				Far);
		}

		public Camera3D()
		{
			Viewport = new Rectangle(0, 0, 1, 1);
			PreDrawables = new List<IPreDraw>();
			PostDrawables = new List<IPostDraw>();
	}

		public override void Initialize()
		{
			base.Initialize();
			Entity.Scene.Cameras.Add(this);
		}

		public void PreDraw(DrawArgs3D args)
		{
			foreach(IPreDraw predraw in PreDrawables)
			{
				predraw.PreDraw(args);
			}
		}

		public void PostDraw(DrawArgs3D args)
		{
			foreach (IPostDraw postdraw in PostDrawables)
			{
				postdraw.PostDraw(args);
			}
		}
	}
}
