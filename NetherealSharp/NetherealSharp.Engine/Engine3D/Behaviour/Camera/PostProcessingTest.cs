﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Example for setting up a post processing effect.
	/// Uses a simple copy pass stage by:
	///  setting default target to a new rendertexture
	///  drawing to the new rendertexture
	///  copying back to the original texture
	///  
	/// 
	/// (May prove to be more complicated if multiple output targets exist.)
	/// 
	/// </summary>
	public class PostProcessingTest : Behaviour3D, IPreDraw, IPostDraw, IDisposable
	{

		protected Camera3D Camera;

		protected RenderTarget defaultTarget;
		protected RenderTexture2D diffuseBuffer;

		protected Mesh Quad;
		protected EffectPass CopyPass;

		public override void Initialize()
		{
			base.Initialize();

			Camera = Entity.GetBehaviour<Camera3D>();
			Quad = ContentManager.Get<Mesh>("Quad");

			//Intermediate diffuse location instead of drawing straight to default target
			//Need to bind resize callback to the game window target!
			//(This will probably need to change for VR. Possibly an abstract render target for window or VR)
			diffuseBuffer = RenderTexture2D.Create(
				GraphicsManager.GraphicsDevice,
				new System.Drawing.Size(Entity.Scene.Game.Target.Width, Entity.Scene.Game.Target.Height),
				TextureFormat.RGBA_8_UNorm);

			Entity.Scene.Game.Target.Resized += (size) =>
			{
				diffuseBuffer.Dispose();
				diffuseBuffer = RenderTexture2D.Create(
					GraphicsManager.GraphicsDevice,
					new System.Drawing.Size(Entity.Scene.Game.Target.Width, Entity.Scene.Game.Target.Height),
					TextureFormat.RGBA_8_UNorm);
			};

			CopyPass = ContentManager.Get<EffectPass>("CopyPass");

			Camera.PreDrawables.Add(this);
			Camera.PostDrawables.Add(this);
		}

		public void PreDraw(DrawArgs3D args)
		{
			defaultTarget = args.OutputTarget; //TODO: check protection on args, may or may not want to allow modifying args directly.

			args.Context.SetRenderTarget(diffuseBuffer);
			args.Context.ClearRenderTargetView(diffuseBuffer, Camera.ClearColor);
			args.Context.ClearDepthStencilView(diffuseBuffer);
		}

		public void PostDraw(DrawArgs3D args)
		{

			args.Context.SetRenderTarget(args.OutputTarget);

			CopyPass.Bind(args.Context);
			CopyPass.BindTexture2D(args.Context, "DiffuseMap", diffuseBuffer);
			Quad.Draw(args.Context, PrimitiveMode.TriangleList);
			CopyPass.BindTexture2D(args.Context, "DiffuseMap", null);
			CopyPass.Unbind(args.Context);
		}

		public void Dispose()
		{

		}

	}
}
