﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Engine;
using NetherealSharp.Core;
using System.Runtime.Serialization;

namespace NetherealSharp.Engine3D
{
	[DataContract]
    public class Camera2D : Behaviour3D, ICamera
    {
		public float Zoom { get; set; }
		public float Near { get; set; }
		public float Far { get; set; }

		public Rectangle Viewport { get; set; }

		public Matrix ViewMatrix
		{
			get
			{
				Vector3 to = Entity.Transform.Position + Vector3.Transform(Vector3.ForwardRH, Entity.Transform.Rotation);
				Vector3 up = Vector3.Transform(Vector3.Up, Entity.Transform.Rotation);
				return Matrix.LookAtRH(Entity.Transform.Position, to, up);
			}
		}

		public Matrix ProjectMatrix(float targetWidth, float targetHeight)
		{
			return Matrix.OrthoRH(
				targetWidth / Zoom,
				targetHeight / Zoom,
				Near,
				Far);
		}

		public Color ClearColor { get; set; }

		public Transform3D Transform
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public Camera2D()
		{
			Viewport = new Rectangle(0, 0, 1, 1);
			PreDrawables = new List<IPreDraw>();
			PostDrawables = new List<IPostDraw>();
		}

		public override void Initialize()
		{
			base.Initialize();
			Entity.Scene.Cameras.Add(this);
		}


		public List<IPreDraw> PreDrawables { get; private set; }

		public List<IPostDraw> PostDrawables { get; private set; }



		public void PreDraw(DrawArgs3D args)
		{
			foreach (IPreDraw predraw in PreDrawables)
			{
				predraw.PreDraw(args);
			}
		}

		public void PostDraw(DrawArgs3D args)
		{
			foreach (IPostDraw postdraw in PostDrawables)
			{
				postdraw.PostDraw(args);
			}
		}


	}
}
