﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using NetherealSharp.Core;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace NetherealSharp.Engine
{
	[DataContract]
    public class MeshRenderer3D : Behaviour3D, IDrawable3D, IShadowDrawable, IDisposable
    {
		[DataMember]
		public bool CastsShadows;
		[DataMember]
		public bool ReceiveShadows; //add this as an objectuniform param

		public BoundingBox Box;
		public BoundingSphere BSphere;

		[DataMember]
		public BasicMaterial Material;

		[DataMember]
		public Mesh Mesh;

		[XmlIgnore]
		protected UniformBuffer<ObjectData> objectUniform;

		public MeshRenderer3D()
		{
			//Mesh = ContentManager.Get<IndexedMesh>("Cube");
			//Material = ContentManager.Get<Material>("TestMaterial") as BasicMaterial;
			
		}

		public override void Initialize()
		{
			objectUniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		public void BeginDraw(DrawArgs3D args)
		{
			BSphere = Mesh.BoundingSphere;
			BSphere.Center = Entity.Transform.Position;

			//var obb = Mesh.OBB;
			//obb.Transformation = Entity.Transform.Matrix;
			//Box = obb.GetBoundingBox();

			Material.Update();

			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Update();
		}

		public void ShadowDraw(DrawArgs3D args)
		{
			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;

			objectUniform.Update();
			objectUniform.Bind(args.Context, (int)UniformLocation.Object);

			Material.ShadowEffect.Bind(args.Context);
			Mesh.Draw(args.Context, PrimitiveMode.TriangleList);
			Material.ShadowEffect.Unbind(args.Context);

		}

		public void Draw(DrawArgs3D args)
		{
			//if(ContainmentType.Disjoint != args.CameraFrustum.Contains(ref BSphere))
			//{
			//objectUniform.Data.World = Entity.Transform.Matrix;
			//objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;
			//objectUniform.Update();

			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;
			objectUniform.Update();
			objectUniform.Bind(args.Context, (int)UniformLocation.Object);

			Material.Bind(args.Context);
			Mesh.Draw(args.Context, PrimitiveMode.TriangleList);
			Material.Unbind(args.Context);

			//}

		}

		public void Dispose()
		{
			objectUniform.Dispose();
		}
	}
}
