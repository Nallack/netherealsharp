﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using NetherealSharp.Core;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace NetherealSharp.Engine
{
	[DataContract]
    public class HeightmapTerrain : Behaviour3D, IDrawable3D, IShadowDrawable, IDisposable
    {
		public BasicMaterial Material;

		//[XmlIgnore]
		//public BasicEffect Effect;
		//[XmlIgnore]
		//public Effect shadowEffect;
		//[XmlIgnore]
		//public Texture2D Diffuse;


		[XmlIgnore]
		public IndexedMesh Mesh;


		UniformBuffer<ObjectData> objectUniform;

		[DataMember]
		public uint width = 10;
		[DataMember]
		public uint length = 10;

		public HeightmapTerrain()
		{
			
		}

		public override void Initialize()
		{
			base.Initialize();

			//Effect = ContentManager.Get<Effect>("Phong") as BasicEffect;
			//shadowEffect = ContentManager.Get<Effect>("Shadow");
			//Diffuse = ContentManager.Get<Texture2D>("TestTexture");

			objectUniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);

			var data = new IndexedMeshData<VertexPNT>(VertexPNT.Layout);

			width = 10;
			length = 10;

			for (int x = 0; x <= width; x++)
			{
				for (int z = 0; z <= length; z++)
				{
					data.Vertices.Add(new VertexPNT(new Vector3(x, 0, z), Vector3.Up, new Vector2(x / (float)width, z / (float)length)));
				}
			}


			uint vw = width + 1;
			uint vl = length + 1;

			for (uint z = 0; z < length; z++)
			{
				for (uint x = 0; x < width; x++)
				{
					data.Indices.Add(x + z * vw);
					data.Indices.Add(x + (z + 1) * vw);
					data.Indices.Add(x + 1 + z * vw);

					data.Indices.Add(x + 1 + z * vw);
					data.Indices.Add(x + (z + 1) * vw);
					data.Indices.Add(x + 1 + (z + 1) * vw);
				}
			}

			Mesh = IndexedMesh.CreateFromData(GraphicsManager.GraphicsDevice, data, BufferUsage.Static);
		}

		public void ShadowDraw(DrawArgs3D args)
		{
			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;
			objectUniform.Update();
			objectUniform.Bind(args.Context, (int)UniformLocation.Object);

			Material.ShadowEffect.Bind(args.Context);
			Mesh.Draw(args.Context, PrimitiveMode.TriangleList);
			Material.ShadowEffect.Unbind(args.Context);
		}

		public void BeginDraw(DrawArgs3D args)
		{
			//throw new NotImplementedException();

			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Update();

		}

		public void Draw(DrawArgs3D args)
		{
			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;
			objectUniform.Update();

			objectUniform.Bind(args.Context, (int)UniformLocation.Object);

			//args.Context.SetShaderResources((int)TextureLocation.ShadowMap, args.ShadowMaps);
			Material.Bind(args.Context);			
			Mesh.Draw(args.Context, PrimitiveMode.TriangleList);
			Material.Unbind(args.Context);
			//args.Context.EmptyShaderResources((int)TextureLocation.ShadowMap, args.ShadowMaps.Length);
		}

		public void Dispose()
		{
			Mesh.Dispose();
			objectUniform.Dispose();
		}
	}
}
