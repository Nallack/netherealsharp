﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine.Input
{
    /// <summary>
    /// Collection of keybindings for a player that can be saved and loaded.
    /// Devices are intercahangeable, but bindings are same.
    /// </summary>
    public class InputDeviceProfile
    {
        public int PlayerNumber { get; protected set; }
        public SortedDictionary<int, InputDeviceBinding> Bindings { get; protected set; }

        public InputDeviceProfile(int playerNumber, InputProfile profile)
        {
            PlayerNumber = playerNumber;
            Bindings = new SortedDictionary<int, InputDeviceBinding>();

            foreach(var pair in profile.Bindings)
            {

            }
        }
    }
}
