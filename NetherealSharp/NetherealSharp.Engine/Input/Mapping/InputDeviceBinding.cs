﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Input;

namespace NetherealSharp.Engine.Input
{
    /// <summary>
    /// Wraps a button or axis to an integer value.
    /// </summary>
    public class InputDeviceBinding
    {
        public string Name { get; set; }
        public bool IsAxis { get; set; }
        public int Keycode { get; set; }
        public InputDevice Device { get; set; }
    }
}
