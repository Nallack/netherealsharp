﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Input;

namespace NetherealSharp.Engine.Input
{
    public enum DefaultInputs
    {
        MoveX,
        MoveY,
        LookX,
        LookY,
        Run,
        Crouch,
        Jump
    }


    public static class DefaultInputProfile
    {
        public static InputProfile GetDefault()
        {
            return new InputProfile()
            {
                Bindings =
                {
                    { (int)DefaultInputs.MoveX, new InputBinding() },
                    { (int)DefaultInputs.MoveY, new InputBinding() },
                    { (int)DefaultInputs.LookX, new InputBinding() },
                    { (int)DefaultInputs.LookX, new InputBinding() },
                    { (int)DefaultInputs.Run, new InputBinding() },
                    { (int)DefaultInputs.Crouch, new InputBinding() },
                    { (int)DefaultInputs.Jump, new InputBinding() }
                }
            };
        }
    }
}
