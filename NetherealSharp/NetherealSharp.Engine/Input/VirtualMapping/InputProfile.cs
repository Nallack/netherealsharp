﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine.Input
{
    /// <summary>
    /// Profile/mapping of a player's settings,
    /// contains only bindings of action to key/axis, not device 
    /// </summary>
    public class InputProfile
    {

        public SortedDictionary<int, InputBinding> Bindings;

        public InputProfile()
        {

        }

    }
}
