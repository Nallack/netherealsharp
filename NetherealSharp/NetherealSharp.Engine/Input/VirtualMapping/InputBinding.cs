﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine.Input
{
    //bindings of action to key/axis.
    //is serializable and does not reference a device.
    public class InputBinding
    {
        public string Name { get; set; }
        public bool IsAxis { get; set; }
        public int Keycode { get; set; }
    }
}
