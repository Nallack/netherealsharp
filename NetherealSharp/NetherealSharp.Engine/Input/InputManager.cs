﻿

using NetherealSharp.Core.Input;

#if WINDOWS
using NetherealSharp.Core.Windows;
#endif

using System;
using System.Collections.Generic;
using System.Diagnostics;



namespace NetherealSharp.Engine.Input
{
    public enum InputDeviceType
    {
        Mouse,
        Keyboard,
        Controller,
        Touch,
        HMD
    }

    public static class InputManager
    {

        public static List<InputKeyboard> Keyboards { get; private set;}
        public static List<InputMouse> Mice { get; private set; }
        public static List<XInputController> Controllers { get; private set; }
        public static List<InputDeviceProfile> Profiles { get; private set; }

#if WINDOWS
		public static void Init(System.Windows.Forms.Control control)
        {
            Keyboards = new List<InputKeyboard>();
            Mice = new List<InputMouse>();
            Controllers = new List<XInputController>();
            Profiles = new List<InputDeviceProfile>();

            //TODO:
            //try moving this somewhere else
            Keyboards.Add(InputKeyboard.Create(InputImplementation.Forms, control));
            Mice.Add(InputMouse.Create(InputImplementation.Forms, control));
        }

        public static void SetProfile(int player, InputProfile profile)
        {

        }

        public static void Init(System.Windows.FrameworkElement panel)
        {

        }
#endif

        public static void Update()
        {
            foreach(var o in Keyboards) o.Update();
            foreach (var o in Mice) o.Update();
            foreach (var o in Controllers) o.Update();
        }


#region Legacy
        /*

        public enum InputMode
		{
			Native,
			XInput,
			RawInput
		};

		public enum InputPlatform
		{
			Forms,
			WPF
		};

		public static InputPlatform Platform;
		public static InputMode Mode = InputMode.Native;

		static ButtonState[] Mouse = new ButtonState[3];
		static ButtonState[] Keyboard = new ButtonState[512];


		public static Vector2 MousePos = new Vector2(0);
		public static Vector2 MouseDelta = new Vector2(0);


		public static event Action GotFocus;
		public static event Action LostFocus;

#if WPF
		static System.Windows.FrameworkElement Element;
#endif
#if FORMS
		static System.Windows.Forms.Form Form;
		static System.Windows.Forms.Control Control;
#endif

		private static bool m_LockCursor = false;
		private static bool m_HideCursor = false;

		public static Vector2 m_LockPosition;

		public static bool LockCursor
		{
			get { return m_LockCursor; }
			set
			{
				m_LockCursor = value;
#if FORMS
				if (value)
				{
					m_LockPosition = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);

					//System.Windows.Forms.Cursor.Position = new System.Drawing.Point(
					//	Form.Location.X + Form.Width / 2,
					//	Form.Location.Y + Form.Height / 2);
					//MousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
				}
#endif

			}
		}
		public static bool HideCursor { get { return m_HideCursor; } set { m_HideCursor = value; } }

		private static bool firstrun = true;

		//#if WPF && FORMS
		//		public static void Init(WindowsFormsHost host)
		//		{
		//			switch (Mode)
		//			{
		//				case InputMode.Native:
		//					host.KeyDown += (sender, e) =>
		//					{
		//						Keyboard[(int)e.Key] = KeyState.Down;
		//					};

		//					host.KeyUp += (sender, e) =>
		//					{
		//						Keyboard[(int)e.Key] = KeyState.Up;
		//					};
		//					break;
		//				default:
		//					throw new NotImplementedException();
		//			}
		//		}
		//#endif


#if FORMS

		[System.Runtime.InteropServices.DllImport("user32.dll")]
		private static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey);

		public static void Init(System.Windows.Forms.Control control)
		{
			Platform = InputPlatform.Forms;
			Control = control;
			Form = Control.FindForm();

			control.GotFocus += (sender, e) => { if(GotFocus != null) GotFocus(); };
			control.LostFocus += (sender, e) => { if (LostFocus != null) LostFocus(); };

			MousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);

			switch (Mode)
			{
				case InputMode.Native:
					control.KeyDown += (sender, e) =>
					{
						//Console.WriteLine(e.KeyCode);
						if (e.KeyCode == System.Windows.Forms.Keys.ShiftKey)
						{
							if(Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.LShiftKey))) Keyboard[(int)Key.LeftShift] = ButtonState.Down;
							if (Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.RShiftKey))) Keyboard[(int)Key.RightShift] = ButtonState.Down;
						}
						else Keyboard[(int)e.KeyCode] = ButtonState.Down;
					};

					control.KeyUp += (sender, e) =>
					{
						//Console.WriteLine(e.KeyCode);
						if (e.KeyCode == System.Windows.Forms.Keys.ShiftKey)
						{
							if (!Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.LShiftKey))) Keyboard[(int)Key.LeftShift] = ButtonState.Up;
							if (!Convert.ToBoolean(GetAsyncKeyState(System.Windows.Forms.Keys.RShiftKey))) Keyboard[(int)Key.RightShift] = ButtonState.Up;
						}
						else Keyboard[(int)e.KeyCode] = ButtonState.Up;
					};

					control.MouseDown += (sender, e) =>
					{
						switch(e.Button)
						{
							case System.Windows.Forms.MouseButtons.Left:
								Mouse[0] = ButtonState.Down;
								break;
							case System.Windows.Forms.MouseButtons.Right:
								Mouse[1] = ButtonState.Down;
								break;
							default:
								break;
						}
					};

					control.MouseUp += (sender, e) =>
					{
						switch (e.Button)
						{
							case System.Windows.Forms.MouseButtons.Left:
								Mouse[0] = ButtonState.Up;
								break;
							case System.Windows.Forms.MouseButtons.Right:
								Mouse[1] = ButtonState.Up;
								break;
							default:
								break;
						}
					};

					break;
#if DIRECTX11
				case InputMode.RawInput:
					break;
#endif
				default:
					throw new NotImplementedException();
			}

		}

#endif

#if WPF
		public static void Init(System.Windows.FrameworkElement element)
		{
			Platform = InputPlatform.WPF;
			Element = element;

			Element.GotFocus += (sender, e) => { if (GotFocus != null) GotFocus(); };
			Element.LostFocus += (sender, e) => { if (LostFocus != null) LostFocus(); };

			MousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);

			switch (Mode)
			{
				case InputMode.Native:


					Element.KeyDown += (sender, e) => {
						Keyboard[(int)e.Key + 21] = ButtonState.Down;
					};

					Element.KeyUp += (sender, e) =>
					{
						Keyboard[(int)e.Key + 21] = ButtonState.Up;
					};

					Element.MouseLeftButtonDown += (sender, e) =>
					{
						Element.Focus();
						Mouse[0] = ButtonState.Down;
					};

					Element.MouseLeftButtonUp += (sender, e) =>
					{
						Mouse[0] = ButtonState.Up;
					};

					Element.MouseRightButtonDown += (sender, e) =>
					{
						Element.Focus();
						Mouse[1] = ButtonState.Down;
					};

					Element.MouseRightButtonUp += (sender, e) =>
					{
						Mouse[1] = ButtonState.Up;
					};


					break;
#if DIRECTX11
				case InputMode.RawInput:
					break;
#endif
				default:
					throw new NotImplementedException();
			}

		}
#endif



		public static bool GetMouseButton(MouseButton button)
		{
			switch (button)
			{
				case MouseButton.Left:
					return Mouse[0] == ButtonState.Down;
				case MouseButton.Right:
					return Mouse[1] == ButtonState.Down;
				default:
					return false;
			}
		}

		public static bool GetButton(Key key)
		{
			return Keyboard[(int)key] == ButtonState.Down;
		}
		

		public static bool GetKeyDown(Key key)
		{
			//System.Windows.Forms.Keys.
			//var keyboard = OpenTK.Input.Keyboard.GetState();
			//Debug.WriteLine(keyboard.IsKeyDown(OpenTK.Input.Key.W));
			//return keyboard.IsKeyDown((OpenTK.Input.Key)key);
            return Keyboard[(int)key] == ButtonState.Down;
		}

		public static void Update()
		{

#if FORMS
			if(Platform == InputPlatform.Forms)
			{
				if(firstrun)
				{
					firstrun = false;

					if(LockCursor)
					{
						System.Windows.Forms.Cursor.Position = new System.Drawing.Point(
							Form.Location.X + Form.Width / 2,
							Form.Location.Y + Form.Height / 2);
					}

					MousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
				}


				if (HideCursor) System.Windows.Forms.Cursor.Hide();
				else System.Windows.Forms.Cursor.Show();

					if (!LockCursor)
					{
						var CurrMousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);
						MouseDelta = CurrMousePos - MousePos;
						MousePos = CurrMousePos;
					}
					else if (LockCursor)
					{
						var CurrMousePos = new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y);

						//Measure delta from center of window
						MouseDelta = CurrMousePos - m_LockPosition;
						MousePos = CurrMousePos;

						System.Windows.Forms.Cursor.Position = new System.Drawing.Point(
							(int)m_LockPosition.X,
							(int)m_LockPosition.Y);


					///
					//System.Windows.Forms.Cursor.Clip = new System.Drawing.Rectangle(
					//	f.Location + new System.Drawing.Size(f.Width / 2, f.Height / 2), 
					//	new System.Drawing.Size(1, 1));
					//

					//System.Windows.Forms.Cursor.Clip = new System.Drawing.Rectangle(f.Location, f.Size);
				}
			}
#endif
#if WPF
			if(Platform == InputPlatform.WPF)
			{

				if(firstrun)
				{
					firstrun = false;
					MousePos = new Vector2(System.Windows.Forms.Control.MousePosition.X, System.Windows.Forms.Control.MousePosition.Y);
				}

				var CurrMousePos = new Vector2(System.Windows.Forms.Control.MousePosition.X, System.Windows.Forms.Control.MousePosition.Y);
				MouseDelta = CurrMousePos - MousePos;
				MousePos = CurrMousePos;

			}
#endif
		}
        */
#endregion
    }
}
