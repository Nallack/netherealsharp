﻿#if WPF
using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Core.Windows;
using NetherealSharp.Engine.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    public class WPFGameImplementor : GameImplementor
    {
		Game3D m_Game;

		public WPFGameImplementor(
			Game3D game,
			RenderPanel panel,
			GraphicsImplementation graphics,
			PhysicsImplementation3D physics3d)
		{
			m_Game = game;

			//target needs to create resources using this
			GraphicsManager.Init(graphics);

			WindowTarget = PanelTarget.Create(GraphicsManager.GraphicsDevice, panel);

			//PhysicsManager.Init();
			ContentManager.Init();
			TimeManager.Init();
			AudioManager.Init();
			InputManager.Init(panel);

			panel.Loaded += (sender, e) =>
			{
				System.Windows.Media.CompositionTarget.Rendering += (sender1, e1) =>
				{
					m_Game.Loop();
				};
			};
		}


		public override void Initialize()
		{
			throw new NotImplementedException();
		}

		public override void StartRenderLoop()
		{
			throw new NotImplementedException();
		}

		//Holy crap did I spend a long time getting WPF to work.
		#region Legacy Initializers


		//Broken, cannot use image as visualchild
		//Probably broken, suggest using renderpanel which abstracts some of this
		public WPFGameImplementor(Game3D game, Core.Windows.DX11.RenderElement element)
		{

			int swapinterval = 1;
			//if (vrMode) swapinterval = 0;

			GraphicsManager.Init(GraphicsImplementation.DirectX11);

			element.Loaded += (sender, e) =>
			{
				element.SurfaceChanged += (surface) =>
				{
					WindowTarget = D3DImageTarget.Create(
						GraphicsManager.GraphicsDevice, 
						element.D3D11Image, 
						surface, 
						swapinterval);
				};

				element.RequestRender();

				System.Windows.Media.CompositionTarget.Rendering += (sender1, e1) =>
				{
					game.Loop();
					element.RequestRender();
				};
			};

			//PhysicsManager.Init();
			ContentManager.Init();
			TimeManager.Init();
			AudioManager.Init();
			InputManager.Init(element);
		}

		
		// Tried using any panel to host interop D3DImage
		public WPFGameImplementor(Game3D game, System.Windows.Controls.Panel panel)
		{
			GraphicsManager.Init(GraphicsImplementation.DirectX11);
			var d3d11image = new Microsoft.Wpf.Interop.DirectX.D3D11Image();

			//PhysicsManager.Init();
			ContentManager.Init();
			TimeManager.Init();
			AudioManager.Init();
			InputManager.Init(panel);


			panel.Children.Add(new System.Windows.Controls.Image()
			{
				Source = d3d11image
			});

			panel.SizeChanged += (sender, e) =>
			{
				d3d11image.SetPixelSize((int)panel.ActualWidth, (int)panel.ActualHeight);
			};

			panel.Loaded += (sender, e) =>
			{
				d3d11image.WindowOwner = (new System.Windows.Interop.WindowInteropHelper(
					System.Windows.Window.GetWindow(panel))).Handle;

				d3d11image.OnRender += (surface, newsurface) =>
				{
					if (newsurface)
					{
						WindowTarget = D3DImageTarget.Create(GraphicsManager.GraphicsDevice, d3d11image, surface, 1);
						//Context.SetRenderTarget(Target);

						System.Windows.Media.CompositionTarget.Rendering += (sender1, e1) =>
						{
							game.Loop();
							d3d11image.RequestRender();
						};
					}
				};

				d3d11image.RequestRender();
			};

		}
		

		//Think this might need a WPF element as well to captuire input.
		public WPFGameImplementor(Game3D game, Microsoft.Wpf.Interop.DirectX.D3D11Image image, IntPtr surface)
		{
			GraphicsManager.Init(GraphicsImplementation.DirectX11);
			//PhysicsManager.Init(physics3D);
			ContentManager.Init();
			TimeManager.Init();

			var vrMode = false;

			int swapinterval = 1;
			if (vrMode) swapinterval = 0;

			var Graphics = GraphicsManager.GraphicsDevice;

			WindowTarget = D3DImageTarget.Create(Graphics, image, surface, swapinterval);
			//Graphics.SetRenderTarget(Target);

#if OCULUS
			if (Oculusmode)
			{
				VRManager.Init();
				VRTarget = new Core.VR.Oculus.VRTarget(VRManager.VRDevice as Core.VR.Oculus.VRDevice);
				VRMirror = VRMirrorTexture.Create(VRManager.VRDevice, Target);
			}
#endif

			//InputManager.Init(image);
			//TimeManager.Init();

		}


		#endregion
	}
}
#endif