﻿#if ANDROID
using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Platform.Android;
using NetherealSharp.Core.Graphics;
using OpenTK;
using NetherealSharp.Core;
using NetherealSharp.Engine.Input;
using Android.App;
using NetherealSharp.Core.Platform;

namespace NetherealSharp.Engine
{
	public class AndroidGameImplementor : GameImplementor
	{

		AndroidGameView m_view;
		Activity m_activity;

		public AndroidGameImplementor(
			Game game,
			AndroidGameView view,
			Activity activity)
		{
			Game = game;
			m_view = view;
			m_activity = activity;
		}

		public override void Initialize()
		{
			AndroidManager.Init(m_activity);

			GraphicsManager.Init(GraphicsImplementation.OpenGLES);
			ContentManager.Init();
			ContentManager.AssetManager = m_activity.Assets;

			TimeManager.Init();
			//InputManager.Init();

			//need to override view.createFramebuffer to choose graphicsMode

		}

		public override void StartRenderLoop()
		{
			m_view.RenderFrame += HandleRenderFrame;
		}

		private void HandleRenderFrame(object sender, FrameEventArgs e)
		{
			Game.Loop();
		}
	}
}
#endif