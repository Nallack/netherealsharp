﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;

namespace NetherealSharp.Engine
{
    public class GameDescriptor
    {
		public GraphicsImplementation Graphics;
		public PhysicsImplementation3D Physics3D;
		public PhysicsImplementation2D Physics2D;
		public RenderMode RenderMode;
		public bool VREnabled;
	}
}
