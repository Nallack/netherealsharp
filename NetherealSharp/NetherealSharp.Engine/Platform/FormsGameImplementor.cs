﻿#if WINDOWS && FORMS
using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Core.Windows;
using NetherealSharp.Engine.Input;
#if OCULUS
using NetherealSharp.Core.VR;
#endif

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Implementor class which controls major startup operations of
	/// a game depending on the platform.
	/// </summary>
	public class FormsGameImplementor : GameImplementor
	{

		Control m_Control;
		Panel m_Panel;
		GraphicsImplementation m_Graphics;
		PhysicsImplementation3D m_Physics3D;
		bool m_EnableVR;
		
		public FormsGameImplementor(Panel panel, GameDescriptor descriptor)
		{
			m_Panel = panel;
			m_Graphics = descriptor.Graphics;
			m_Physics3D = descriptor.Physics3D;
			m_EnableVR = descriptor.VREnabled;
			RenderMode = descriptor.RenderMode;
		}

		public override void Initialize()
		{
			if (Game == null) throw new Exception("Game not not assigned to implementor");
			m_Control = RenderControl.CreateRenderControl(m_Graphics);
			m_Control.Dock = System.Windows.Forms.DockStyle.Fill;
			m_Control.Name = "NetherealSharpControl";
			m_Panel.Controls.Add(m_Control);

			GraphicsManager.Init(m_Graphics);
			//PhysicsManager.Init2D
			PhysicsManager.Init(m_Physics3D);
			ContentManager.Init();
			TimeManager.Init();
			AudioManager.Init();
			InputManager.Init(m_Control);

			WindowTarget = ControlTarget.Create(GraphicsManager.GraphicsDevice, m_Control, 1);

			//Always update and draw for paint
			m_Control.Paint += (sender, e) => { Game.Loop(); };
			//Redraw control when idle
			Application.Idle += (sender, e) => { m_Control.Invalidate(); };

#if OCULUS
			if (m_EnableVR)
			{
				VRManager.Initialize();

				VRManager.Enabled = true;

				VRTarget = new Core.VR.Oculus.VRTarget(
					VRManager.VRDevice as Core.VR.Oculus.VRDevice, 
					GraphicsManager.GraphicsDevice);

				VRMirror = Core.VR.VRMirrorTexture.Create(
					VRManager.VRDevice, 
					GraphicsManager.GraphicsDevice, 
					WindowTarget);
			}
#endif


		}

		public override void StartRenderLoop()
		{
			//Always update and draw for paint
			m_Control.Paint += (sender, e) => { Game.Loop(); };

			//Redraw control when idle
			Application.Idle += (sender, e) => { m_Control.Invalidate(); };
		}

	}
}
#endif