﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Bridge implementor for game initialization
	/// code and handle I/O to windowing libraries.
	/// </summary>
    public abstract class GameImplementor
    {
		public Game Game { get; set; }

		public Core.Graphics.RenderTarget WindowTarget { get; set; }

#if OCULUS
		public Core.VR.VRTarget VRTarget { get; protected set; }
		public Core.VR.VRMirrorTexture VRMirror { get; protected set; }
#endif

		public Engine.RenderMode RenderMode { get; protected set; }

		public abstract void Initialize();

		public abstract void StartRenderLoop();
    }


}
