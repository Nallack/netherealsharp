﻿using NetherealSharp.Core.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace NetherealSharp.Engine.Serialization
{
	/// <summary>
	/// Serialize using DataContract Serializer
	/// </summary>
	public class XmlContentSerializer : ContentSerializer
	{

		public override void Serialize<T>(string filename, T data)
		{
			using (var writer = new StreamWriter(filename + ".xml"))
			{
				Serialize<T>(writer.BaseStream, data);
			}
		}

		public override void Serialize<T>(Stream stream, T data)
		{
			var namespaces = new Dictionary<string, string>()
			{
				{ "core", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core" },
				{ "engine", "http://schemas.datacontract.org/2004/07/NetherealSharp.Engine" },
				{ "graphics", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core.Graphics" },
				{ "serialization", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core.Serialization" },
				{ "demo", "http://schemas.datacontract.org/2004/07/NetherealSharp.Demo" }
			};

			var resolver = new CustomDataContractResolver(null);
			var surrogate = new CustomDataContractSurrogate();

			DataContractSerializer serializer = new DataContractSerializer(typeof(T), new DataContractSerializerSettings()
			{
				//KnownTypes = types,
				//PreserveObjectReferences = true,
				//IgnoreExtensionDataObject = true,
				DataContractResolver = resolver,
				DataContractSurrogate = surrogate

			});

			using (var writer = XmlWriter.Create(stream, new XmlWriterSettings() { Indent = true }))
			using (var dictwriter = XmlDictionaryWriter.CreateDictionaryWriter(writer))
			{
				serializer.WriteObject(dictwriter, resolver, data, namespaces);
			}
		}

		public override T Deserialize<T>(Stream stream)
		{
			throw new NotImplementedException();
		}

		public override T Deserialize<T>(string filename)
		{
			throw new NotImplementedException();
		}
	}
}
