﻿using NetherealSharp.Core.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;

namespace NetherealSharp.Engine.Serialization
{

	/// <summary>
	/// For now this serializer supports all formats via confugration in settings
	/// Will want to abstract this out to an implementor or with inheritence
	/// </summary>
	public class AllContentSerializer : ContentSerializer
	{

		public override void Serialize<T>(string filename, T data)
		{
			switch (Settings.Mode)
			{
			case SerializationMode.DataContractXML:
				using (var writer = new StreamWriter(filename + ".xml"))
				{
					Serialize<T>(writer.BaseStream, data);
				}
				break;
			case SerializationMode.DataContractJSON:
				using (var writer = new StreamWriter(filename + ".json"))
				{
					Serialize<T>(writer.BaseStream, data);
				}
				break;
			default:
				throw new NotImplementedException();
			}


		}

		public override void Serialize<T>(Stream stream, T data)
		{



			var namespaces = new Dictionary<string, string>()
			{
				{ "core", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core" },
				{ "engine", "http://schemas.datacontract.org/2004/07/NetherealSharp.Engine" },
				{ "graphics", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core.Graphics" },
				{ "serialization", "http://schemas.datacontract.org/2004/07/NetherealSharp.Core.Serialization" },
				{ "demo", "http://schemas.datacontract.org/2004/07/NetherealSharp.Demo" }
			};

			var resolver = new CustomDataContractResolver(null);
			var surrogate = new CustomDataContractSurrogate();


			switch (Settings.Mode)
			{
			case SerializationMode.XML:
				{
					//XmlAttributeOverrides aor = new XmlAttributeOverrides();
					//XmlAttributes attributes = new XmlAttributes();
					//attributes.XmlElements.Add(new XmlElementAttribute(typeof(MeshRenderer3D)));
					//aor.Add(typeof(Scene3D), "Behaviours", attributes);

					//XmlSerializer serializer = new XmlSerializer(typeof(Scene3D), behaviours);
					//serializer.Serialize(stream, this);
					break;
				}
			case SerializationMode.DataContractXML:
				{
					//var dict = new XmlDictionary();
					//dict.Add("http://schemas.datacontract.org/2004/07/NetherealSharp.Engine");
					//dict.Add("http://schemas.datacontract.org/2004/07/NetherealSharp.Core");

					DataContractSerializer serializer = new DataContractSerializer(typeof(T), new DataContractSerializerSettings()
					{
						//KnownTypes = types,
						//PreserveObjectReferences = true,
						//IgnoreExtensionDataObject = true,
						DataContractResolver = resolver,
						DataContractSurrogate = surrogate

					});

					using (var writer = XmlWriter.Create(stream, new XmlWriterSettings() { Indent = true }))
					using (var dictwriter = XmlDictionaryWriter.CreateDictionaryWriter(writer))
					{
						serializer.WriteObject(dictwriter, resolver, data, namespaces);
					}
					break;
				}
			case SerializationMode.DataContractJSON:
				{

					//TODO: will want to make this modular
					var behaviours = (from lAssembly in AppDomain.CurrentDomain.GetAssemblies()
									  from lType in lAssembly.GetTypes()
									  where typeof(Engine.Behaviour3D).IsAssignableFrom(lType)
									  select lType).ToArray();

					////do somnething like check for serializable
					//var graphics = (from lAssembly in AppDomain.CurrentDomain.GetAssemblies()
					//				from lType in lAssembly.GetTypes()
					//				where lType.Namespace == "NetherealSharp.Core.Graphics.DX11"
					//				select lType).ToArray();

					var types = new List<Type>();
					types.AddRange(behaviours);
					//types.Add(typeof(NetherealSharp.Core.Graphics.DX11.Texture2D));
					//types.Add(typeof(NetherealSharp.Core.Graphics.BasicEffect));

					DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), new DataContractJsonSerializerSettings()
					{
						KnownTypes = types,
						DataContractSurrogate = surrogate
					});


					using (var writer = JsonReaderWriterFactory.CreateJsonWriter(stream, Encoding.UTF8, false, true))
					{
						serializer.WriteObject(writer, data);
					}
					break;
				}
			default:
				throw new NotImplementedException();
			}
		}

		public override T Deserialize<T>(string filename)
		{
			using (var reader = new StreamReader(filename + ".xml"))
			{
				return Deserialize<T>(reader.BaseStream);
			}
		}

		public override T Deserialize<T>(Stream stream)
		{


			var behaviours = (from lAssembly in AppDomain.CurrentDomain.GetAssemblies()
							  from lType in lAssembly.GetTypes()
							  //where typeof(Engine.Behaviour3D).IsAssignableFrom(lType)
							  select lType).ToArray();

			var graphics = new List<Type>()
			{
				typeof(Core.Graphics.EffectPass),
				typeof(Core.Graphics.BasicEffectPass)
			};

			var knowntypes = new List<Type>();
			knowntypes.AddRange(behaviours);
			knowntypes.AddRange(graphics);


			var resolver = new CustomDataContractResolver(null);
			var surrogate = new CustomDataContractSurrogate();

			switch (Settings.Mode)
			{
			case SerializationMode.XML:
				{
					//Scene3D scene = null;
					//XmlSerializer serializer = new XmlSerializer(typeof(Scene3D));

					//StreamReader reader = new StreamReader(stream);
					//scene = (Scene3D)serializer.Deserialize(reader);
					//reader.Close();
					//return scene;
					return null;
				}
			case SerializationMode.DataContractXML:
				{
					object result = null;
					DataContractSerializer serializer = new DataContractSerializer(typeof(T), new DataContractSerializerSettings()
					{
						KnownTypes = knowntypes,
						DataContractResolver = resolver,
						DataContractSurrogate = surrogate
					});

					using (var reader = XmlReader.Create(stream))
					using (var dictreader = XmlDictionaryReader.CreateDictionaryReader(reader))
					{
						result = serializer.ReadObject(dictreader, false, resolver);
					}

					return result as T;
				}
			case SerializationMode.DataContractJSON:
				{
					object result = null;
					DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), new DataContractJsonSerializerSettings()
					{
						//KnownTypes = types,
						UseSimpleDictionaryFormat = true
					});

					using (var reader = JsonReaderWriterFactory.CreateJsonReader(stream, new XmlDictionaryReaderQuotas() { }))
					{
						result = serializer.ReadObject(reader);
					}
					return result as T;
				}
			default:
				throw new NotImplementedException();
			}
		}
	}

}
