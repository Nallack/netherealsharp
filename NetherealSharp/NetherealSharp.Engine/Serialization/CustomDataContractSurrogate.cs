﻿using NetherealSharp.Core;
using NetherealSharp.Core.Serialization;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Engine.Serialization
{
	public class CustomDataContractSurrogate : IDataContractSurrogate
	{
		public object GetCustomDataToExport(Type clrType, Type dataContractType)
		{
			throw new NotImplementedException();
		}

		public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
		{
			throw new NotImplementedException();
		}

		public Type GetDataContractType(Type type)
		{
			//if Content, serialize as content surrogate
			if (typeof(IContentable).IsAssignableFrom(type)) return typeof(ContentSurrogate);
			else return type;
		}

		public object GetDeserializedObject(object obj, Type targetType)
		{
			if(obj is ContentSurrogate)
			{
				var surrogate = obj as ContentSurrogate;

                //TODO: Shouldn't reference engine from core namespace, move this class to Engine
				return NetherealSharp.Engine.ContentManager.Get(targetType.GetCustomAttribute<ContentAttribute>().RegistryType, surrogate.Name);
			}

			return obj;
		}

		public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
		{
			throw new NotImplementedException();
		}

		//Convert obj into target type
		public object GetObjectToSerialize(object obj, Type targetType)
		{
			if(targetType == typeof(ContentSurrogate))
			{
				var content = obj as IContentable;
				return new ContentSurrogate(content.Name, content.ContentType);
			}
			return obj;
		}

		public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
		{
			throw new NotImplementedException();
		}

#if !ANDROID
		public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
		{
			throw new NotImplementedException();
		}
#endif
	}
}
