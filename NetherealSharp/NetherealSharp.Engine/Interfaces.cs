﻿using NetherealSharp.Core;
using NetherealSharp.Engine2D;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{

	public interface IInitializable
	{
		void Initialize();
	}

	public interface IUpdatable
	{
		void Update();
	}

	public interface IShadowDrawable
	{
		void ShadowDraw(DrawArgs3D args);
	}

	public interface IReflectionDrawable
	{
		void ReflectionDraw(DrawArgs3D args);
	}

	public interface IBeginDrawable
	{
		void BeginDraw(DrawArgs3D args);
	}

	public interface IDrawable2D
	{
		void BeginDraw(DrawArgs2D args);
		void Draw(DrawArgs2D args);
	}

    public interface IDrawable3D
    {
		//Drawing stuff that only needs to be performed once per frame
		void BeginDraw(DrawArgs3D args);

		//Drawing that needs to be performed per view
		void Draw(DrawArgs3D args);
    }

	public interface IPreDraw
	{
		void PreDraw(DrawArgs3D args);
	}

	public interface IPostDraw
	{
		void PostDraw(DrawArgs3D args);
	}
}
