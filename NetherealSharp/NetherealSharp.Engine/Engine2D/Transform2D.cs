﻿using NetherealSharp.Core;
using System;
using System.Collections.Generic;
using System.Text;


namespace NetherealSharp.Engine2D
{
    public class Transform2D
    {
		public Vector3 Position;
		public float Rotation;
		public Vector2 Scale;

		public Matrix Matrix
		{
			get
			{
				return Matrix.Scaling(Scale.X, Scale.Y, 1) *
					   Matrix.RotationZ(Rotation) *
					   Matrix.Translation(Position);
				//return Matrix4.CreateScale(Scale.X, Scale.Y, 1) *
				//	   Matrix4.CreateRotationZ(Rotation) *
				//	   Matrix4.CreateTranslation(Position);
			}
		}

		public Transform2D() : this(Vector3.Zero) { }

		public Transform2D(Vector3 position) : this(position, 0, Vector2.One) { }

		public Transform2D(Vector3 position, float rotation, Vector2 scale)
		{
			Position = position;
			Rotation = rotation;
			Scale = scale;
		}
    }
}
