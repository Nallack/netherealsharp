﻿


using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;


namespace NetherealSharp.Engine2D
{

	//Current set for Windows Forms based games
	public abstract class Game2D : Game
	{
		protected RenderTarget Target;

		protected Scene2D ActiveScene;

		protected SceneRenderer2D renderer;

		protected UniformBuffer<CameraData> cameraUniform;

		protected RenderArgs renderargs;

		public Game2D() { }

		public override void Initialize()
		{
			Implementor.Initialize();
			PhysicsManager.Init2D(Core.Physics.PhysicsImplementation2D.Box2D);

			Target = Implementor.WindowTarget;

			LoadContent();

			cameraUniform = UniformBuffer<CameraData>.Create(GraphicsManager.GraphicsDevice);

			renderer = new ForwardRenderer(GraphicsManager.GraphicsDevice);

			renderargs = new RenderArgs();
			renderargs.Device = GraphicsManager.GraphicsDevice;
			renderargs.Context = GraphicsManager.GraphicsDevice.ImmediateContext;
			renderargs.Target = Target;

			ActiveScene.Initialize();
		}

		public virtual void LoadContent() { }

		public override void Loop()
		{
			Target.CheckForResize();

			TimeManager.Update();

			TimeManager.updatewatch.Restart();
			ActiveScene.Update();
			TimeManager.updatewatch.Stop();
			TimeManager.updatetime = TimeManager.updatewatch.Elapsed.TotalSeconds;


			//Draw();
			renderer.Render(ActiveScene, renderargs);
		}

		public virtual void Update()
		{

		}

		public virtual void Draw()
		{
			TimeManager.drawwatch.Restart();

			var Graphics = GraphicsManager.GraphicsDevice;
			var Context = Graphics.ImmediateContext;


			Context.SetRenderTarget(Target);
			Context.ClearTarget(Target, Color.SkyBlue);

			//could optionally move this to a renderer class

			foreach (var camera in ActiveScene.Cameras)
			{

				Context.SetViewport(
					camera.Viewport.X * Target.Width,
					camera.Viewport.Y * Target.Height,
					(camera.Viewport.X + camera.Viewport.Width) * Target.Width,
					(camera.Viewport.Y + camera.Viewport.Height) * Target.Height);

				cameraUniform.Data.Screen = Matrix.OrthoRH(
				Target.Width,
				Target.Height,
				camera.Near,
				camera.Far);

				cameraUniform.Data.View = camera.ViewMatrix;

				cameraUniform.Data.Project = Matrix.OrthoRH(
					Target.Width / camera.Zoom,
					Target.Height / camera.Zoom,
					camera.Near,
					camera.Far);

				cameraUniform.Data.ViewProj = cameraUniform.Data.View * cameraUniform.Data.Project;


				cameraUniform.Update();
				cameraUniform.Bind(Graphics.ImmediateContext, (int)UniformLocation.Camera);

				DrawArgs2D args = new DrawArgs2D();
				args.Context = Context;

				ActiveScene.Draw(args);
			}

		}

		public override void Dispose()
		{
			cameraUniform.Dispose();
			ActiveScene.Dispose();

			Target.Dispose();

			PhysicsManager.Dispose();
			GraphicsManager.Dispose();

		}
	}
}