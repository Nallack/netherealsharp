﻿
#if FARSEER

using System;
using System.Collections.Generic;
using System.Text;

using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using XNA = Microsoft.Xna.Framework;
using System.Diagnostics;

namespace NetherealSharp.Core.Scene2D
{
	public class RigidBody2D : Behaviour2D
	{
		Body Body;

		public bool IsStatic { get { return Body.IsStatic; } set { Body.IsStatic = value; } }

		public RigidBody2D(Entity2D entity) : base(entity)
		{
			Body = BodyFactory.CreateRectangle(
				entity.Scene.Physics.World, 
				1, 
				1, 
				1, 
				new XNA.Vector2(entity.Transform.Position.X, Entity.Transform.Position.Y),
				BodyType.Dynamic);

			Body.IsStatic = false;
			//Body.Restitution = 0.3f;
			//Body.Friction = 0.5f;

		}

		public override void Update()
		{
			Entity.Transform.Position.X = Body.Position.X;
			Debug.WriteLine(Body.Position.Y);

			Entity.Transform.Position.Y = Body.Position.Y;

			Entity.Transform.Rotation = Body.Rotation;

			base.Update();
		}

		public override void Draw(DrawArgs2D args)
		{

			base.Draw(args);
		}
	}
}
#endif