﻿using NetherealSharp.Core;
using System;
using System.Collections.Generic;

using System.Text;

namespace NetherealSharp.Engine2D
{
    public class Camera2D : Behaviour2D
    {
		/// <summary>
		/// The scale of world units to pixels
		/// </summary>
		public float Zoom { get; set; }
		/// <summary>
		/// The near Z plane
		/// </summary>
		public float Near { get; set; }
		/// <summary>
		/// The far z plane
		/// </summary>
		public float Far { get; set; }
		/// <summary>
		/// The viewport of the render target to draw to.
		/// </summary>
		public Rectangle Viewport { get; set; }

		public Matrix ViewMatrix
		{
			get
			{
				Vector3 to = new Vector3(0, 0, -1) + Entity.Transform.Position;
				Vector3 up = new Vector3((float)Math.Sin(Entity.Transform.Rotation), (float)Math.Cos(Entity.Transform.Rotation), 0);
				return Matrix.LookAtRH(Entity.Transform.Position, to, up);
				//return Matrix4.LookAt(Entity.Transform.Position, to, up);
			}
		}

		public Camera2D()
		{
			Viewport = new Rectangle(0, 0, 1, 1);
		}

		public override void Initialize()
		{
			base.Initialize();
			Entity.Scene.Cameras.Add(this);
		}
	}
}
