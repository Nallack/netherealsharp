﻿
using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine2D;

namespace NetherealSharp.Engine
{
	public interface ILight2D
	{
		LightType LightType { get; }

		void BeginDraw(DrawArgs2D args);
		void DrawShadows(DrawArgs2D args);
	}
}
