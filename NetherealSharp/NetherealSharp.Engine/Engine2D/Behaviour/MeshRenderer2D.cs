﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;
using NetherealSharp.Core.Graphics;
using System.Diagnostics;
using NetherealSharp.Engine;

namespace NetherealSharp.Engine2D
{
    public class MeshRenderer2D : Behaviour2D, IDrawable2D
    {
		public BasicEffectPass Effect { get; set; }
		public Mesh Mesh { get; set; }
		public Texture2D Texture { get; set; }

		UniformBuffer<ObjectData> objectUniform;

		public MeshRenderer2D()
		{
			Effect = ContentManager.Get<EffectPass>("Sprite") as BasicEffectPass;
			Texture = ContentManager.Get<Texture2D>("TestTexture");
			Mesh = ContentManager.Get<IndexedMesh>("Quad");

			objectUniform = UniformBuffer<ObjectData>.Create(GraphicsManager.GraphicsDevice);
		}

		public void BeginDraw(DrawArgs2D args)
		{
			//throw new NotImplementedException();
		}

		public void Draw(DrawArgs2D args)
		{
			objectUniform.Data.World = Entity.Transform.Matrix;
			objectUniform.Data.WorldViewProj = objectUniform.Data.World * args.Camera.Data.ViewProj;

			objectUniform.Update();
			objectUniform.Bind(args.Context, (int)UniformLocation.Object);

			Effect.Bind(args.Context);
			//texture.Apply(0);
			Effect.BindTexture2D(args.Context, "diffuseMap", Texture);

			Mesh.Draw(args.Context, PrimitiveMode.TriangleList);
			Effect.Unbind(args.Context);
		}


	}
}
