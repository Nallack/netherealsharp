﻿using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Engine2D
{
	[Serializable, DataContract]
	public class Entity2D : IInitializable, IUpdatable, IDrawable2D, IDisposable
    {
		private bool m_Initialized = false;

		public Scene2D Scene { get; internal set; }

		[DataMember(Order = 0)]
		public string Name { get; set; }

		[DataMember(Order = 1)]
		public Transform2D Transform { get; set; }

		public ObservableCollection<Behaviour2D> Behaviours
		{
			get { return m_Behaviours; }
			private set
			{
				m_Behaviours = value;
				foreach (var b in Behaviours) AddBehaviour(b);

				m_Behaviours.CollectionChanged += (sender, e) =>
				{
					foreach (var b in e.NewItems)
					{
						AddBehaviour(b as Behaviour2D);
					}
				};
			}
		}

		#region private
		private ObservableCollection<Behaviour2D> m_Behaviours;
		private Dictionary<Type, Behaviour2D> BehaviourRegistry { get; set; }

		List<IUpdatable> Updatables { get; set; }
		List<IDrawable2D> Drawables { get; set; }
		List<IShadowDrawable> ShadowDrawables { get; set; } 

		#endregion

		public Entity2D()
		{
			Transform = new Transform2D();
			Behaviours = new ObservableCollection<Behaviour2D>();

			BehaviourRegistry = new Dictionary<Type, Behaviour2D>();
			Updatables = new List<IUpdatable>();
			Drawables = new List<IDrawable2D>();
			ShadowDrawables = new List<IShadowDrawable>();
		}

		[OnDeserializing]
		public void Deserializing(StreamingContext context)
		{
			BehaviourRegistry = new Dictionary<Type, Behaviour2D>();
			Updatables = new List<IUpdatable>();
			Drawables = new List<IDrawable2D>();
			ShadowDrawables = new List<IShadowDrawable>();
		}

		public void AddBehaviour(Behaviour2D behaviour)
		{
			behaviour.Entity = this;
			if (behaviour as IUpdatable != null) Updatables.Add(behaviour as IUpdatable);
			if (behaviour as IDrawable2D != null) Drawables.Add(behaviour as IDrawable2D);
			if (behaviour as IShadowDrawable != null) ShadowDrawables.Add(behaviour as IShadowDrawable);

			BehaviourRegistry.Add(behaviour.GetType(), behaviour);
			if (m_Initialized) behaviour.Initialize();
		}

		public T GetBehaviour<T>() where T : Behaviour2D
		{
			return BehaviourRegistry[typeof(T)] as T;
		}

		public void Initialize()
		{
#if DEBUG
			if (Scene == null) throw new MissingMemberException();
#endif
			foreach (var b in Behaviours) b.Initialize();
			m_Initialized = true;
		}


		public void Update()
		{
			foreach (var b in Behaviours) b.Update();
		}

		public void BeginDraw(DrawArgs2D args)
		{
			foreach (var b in Drawables) b.BeginDraw(args);
		}

		public void Draw(DrawArgs2D args)
		{
			foreach (var b in Drawables) b.Draw(args);
		}

		public void Dispose()
		{
			//foreach (IDisposable b in Behaviours) if (b != null) b.Dispose();
		}
	}
}
