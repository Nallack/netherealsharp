﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core;
using NetherealSharp.Core.Physics;
using NetherealSharp.Engine;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace NetherealSharp.Engine2D
{
	[Serializable, DataContract]
    public class Scene2D : IInitializable, IUpdatable, IDrawable2D, IDisposable
    {
		private bool m_Initialized = false;
		private ObservableCollection<Entity2D> m_Entities;

		public Game2D game { get; internal set; }

		[DataMember(Order = 0)]
		public string Name { get; set; }

		[DataMember(Order = 1)]
		public ObservableCollection<Entity2D> Entities
		{
			get { return m_Entities; }
			private set
			{
				m_Entities = value;
				foreach (var e in m_Entities) BindEntity(e);

				m_Entities.CollectionChanged += (sender, e) =>
				{
					foreach (var obj in e.NewItems)
					{
						BindEntity(obj as Entity2D);
					}
				};
			}

		}

		public List<Camera2D> Cameras { get; private set; }
		public List<ILight2D> Lights { get; private set; }

		public PhysicsWorld2D PhysicsWorld { get; private set; }


		public Scene2D()
		{
			Entities = new ObservableCollection<Entity2D>();
			Cameras = new List<Camera2D>();
			Lights = new List<ILight2D>();

			PhysicsWorld = PhysicsManager.Context2D.CreateWorld();
		}

		[OnDeserializing]
		private void Deserializing(StreamingContext context)
		{
			Cameras = new List<Camera2D>();
			Lights = new List<ILight2D>();
		}

		private void BindEntity(Entity2D entity)
		{
			entity.Scene = this;
			if (m_Initialized) entity.Initialize();
		}


		public void Initialize()
		{
			foreach (var e in Entities) e.Initialize();
		}

		public void Update()
		{

			foreach (var e in Entities) e.Update();

			PhysicsWorld.Update(TimeManager.DeltaTime);
		}

		public void BeginDraw(DrawArgs2D args)
		{
			foreach (var e in Entities) e.BeginDraw(args);
		}

		public void Draw(DrawArgs2D args)
		{
			foreach (var e in Entities) e.Draw(args);
		}

		public void Dispose()
		{
			foreach (var e in Entities) e.Dispose();
		}
	}
}