﻿using NetherealSharp.Engine;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace NetherealSharp.Engine2D
{
	[DataContract]
    public abstract class Behaviour2D : IInitializable, IUpdatable
    {
		public Entity2D Entity { get; internal set; }

		public Behaviour2D()
		{
		}

		public virtual void Initialize()
		{
#if DEBUG
			if (Entity == null) throw new MissingMemberException();
#endif
		}


		public virtual void Update() { }
    }
}
