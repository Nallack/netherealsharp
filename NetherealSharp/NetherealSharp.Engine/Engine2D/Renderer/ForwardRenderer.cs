﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Graphics;
using NetherealSharp.Engine;
using NetherealSharp.Core;

namespace NetherealSharp.Engine2D
{
    public class ForwardRenderer : SceneRenderer2D
    {
		public UniformBuffer<CameraData> cameraUniform;

		private DrawArgs2D drawArgs;

		public ForwardRenderer(GraphicsDevice device)
		{
			cameraUniform = UniformBuffer<CameraData>.Create(device);
			drawArgs = new DrawArgs2D();
		}

		public override void Render(Scene2D scene, RenderArgs renderArgs)
		{
			var Graphics = renderArgs.Device;
			var Context = renderArgs.Context;

			drawArgs.Device = Graphics;
			drawArgs.Context = Graphics.ImmediateContext;
			drawArgs.Camera = cameraUniform;

			scene.BeginDraw(drawArgs);

			//Do shadows and lighting here

			Context.Begin(renderArgs.Target);
			Context.SetRenderTarget(renderArgs.Target);
			Context.ClearTarget(renderArgs.Target, Color.Red);

			foreach (var camera in scene.Cameras)
			{

				Context.SetViewport(
					camera.Viewport.X * renderArgs.Target.Width,
					camera.Viewport.Y * renderArgs.Target.Height,
					(camera.Viewport.X + camera.Viewport.Width) * renderArgs.Target.Width,
					(camera.Viewport.Y + camera.Viewport.Height) * renderArgs.Target.Height);

				cameraUniform.Data.Screen = Matrix.OrthoRH(
				renderArgs.Target.Width,
				renderArgs.Target.Height,
				camera.Near,
				camera.Far);

				cameraUniform.Data.View = camera.ViewMatrix;

				cameraUniform.Data.Project = Matrix.OrthoRH(
					renderArgs.Target.Width / camera.Zoom,
					renderArgs.Target.Height / camera.Zoom,
					camera.Near,
					camera.Far);

				cameraUniform.Data.ViewProj = cameraUniform.Data.View * cameraUniform.Data.Project;


				cameraUniform.Update();
				cameraUniform.Bind(Graphics.ImmediateContext, (int)UniformLocation.Camera); 

				scene.Draw(drawArgs);
			}

			Graphics.End(renderArgs.Target);

			renderArgs.Target.Present();

		}

		public override void Dispose()
		{
			cameraUniform.Dispose();
		}
	}
}
