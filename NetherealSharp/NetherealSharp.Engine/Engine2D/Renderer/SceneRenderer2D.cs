﻿
using System;
using System.Collections.Generic;
using System.Text;
using NetherealSharp.Engine;

namespace NetherealSharp.Engine2D
{
    public abstract class SceneRenderer2D : IDisposable
    {
		public abstract void Render(Scene2D scene, RenderArgs renderArgs);

		public abstract void Dispose();
    }
}
