﻿using NetherealSharp.Core.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine2D
{
    public class DrawArgs2D
    {
		public GraphicsDevice Device;
		public GraphicsContext Context;

		public UniformBuffer<CameraData> Camera;
		//lightuniformbuffer
		//shadowuniformbuffer

		public ITexture[] ShadowMaps;
    }
}
