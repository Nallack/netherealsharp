﻿using NetherealSharp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetherealSharp.Engine
{
    static class MathExtension
    {
		/// <summary>
		/// Matrix to convert screen coordinates to texture coordinates
		/// </summary>
		public static readonly Matrix ScreenToTexture = new Matrix
		{
			M11 = 0.5f,
			M22 = -0.5f,
			M33 = 1.0f,
			M41 = 0.5f,
			M42 = 0.5f,
			M44 = 1.0f
		};
	}
}
