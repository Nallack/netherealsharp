﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

using System.Diagnostics;

namespace NetherealSharp.Core
{
    public static class TimeManager
    {

		public static int FrameRate { get; private set; }
		public static int FramerateCounter { get; private set; }

		public static float TotalTime { get; private set; }
		public static float DeltaTime { get; private set; }


		//move these to rendertarget and scene instead as there can can multiple instances of each
		public static Stopwatch drawwatch;
		public static double drawtime;

		public static Stopwatch updatewatch;
		public static double updatetime;

		private static Stopwatch watch;
		private static Timer frameratetimer;

		public static void Init()
		{
			FramerateCounter = 0;
			FrameRate = 0;

			frameratetimer = new Timer();
			frameratetimer.Interval = 1000;
			frameratetimer.Elapsed += (sender, e) =>
			{
				FrameRate = FramerateCounter;
				FramerateCounter = 0;
			};

			frameratetimer.AutoReset = true;
			frameratetimer.Start();

			watch = new Stopwatch();
			watch.Start();

			drawwatch = new Stopwatch();
			updatewatch = new Stopwatch();
		}


		public static void Update() {

			FramerateCounter += 1;

			DeltaTime = (float)watch.Elapsed.TotalMilliseconds / 1000.0f;
			//Debug.WriteLine("FPS: " + 1000 / delta);
			watch.Restart();

			//DeltaTime = (float)Delta;
			//TotalTime = currTime;
		} 

    }
}
