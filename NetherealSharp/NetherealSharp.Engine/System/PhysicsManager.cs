﻿using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;

namespace NetherealSharp.Engine
{
	public static class PhysicsManager
    {
		public static PhysicsImplementation2D Implementation2D = PhysicsImplementation2D.Box2D;

		public static PhysicsImplementation3D Implementation3D = PhysicsImplementation3D.PhysX;

		public static PhysicsContext2D Context2D { get; private set; }

		public static PhysicsContext3D Context3D { get; private set; }

		public static void Init2D(PhysicsImplementation2D implementation)
		{
			Implementation2D = implementation;
			Context2D = PhysicsContext2D.Create(implementation);
		}

		public static void Init(PhysicsImplementation3D implementation)
		{
			Implementation3D = implementation;
			Context3D = PhysicsContext3D.Create(implementation);
		}

		public static void Dispose()
		{
			if (Context2D != null) Context2D.Dispose();
			if (Context3D != null) Context3D.Dispose();
		}
	}
}
