﻿


using System;
using System.Diagnostics;

using NetherealSharp.Core.Graphics;

#if FORMS
using System.Windows.Forms;
#endif
#if DIRECTX11
using SharpDX;
using SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using WIC = SharpDX.WIC;
#endif
#if OPENGL
using OpenTK;
using OpenTK.Graphics.OpenGL;
#endif

//May want to look at storing context as via ThreadLocal and singleton pattern
//http://stackoverflow.com/questions/22719596/singleton-in-current-thread

namespace NetherealSharp.Engine
{
	/// <summary>
	/// Currently this stores the Graphics Device as a static variable, however this may likely change
	/// hence initialization is done using an object like constructor. 
	/// </summary>
	public static class GraphicsManager
	{
		public static GraphicsFactory GraphicsFactory { get; private set; }
		public static GraphicsDevice GraphicsDevice { get; private set; }
        public static GraphicsSettings GraphicsSettings { get; private set; }
		public static StateCollection States { get; private set; }

		public static void Init(GraphicsImplementation implementation)
		{
			//GraphicsFactory = new GraphicsFactory(implementation);
			GraphicsDevice = GraphicsDevice.Create(implementation);

            //TODO: serialize this in some way
            GraphicsSettings = new GraphicsSettings();

#if DIRECTX11
			if (implementation == GraphicsImplementation.DirectX11)
			{
				var graphics = GraphicsDevice as Core.Graphics.DX11.GraphicsDevice;

				States = new Core.Graphics.DX11.StateCollection(graphics.D3D11Device);

				var states = States as Core.Graphics.DX11.StateCollection;

				graphics.D3D11Device.ImmediateContext.PixelShader.SetSampler(0, states.SamplerDefault);
				graphics.D3D11Device.ImmediateContext.PixelShader.SetSampler(1, states.SamplerComparisionShadow);
				graphics.D3D11Device.ImmediateContext.PixelShader.SetSampler(2, states.SamplerShadow);

				graphics.D3D11Device.ImmediateContext.OutputMerger.SetBlendState(states.BlendDefault);
				graphics.D3D11Device.ImmediateContext.Rasterizer.State = states.RasterizerDefault;
			}
#endif
		}

		public static void Dispose()
		{
#if DIRECTX11
			if (GraphicsDevice.Implementation == GraphicsImplementation.DirectX11)
			{
				States.Dispose();
			}
#endif
			GraphicsDevice.Dispose();
		}
	}
}
