﻿using NetherealSharp.Core;
using NetherealSharp.Core.Content;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Serialization;
using NetherealSharp.Engine;
using NetherealSharp.Engine.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

#if ANDROID
using And = Android;
#endif

namespace NetherealSharp.Engine
{
    public static class ContentManager
    {
#if ANDROID
		public static And.Content.Res.AssetManager AssetManager { get; internal set; }
#endif

		public static ContentImporter Importer;
		public static AllContentSerializer Serializer;

		//public static Serialization.SerializationSettings SerializationSettings;
		private static IDictionary<Type, Dictionary<string, IDisposable>> Registries = new Dictionary<Type, Dictionary<string, IDisposable>>();




		public static void Init()
		{
			Importer = ContentImporter.Create();

			Serializer = new AllContentSerializer()
			{
				Settings = new SerializationSettings()
				{
					//Mode = Serialization.SerializationMode.DataContractXML
					Mode = SerializationMode.DataContractJSON
				}
			};
		}

		public static void Add<T>(string name, T content) where T : class, IContentable, IDisposable
		{
			
			if (!Registries.ContainsKey(typeof(T)))
			{
				Registries[typeof(T)] = new Dictionary<string, IDisposable>();
			}
			Registries[typeof(T)][name] = content;
			content.Name = name;
		}

		/// <summary>
		/// Retreive content stored with content manager.
		/// TODO: XNA way would be to check memory first, then try loading from a pipeline object
		/// SUGGESTION: Another way might be to load a scene package at a loading screne for example, or allow streaming in assets like WoW.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static object Get(Type type, string name)
		{
			try
			{
				return Registries[type][name];
			}
			catch (KeyNotFoundException e)
			{
				Core.Diagnostics.Debug.LogError("Content Load Error", "Could not retreive " + type + " with name \"" + name + "\".\n" + e.Message);
				throw e;
			}
		}

		public static T Get<T>(string name) where T : class
		{
			return Get(typeof(T), name) as T;
		}

		/// <summary>
		/// Serialize some stuff here to make sure contentable stuff can be serialized at this stage.
		/// </summary>
		/// <param name="file"></param>
		public static void SerializeRegistries(string file)
		{

		}


		public static void Dispose()
		{
			foreach(var reg in Registries.Values)
			{
				foreach(var content in reg.Values)
				{
					content.Dispose();
				}
			}
		}


		#region Convenience Loaders
		public static Texture2D GetTexture2D(string filename)
		{
			try
			{
				if (Registries[typeof(Texture2D)].ContainsKey(filename))
					return Registries[typeof(Texture2D)][filename] as Texture2D;
				else
				{
					Registries[typeof(Texture2D)].Add(filename, Texture2D.LoadFromFile(GraphicsManager.GraphicsDevice, filename));
					return Registries[typeof(Texture2D)][filename] as Texture2D;
				}
			}
			catch (KeyNotFoundException e)
			{
				Core.Diagnostics.Debug.LogError("Content Load Error", "Could not retreive Texture2D with name \"" + filename + "\".\n" + e.Message);
				throw e;
			}
		}

		#endregion
	}
}
