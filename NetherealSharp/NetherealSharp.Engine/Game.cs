﻿
using System;
using System.Collections.Generic;
using System.Text;

using NetherealSharp.Core.Physics;
using NetherealSharp.Core.Graphics;

namespace NetherealSharp.Engine
{
	//Typical game rende
	public abstract class Game : IDisposable
	{
		GameImplementor m_implementor;
		public GameImplementor Implementor
		{
			get { return m_implementor; }
			set
			{
				m_implementor = value;
				m_implementor.Game = this;
			}
		}

		public abstract void Initialize();

		public abstract void Loop();

		public abstract void Dispose();
	}
}
