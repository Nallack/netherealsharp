﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetherealSharp.Editor.Model
{
	public class Employee
	{
		public int EmployeeID { get; set; }
		public string EmployeeName { get; set; }
	}

	public class Department
	{
		public int DepartmentID { get; set; }
		public string DepartmentName { get; set; }
		public List<Employee> EmployeeList { get; set; }
	}
}
