﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetherealSharp.Editor.Model
{
	public class ProjectConfig
	{
		public string ProjectName { get; set; }
		public string CorperateName { get; set; }
		public string Engine { get; set; }
	}
}
