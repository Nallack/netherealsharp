﻿using NetherealSharp.Editor.Components;
using NetherealSharp.Editor.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NetherealSharp.Editor.ViewModel
{
	public class MainViewModel
	{
		public ObservableCollection<TabItemViewModel> Tabs { get; private set; }
		
		public SceneGraphViewModel SceneGraph { get; private set; }

		public MainViewModel()
		{
			Tabs = new ObservableCollection<TabItemViewModel>();
			Tabs.Add(new TabItemViewModel { Header = "One", Content = "Content Here" });
			Tabs.Add(new TabItemViewModel { Header = "Two", Content = "Content Here" });

			SceneGraph = new SceneGraphViewModel();
		}
	}
}
