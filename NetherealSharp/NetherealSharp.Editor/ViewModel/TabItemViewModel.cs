﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NetherealSharp.Editor.Components
{
	public class TabItemViewModel
	{
		public string Header { get; set; }
		public string Content { get; set; }

		public Frame Frame { get; set; }
	}
}
