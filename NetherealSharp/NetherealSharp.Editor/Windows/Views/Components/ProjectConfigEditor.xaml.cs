﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetherealSharp.Editor.Windows.Views
{
	/// <summary>
	/// Interaction logic for ProjectConfigEditor.xaml
	/// </summary>
	public partial class ProjectConfigEditor : Page
	{
		public Model.ProjectConfig Config { get; set; }

		public string ProjectName { get; set; }
		public string CorperateName { get; set; }


		public ProjectConfigEditor()
		{
			//In this instance, we dont want the window modifying the object until applied is performed.
			//Binding should be done with a copy modelview, (eg this).

			this.DataContext = this;

			InitializeComponent();
			
		}
	}
}
