﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetherealSharp.Editor.Windows
{
	/// <summary>
	/// Interaction logic for StartupPage.xaml
	/// </summary>
	public partial class StartupPage : Page
	{
		public Views.MainWindow MainWindow;

		public StartupPage()
		{
			InitializeComponent();

			NewButton.Click += (sender, e) =>
			{
				MainWindow.CreateAndLoadNewProject();
			};

			LoadButton.Click += (sender, e) =>
			{
				MainWindow.LoadProject();
			};
		}
	}
}
