﻿using NetherealSharp.Editor.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NetherealSharp.Editor.Windows.Views
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public Model.Project ActiveProject { get; protected set; }

		public MainWindow()
		{
			InitializeComponent();
			DataContext = new MainViewModel();

			/*

			*/
		}

		/// <summary>
		/// Saves previously loaded project, creates and loads a new project
		/// </summary>
		public void CreateAndLoadNewProject()
		{

			var cfgdlg = new DialogWindow();
			cfgdlg.MainFrame.Content = new ProjectConfigEditor();
			cfgdlg.ShowDialog();

			var dlg = new System.Windows.Forms.FolderBrowserDialog()
			{
				RootFolder = Environment.SpecialFolder.MyDocuments
			};

			var result = dlg.ShowDialog();

			if(result == System.Windows.Forms.DialogResult.OK)
			{
				System.Windows.MessageBox.Show(dlg.SelectedPath);
			}


			/*
			Save example
			Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog()
			{
				FileName = "NewProject",
				DefaultExt = ".nrsproj",
				Filter = "NetherealSharp Project (.nrsproj)|*.nrsproj"
			};

			bool? result = dlg.ShowDialog();

			if(result==true)
			{
				string filename = dlg.FileName;
			}
			*/

		}

		public void LoadProject()
		{

		}
	}
}
