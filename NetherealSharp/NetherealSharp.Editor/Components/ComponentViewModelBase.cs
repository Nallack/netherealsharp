﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetherealSharp.Editor.Components
{
	public interface IComponentViewModel
	{
		string Name { get; }

	}
}
