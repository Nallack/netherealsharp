﻿using NetherealSharp.Editor.Model;
using NetherealSharp.Editor.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetherealSharp.Editor.Components
{
	public class SceneGraphViewModel : IComponentViewModel
	{
		private string m_Name = "SceneGraph";
		public string Name { get { return m_Name; } }

		public ObservableCollection<DepartmentViewModel> DepartmentCollection { get; private set; }


		public SceneGraphViewModel()
		{
			DepartmentCollection = new ObservableCollection<DepartmentViewModel>();
			var departmentdata = GetDepartmentList();
			foreach (Department department in departmentdata)
			{
				DepartmentCollection.Add(new DepartmentViewModel(department));
			}
		}

		private List<Department> GetDepartmentList()
		{
			var employees = GetEmployeeList();
			List<Department> departments = new List<Department>()
			{
				new Department()
				{
					DepartmentID = 1,
					DepartmentName = "Microsoft.Net",
					EmployeeList = employees.Take(3).ToList()
				},
				new Department()
				{
					DepartmentID = 2,
					DepartmentName = "Open Source",
					EmployeeList = employees.Skip(3).Take(3).ToList()
				},
				new Department()
				{
					DepartmentID = 3,
					DepartmentName = "Other",
					EmployeeList = employees.Skip(6).Take(4).ToList()
				}
			};

			return departments;
		}

		private List<Employee> GetEmployeeList()
		{
			List<Employee> employees = new List<Employee>()
			{
				new Employee() {EmployeeID = 1, EmployeeName = "Hiren" },
				new Employee() { EmployeeID = 2, EmployeeName = "Imran" },
				new Employee() { EmployeeID = 3, EmployeeName = "Shivpal" },
				new Employee() { EmployeeID = 4, EmployeeName = "Prabhat" },
				new Employee() { EmployeeID = 5, EmployeeName = "Sandip" },
				new Employee() { EmployeeID = 6, EmployeeName = "Chetan" },
				new Employee() { EmployeeID = 7, EmployeeName = "Jayesh" },
				new Employee() { EmployeeID = 8, EmployeeName = "Bhavik" },
				new Employee() { EmployeeID = 9, EmployeeName = "Amit" },
				new Employee() { EmployeeID = 10, EmployeeName = "Brijesh" }
			};
			return employees;
		}
	}
}
