﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using NetherealSharp.Engine;

namespace AndroidDemo
{
	[Activity(Label = "AndroidDemo",
		MainLauncher = true,
		Icon = "@drawable/icon",
		ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden
#if __ANDROID_11__
		,HardwareAccelerated=false
#endif
		)]
	public class MainActivity : Activity
	{
		NetherealSharp.Engine.Game3D game;
		MainView view;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			game = new NetherealSharp.Demo.FirstPersonGame();

			// Create our OpenGL view, and display it
			view = new MainView(this);
			SetContentView(view);

			var implementor = new AndroidGameImplementor(game, view, this);
			game.Implementor = implementor;

			game.Initialize();

			
		}

		protected override void OnPause()
		{
			base.OnPause();
			view.Pause();
		}

		protected override void OnResume()
		{
			base.OnResume();
			view.Resume();
		}
	}
}

