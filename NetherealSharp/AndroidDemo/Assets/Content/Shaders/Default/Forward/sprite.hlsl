﻿
struct VertexIn
{
	float4 position : POSITION;
	float2 texcoord : TEXCOORD;
};

struct PixelIn
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

cbuffer Object : register(b0)
{
	float4x4 World;
	float4x4 WorldViewProj;
};

cbuffer Camera : register(b1)
{
	float4x4 View;
	float4x4 Proj;
	float4x4 ViewProj;
	float4x4 Screen;
};

Texture2D diffuseMap : register (t0);
SamplerState linearSampler : register(s0);

PixelIn VS(VertexIn vertex)
{
	PixelIn pixel = (PixelIn)0;
	pixel.position = mul(WorldViewProj, vertex.position);
	//pixel.position = vertex.position;
	pixel.texcoord = vertex.texcoord;
	return pixel;
}

float4 PS(PixelIn pixel) : SV_TARGET
{
	return diffuseMap.Sample(linearSampler, pixel.texcoord);

	//----variance------//

	////find mean first
	//
	//float mean = diffuseMap.SampleLevel(linearSampler, pixel.texcoord, 0).r;

	////calc variance
	//float variance = 0.0f;

	//for (int x = -2; x < 2; x++)
	//{
	//	for (int y = -2; y < 2; y++)
	//	{
	//		float diff = mean - diffuseMap.SampleLevel(linearSampler, pixel.texcoord, 0, int2(x, y)).r;
	//		variance += diff * diff;
	//	}
	//}

	//variance *= 16.0f;
	//return float4(variance, 0, 0, 1);
	

	//----texcoord=---//
	//return float4(pixel.texcoord, 1, 1);
}