﻿


uniform Camera {
	mat4 View;
	mat4 Proj;
	mat4 ViewProj;
	mat4 Screen;
};

#if VERTEX_SHADER

in vec4 vPosition;
in vec4 vColor;

out vec4 fColor;

void main() {
	gl_Position = Screen * vPosition;
	fColor = vColor;
}

#elif FRAGMENT_SHADER

in vec4 fColor;
out vec4 color;

void main() {
	color = fColor;
}
#endif