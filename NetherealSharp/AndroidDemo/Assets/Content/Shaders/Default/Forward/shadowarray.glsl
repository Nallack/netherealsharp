﻿
#extension GL_EXT_geometry_shader4 : enable
#define MAXSHADOWCASCADES 3



uniform Object {
	mat4 World;
	mat4 WorldViewProj;
};

uniform Shadow {
	mat4 ViewProj[MAXSHADOWCASCADES];
};

#if VERTEX_SHADER

in vec4 vPosition;
out vec4 gPositionW;

void main() {
	gPositionW = World * vPosition;
	gl_Position = gPositionW;
}

#elif GEOMETRY_SHADER

in vec4 gPositionW[3];
out vec4 fPosition;

void main() {
	for (int i = 0; i < MAXSHADOWCASCADES; i++) {

		for (int j = 0; j < gl_VerticesIn; j++) {
			fPosition = ViewProj[i] * gl_PositionIn[j];
			gl_Position = fPosition;
			EmitVertex();
		}
		EndPrimitive();
	}
}

#elif FRAGMENT_SHADER

in vec4 fPosition;
out vec4 color;

void main() {
	float depth = fPosition.z;
	color = vec4(depth, depth * depth, 0, 1);
}
#endif