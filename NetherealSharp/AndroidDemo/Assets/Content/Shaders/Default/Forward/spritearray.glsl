﻿


uniform Object {
	mat4 World;
	mat4 WorldViewProj;
	int ArrayIndex;
};

uniform Camera {
	mat4 View;
	mat4 Proj;
	mat4 ViewProj;
};

uniform sampler2DArray diffuseMap;



#ifdef VERTEX_SHADER

in vec4 vPosition;
in vec2 vTexCoord;

out vec4 fPosition;
out vec2 fTexCoord;

void main() {
	fPosition = WorldViewProj * vPosition;
	gl_Position = fPosition;
	fTexCoord = vTexCoord;
}

#endif

#ifdef FRAGMENT_SHADER

in vec4 fPosition;
in vec2 fTexCoord;

out vec4 color;

void main() {
	color = texture(diffuseMap, vec3(fTexCoord, ArrayIndex));
}

#endif