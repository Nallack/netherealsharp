﻿


uniform Object {
	mat4 World;
	mat4 WorldViewProj;
};

uniform Camera {
	mat4 View;
	mat4 Proj;
	mat4 ViewProj;
	mat4 Screen;
	vec4 Eye;
};

#if VERTEX_SHADER

in vec4 vPosition;
out vec4 fPosition;

void main() {
	fPosition = WorldViewProj * vPosition;
	gl_Position = fPosition;
}

#endif

#if FRAGMENT_SHADER
in vec4 fPosition;

out vec4 color;

void main() {
	float depth = fPosition.z;
	color = vec4(depth, depth * depth, 0, 1);
}
#endif