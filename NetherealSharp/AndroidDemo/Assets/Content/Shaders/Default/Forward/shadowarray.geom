﻿
#extension GL_EXT_geometry_shader4 : enable

#define MAXSHADOWCASCADES 3

in vec4 gPositionW[3];

out vec4 fPosition;

uniform Shadow {
	mat4 ViewProj[MAXSHADOWCASCADES];
};

void main() {
	for(int i = 0; i < MAXSHADOWCASCADES; i++) {
		
		for(int j = 0; j < gl_VerticesIn; j++) {
			fPosition = ViewProj[i] * gl_PositionIn[j];
			gl_Position = fPosition;
			EmitVertex();
		}
		EndPrimitive();
	}
}