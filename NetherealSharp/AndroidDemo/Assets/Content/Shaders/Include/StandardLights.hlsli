﻿
#ifndef STANDARD_LIGHTS
#define STANDARD_LIGHTS

//Light Uniform Data defined here

struct AmbientLight
{
	float4 Down;
	float4 Range;
};

struct AmorphicLight
{
	bool Enabled;
	bool ShadowEnabled;
	int MatrixIndex;
	int MapIndex; //cannot access block array with this, atm using index of this element
	//int cascades;

	float3 Position;
	float Type;

	float3 Direction;
	float pad1;

	float3 ThetaPhiGamma;
	float Intensity;

	float4 Color;


};

#endif