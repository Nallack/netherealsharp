﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using NetherealSharp.Demo;
using NetherealSharp.Core.Graphics;
using NetherealSharp.Core.Physics;
using NetherealSharp.Engine;

namespace WPFDemo
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			renderpanel2.Focusable = true;

			var game = new FirstPersonGame();
			var implementor = new WPFGameImplementor(
				game,
				renderpanel2,
				GraphicsImplementation.DirectX11,
				PhysicsImplementation3D.PhysX);

			game.Initialize();

			this.Closing += (sender, e) =>
			{
				game.Dispose();
			};









			//game.InitD3D11(renderpanel);

			//game.Init(renderelement);
			

			//renderpanel.Loaded += (sender, e) =>
			//{
			//	renderImage.WindowOwner = (new System.Windows.Interop.WindowInteropHelper(this)).Handle;

			//	renderImage.OnRender = (surface, newsurface) =>
			//	{
			//		if (newsurface)
			//		{
			//			game.Init(renderImage, surface);

			//			CompositionTarget.Rendering += (sender1, e1) =>
			//			{
			//				game.Loop();
			//				renderImage.RequestRender();
			//			};
			//		}
			//	};

			//	renderImage.RequestRender();

			//};


			//renderpanel.SizeChanged += (sender, e) =>
			//{
			//	renderImage.SetPixelSize((int)renderpanel.ActualWidth, (int)renderpanel.ActualHeight);
			//};


				/*
				renderElement.Loaded += (sender, e) =>
				{
					renderElement.Focusable = true;

					//demo
					var game = new ExampleGame3D();
					game.Init(renderElement);
				};
				*/
		}
	}
}
